package net.ihe.gazelle.evs.client.integration;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

import junit.framework.TestCase;

/**
 * The aim of this class is to test the reachability of 	the SVS ValueSet provider, which is used by the validators.
 *
 * @author abderrazek boufahja / Kereval / IHE-Europe
 * @version 1.0 - 22 jul 2013
 * @see > abderrazek.boufahja@ihe-europe.com  -  http://www.ihe-europe.org
 */
public class SVSTest extends TestCase {

    private static final String SVS_ENDPOINT = "https://gazelle.ihe.net/SVSSimulator/rest";

    /**
     * Test the Reachability of the SVSEndpoint
     */
    public void testReachability() {

        assertTrue(URLCheck.checkURL(SVS_ENDPOINT + "/RetrieveValueSet?id=1.3.6.1.4.1.19376.1.4.1.6.5.10053"));
    }

    /**
     * test the ability to retrieve list of valueSet
     *
     * @throws Exception
     */
    public void testRetrieveValueSet() throws Exception {
        ClientRequest request = new ClientRequest(SVS_ENDPOINT + "/RetrieveValueSet");
        request.queryParameter("id", "1.3.6.1.4.1.12559.11.10.1.3.1.42.38");
        ClientResponse<String> response = request.get(String.class);
        assertTrue(response.getStatus() == 200);
        String xmlContent = response.getEntity();
        assertTrue(xmlContent.contains("Concept"));
    }

}
