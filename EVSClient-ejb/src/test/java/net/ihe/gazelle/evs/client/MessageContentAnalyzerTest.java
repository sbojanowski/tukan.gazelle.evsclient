package net.ihe.gazelle.evs.client;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.analyzer.AnalyzerBean;
import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.analyzer.file.B64;
import net.ihe.gazelle.evs.client.analyzer.file.HTTP;
import net.ihe.gazelle.evs.client.analyzer.file.MTOM;
import net.ihe.gazelle.evs.client.analyzer.file.XML;
import net.ihe.gazelle.evs.client.analyzer.file.xml.*;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.analyzer.utils.NamespaceAnalyze;
import net.ihe.gazelle.evs.client.common.action.FileToValidate;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.common.model.ObjectForValidatorDetector;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.apache.commons.compress.archivers.ArchiveException;
import org.custommonkey.xmlunit.XMLAssert;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.xml.sax.SAXException;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils.fileToString;
import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static net.ihe.gazelle.files.FilesUtils.loadStringsFromFile;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ApplicationPreferenceManager.class, OIDGenerator.class, ValidatedObject.class, FacesMessages.class,
        Component.class, GazelleValidationCacheManager.class, FacesContext.class, EntityManager.class,
        EntityManagerService.class })
@PowerMockIgnore({ "org.xml.*", "org.w3c.*", "javax.xml.*" })
public class MessageContentAnalyzerTest {
    MessageContentAnalyzer m;
    AnalyzerBean vd;
    XML xml;
    HTTP http;
    FileToValidate fileToValidate;
    NamespaceAnalyze na;

    @Before
    public void setUp() throws Exception {
        na = new NamespaceAnalyze();
        vd = new AnalyzerBean();
        m = new MessageContentAnalyzer();
        xml = new XML();
        http = new HTTP();
        m.setBody(null);
        m.setHeader(null);
        m.setAnalyzedObject(new ObjectForValidatorDetector());

        mockRootOid();
        mockFacesMessages();
        mockEntityManager();
    }

    private void mockRootOid() {
        PowerMockito.mockStatic(ApplicationPreferenceManager.class);
        PowerMockito.mockStatic(OIDGenerator.class);
        PowerMockito.mockStatic(ValidatedObject.class);
        PowerMockito.mockStatic(GazelleValidationCacheManager.class);

        when(GazelleValidationCacheManager
                .refreshGazelleCacheWebService(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                        Mockito.anyString())).thenReturn(true);

        when(ApplicationPreferenceManager.getStringValue("root_oid")).thenReturn("1.3.6.1.4.1.12559.11.1.2.1.4.");
        when(ApplicationPreferenceManager.getStringValue("object_forValidator_detector_repository"))
                .thenReturn("/opt/EVSClient_prod/validatedObjects/validatorDetector");
        when(ApplicationPreferenceManager.getStringValue("application_url"))
                .thenReturn("https://gazelle.ihe.net/EU-CAT/");
        when(OIDGenerator.getNewOid()).thenReturn("1.3.6.1.4.1.12559.11.1.2.1.4.1");

        when(ValidatedObject
                .save(Mockito.eq(ObjectForValidatorDetector.class), (ObjectForValidatorDetector) Mockito.anyObject()))
                .thenAnswer(new Answer<ObjectForValidatorDetector>() {

                    @Override
                    public ObjectForValidatorDetector answer(InvocationOnMock invocation) throws Throwable {
                        Object[] args = invocation.getArguments();
                        return (ObjectForValidatorDetector) args[1];
                    }
                });

    }

    private void mockFacesMessages() {
        FacesMessages facesMessageMock = PowerMockito.mock(FacesMessages.class);
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(Component.class);
        PowerMockito.when(FacesMessages.instance()).thenReturn(facesMessageMock);
    }

    private void mockEntityManager() {
        EntityManager em = PowerMockito.mock(EntityManager.class);
        PowerMockito.mockStatic(EntityManagerService.class);
        PowerMockito.when(EntityManagerService.provideEntityManager()).thenReturn(em);
    }

    @After
    public void tearDown() throws Exception {
        m = null;
    }





    // WS-Trust DETECTION
    @Test
    public void testContainsWSTrustValidRSTWithoutSAMLXMLWSTrust() {
        assertTrue("We can detect the WS_TRUST Content", WSTrust
                .containsWS_Trust(loadFile("/contentAnalyzer/RSTWithoutSAMLXMLValid.xml"), m));
    }

    @Test
    public void testContainsWSTrustValidRSTWithoutSAMLXMLSAML() {
        assertFalse("Check that no SAML assertion is detected", SAML.containsAssertion(loadStringsFromFile("/contentAnalyzer/RSTWithoutSAMLXMLValid.xml"), m));
    }

    @Test
    public void testContainsWSTrustValidRSTWithoutSAMLXMLWithoutPrefixWSTrustPart() {
        assertFalse("containsWS_TrustValidRSTWithoutSAMLXMLWithoutPrefix", SAML.containsAssertion(
                loadStringsFromFile("/contentAnalyzer/RSTRWithoutSAMLXMLNotValidWithoutPrefix.xml"), m));
    }

    @Test
    public void testContainsWSTrustValidRSTWithoutSAMLXMLWithoutPrefixSAMLPart() {
        assertFalse("containsWS_TrustValidRSTWithoutSAMLXMLWithoutPrefix", SAML.containsAssertion(
                loadStringsFromFile("/contentAnalyzer/RSTRWithoutSAMLXMLNotValidWithoutPrefix.xml"), m));
    }

    @Test
    public void containsWSTrustNotValidRSTWithoutSAMLXML() {
        assertFalse("containsWSTrustNotValidRSTWithoutSAMLXML", WSTrust
                .containsWS_Trust(loadFile("/contentAnalyzer/RSTWithoutSAMLXMLNotValid.xml"), m));
        assertFalse("containsWSTrustNotValidRSTWithoutSAMLXML",
                SAML.containsAssertion(loadStringsFromFile("/contentAnalyzer/RSTWithoutSAMLXMLNotValid.xml"), m));
    }

    @Test
    public void containsWSTrustValidRSTWithSAMLXML() {
        assertTrue("containsWSTrustValidRSTWithSAMLXML", WSTrust
                .containsWS_Trust(loadFile("/contentAnalyzer/RSTWithSAMLXMLValid.xml"), m));
        assertTrue("containsWSTrustValidRSTWithSAMLXML", SAML.containsAssertion(loadStringsFromFile("/contentAnalyzer/RSTWithSAMLXMLValid.xml"), m));
    }

    @Test
    public void containsWSTrustNotValidRSTWithSAMLXML() {
        assertFalse("containsWSTrustNotValidRSTWithSAMLXML", WSTrust
                .containsWS_Trust(loadFile("/contentAnalyzer/RSTWithSAMLXMLNotValid.xml"), m));
        assertTrue("containsWSTrustNotValidRSTWithSAMLXML", SAML.containsAssertion(loadStringsFromFile("/contentAnalyzer/RSTWithSAMLXMLNotValid.xml"), m));
    }

    @Test
    public void containsWSTrustValid_RSTRWithoutSAMLXML() {
        assertTrue("containsWSTrustValid_RSTRWithoutSAMLXML", WSTrust
                .containsWS_Trust(loadFile("/contentAnalyzer/RSTRWithoutSAMLXMLValid.xml"), m));
        assertFalse("containsWSTrustValid_RSTRWithoutSAMLXML", SAML.containsAssertion(loadStringsFromFile("/contentAnalyzer/RSTRWithoutSAMLXMLValid.xml"), m));
    }

    @Test
    public void containsWSTrustNotValid_RSTRWithoutSAMLXML() {
        assertFalse("containsWSTrustNotValidRSTRWithoutSAMLXML", WSTrust
                .containsWS_Trust(loadFile("/contentAnalyzer/RSTRWithoutSAMLXMLNotValid.xml"), m));
        assertFalse("containsWSTrustNotValidRSTRWithoutSAMLXML",
                SAML.containsAssertion(loadStringsFromFile("/contentAnalyzer/RSTRWithoutSAMLXML_notValid.xml"), m));
    }

    @Test
    public void containsWSTrustValidRSTRWithSAMLXML() {
        assertTrue("containsWS_TrustValid_RSTR_withSAMLXML", WSTrust
                .containsWS_Trust(loadFile("/contentAnalyzer/RSTRWithSAMLXMLValid.xml"), m));
        assertTrue("containsWS_TrustValid_RSTR_withSAMLXML", SAML.containsAssertion(loadStringsFromFile("/contentAnalyzer/RSTRWithSAMLXMLValid.xml"), m));
    }

    @Test
    public void containsWSTrustNotValidRSTRWithSAMLXML() {
        assertFalse("containsWSTrustNotValidRSTR_withSAMLXML", WSTrust
                .containsWS_Trust(loadFile("/contentAnalyzer/RSTRWithSAMLXMLNotValid.xml"), m));
        assertTrue("containsWSTrustNotValidRSTR_withSAMLXML", SAML.containsAssertion(loadStringsFromFile("/contentAnalyzer/RSTRWithSAMLXMLNotValid.xml"), m));
    }

    // DICOM DETECTION
    @Test
    public void testDicomDetectionWithValidDicomFile() {
        assertTrue("testDicomDetectionWithValidDicomFile", m.dicomDetection(loadFile("/contentAnalyzer/validDicomFile.dcm"), 0, 0));
    }

    @Test
    public void testDicomDetectionWithValidDicomFileAndOffset() {
        assertFalse("testDicomDetectionWithValidDicomFileAndOffset", m.dicomDetection(loadFile("/contentAnalyzer/validDicomFile.dcm"), 1, 20));
    }

    @Test
    public void testDicomDetectionWithnotValidDicomFile() {
        assertFalse("testDicomDetectionWithnotValidDicomFile", m.dicomDetection(loadFile("/contentAnalyzer/notValidDicomFile.dcm"), 0, 0));
    }

    @Test
    public void testDicomDetectionWithoutDicomFile() {
        //TODO
        assertTrue("testDicomDetectionWithoutDicomFile", true);
        //  assertFalse(m.dicomDetection(new File("tterte.gfc"), 0, 0));
    }

    // BASE64 DETECTION IN FILE
    @Test
    public void testBase64DetectionWithValidBase64File() {
        assertTrue("testBase64DetectionWithValidBase64File", m.base64Detection(loadFile("/contentAnalyzer/validBase64File.txt"), 0, 0));
    }

    @Test
    public void testBase64DetectionWithValidBase64FileAndOffset() {
        assertTrue("testBase64DetectionWithValidBase64FileAndOffset", m.base64Detection(loadFile("/contentAnalyzer/validBase64File.txt"), 1, 10));
    }

    @Test
    public void testBase64DetectionWithnotValidBase64File() {
        assertFalse("testBase64DetectionWithnotValidBase64File", m.base64Detection(loadFile("/contentAnalyzer/notValidBase64File.txt"), 0, 0));
    }

    @Test
    public void testBase64DetectionWithfolder() {
        assertFalse("testBase64DetectionWithfolder", m.base64Detection(loadFile("/folder"), 0, 0));
    }

    @Test
    public void testBase64DetectionWithoutBase64File() {
        //TODO
        assertTrue("testBase64DetectionWithoutBase64File", true);
        //  assertFalse(m.base64Detection(new File("tterte.gg"), 0, 0));
    }

    // BASE64 DETECTION IN STRING
    @Test
    public void testBase64DetectionWithValid_base64_string() {
        assertTrue("testBase64DetectionWithValid_base64_string", m.base64Detection(loadStringsFromFile("/contentAnalyzer/validBase64File.txt"), 0, 0));
    }

    @Test
    public void testBase64DetectionWithValid_base64_stringAndOffset() {
        assertTrue("testBase64DetectionWithValid_base64_stringAndOffset", m.base64Detection(loadStringsFromFile("/contentAnalyzer/validBase64File.txt"), 1, 10));
    }

    @Test
    public void testBase64DetectionWithout_base64_string() {
        String s = null;
        assertTrue("testBase64DetectionWithout_base64_string", m.base64Detection(s, 0, 0));
    }

    // MTOM DETECTION
    @Test
    public void test_mtomDetectionWithValid_mtomFile() {
        assertTrue("test_mtomDetectionWithValid_mtomFile", m.mtomDetection(loadFile("/contentAnalyzer/validMtomFile.xml"), 0, 0));
    }

    @Test
    public void test_mtomDetectionWithValid_mtomFile_bis() {
        assertTrue("test_mtomDetectionWithValid_mtomFile_bis", m.mtomDetection(loadFile("/contentAnalyzer/validMtomFile_bis.xml"), 0, 0));
    }

    @Test
    public void test_mtomDetectionWithValid_mtomFileAndOffset() {
        assertFalse("test_mtomDetectionWithValid_mtomFileAndOffset", m.mtomDetection(loadFile("/contentAnalyzer/validMtomFile.xml"), 10, 100));
    }

    @Test
    public void test_mtomDetectionWithnotValid_mtomFile() {
        assertFalse("test_mtomDetectionWithnotValid_mtomFile", m.mtomDetection(loadFile("/contentAnalyzer/notValidMtomFile.xml"), 0, 0));
    }

    @Test
    public void test_mtomDetectionWithout_mtomFile() {
        assertFalse("test_mtomDetectionWithout_mtomFile", m.mtomDetection(new File("ttte.gfgc"), 0, 0));
    }

    // MTOM EXTRACT PART
    @Test
    public void test_extractMtomPartsWithValid_mtomFile() {
        fileToValidate = new FileToValidate(null);
        MTOM mtom = new MTOM();
        mtom.extractMtomParts(loadFile("/contentAnalyzer/validMtomFile.xml"), fileToValidate, m);
        for (int i = 0; i < fileToValidate.getFileToValidateList().size() - 1; i++) {
            assertEquals("Mtom_part_" + (i + 1), fileToValidate.getFileToValidateList().get(i).getFileName());
        }
    }

    @Test
    public void test_extractMtomPartsWithnotValid_mtomFile() {
        MTOM mtom = new MTOM();
        fileToValidate = new FileToValidate(null);
        mtom.extractMtomParts(loadFile("/contentAnalyzer/notValidMtomFile.xml"), fileToValidate, m);
        assertEquals(0, fileToValidate.getFileToValidateList().size());
    }

    @Test
    public void test_extractMtomPartsWithout_mtomFile() {
        MTOM mtom = new MTOM();
        fileToValidate = new FileToValidate(null);
        mtom.extractMtomParts(new File("ttte.gfgc"), fileToValidate, m);
        assertEquals(0, fileToValidate.getFileToValidateList().size());
    }

    // HTTP DETECTION
    @Test
    public void test_httpDetectionWithValid_httpFile() {
        assertTrue(m.httpDetection(loadFile("/contentAnalyzer/validHttpFile.xml"), 0, 0));
    }

    @Test
    public void test_httpDetectionWithValid_httpFileAndOffset() {
        assertFalse(m.httpDetection(loadFile("/contentAnalyzer/validHttpFile.xml"), 10, 100));
    }

    @Test
    public void test_httpDetectionWithnotValid_httpFile() {
        assertFalse(m.httpDetection(loadFile("/contentAnalyzer/notValidHttpFile.xml"), 0, 0));
    }

    @Test
    public void test_httpDetectionWithout_httpFile() {
        assertFalse(m.httpDetection(new File("ttte.gfgc"), 0, 0));
    }

    // SEPARATE HTTP HEADER/BODY
    @Test
    public void testSeparateHttpHeaderBodyWithValidXMLFile() {
        String header = loadStringsFromFile("/contentAnalyzer/validHttpHeader.xml");
        String body = loadStringsFromFile("/contentAnalyzer/validHttpBody.xml");
        assertTrue(http.extractHttpParts(loadFile("/contentAnalyzer/validHttpFile.xml"), 0, 0, m));
        assertEquals(m.getHttpHeader(), header);
        assertEquals(m.getHttpBody(), body);
    }

    @Test
    public void testSeparateHttpHeaderBodyWithValidXMLFileAndOffset() {
        String header = loadStringsFromFile("/contentAnalyzer/validHttpHeader.xml");
        String body = loadStringsFromFile("/contentAnalyzer/validHttpBody.xml");
        assertTrue(http.extractHttpParts(loadFile("/contentAnalyzer/validHttpFile2.xml"), 0, 0, m));
        try {
            XMLAssert.assertXMLEqual(m.getHttpHeader(), header);
            XMLAssert.assertXMLEqual(m.getHttpBody(), body);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSeparateHttpHeaderBodyWithnotValidXMLFile() {
        String header = loadStringsFromFile("/contentAnalyzer/validHeader.xml");
        String body = loadStringsFromFile("/contentAnalyzer/validBody.xml");
        assertFalse(http.extractHttpParts(loadFile("/contentAnalyzer/notValidHttpFile.xml"), 0, 0, m));
        assertNotSame(m.getHttpHeader(), header);
        assertNotSame(m.getHttpBody(), body);
    }

    @Test
    public void testSeparateHttpHeaderBodyWithoutXMLFile() {
        String header = loadStringsFromFile("/contentAnalyzer/validHeader.xml");
        String body = loadStringsFromFile("/contentAnalyzer/validBody.xml");
        assertFalse(http.extractHttpParts(new File("tterte.m"), 0, 0, m));
        assertNotSame(m.getHttpHeader(), header);
        assertNotSame(m.getHttpBody(), body);
    }

    // HL7 DETECTION
    @Test
    public void testIsHL7WithValidHl7File() {
        assertTrue(m.isHL7(loadFile("/contentAnalyzer/validHl7File.hl7"), 0, 0));
    }

    @Test
    public void testIsHL7WithValidHl7FileAndOffset() {
        assertFalse(m.isHL7(loadFile("/contentAnalyzer/validHl7File.hl7"), 2, 10));
    }

    @Test
    public void testIsHL7WithnotValidHl7File() {
        assertFalse(m.isHL7(loadFile("/contentAnalyzer/notValidHl7File.hl7"), 0, 0));
    }

    @Test
    public void testIsHL7WithoutHl7File() {
        assertFalse(m.isHL7(new File("tter.gfgc"), 0, 0));
    }

    // SYSLOG DETECTION
    @Test
    public void testIsSyslogWithValid_syslogFile() {
        assertTrue(m.isSyslog(loadFile("/contentAnalyzer/validSyslogFile.syslog"), 0, 0));
    }


    @Test
    public void testIsSyslogWithnotValid_syslogFile() {
        assertFalse(m.isSyslog(loadFile("/contentAnalyzer/notValidSyslogFile.syslog"), 0, 0));
    }



    // CERTIFICATE DETECTION
    @Test
    public void testIsCertificateWithValidCertificateFile() {
        assertTrue(m.isCertificate(loadFile("/contentAnalyzer/validCertificateFile.pem"), 0, 0));
    }

    @Test
    public void testIsCertificateWithValidCertificateFileAndOffset() {
        assertFalse(m.isCertificate(loadFile("/contentAnalyzer/validCertificateFile.pem"), 10, 20));
    }

    @Test
    public void testIsCertificateWithnotValidCertificateFile() {
        assertFalse(m.isCertificate(loadFile("/contentAnalyzer/notValidCertificateFile.pem"), 0, 0));
    }

    @Test
    public void testIsCertificateWithoutCertificateFile() {
        assertFalse(m.isCertificate(new File("ttert.fgc"), 0, 0));
    }

    // XML WELL FORMED DETECTION
    @Test
    public void test_xmlIsWellFormedWithWellFormedXMLFile() {
        assertTrue(m.xmlIsWellFormed(loadFile("/contentAnalyzer/wellFormedXmlFile.xml"), 0, 0));
    }

    @Test
    public void test_xmlIsWellFormedSlowWithWellFormedXMLFile() {
        assertTrue(m.xmlIsWellFormed(loadFile("/contentAnalyzer/xmlWellFormedSlow.xml"), 0, 0));
    }

    @Test
    public void test_getListOfPrefixAndURIForNamespaces() {
        Map<String, String> result;
        try {
            result = na.getListOfPrefixAndURIForNamespaces(loadFile("/contentAnalyzer/xmlNamespaces.xml"));
            assertTrue(result.size() == 9);
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_xmlIsWellFormedWithWellFormedXMLFileAndOffset() {
        assertFalse(m.xmlIsWellFormed(loadFile("/contentAnalyzer/wellFormedXmlFile.xml"), 20, 30));
    }

    @Test
    public void test_xmlIsWellFormedWithnot_WellFormedXMLFile() {
        assertFalse(m.xmlIsWellFormed(loadFile("/contentAnalyzer/notWellFormedXmlFile.xml"), 0, 0));
    }

    @Test
    public void test_xmlIsWellFormedWithoutXMLFile() {
        assertFalse(m.xmlIsWellFormed(new File("te.gfc"), 0, 0));
    }

    // CONTAINS ENVELOPE ELEMENT DETECTION
    @Test
    public void testContainsEnvelopElementWithValidXMLFile() {
        assertTrue(xml.containsEnvelopElement(loadFile("/contentAnalyzer/validXmlFile.xml"), 0, 0, m));
    }

    @Test
    public void testContainsEnvelopElementWithPrefixWithValidXMLFile() {
        assertTrue(xml.containsEnvelopElement(loadFile("/contentAnalyzer/validXmlFileWithPrefix.xml"), 0, 0, m));
    }

    @Test
    public void testContainsEnvelopElementWithValidXMLFileWithout_suffix() {
        assertTrue(xml.containsEnvelopElement(loadFile("/contentAnalyzer/validXmlFileEnveloppe.xml"), 0, 0, m));
    }

    @Test
    public void testContainsEnvelopElementWithValidXMLFileAndOffset() {
        assertFalse(xml.containsEnvelopElement(loadFile("/contentAnalyzer/validXmlFile.xml"), 1, 20, m));
    }

    @Test
    public void testContainsEnvelopElementWithnotValidXMLFile() {
        assertFalse(xml.containsEnvelopElement(loadFile("/contentAnalyzer/notValidXmlFile.xml"), 0, 0, m));
    }

    @Test
    public void testContainsEnvelopElementWithoutXMLFile() {
        assertFalse(xml.containsEnvelopElement(new File("rte.gfgc"), 0, 0, m));
    }

    // SEPARATE HEADER/BODY
    @Test
    public void testSeparateHeaderBodyWithValidXMLFile() {
        String header = loadStringsFromFile("/contentAnalyzer/validHeader.xml");
        String body = loadStringsFromFile("/contentAnalyzer/validBody.xml");
        assertTrue(xml.separateHeaderBody(loadFile("/contentAnalyzer/validXmlFile.xml"), 0, 0, m));
        try {
            XMLAssert.assertXMLEqual(m.getHeader(), header);
            XMLAssert.assertXMLEqual(m.getBody(), body);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
/*
	@Test

	public void testSeparateHeaderBodyWithValidXMLFile_but_empty_header() {
		String header = loadStringsFromFile("/contentAnalyzer/validEmptyHeader.xml");
		String body = loadStringsFromFile("/contentAnalyzer/validBody.xml");
		assertTrue(xml.separateHeaderBody(loadFile("/contentAnalyzer/validXmlFileWithEmptyHeader.xml"), 0, 0, m));
		assertEquals(m.getHeader(), header);
		assertEquals(m.getBody(), body);
	}
	*/

    @Test
    public void testSeparateHeaderBodyWithValidXMLFileAndOffset() {


        String header = loadStringsFromFile("/contentAnalyzer/validHeader.xml");
        String body = loadStringsFromFile("/contentAnalyzer/validBody.xml");
        assertTrue(xml.separateHeaderBody(loadFile("/contentAnalyzer/validXmlFile.xml"), 2, 1304, m));
        try {
            XMLAssert.assertXMLEqual(m.getHeader(), header);
            XMLAssert.assertXMLEqual(m.getBody(), body);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }






    @Test
    public void testSeparateHeaderBodyWithnotValidXMLFile() {
        String header = loadStringsFromFile("/contentAnalyzer/validHeader.xml");
        String body = loadStringsFromFile("/contentAnalyzer/validBody.xml");
        assertFalse(xml.separateHeaderBody(loadFile("/contentAnalyzer/notValidXmlFile.xml"), 0, 0, m));
        assertNotSame(m.getHeader(), header);
        assertNotSame(m.getBody(), body);
    }

    @Test
    public void testSeparateHeaderBodyWithoutXMLFile() {
        String header = loadStringsFromFile("/contentAnalyzer/validHeader.xml");
        String body = loadStringsFromFile("/contentAnalyzer/validBody.xml");
        assertFalse(xml.separateHeaderBody(new File("tterte.m"), 0, 0, m));
        assertNotSame(m.getHeader(), header);
        assertNotSame(m.getBody(), body);
    }

    // CONTAINS ASSERTION ELEMENT DETECTION IN HEADER
    @Test
    public void testContainsAssertionWithValidXMLString() {
        assertTrue(SAML.containsAssertion(loadStringsFromFile("/contentAnalyzer/validAssertionXmlFile.xml"), m));
    }

    @Test
    public void testContainsAssertionWithPrefixWithValidXMLString() {
        assertTrue(
                SAML.containsAssertion(loadStringsFromFile("/contentAnalyzer/validAssertionWithPrefixXmlFile.xml"), m));
    }

    @Test
    public void testContainsAssertionWithValidXMLStringWithout_prefix() {
        assertTrue(SAML.containsAssertion(loadStringsFromFile("/contentAnalyzer/validAssertionSimple.xml"), m));
    }

    @Test
    public void testContainsAssertionWithnotValidXMLFile() {
        assertFalse(SAML.containsAssertion(loadStringsFromFile("/contentAnalyzer/notValidAssertionXmlFile.xml"), m));
    }

    @Test
    public void testContainsAssertionWithnull() {
        assertFalse(SAML.containsAssertion(new String(""), m));
    }

    @Test
    public void testContainsAssertionWithoutXMLString() {
        assertFalse(SAML.containsAssertion(new String("tterte.y"), m));
    }

    // CONTAINS ASSERTION ELEMENT DETECTION IN HEADER OF FILE
    @Test
    public void testContainsAssertionWithValidXMLFile() {
        assertTrue(SAML.containsAssertionToString(loadFile("/contentAnalyzer/validAssertionXmlFile.xml"), 0, 0, m));
    }

    // BASE64 ENCODER
    @Test
    public void testBase64EncodingWithValidBase64File() {
        assertTrue(AnalyzerFileUtils.base64Encoding(loadFile("/contentAnalyzer/validBase64DecodedFile.txt"),
                loadFile("/contentAnalyzer/encodedFile.txt")));
        assertEquals(loadStringsFromFile("/contentAnalyzer/validBase64EncodedFile.txt"),
                loadStringsFromFile("/contentAnalyzer/encodedFile.txt"));
    }

    @Test
    public void testBase64EncodingWithnotValidBase64File() {
        File file = new File("tmp.txt");
        assertTrue(AnalyzerFileUtils.base64Encoding(loadFile("/contentAnalyzer/notValidBase64File.txt"), file));
        assertFalse(fileToString(file).equals(loadStringsFromFile("/contentAnalyzer/validBase64EncodedFile.txt")));
    }

    @Test
    public void testBase64EncodingWithAFolder() {
        File file = loadFile("/folder");
        assertTrue(file.isDirectory());
        assertFalse(AnalyzerFileUtils.base64Encoding(loadFile("/contentAnalyzer/notValidBase64File.txt"), file));
    }

    // @Test
    // public void testBase64EncodingWithnull() {
    // File file = null;
    // assertNull(file);
    // assertFalse(AnalyzerFileUtils.base64Encoding(FilesUtils.loadFile("/contentAnalyzer/notValidBase64File.txt"), file));
    // }

    @Test
    public void testBase64EncodingWithoutBase64File() {
        File file = new File("");
        assertFalse(AnalyzerFileUtils.base64Encoding(file, file));
    }

    // BASE64 DECODER
    @Test
    public void testBase64DecodingWithValidBase64File() {
        B64 b64 = new B64();
        assertTrue(b64.base64Decoding(loadFile("/contentAnalyzer/validBase64EncodedFile.txt"),
                loadFile("/contentAnalyzer/decodedFile.txt")));
        assertEquals(loadStringsFromFile("/contentAnalyzer/validBase64DecodedFile.txt"),
                loadStringsFromFile("/contentAnalyzer/decodedFile.txt"));
    }

    @Test
    public void testBase64DecodingWithnotValidBase64File() {
        File file = new File("tmp2.txt");
        B64 b64 = new B64();
        assertTrue(b64.base64Decoding(loadFile("/contentAnalyzer/notValidBase64File.txt"), file));
        assertFalse(fileToString(file).equals(loadStringsFromFile("/contentAnalyzer/validBase64DecodedFile.txt")));
    }

    @Test

    public void base64DecodingWithfolder() {
        File file = loadFile("/folder");
        B64 b64 = new B64();
        assertTrue(file.isDirectory());
        assertFalse(b64.base64Decoding(loadFile("/contentAnalyzer/notValidBase64File.txt"), file));
    }

    @Test
    public void testBase64DecodingWithoutBase64File() {
        File file = new File("");
        B64 b64 = new B64();
        assertFalse(b64.base64Decoding(file, file));
    }

    // BASE64 DECODER
    // @Test
    // public void testBase64DecodingWithValid_base64_string() {
    // m.base64Decoding(FilesUtils.loadStringsFromFile("/contentAnalyzer/validBase64EncodedFile.txt"), FilesUtils.loadStringsFromFile("/contentAnalyzer/decodedFile.txt"));
    // assertEquals(FilesUtils.loadStringsFromFile("/contentAnalyzer/decodedFile.txt"), FilesUtils.loadStringsFromFile("/contentAnalyzer/validBase64DecodedFile.txt"));
    // }

    // UNZIP
    // listA is added for launch this test BEFORE test_listFilesForFolderWithValidFolder
    @Test
    public void testListAUnzipWithValidZipFile() {
        String[] contentList = { "goodHl7File.hl7", "goodXmlFile.xml", "wado.dcm", "folder" };
        vd.unzip(loadFile("/contentAnalyzer/zipFile.zip"), new File("/tmp/test"));
        String[] list = new File("/tmp/test").list();

        List<String> expectedList = Arrays.asList(contentList);
        List<String> actualList = Arrays.asList(list);

        assertEquals(contentList.length, list.length);
        if (list.length > 0) {
            assertTrue(expectedList.containsAll(actualList));
        }
    }

    @Test
    public void testUnzipWithNotValidFolder() {
        File file = new File("/contentAnalyzer/folderZip");
        vd.unzip(loadFile("/contentAnalyzer/zipFile.zip"), file);
        //TODO
        assertTrue(true);

    }

    // LIST FILES FOLDER
    @Test
    public void testListFilesForFolderWithValidFolder() {
        String[] contentList = { "goodHl7File.hl7", "goodXmlFile.xml", "wado.dcm", "file.txt" };

        assertTrue(vd.listFilesForFolder(new File("/tmp/test")));
        assertEquals(4, vd.getFileList().size());

        List<String> expectedList = Arrays.asList(contentList);
        List<String> actualList = vd.getFileList();

        if (!actualList.isEmpty()) {
            assertTrue(expectedList.containsAll(actualList));
        }
        AnalyzerFileUtils.deleteTmpFile(new File("/contentAnalyzer/tmp/test"));
    }

    @Test
    public void listFilesForFolderWithNull() {
        File file = null;
        assertFalse(vd.listFilesForFolder(file));
    }

    @Test
    public void testListFilesForFolderWithoutFolder() {
        File file = new File("");
        assertFalse(vd.listFilesForFolder(file));
    }

    // unTar
    // @Test
    // public void test_unTarWithgood_tarFile() throws FileNotFoundException, IOException, ArchiveException {
    // String[] contentList = { "goodHl7File.hl7", "goodXmlFile.xml", "wado.dcm", "folder" };
    // vd.unTar(FilesUtils.loadFile("/contentAnalyzer/tarFile.tar"), loadFile("/contentAnalyzer/unTarFolder"));
    // String[] list = loadFile("/contentAnalyzer/unTarFolder").list();
    // assertEquals(list.length, contentList.length);
    // if (list.length > 0) {
    // for (int i = 0; i < list.length; i++) {
    // System.out.println(list[i]);
    // System.out.println(contentList[i]);
    // assertEquals(list[i], contentList[i]);
    // }
    // }
    // }

    @Test
    public void testUnTarWithnotValidFolder() throws FileNotFoundException, IOException, ArchiveException {
        File file = new File("/contentAnalyzer/folderTar");
        assertFalse(file.exists());
        List<File> untaredFiles = vd.unTar(loadFile("/contentAnalyzer/tarFile.tar"), file);
        assertNull(untaredFiles);
        String[] list = file.list();
        assertNull(list);

    }

    @Test
    public void unTarWithnull() throws FileNotFoundException, IOException, ArchiveException {
        File file = null;
        assertNull(file);
        assertNull(vd.unTar(loadFile("/contentAnalyzer/tarFile.tar"), file));
    }

    // CONTAINS HL7V3 ELEMENT DETECTION IN STRING
    @Test
    public void testContainsHl7v3MessageIdWithValidStringInXMLFile() {
        assertTrue(HL7V3.containsHl7v3MessageId(loadStringsFromFile("/contentAnalyzer/validHl7v3MessageIdXmlFile.xml"),
                m));
    }

    @Test
    public void testContainsHl7v3MessageIdWithPrefixWithValidStringInXMLFile() {
        assertTrue(HL7V3.containsHl7v3MessageId(
                loadStringsFromFile("/contentAnalyzer/validHl7v3MessageIdWithPrefixXmlFile.xml"), m));
    }

    @Test
    public void testContainsHl7v3MessageIdWithnotValidXMLFile() {
        assertFalse(HL7V3.containsHl7v3MessageId(loadStringsFromFile("/contentAnalyzer/notValidXmlFile.xml"), m));
    }

    @Test
    public void testContainsHl7v3MessageIdWithemptyFile() {
        assertFalse(HL7V3.containsHl7v3MessageId("", m));
    }

    @Test
    public void testContainsHl7v3MessageIdWithoutXMLFile() {
        assertFalse(HL7V3.containsHl7v3MessageId(new String("aserte.y"), m));
    }

    // CONTAINS HL7V3 ELEMENT DETECTION IN FILE
    @Test
    public void testContainsHl7v3MessageIdWithValidXMLFile() {
        assertTrue(HL7V3.containsHl7v3MessageId(loadFile("/contentAnalyzer/validHl7v3MessageIdXmlFile.xml"), m));
    }

    @Test
    public void testContainsHl7v3MessageIdWithfolder() {
        assertFalse(HL7V3.containsHl7v3MessageId(loadFile("/folder"), m));
    }

    // CONTAINS XD* ELEMENT DETECTION IN String
    @Test
    public void testContainsXDSWithValidStringInXMLFile() {
        assertTrue(XDS.containsXDS(loadStringsFromFile("/contentAnalyzer/validXdsXmlFile.xml"), m));
    }

    @Test
    public void testContainsXDSWithPrefixWithValidStringInXMLFile() {
        assertTrue(XDS.containsXDS(loadStringsFromFile("/contentAnalyzer/validXdsWithPrefixXmlFile.xml"), m));
    }

    @Test
    public void testContainsXDSWithStringEmpty() {
        String message = "";
        assertFalse(XDS.containsXDS(message, m));
    }

    // CONTAINS XD* ELEMENT DETECTION IN FILE
    @Test
    public void testContainsXDSWithnotValidXMLFile() {
        assertFalse(XDS.containsXDS(loadFile("/contentAnalyzer/notValidXmlFile.xml"), m));
    }

    // CONTAINS DSUB ELEMENT DETECTION IN String
    @Test
    public void testContainsDSUBWithValidStringInXMLFile() {
        assertTrue(DSUB.containsDSUB(loadStringsFromFile("/contentAnalyzer/validDsubXmlFile.xml"), m));
    }

    @Test
    public void testContainsDSUBWithPrefixWithValidStringInXMLFile() {
        assertTrue(DSUB.containsDSUB(loadStringsFromFile("/contentAnalyzer/validDsubWithPrefixXmlFile.xml"), m));
    }

    @Test
    public void testContainsDSUBWithStringEmpty() {
        String message = "";
        assertFalse(DSUB.containsDSUB(message, m));
    }

    @Test
    public void testContainsDSUBUnsubscribeWithValidStringInXMLFile() {
        assertTrue(DSUB.containsDSUB(loadStringsFromFile("/contentAnalyzer/validDsubUnsubscribe.xml"), m));
        assertTrue(DSUB.containsDSUB(loadStringsFromFile("/contentAnalyzer/validDsubUnsubscribe2.xml"), m));

    }

    // CONTAINS DSUB ELEMENT DETECTION IN FILE
    @Test
    public void testContainsDSUBWithnotValidXMLFile() {
        assertFalse(DSUB.containsDSUB(loadFile("/contentAnalyzer/notValidXmlFile.xml"), null, m));
    }

    // CONTAINS CDA ELEMENT DETECTION IN String
    @Test
    public void testContainsCDAWithValidStringInXMLFile() {
        assertTrue(CDA.containsCDA(loadStringsFromFile("/contentAnalyzer/validCdaXmlFile.xml"), m));
    }

    @Test
    public void testContainsCDAWithPrefixWithValidStringInXMLFile() {
        assertTrue(CDA.containsCDA(loadStringsFromFile("/contentAnalyzer/validCdaWithPrefixXmlFile.xml"), m));
    }

    @Test
    public void testContainsCDAWithStringEmpty() {
        String message = "";
        assertFalse(CDA.containsCDA(message, m));
    }

    // CONTAINS CDA ELEMENT DETECTION IN FILE
    @Test
    public void testContainsCDAWithnotValidXMLFile() {
        assertFalse(CDA.containsCDA(loadFile("/contentAnalyzer/notValidXmlFile.xml"), m));
    }

    @Test
    public void testContainsCDAWithfolder() {
        File file = loadFile("/folder");
        assertTrue(file.isDirectory());
        assertFalse(CDA.containsCDA(file, m));
    }

    // CONTAINS XDW ELEMENT DETECTION IN String
    @Test
    public void testContainsXDWWithValidStringInXMLFile() {
        assertTrue(XDW.containsXDW(loadStringsFromFile("/contentAnalyzer/validXdwXmlFile.xml"), m));
    }

    @Test
    public void testContainsXDWWithprefixWithValidStringInXMLFile() {
        assertTrue(XDW.containsXDW(loadStringsFromFile("/contentAnalyzer/validXdwWithPrefixXmlFile.xml"), m));
    }

    @Test
    public void testContainsXDWWithStringEmpty() {
        String message = "";
        assertFalse(XDW.containsXDW(message, m));
    }

    // CONTAINS XDW ELEMENT DETECTION IN FILE
    @Test
    public void testContainsXDWWithnotValidXMLFile() {
        assertFalse(XDW.containsXDW(loadFile("/contentAnalyzer/notValidXmlFile.xml"), m));
    }

    @Test
    public void testContainsXDWWithfolder() {
        File file = loadFile("/folder");
        assertTrue(file.isDirectory());
        assertFalse(XDW.containsXDW(file, m));
    }

    // CONTAINS XACML ELEMENT DETECTION IN String
    @Test
    public void testContainsXACMLWithValidStringInXMLFile() {
        assertTrue(XACML.containsXACML(loadStringsFromFile("/contentAnalyzer/validXACMLXmlFile.xml"), m));
    }

    @Test
    public void testContainsXACMLWithprefixWithValidStringInXMLFile() {
        assertTrue(XACML.containsXACML(loadStringsFromFile("/contentAnalyzer/validXACMLWithPrefixXmlFile.xml"), m));
    }

    @Test
    public void testContainsXACMLWithStringEmpty() {
        String message = "";
        assertFalse(XACML.containsXACML(message, m));
    }

    // CONTAINS XACML ELEMENT DETECTION IN FILE
    @Test
    public void testContainsXACMLWithnotValidXMLFile() {
        assertFalse(XACML.containsXACML(loadFile("/contentAnalyzer/notValidXmlFile.xml"), m));
    }

    @Test
    public void testContainsXACMLWithfolder() {
        File file = loadFile("/folder");
        assertTrue(file.isDirectory());
        assertFalse(XACML.containsXACML(file, m));
    }

    // CONTAINS AUDIT_MESSAGE ELEMENT DETECTION IN String
    @Test
    public void testContainsAuditMessageWithValidStringInXMLFile() {
        assertTrue(AUDIT_MESSAGE
                .containsAuditMessage(loadStringsFromFile("/contentAnalyzer/validAuditMessageXmlFile.xml"), m));
    }

    @Test
    public void testContainsAuditMessageWithPrefixWithValidStringInXMLFile() {
        assertTrue(AUDIT_MESSAGE
                .containsAuditMessage(loadStringsFromFile("/contentAnalyzer/validAuditMessageWithPrefixXmlFile.xml"),
                        m));
    }

    @Test
    public void testContainsAuditMessageWithStringEmpty() {
        String message = "";
        assertFalse(AUDIT_MESSAGE.containsAuditMessage(message, m));
    }

    // CONTAINS AUDIT_MESSAGE ELEMENT DETECTION IN FILE
    @Test
    public void testContainsAuditMessageWithnotValidXMLFile() {
        assertFalse(AUDIT_MESSAGE.containsAuditMessage(loadFile("/contentAnalyzer/notValidXmlFile.xml"), m));
    }

    @Test
    public void testContainsAuditMessageWithfolder() {
        File file = loadFile("/folder");
        assertTrue(file.isDirectory());
        assertFalse(AUDIT_MESSAGE.containsAuditMessage(file, m));
    }

    // CONTAINS HPD ELEMENT DETECTION IN String
    @Test
    public void testContainsHPDWithValidStringInXMLFile() {
        assertTrue(HPD.containsHPD(loadStringsFromFile("/contentAnalyzer/validHpdXmlFile.xml"), m));
    }

    @Test
    public void testContainsHPDWithprefixWithValidStringInXMLFile() {
        assertTrue(HPD.containsHPD(loadStringsFromFile("/contentAnalyzer/validHpdWithPrefixXmlFile.xml"), m));
    }

    @Test
    public void testContainsHPDWithStringEmpty() {
        String message = "";
        assertFalse(HPD.containsHPD(message, m));
    }

    // CONTAINS HPD ELEMENT DETECTION IN FILE
    @Test
    public void testContainsHPDWithnotValidXMLFile() {
        assertFalse(HPD.containsHPD(loadFile("/contentAnalyzer/notValidXmlFile.xml"), m));
    }

    @Test
    public void testContainsHPDWithfolder() {
        File file = loadFile("/folder");
        assertTrue(file.isDirectory());
        assertFalse(HPD.containsHPD(file, m));
    }

    // CONTAINS SVS ELEMENT DETECTION IN STRING
    @Test
    public void testContainsSVSWithValidStringInXMLFile() {
        assertTrue(SVS.containsSVS(loadStringsFromFile("/contentAnalyzer/validSvsXmlFile.xml"), m));
    }

    @Test
    public void testContainsSVSWithprefixWithValidStringInXMLFile() {
        assertTrue(SVS.containsSVS(loadStringsFromFile("/contentAnalyzer/validSvsWithPrefixXmlFile.xml"), m));
    }

    @Test
    public void testContainsSVSWithStringEmpty() {
        String message = "";
        assertFalse(SVS.containsSVS(message, m));
    }

    // CONTAINS SVS ELEMENT DETECTION IN FILE
    @Test
    public void testContainsSVSWithnotValidXMLFile() {
        assertFalse(SVS.containsSVS(loadFile("/contentAnalyzer/notValidXmlFile.xml"), m));
    }

    @Test
    public void testContainsSVSWithfolder() {
        File file = loadFile("/folder");
        assertTrue(file.isDirectory());
        assertFalse(SVS.containsSVS(file, m));
    }

    // DETECT FILE TYPE
    @Test
    public void test_detectFileTypeWithsvsXMLFile() {
        assertEquals("SVS",
                xml.detectFileType(loadFile("/contentAnalyzer/validSvsXmlFile.xml"), 0, 0, new FileToValidate(null), m)
                        .getClass().getSimpleName());
    }

    @Test
    public void test_detectFileTypeWithsvsXMLFileAndOffset() {
        assertEquals("SVS",
                xml.detectFileType(loadFile("/contentAnalyzer/validSvsXmlFile.xml"), 0, 0, new FileToValidate(null),
                        m).getClass().getSimpleName());
    }

    @Test
    public void test_detectFileTypeWithhpdXMLFile() {
        assertEquals("HPD",
                xml.detectFileType(loadFile("/contentAnalyzer/validHpdXmlFile.xml"), 0, 0, new FileToValidate(null), m)
                        .getClass().getSimpleName());
    }

    @Test
    public void test_detectFileTypeWithAuditMessageXMLFile() {
        assertEquals("AUDIT_MESSAGE", xml.detectFileType(loadFile("/contentAnalyzer/validAuditMessageXmlFile.xml"), 0, 0,
                new FileToValidate(null), m).getClass().getSimpleName());
    }

    // @Test
    // public void test_detectFileTypeWithdsubXMLFile() {
    // assertEquals("DSUB", m.detectFileType(FilesUtils.loadFile("/contentAnalyzer/validDsubXmlFile.xml"), 0, 0, new FileToValidate(null)).getClass().getSimpleName());
    // }

    @Test
    public void test_detectFileTypeWithxdwXMLFile() {
        assertEquals("XDW",
                xml.detectFileType(loadFile("/contentAnalyzer/validXdwXmlFile.xml"), 0, 0, new FileToValidate(null), m)
                        .getClass().getSimpleName());
    }

    @Test
    public void test_detectFileTypeWithxdsXMLFile() {
        assertEquals("XDS",
                xml.detectFileType(loadFile("/contentAnalyzer/validXdsXmlFile.xml"), 0, 0, new FileToValidate(null), m)
                        .getClass().getSimpleName());
    }

    @Test
    public void test_detectFileTypeWithhl7v3XMLFile() {
        assertEquals("HL7V3", xml.detectFileType(loadFile("/contentAnalyzer/validHl7v3MessageIdXmlFile.xml"), 0, 0,
                new FileToValidate(null), m).getClass().getSimpleName());
    }

    @Test
    public void test_detectFileTypeWithcdaXMLFile() {
        assertEquals("CDA",
                xml.detectFileType(loadFile("/contentAnalyzer/validCdaXmlFile.xml"), 0, 0, new FileToValidate(null), m)
                        .getClass().getSimpleName());
    }

    @Test
    public void test_detectFileTypeWithnotValidXMLFile() {
        assertEquals("NullAnalyzable",
                xml.detectFileType(loadFile("/contentAnalyzer/notValidXmlFile.xml"), 0, 0, new FileToValidate(null), m)
                        .getClass().getSimpleName());
    }

    @Test
    public void test_detectFileTypeWithnotValidXMLFile2() {
        assertEquals("NullAnalyzable",
                xml.detectFileType(loadFile("/contentAnalyzer/notValidXmlFile2.xml"), 0, 0, new FileToValidate(null), m)
                        .getClass().getSimpleName());
    }


    // Reset Validator detector
    @Test
    public void test_reset() {
        vd.reset();
        assertNull(vd.getAnalyzedObject());
        assertTrue(vd.getParentFileToValidate().getFileName().isEmpty());
        assertTrue(vd.getParentFileToValidate().getValidationType().isEmpty());
        assertTrue(vd.getParentFileToValidate().getDocType().isEmpty());
        assertTrue(vd.getParentFileToValidate().getResult().isEmpty());
        assertTrue(vd.getParentFileToValidate().getTextLayout().isEmpty());
        assertNull(vd.getParentFileToValidate().getParent());
        assertEquals(0, vd.getParentFileToValidate().getFileToValidateListToDisplay().size());
        assertNull(m.getHeaderValue());
        assertNull(m.getBodyValue());
        assertEquals(0, m.getSaveStartOffset());
    }

    // View message part
    @Test
    @Ignore
    public void testViewMessagePartWithDicomFile() {
        String objectPath = loadFile("/contentAnalyzer/validDicomFile.dcm").getPath();
        String validationType = "DICOM";
        int startOffset = 0;
        int endOffset = 0;

        vd.viewMessagePart(objectPath, validationType, startOffset, endOffset);
        String result = vd.getMessagePart();
        assertEquals(result, fileToString(loadFile("/contentAnalyzer/decodedDicom.txt")));

    }

}
