package net.ihe.gazelle.evs.client.integration;

import net.ihe.gazelle.evs.client.MessageContentAnalyzerTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MBValidatorTest.class, SCHValidatorTest.class, SVSTest.class, MessageContentAnalyzerTest.class })
public class AllTests {

}
