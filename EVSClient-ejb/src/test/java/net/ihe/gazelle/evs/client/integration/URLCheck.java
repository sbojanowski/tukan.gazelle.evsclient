package net.ihe.gazelle.evs.client.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyStore;

/**
 * Utility class used to perform a check into an URL for test execution
 *
 * @author abderrazek boufahja
 */
class URLCheck {

    private static final Logger LOGGER = LoggerFactory.getLogger(URLCheck.class);

    private URLCheck() {
    }

    static boolean checkURL(String inURL) {
        if (inURL != null) {
            try {
                URLConnection connection = null;
                if (inURL.startsWith("https")) {
                    URL url = new URL(inURL);
                    String keystorePath = "/opt/gazelle/cert/truststore.jks";
                    connection = createHttpsConnection(url, keystorePath, "password", "password");
                }
 else {
                    connection = new URL(inURL).openConnection();
                }
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                Integer timeout = 2000;
                connection.setConnectTimeout(timeout);
                connection.setReadTimeout(timeout);
                connection.connect();
                return true;
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                return false;
            }
        }
        return false;
    }

    private static HttpsURLConnection createHttpsConnection(URL inUrl, String keystorePath, String keyStorePassword,
            String keyPassword) throws Exception {
        try {
            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(new FileInputStream(keystorePath),
                    keyStorePassword == null ? null : keyStorePassword.toCharArray());

            KeyManagerFactory keyManagerFactory = KeyManagerFactory
                    .getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(keyStore, keyPassword == null ? null : keyPassword.toCharArray());
            KeyManager[] km = keyManagerFactory.getKeyManagers();

            TrustManagerFactory trustManagerFactory = TrustManagerFactory
                    .getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(keyStore);
            TrustManager[] tm = trustManagerFactory.getTrustManagers();

            SSLContext context = SSLContext.getInstance("TLS");
            context.init(km, tm, null);
            SSLSocketFactory sslSocketFactory = context.getSocketFactory();
            HttpsURLConnection connection = (HttpsURLConnection) inUrl.openConnection();
            HostnameVerifier verifier = new CustomizedHostnameVerifier();
            connection.setHostnameVerifier(verifier);
            connection.setSSLSocketFactory(sslSocketFactory);
            return connection;
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * This class is used to avoid java.security.cert.CertificateException: No name matching "hostname" found exception
     *
     * @author aberge
     */
    private static class CustomizedHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

}
