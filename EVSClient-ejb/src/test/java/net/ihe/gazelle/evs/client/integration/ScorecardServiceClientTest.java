package net.ihe.gazelle.evs.client.integration;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Test;

import net.ihe.gazelle.evs.client.cdascorecard.ScorecardServiceClient;

public class ScorecardServiceClientTest {

	//@Test
	public void testGetScorecardFromValidator() throws IOException, Exception {
		String scorecard = ScorecardServiceClient.getScorecardFromValidator("1.2.3",readDoc("src/test/resources/scorecard/mbvResult.xml"), 
				"https://gazellecontent.sequoiaproject.org/CDAGenerator");
		assertTrue(scorecard.contains("<ValidationStatistics>"));
		assertTrue(scorecard.contains("<numberExecutedCheck>4237</numberExecutedCheck>"));
		assertTrue(scorecard.contains("<numberErrorsFound>68</numberErrorsFound>"));
	}
	
	//@Test
	public void testContextualInformation() throws IOException, Exception {
		String scorecard = ScorecardServiceClient.getScorecardFromValidator("1.2.3",readDoc("src/test/resources/scorecard/mbvResult.xml"), 
				"https://gazellecontent.sequoiaproject.org/CDAGenerator");
		System.out.println(scorecard);
		assertTrue(scorecard.contains("<validationIdentifier>1.2.3</validationIdentifier>"));
		assertTrue(scorecard.contains("<statisticsIdentifier>"));
		assertTrue(scorecard.contains("<statisticToolIdentifier>Gazelle CDA Validation</statisticToolIdentifier>"));
		assertTrue(scorecard.contains("<statisticVersion>"));
		assertTrue(scorecard.contains("<effectiveDate>"));
	}
	
	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new InputStreamReader(new FileInputStream(name)));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}

}
