package net.ihe.gazelle.evs.testperf;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub.ValidateObject;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub.ValidateObjectE;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub.ValidateObjectResponseE;
import net.ihe.gazelle.sch.validator.ws.client.SOAPExceptionException;
import org.apache.axis2.Constants;
import org.junit.Test;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TestPerfSch {

	private static final String SCH_ENDPOINT_OLD =
	             "https://gazelle.ihe.net/SchematronValidator-ejb/GazelleObjectValidatorService/GazelleObjectValidator?wsdl";

	@Test
	public void testSchematronWSXSD10() throws IOException, SOAPExceptionException {
		for (int i=0; i<30; i++) {
			GazelleObjectValidatorServiceStub stub = new GazelleObjectValidatorServiceStub(SCH_ENDPOINT_OLD);
			stub._getServiceClient().getOptions().setProperty(Constants.Configuration.DISABLE_SOAP_ACTION, true);
			ValidateObject params = new ValidateObject();
			params.setXmlMetadata("IHE - ITI - Cross-Enterprise Sharing of Scanned Documents (XDS-SD)");
			params.setXmlReferencedStandard("CDA-IHE");
			params.setBase64ObjectToValidate(DatatypeConverter.printBase64Binary(Files.readAllBytes(Paths.get("src/test/resources/cda/sample1.xml"))));
			ValidateObjectE paramsE = new ValidateObjectE();
			paramsE.setValidateObject(params);
			ValidateObjectResponseE responseE = stub.validateObject(paramsE);
		}
	}

	@Test
	public void testSchematronWSXSD11() throws IOException, SOAPExceptionException {
		for (int i=0; i<30; i++) {
			GazelleObjectValidatorServiceStub stub = new GazelleObjectValidatorServiceStub(SCH_ENDPOINT_OLD);
			stub._getServiceClient().getOptions().setProperty(Constants.Configuration.DISABLE_SOAP_ACTION, true);
			ValidateObject params = new ValidateObject();
			params.setXmlMetadata("IHE - ITI - Cross-Enterprise Sharing of Scanned Documents (XDS-SD)");
			params.setXmlReferencedStandard("CDA-IHE");
			params.setBase64ObjectToValidate(DatatypeConverter.printBase64Binary(Files.readAllBytes(Paths.get("src/test/resources/cda/sample1.xml"))));
			ValidateObjectE paramsE = new ValidateObjectE();
			paramsE.setValidateObject(params);
			ValidateObjectResponseE responseE = stub.validateObject(paramsE);
		}
	}
}