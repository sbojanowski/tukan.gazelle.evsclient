package net.ihe.gazelle.evs.client.util;

import net.ihe.gazelle.files.FilesUtils;
import org.junit.Test;

import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by epoiseau on 31/05/2016.
 */

public class UtilTest {


    //@Test
    public void testTransformXMLFileToHTML() throws Exception {
        String xml = FilesUtils.loadStringsFromFile("/saml/assertion.xml");
        URL file_url = FilesUtils.class.getResource("/saml/saml2html.xsl");
        String xsl = file_url.getPath();
        String a = FilesUtils.loadStringsFromFile("/saml/saml2html.html");
        String b = Util.transformXMLStringToHTML(xml, xsl);
        // checking the transformation works. Ignoring spaces and cases !
        assertTrue(a.replaceAll("\\s+","").equalsIgnoreCase(b.replaceAll("\\s+","")));
        }

    @Test
    public void testMakeHyperLinkHTTP() {
        assertEquals(Util.makeHyperLink("this is a string http://ftp.ihe-europe.net"),"this is a string <a href=\"http://ftp.ihe-europe.net\">http://ftp.ihe-europe.net</a>");
    }

    @Test
    public void testMakeHyperLinkHTTPS() {
        assertEquals(Util.makeHyperLink("this is a string https://ftp.ihe-europe.net"),"this is a string <a href=\"https://ftp.ihe-europe.net\">https://ftp.ihe-europe.net</a>");
    }

    @Test
    public void testMakeHyperMultipleLink() {
        assertEquals(Util.makeHyperLink("this is a string https://ftp.ihe-europe.net and https://ftp.ihe-europe.net"),"this is a string <a href=\"https://ftp.ihe-europe.net\">https://ftp.ihe-europe.net</a> and <a href=\"https://ftp.ihe-europe.net\">https://ftp.ihe-europe.net</a>");
    }

    @Test
    public void testPrettyFormat() {
        String input = "<ClinicalDocument xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" classCode=\"DOCCLIN\" moodCode=\"EVN\" xmlns=\"urn:hl7-org:v3\"><RetrieveMultipleValueSetsRequest><!--This is a comment--><DisplayNameContains>yoyo</DisplayNameContains><DisplayNameContains>yoyo</DisplayNameContains><DisplayNameContains>yoyo</DisplayNameContains></RetrieveMultipleValueSetsRequest></ClinicalDocument>";
        String test = Util.prettyFormat(input, 2);
        System.out.print("---------\n");
        System.out.print(input);
        System.out.print("\n---------\n");

        System.out.print(test);
        System.out.print("\n---------\n");

        assertEquals(true, true);

    }
}
