 --
 -- Copyright 2010 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --




--
-- Data for Name: evsc_referenced_standard; Type: TABLE DATA; Schema: public; Owner: gazelle
--
INSERT INTO evsc_referenced_standard (id, name, extension, version, description, label) VALUES (1, 'HL7', 'None', '2.3.1', 'version 2.3.1 of HL7 standard', 'HL7v2.3.1');
INSERT INTO evsc_referenced_standard (id, name, extension, version, description, label) VALUES (2, 'HL7', 'None', '2.4', 'version 2.4 of HL7 standard', 'HL7v2.4');
INSERT INTO evsc_referenced_standard (id, name, extension, version, description, label) VALUES (3, 'HL7', 'None', '2.5', 'version 2.5 of HL7 standard', 'HL7v2.5');
INSERT INTO evsc_referenced_standard (id, name, extension, version, description, label) VALUES (4, 'CDA', 'IHE', null, 'CDA', 'CDA');
INSERT INTO evsc_referenced_standard (id, name, extension, version, description, label) VALUES (5, 'CDA', 'epSOS', null, 'epSOS specific CDA', 'CDA-epSOS');
INSERT INTO evsc_referenced_standard (id, name, extension, version, description, label) VALUES (6, 'HL7', 'None', '3', 'version 3 of HL7 standard', 'HL7v3');
INSERT INTO evsc_referenced_standard (id, name, extension, version, description, label) VALUES (7, 'Audit Trail', 'None', 'RFC3881', 'audit messages for ATNA', 'Audit Trail');
INSERT INTO evsc_referenced_standard (id, name, extension, version, description, label) VALUES (8, 'SAML', 'None', '2.0', 'security assertion markup language', 'SAML 2.0');
INSERT INTO evsc_referenced_standard (id, name, extension, version, description, label) VALUES (9, 'DICOM', 'None', 'None', 'Digital Imaging And Communication in Medicine', 'DICOM');
INSERT INTO evsc_referenced_standard (id, name, extension, version, description, label) VALUES (10, 'HL7', 'None', '2.5.1', 'version 2.5.1 of HL7 standard', 'HL7v2.5.1');
INSERT INTO evsc_referenced_standard (id, name, extension, version, description, label) VALUES (11, 'HL7', 'None', '2.6', 'version 2.6 of HL7 standard', 'HL7v2.6');


SELECT pg_catalog.setval('evsc_referenced_standard_id_seq', 11 , true);


--
-- PostgreSQL database dump complete
--

