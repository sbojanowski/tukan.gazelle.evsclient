INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (1, '1.3.6.1.4.12559.11.1.1.35', 'net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (2, '1.3.6.1.4.12559.11.1.1.135', 'net.ihe.gazelle.laboratory.hl7v2.model.v25.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (3, '1.3.6.1.4.12559.11.1.1.137', 'net.ihe.gazelle.laboratory.hl7v2.model.v25.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (4, '1.3.6.1.4.12559.11.1.1.134', 'net.ihe.gazelle.laboratory.hl7v2.model.v25.message');

INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (5, '1.3.6.1.4.12559.11.1.1.37', 'net.ihe.gazelle.pam.hl7v2.model.v25.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (6, '1.3.6.1.4.12559.11.1.1.41', 'net.ihe.gazelle.pam.hl7v2.model.v25.message');

INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (7, '1.3.6.1.4.12559.11.1.1.121', 'net.ihe.gazelle.laboratory.hl7v2.model.v25.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (8, '1.3.6.1.4.12559.11.1.1.31', 'net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message');

INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (9, '1.3.6.1.4.12559.11.1.1.30', 'net.ihe.gazelle.iti.pix.hl7v2.model.v25.message');

UPDATE "package_name_for_profile_oid" SET package_name = 'net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message' WHERE (sent_message_type='net.ihe.gazelle.iti.hl7v2.model.v25.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (10, '1.3.6.1.4.12559.11.1.1.177', 'net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (12, '1.3.6.1.4.12559.11.1.1.180', 'net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message');

--Update 23/02/2012
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (11, '1.3.6.1.4.12559.11.1.1.136', 'net.ihe.gazelle.laboratory.hl7v2.model.v25.message');

INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (13, '1.3.6.1.4.12559.11.1.1.181', 'net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message');

--UPDATE 20/03/2012
DELETE FROM "package_name_for_profile_oid" WHERE profile_oid='1.3.6.1.4.12559.11.1.1.172';
DELETE FROM "package_name_for_profile_oid" WHERE profile_oid='1.3.6.1.4.12559.11.1.1.121';
DELETE FROM "package_name_for_profile_oid" WHERE profile_oid='1.3.6.1.4.12559.11.1.1.136';
DELETE FROM "package_name_for_profile_oid" WHERE profile_oid='1.3.6.1.4.12559.11.1.1.135';
DELETE FROM "package_name_for_profile_oid" WHERE profile_oid='1.3.6.1.4.12559.11.1.1.137';
DELETE FROM "package_name_for_profile_oid" WHERE profile_oid='1.3.6.1.4.12559.11.1.1.134';

INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (34, '1.3.6.1.4.12559.11.1.1.172', 'net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (35, '1.3.6.1.4.12559.11.1.1.121', 'net.ihe.gazelle.laboratory.lcsd.hl7v2.model.v25.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (36, '1.3.6.1.4.12559.11.1.1.136', 'net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (37, '1.3.6.1.4.12559.11.1.1.135', 'net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (38, '1.3.6.1.4.12559.11.1.1.137', 'net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (39, '1.3.6.1.4.12559.11.1.1.134', 'net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.message');

