 --
 -- Copyright 2010 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --




--
-- Data for Name: evsc_user; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO evsc_user (id, first_name, last_name, email, login, password, last_login, logins_counter, blocked, activated, activation_code) VALUES (1, 'admin', 'admin', 'aberge@inria.fr', 'admin', 'e10adc3949ba59abbe56e057f20f883e', NULL, 0, false, true, NULL);


SELECT pg_catalog.setval('evsc_user_id_seq', 1 , true);


--
-- PostgreSQL database dump complete
--

