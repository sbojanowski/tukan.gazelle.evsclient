 --
 -- Copyright 2010 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --




--
-- Data for Name: evsc_user_role; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO evsc_user_role (id, keyword, description, label) VALUES (1, 'admin', 'administrator of the application', 'admin role');
INSERT INTO evsc_user_role (id, keyword, description, label) VALUES (2, 'basic', 'basic user of the application', 'basic role');
INSERT INTO evsc_user_role (id, keyword, description, label) VALUES (3, 'profile_writer', 'user allowed to write new profiles', 'profile writer role');


SELECT pg_catalog.setval('evsc_user_role_id_seq', 3 , true);


--
-- PostgreSQL database dump complete
--

