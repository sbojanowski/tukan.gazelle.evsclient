 --
 -- Copyright 2010 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --




--
-- Data for Name: evsc_user_user_role; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO evsc_user_user_role (role_id, user_id) VALUES (1, 1);
INSERT INTO evsc_user_user_role (role_id, user_id) VALUES (3, 1);

--
-- PostgreSQL database dump complete
--

