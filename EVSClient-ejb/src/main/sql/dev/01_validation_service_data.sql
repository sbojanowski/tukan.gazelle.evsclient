 --
 -- Copyright 2010 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --




--
-- Data for Name: tf_domain; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO evsc_validation_service (id, keyword, description, name, wsdl, available, provider) VALUES (1, 'HL7v2_INRIA', 'calls hl7CupWS from InriaHL7Validator project', 'HL7v2.x Validator', 'http://maki.irisa.fr:8080/InriaHL7Validator-dev-InriaHL7Validator-ejb/InriaHL7CupWS?wsdl', true, 'INRIA');
--INSERT INTO evsc_validation_service (id, keyword, description, label, wsdl, available) VALUES (1, 'HL7v2_INRIA', 'calls hl7CupWS from InriaHL7Validator project', 'INRIA', 'http://131.254.209.16:8080/Hl7V2Evs-prod-InriaHL7Validator-ejb/InriaHL7CupWS?wsdl', true);
INSERT INTO evsc_validation_service (id, keyword, description, name, wsdl, available, provider) VALUES (2, 'HL7v2_NIST', 'calls NIST web service ', 'HL7 V2 Validation', 'http://xreg2.nist.gov:8080/HL7WS/services/MessageValidation', true, 'NIST');
--INSERT INTO evsc_validation_service (id, keyword, description, label, wsdl, available) VALUES (3, 'Gazelle', 'calls Gazelle web services ', 'Gazelle', 'http://jumbo.irisa.fr:8080/SchematronValidator-prod-SchematronValidator-ejb/GazelleObjectValidatorWS?wsdl', true);
INSERT INTO evsc_validation_service (id, keyword, description, name, wsdl, available, provider) VALUES (3, 'Gazelle', 'calls Gazelle web services ', 'Gazelle', 'http://131.254.209.12:8080/SchematronValidator-prod-SchematronValidator-ejb/GazelleObjectValidatorWS?wsdl', true, 'INRIA');
INSERT INTO evsc_validation_service (id, keyword, description, name, wsdl, available, provider) VALUES (4, 'dciodvfy', 'Dicom 3 tools validator', 'Dicom 3 tools - Dciodvfy', '/opt/EVSClient_dev/bin/dicom3tools/dciodvfy', true, 'David Clunie');
INSERT INTO evsc_validation_service (id, keyword, description, name, wsdl, available, provider) VALUES (5, 'dcdump', 'Dicom 3 tools validator', 'Dicom 3 tools - DcDump', '/opt/EVSClient_dev/bin/dicom3tools/dcdump', true, 'David Clunie');
INSERT INTO evsc_validation_service (id, keyword, description, name, wsdl, available, provider) VALUES (6, 'HAPI', 'calls hl7CupWS (HAPI) from InriaHL7Validator project', 'HL7v2.x HAPI Validator', 'http://maki.irisa.fr:8080/InriaHL7Validator-dev-InriaHL7Validator-ejb/InriaHL7CupWS?wsdl', true, 'INRIA');
INSERT INTO evsc_validation_service (id, keyword, description, name, wsdl, available, provider) VALUES (7, 'TLS', 'calls TLS validation from PKI project', 'TLS certificate Validator', 'http://tatami.irisa.fr:8080/TLSSimulator-TLSSimulator-ejb/CertificateValidator?wsdl', true, 'INRIA');

SELECT pg_catalog.setval('evsc_validation_service_id_seq', 7 , true);


--
-- PostgreSQL database dump complete
--

