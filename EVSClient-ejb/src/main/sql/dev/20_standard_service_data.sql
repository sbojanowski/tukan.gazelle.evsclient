 --
 -- Copyright 2010 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --




--
-- Data for Name: evsc_standard_service; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (1, 1);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (1, 2);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (2, 1);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (2, 2);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (3, 1);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (3, 2);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (4, 3);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (5, 3);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (6, 3);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (7, 3);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (8, 3);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (9, 4);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (9, 5);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (1, 6);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (2, 6);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (3, 6);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (10, 6);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (10, 1);
INSERT INTO evsc_standard_service (referenced_standard_id, validation_service_id) VALUES (11, 6);

--
-- PostgreSQL database dump complete
--

