 --
 -- Copyright 2010 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --




--
-- Data for Name: evsc_user; Type: TABLE DATA; Schema: public; Owner: gazelle
--

 -- admin password is blank


INSERT INTO evsc_user (id, username, password_hash, enabled) VALUES (1, 'admin', 'Ss/jICpf9c9GeJj8WKqx1hUClEE=', true);
INSERT INTO evsc_user (id, username, password_hash, enabled) VALUES (2, 'aberge', 'Ss/jICpf9c9GeJj8WKqx1hUClEE=', true);


SELECT pg_catalog.setval('evsc_user_id_seq', 2 , true);


--
-- PostgreSQL database dump complete
--

