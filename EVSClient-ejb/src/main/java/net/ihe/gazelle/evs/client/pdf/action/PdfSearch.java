/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.pdf.action;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.action.AbstractLogBrowser;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.pdf.model.ValidatedPdf;
import net.ihe.gazelle.evs.client.pdf.model.ValidatedPdfQuery;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.Map;

@Name("pdfSearch")
@Scope(ScopeType.PAGE)
public class PdfSearch extends AbstractLogBrowser<ValidatedPdf> {

    /**
     *
     */
    private static final long serialVersionUID = -7562201612389380968L;

    private String pdfProfileExtract;

    @Override
    protected ValidatedPdf getObjectByOID(final String oid, final String privacyKey) {
        return ValidatedObject.getObjectByOID(ValidatedPdf.class, oid, privacyKey);
    }

    public void setPdfProfileExtract(final String pdfProfileExtract) {
        this.pdfProfileExtract = pdfProfileExtract;
    }

    public String getPdfProfileExtract() {
        return this.pdfProfileExtract;
    }

    @Override
    protected Class<ValidatedPdf> getEntityClass() {
        return ValidatedPdf.class;
    }

    @Override
    protected HQLCriterionsForFilter<ValidatedPdf> getCriterions() {
        final ValidatedPdfQuery validatedPdfQuery = new ValidatedPdfQuery();
        final HQLCriterionsForFilter<ValidatedPdf> result = validatedPdfQuery.getHQLCriterionsForFilter();
        result.addPath("pdfValid", validatedPdfQuery.pdfValid());
        result.addPath("pdfWellFormed", validatedPdfQuery.pdfWellFormed());
        result.addPath("privateUser", validatedPdfQuery.privateUser());
        result.addPath("institutionKeyword", validatedPdfQuery.institutionKeyword());
        result.addPath("validationService", validatedPdfQuery.service());
        result.addPath("standard", validatedPdfQuery.standard().label());
        result.addPath("validationStatus", validatedPdfQuery.validationStatus());
        result.addPath("validationDate", validatedPdfQuery.validationDate());
        return result;
    }

    @Override
    public void modifyQuery(final HQLQueryBuilder<ValidatedPdf> queryBuilder, final Map<String, Object> filterValuesApplied) {
        super.modifyQuery(queryBuilder, filterValuesApplied);
        if (this.pdfProfileExtract != null && !this.pdfProfileExtract.isEmpty()) {
            queryBuilder.addRestriction(
                    HQLRestrictions.like("pdfProfiles", this.pdfProfileExtract, HQLRestrictionLikeMatchMode.ANYWHERE));
        }
    }

    @Override
    public FilterDataModel<ValidatedPdf> getDataModel() {
        return new FilterDataModel<ValidatedPdf>(PdfSearch.this.getFilter()) {
            @Override
            protected Object getId(final ValidatedPdf t) {
                return t.getId();
            }
        };
    }
}
