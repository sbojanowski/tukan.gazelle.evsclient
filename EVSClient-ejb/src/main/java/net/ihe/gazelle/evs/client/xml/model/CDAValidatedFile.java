/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.cdascorecard.ScorecardStatus;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.evs.client.xml.logs.ObjectIndexer;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.hibernate.annotations.Cascade;
import org.hibernate.envers.NotAudited;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.util.Base64;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.filter.Filter;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * <b>Class Description : </b>CDAValidatedFile<br>
 * <br>
 * This class describes the CDAValidatedFile which represents the CDA files uploaded by the user and validated by the selected service
 * <p/>
 * CDAValidatedFile possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id of the message in database</li>
 * <li><b>cdaFilePath</b> : Path to the cda file which has been uploaded and stored ON disk -location is given by an application preference-</li>
 * <li><b>description</b> : This attribute can be used be the user to describe its CDA file and thus retrieve it later more easily</li>
 * <li><b>schematron</b> : the schematron that has been used for validation</li>
 * <li><b>detailedResults</b> : detailed results of the validation (error, warnings ...)</li>
 * <li><b>metadata</b> : message metadata (method, language ...)</li>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, July 1st
 * @class CDAValidatedFile.java
 * @package net.ihe.gazelle.evs.client.cda.model
 * @see Aberge@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Name("cdaValidatedFile")
@Table(name = "cda_validated_file", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "cda_validated_file_sequence", sequenceName = "cda_validated_file_id_seq", allocationSize = 1)
public class CDAValidatedFile extends AbstractValidatedXMLFile {

    /**
     *
     */
    private static final long serialVersionUID = 3435397463635134710L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CDAValidatedFile.class);

    private static final String CDA_FILE_PREFIX = "validatedCDA_";
    private static final String XML_FILE_SUFFIX = ".xml";
    private static final String CDA_PDF_FILE_PREFIX = "pdfFromCDA_";
    private static final String CDA_PDF_FILE_SUFFIX = ".pdf";
    private static final String CDA_SCORECARD_FILE_PREFIX = "scorecard_";

    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cda_validated_file_sequence")
    private Integer id;

    @Column(name = "cda_file_path")
    private String cdaFilePath;

    @Column(name = "called_method")
    private String schematron;

    @Column(name = "pdf_size")
    private Integer pdfSize;

    @ManyToMany
    @JoinTable(name = "object_indexer_cda_validated_file", joinColumns = @JoinColumn(name = "cda_validated_file_id"), inverseJoinColumns = @JoinColumn(name = "object_indexer_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "cda_validated_file_id", "object_indexer_id" }))
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @XmlTransient
    @NotAudited
    private List<ObjectIndexer> objectIndexers;

    @Column(name = "templates_listed")
    private boolean templatesListed;

    @Column(name="scorecard_status")
    private ScorecardStatus scorecardStatus;


    public CDAValidatedFile(final CDAValidatedFile cdaFile) {
        this.cdaFilePath = cdaFile.getCdaFilePath();
        this.description = cdaFile.getDescription();
    }

    public CDAValidatedFile() {

    }

    /**
     * Getters and Setters
     */

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getCdaFilePath() {
        return this.cdaFilePath;
    }

    public void setCdaFilePath(final String cdaFilePath) {
        this.cdaFilePath = cdaFilePath;
    }

    @Override
    public String getSchematron() {
        return this.schematron;
    }

    @Override
    public void setSchematron(final String schematron) {
        this.schematron = schematron;
    }

    public Integer getPdfSize() {
        return this.pdfSize;
    }

    public void setPdfSize(final Integer size) {
        this.pdfSize = size;
    }

    public List<ObjectIndexer> getObjectIndexers() {
        if (this.objectIndexers == null) {
            this.objectIndexers = new ArrayList<>();
        }
        return this.objectIndexers;
    }

    public void setObjectIndexers(final List<ObjectIndexer> objectIndexers) {
        if (objectIndexers != null) {
            this.objectIndexers = new ArrayList<ObjectIndexer>(objectIndexers.size());
            Collections.copy(this.objectIndexers, objectIndexers);
        }
        else {
            this.objectIndexers = null;
        }
    }

    public boolean isTemplatesListed() {
        return this.templatesListed;
    }

    public void setTemplatesListed(final boolean templatesListed) {
        this.templatesListed = templatesListed;
    }

    private void parseXmlForPdfs() throws JDOMException, IOException {
        final SAXBuilder sxb = new SAXBuilder();
        final File newFile = new File(this.cdaFilePath);
//        newFile.getParentFile().mkdirs();
        final Document doc = sxb.build(newFile);


        @SuppressWarnings("unchecked")
        final Iterator<Element> textElements = doc.getRootElement().getDescendants(new Filter() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public boolean matches(final Object obj) {
                if (obj instanceof Element) {
                    final Element element = (Element) obj;
                    if ("text".equals(element.getName()) && "application/pdf"
                            .equals(element.getAttributeValue("mediaType"))
                            && "B64".equals(element.getAttributeValue("representation"))) {
                        return true;
                    }
                }
                return false;
            }
        });

        this.pdfSize = Integer.valueOf(0);
        for (; textElements.hasNext(); ) {
            final Element element = textElements.next();
            Base64.decodeToFile(element.getTextTrim(), this.getPdfFile(this.pdfSize).getAbsolutePath());
            this.pdfSize++;
        }
    }

    public List<File> getPdfFiles() {
        if (this.pdfSize == null) {
            return null;
        } else {
            final List<File> result = new ArrayList<>(this.pdfSize);
            for (int i = 0; i < this.pdfSize; i++) {
                result.add(this.getPdfFile(i));
            }
            return result;
        }
    }

    private File getPdfFile(final int i) {
        final String repository = ApplicationPreferenceManager.getStringValue("cda_repository");
        final String fileName = repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + CDAValidatedFile.CDA_PDF_FILE_PREFIX
                + this
                .getId() + '_' + i + CDAValidatedFile.CDA_PDF_FILE_SUFFIX;
        return new File(fileName);
    }

    /**
     * Returns a CDAValidatedFile object selected by its OID
     *
     * @param inOID OID of the CDA validated file
     * @return the CDA Validated file object
     */
    public static CDAValidatedFile getCDAValidatedFileByOid(final String inOID, final String privacyKey) {
        return ValidatedObject.getObjectByOID(CDAValidatedFile.class, inOID, privacyKey);
    }

    /**
     * Creates a new CDA Validated file in order to set the CDA file name to use
     * to record the file ON disk
     *
     * @return the newly created object with the cdaFilePath set
     */


    public static CDAValidatedFile createNewCDAValidatedFile() {
        CDAValidatedFile cdaFile = new CDAValidatedFile();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        cdaFile = em.merge(cdaFile);
        em.flush();
        if (cdaFile == null || cdaFile.getId() == 0) {
            CDAValidatedFile.LOGGER.error("An error occurred when creating a new CDAValidatedFile object");
            return null;
        }
        cdaFile.setCdaFilePath(buildFilePath(cdaFile, CDA_FILE_PREFIX));
        return cdaFile;
    }

    private static String buildFilePath(CDAValidatedFile cdaFile, String prefix){
        final String repository = ApplicationPreferenceManager.getStringValue("cda_repository");
        StringBuilder builder = new StringBuilder(repository);
        builder.append('/');
        builder.append(Util.buildFilePathAccordingToDateAndTime());
        builder.append('/');
        builder.append(prefix);
        builder.append(cdaFile.getId());
        builder.append(XML_FILE_SUFFIX);
        return builder.toString();
    }
    /**
     * adds or updates a CDAValidatedFile object
     *
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T extends AbstractValidatedXMLFile> T addOrUpdate(final Class<T> entityClass) {
        if (this.getCdaFilePath() == null || this.getCdaFilePath().isEmpty()) {
            CDAValidatedFile.LOGGER.error("The CDA file path is missing, cannot add this object !");
            return (T) this;
        } else {
            final File cdaFile = new File(this.cdaFilePath);
            cdaFile.getParentFile().mkdirs();

            if (!cdaFile.exists()) {
                CDAValidatedFile.LOGGER.error("The given file does not exist: {}", this.cdaFilePath);
                return (T) this;
            }
            if (this.getOid() == null || this.getOid().isEmpty()) {
                this.setOid(OIDGenerator.getNewOid());
            }
            if (this.getPdfFiles() == null) {
                try {
                    this.parseXmlForPdfs();
                } catch (final JDOMException e) {
                    CDAValidatedFile.LOGGER
                            .error("Failed to get PDFs, probably the document is not wellFormed (JDOMException). {}",
                                    e.getMessage());
                } catch (final IOException e) {
                    CDAValidatedFile.LOGGER
                            .error("Failed to get PDFs, probably the document is not present ON the server(IOException). {}",
                                    e.getMessage());
                }
            }

            if (scorecardStatus == null && (schematron != null || mbvalidator != null)){
                if (mbvalidator != null){
                    setScorecardStatus(ScorecardStatus.NEVER_COMPUTED);
                } else {
                    setScorecardStatus(ScorecardStatus.NOT_AVAILABLE);
                }
            }

            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            final CDAValidatedFile validatedFile = em.merge(this);
            em.flush();

            // Update the cache with the newest values
            GazelleValidationCacheManager
                    .refreshGazelleCacheWebService(validatedFile.getExternalId(), validatedFile.getToolOid(),
                            validatedFile.getOid(), "off");
            return (T) validatedFile;
        }
    }

    @Override
    public String getFilePath() {
        return this.getCdaFilePath();
    }

    @Override
    public void setFilePath(final String filePath) {
        this.cdaFilePath = filePath;
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/detailedResult.seam?type=CDA").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

    public int getLastIndex() {
        final EntityManager em = EntityManagerService.provideEntityManager();
        final Query q = em.createQuery("SELECT max(cda.id) FROM CDAValidatedFile cda");
        final int lastId;
        try {
            lastId = (Integer) q.getSingleResult();
            return lastId;
        } catch (final RuntimeException e) {
            CDAValidatedFile.LOGGER.error("Error when trying to retreive the last id");
        }
        return 0;
    }

    public void addObjectIndexer(final List<ObjectIndexer> objectIndexerList) {
        this.getObjectIndexers().addAll(objectIndexerList);
    }

    /***
     * Scorecard is stored along with the CDA file, with name scorecard_id.xml
     * @return
     */
    public String getScorecardPath(){
        if (this.getFilePath() != null){
            return this.getFilePath().replace(CDA_FILE_PREFIX, CDA_SCORECARD_FILE_PREFIX);
        } else {
            return buildFilePath(this, CDA_SCORECARD_FILE_PREFIX);
        }
    }

    public ScorecardStatus getScorecardStatus() {
        return scorecardStatus;
    }

    public void setScorecardStatus(ScorecardStatus scorecardStatus) {
        this.scorecardStatus = scorecardStatus;
    }
}
