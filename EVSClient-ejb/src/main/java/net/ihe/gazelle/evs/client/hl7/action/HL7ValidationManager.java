/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.hl7.action;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.action.EmailNotificationManager;
import net.ihe.gazelle.evs.client.common.action.GetValidationInfo;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.common.model.ValidationService;
import net.ihe.gazelle.evs.client.hl7.model.HL7ValidatedMessage;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.hl7.validator.client.HL7v2Validator;
import net.ihe.gazelle.hl7.validator.client.ValidationContextBuilder;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.tf.model.Hl7MessageProfile;
import net.ihe.gazelle.tf.model.Hl7MessageProfileQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <b>Class Description : </b>HL7MessageProfileValidationManager<br>
 * <br>
 * Manager Bean for displaying message profile retrieved from TF
 *
 * @author Anne-Gaelle Berge and Nicolas LEFEBVRE / INRIA Rennes IHE development Project
 * @version 2.0 - 2012, July 9th
 * @class HL7MessageProfileValidationManager.java
 * @package net.ihe.validationService.evs.client.hl7.action
 * @see > anne-gaelle@ihe-europe.net - http://www.ihe-europe.org
 */

@Name("hl7Validator")
@Scope(ScopeType.PAGE)
public class HL7ValidationManager implements QueryModifier<Hl7MessageProfile> {


    private static final Logger LOGGER = LoggerFactory.getLogger(HL7ValidationManager.class);

    private static final long serialVersionUID = -3826607970222174718L;

    private static final String GAZELLE = "HL7v2_GAZELLE";
    private static final int ALL = 1;
    private static final int GUESS = 2;

    private ValidationService validationService;
    private String hl7VersionFromMessage;
    private HL7ValidatedMessage validatedMessage = new HL7ValidatedMessage();
    private String messageTypeFromMessage;
    private Hl7MessageProfile selectedMessageProfile;
    private transient HL7v2Validator gazelleValidator;
    private Filter<Hl7MessageProfile> filter;
    private int filterMode = HL7ValidationManager.ALL;
    private String affinityDomain;

    private String messageType;
    private String hl7Version;
    private Integer profileId;
    private static final String ACCEPTED_FILE_TYPES = "hl7, er7, txt, LOGGER";
    private boolean validationDone;

    public HL7ValidationManager() {
    }

    public String getAcceptedFileTypes() {
        return HL7ValidationManager.ACCEPTED_FILE_TYPES;
    }

    public boolean getValidationDone() {

        return this.validationDone;
    }

    public void setValidationDone(final boolean value) {
        this.validationDone = value;
    }

    private void extractInfoFromMessage(final String message) {
        this.hl7VersionFromMessage = null;
        this.messageTypeFromMessage = null;
        if (message.startsWith("MSH")) {
            try {
                final PipeParser parser = new PipeParser();
                final Message hapiMessage = parser.parse(message);
                final Terser terser = new Terser(hapiMessage);
                final Segment msh = terser.getSegment("/.MSH");
                this.messageTypeFromMessage = msh.getField(9, 0).encode();
                this.hl7VersionFromMessage = terser.get("/.MSH-12-1");
                HL7ValidationManager.LOGGER.info("version:{}", this.hl7VersionFromMessage);
                HL7ValidationManager.LOGGER.info("message type:{}", this.messageTypeFromMessage);
            } catch (final HL7Exception e) {
                HL7ValidationManager.LOGGER.error(e.getMessage());
            }
        }
    }

    public Hl7MessageProfile getSelectedMessageProfile() {
        return this.selectedMessageProfile;
    }

    public String profileLink(final Hl7MessageProfile profile) {
        if (profile != null) {
            final String gazelleHL7v2ValidatorUrl = PreferenceService.getString("gazelle_hl7v2_validator_url");
            return gazelleHL7v2ValidatorUrl.concat("/viewProfile.seam?oid=").concat(profile.getProfileOid());
        } else {
            return null;
        }
    }

    public void init() {
        final Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        this.setValidationDone(false);
        if (params.containsKey(GetValidationInfo.OID)) {
            final HL7ValidatedMessage messageToValidateAgain = HL7ValidatedMessage
                    .getMessageByOID(params.get(GetValidationInfo.OID), params.get("privacyKey"));
            final String tmp = messageToValidateAgain.getValidatedMessage();
            this.validatedMessage.setValidatedMessage(tmp);
        } else if (params.containsKey("id")) {
            this.validatedMessage = ValidatedObject
                    .getObjectByID(HL7ValidatedMessage.class, Integer.valueOf(params.get("id")), null);
        } else if (this.validatedMessage == null) {
            this.validatedMessage = new HL7ValidatedMessage();
        }
        if (params.containsKey("extension")) {
            this.affinityDomain = params.get("extension");
            this.resetFilter();
        }
        this.validationService = ValidationService.getValidationServiceByKeyword(HL7ValidationManager.GAZELLE);

        if (this.validatedMessage != null && this.validatedMessage.isLucky()) {
            this.guessProfile(true);
            if (this.getMessageProfiles().size() == 1) {
                Hl7MessageProfile messageProfile;
                messageProfile = this.getMessageProfiles().getAllItems(FacesContext.getCurrentInstance())
                        .get(0);
                this.validateMessage(messageProfile);
            }
        }
    }

    private void resetFilter() {
        this.filter = null;
    }

    public void reset() {
        this.validatedMessage = new HL7ValidatedMessage();
        this.selectedMessageProfile = null;
        this.filterMode = HL7ValidationManager.ALL;
        if (this.filter != null) {
            this.filter.clear();
        }
    }

    public void guessProfile(final boolean guess) {
        if (this.filter != null) {
            this.filter.clear();
        }
        this.selectedMessageProfile = null;
        if (guess && this.validatedMessage != null && this.validatedMessage.getValidatedMessage() != null) {
            this.extractInfoFromMessage(this.validatedMessage.getValidatedMessage());
            this.filterMode = HL7ValidationManager.GUESS;
        } else {
            this.filterMode = HL7ValidationManager.ALL;
            this.hl7VersionFromMessage = null;
            this.messageTypeFromMessage = null;
        }
    }

    public String revalidate(final String messageOid) {
        final StringBuilder url = new StringBuilder("/hl7v2/validator.seam?oid=");
        url.append(messageOid).append("&extension=").append(this.affinityDomain);
        return url.toString();
    }

    public void uploadMessageFile(final FileUploadEvent event) {
        final UploadedFile messageFile = event.getUploadedFile();
        String messageContent;
        messageContent = new String(messageFile.getData(), StandardCharsets.UTF_8);
        this.validatedMessage.setDescription(messageFile.getName());
        this.validatedMessage.setValidatedMessage(messageContent);
        HL7ValidationManager.LOGGER.debug("messageContent{}", messageContent);
    }

    public void validateMessage(final Hl7MessageProfile messageProfile) {
        this.selectedMessageProfile = messageProfile;
        if (this.selectedMessageProfile != null){
            HL7ValidationManager.LOGGER.info("selected profile: {}", this.selectedMessageProfile.getProfileOid());
            if (this.validatedMessage.getValidatedMessage() != null
                    && !this.validatedMessage.getValidatedMessage().isEmpty()) {
                final List<ReferencedStandard> standards = ReferencedStandard
                        .getReferencedStandardFiltered("HL7v2", null, this.affinityDomain, null);
                if (standards != null) {
                    this.validatedMessage.setStandard(standards.get(0));
                }
                this.validatedMessage.setProfileOid(this.selectedMessageProfile.getProfileOid());
                this.validatedMessage.setService(this.validationService);
                if (this.gazelleValidator == null) {
                    this.gazelleValidator = new HL7v2Validator(this.validationService.getWsdl(),
                            this.retrieveFaultLevels());
                }
                final String result = this.gazelleValidator
                        .validate(this.validatedMessage.getValidatedMessage(), this.validatedMessage.getProfileOid(), "ER7");
                this.validatedMessage.setDetailedResult(result);
                this.validatedMessage.setValidationStatus(this.gazelleValidator.getLastValidationStatus());
                this.validatedMessage.setValidationDate(new Date());
                this.validatedMessage.setUserIp(Util.getUserIpAddress());
                this.validatedMessage.updateCountryStatistics();
                if (this.validatedMessage.getValidationStatus() != null && "ABORTED"
                        .equals(this.validatedMessage.getValidationStatus())) {
                    EmailNotificationManager.send(null, "HL7v2.x Validation using HAPI has been aborted",
                            "Processed message is " + this.validatedMessage.permanentLink());
                }
                this.validatedMessage = HL7ValidatedMessage.storeHL7Message(this.validatedMessage);
                this.setValidationDone(true);
            }
        }
        else {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "net.ihe.gazelle.evs.NoHL7MessageNoProfileErrorMessage");
            HL7ValidationManager.LOGGER.debug("No profile has been selected");
        }
    }

    public FilterDataModel<Hl7MessageProfile> getMessageProfiles() {
        this.messageType = null;
        this.hl7Version = null;
        this.profileId = null;

        if (this.selectedMessageProfile != null) {
            this.profileId = this.selectedMessageProfile.getId();
        } else if (this.filterMode == HL7ValidationManager.GUESS) {
            this.messageType = this.messageTypeFromMessage;
            this.hl7Version = this.hl7VersionFromMessage;
            FilterDataModel<Hl7MessageProfile> profiles = new FilterDataModel<Hl7MessageProfile>(
                    HL7ValidationManager.this.getFilter()) {
                @Override
                protected Object getId(final Hl7MessageProfile t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
            if (profiles.getRowCount() > 0) {
                return profiles;
            } else {
                this.filterMode = HL7ValidationManager.ALL;
            }
        }
        return new FilterDataModel<Hl7MessageProfile>(HL7ValidationManager.this.getFilter()) {
            @Override
            protected Object getId(final Hl7MessageProfile t) {
                // TODO Auto-generated method stub
                return t.getId();
            }
        };
    }

    public HL7ValidatedMessage getValidatedMessage() {
        return this.validatedMessage;
    }

    public void setFilter(final Filter<Hl7MessageProfile> filter) {
        this.filter = filter;
    }

    public Filter<Hl7MessageProfile> getFilter() {
        if (this.filter == null) {
            this.filter = new Filter<>(this.getCriterions());
        }
        return this.filter;
    }

    private HQLCriterionsForFilter<Hl7MessageProfile> getCriterions() {
        final Hl7MessageProfileQuery query = new Hl7MessageProfileQuery();
        final HQLCriterionsForFilter<Hl7MessageProfile> result = query.getHQLCriterionsForFilter();

        result.addPath("transaction", query.transaction());
        result.addPath("domain", query.domain());
        result.addPath("hl7Version", query.hl7Version());
        result.addPath("messageType", query.messageType());
        result.addPath("actor", query.actor());
        result.addPath("affinityDomain", query.affinityDomains().labelToDisplay(), null, this.affinityDomain);
        result.addQueryModifier(this);
        return result;
    }

    public void setValidatedMessage(final HL7ValidatedMessage validatedMessage) {
        this.validatedMessage = validatedMessage;
    }

    public void setFilterMode(final int filterMode) {
        this.filterMode = filterMode;
    }

    public int getFilterMode() {
        return this.filterMode;
    }

    public int getALL() {
        return HL7ValidationManager.ALL;
    }

    public int getGUESS() {
        return HL7ValidationManager.GUESS;
    }

    @Override
    public void modifyQuery(final HQLQueryBuilder<Hl7MessageProfile> queryBuilder, final Map<String, Object> filterValuesApplied) {
        final Hl7MessageProfileQuery hl7MessageProfileQuery = new Hl7MessageProfileQuery(queryBuilder);
        if (this.messageType != null && !this.messageType.isEmpty()) {
            hl7MessageProfileQuery.messageType().like(this.messageType, HQLRestrictionLikeMatchMode.ANYWHERE);
        }
        if (this.hl7Version != null && !this.hl7Version.isEmpty()) {
            hl7MessageProfileQuery.hl7Version().eq(this.hl7Version);
        }
        if (this.profileId != null) {
            hl7MessageProfileQuery.id().eq(this.profileId);
        }
    }

    private Map<String, ValidationContextBuilder.FaultLevel> retrieveFaultLevels() {
        final Map<String, ValidationContextBuilder.FaultLevel> validationOptions = new HashMap<>();
        validationOptions.put(ValidationContextBuilder.LENGTH,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_length_fault_level")));
        validationOptions.put(ValidationContextBuilder.MESSAGE_STRUCTURE,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_structure_fault_level")));
        validationOptions.put(ValidationContextBuilder.DATATYPE,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_datatype_fault_level")));
        validationOptions.put(ValidationContextBuilder.DATAVALUE,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_value_fault_level")));
        return validationOptions;
    }

    public void setMessageInvalidFileType(){
        FacesMessages.instance().add(Severity.ERROR,"Invalid file type. Expected file types are : "+ this
                .getAcceptedFileTypes());
    }
}
