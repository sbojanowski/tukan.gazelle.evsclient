/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file.xml;

import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.analyzer.utils.XmlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XDS extends Analyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(XDS.class);

    @Override
    public Logger getLog() {
        return XDS.LOGGER;
    }

    @Override
    public String getType() {
        return "XDS";
    }

    public static boolean containsXDS(final File file, final MessageContentAnalyzer mca) {
        final String fileContent = AnalyzerFileUtils.convertFileToString(file);
        return XDS.containsXDS(fileContent, mca);
    }

    private static final List<String> acceptedTagsXDS = new ArrayList<>();

    // Definie all accepted tag for XD*
    public static List<String> getXDSAcceptedTags() {
        XDS.acceptedTagsXDS.add("ProvideAndRegisterDocumentSetRequest");
        XDS.acceptedTagsXDS.add("RetrieveDocumentSetRequest");
        XDS.acceptedTagsXDS.add("RetrieveDocumentSetResponse");
        XDS.acceptedTagsXDS.add("RetrieveImagingDocumentSetRequest");
        XDS.acceptedTagsXDS.add("CatalogContentRequest");
        XDS.acceptedTagsXDS.add("CatalogContentResponse");
        XDS.acceptedTagsXDS.add("ValidateContentRequest");
        XDS.acceptedTagsXDS.add("ValidateContentResponse");
        XDS.acceptedTagsXDS.add("AcceptObjectsRequest");
        XDS.acceptedTagsXDS.add("ApproveObjectsRequest");
        XDS.acceptedTagsXDS.add("DeprecateObjectsRequest");
        XDS.acceptedTagsXDS.add("RelocateObjectsRequest");
        XDS.acceptedTagsXDS.add("RemoveObjectsRequest");
        XDS.acceptedTagsXDS.add("SubmitObjectsRequest");
        XDS.acceptedTagsXDS.add("UndeprecateObjectsRequest");
        XDS.acceptedTagsXDS.add("UpdateObjectsRequest");
        XDS.acceptedTagsXDS.add("AdhocQueryQuery");
        XDS.acceptedTagsXDS.add("AdhocQueryRequest");
        XDS.acceptedTagsXDS.add("AdhocQueryResponse");
        XDS.acceptedTagsXDS.add("AssociationQuery");
        XDS.acceptedTagsXDS.add("AuditableEventQuery");
        XDS.acceptedTagsXDS.add("ClassificationNodeQuery");
        XDS.acceptedTagsXDS.add("ClassificationQuery");
        XDS.acceptedTagsXDS.add("ClassificationSchemeQuery");
        XDS.acceptedTagsXDS.add("ExternalIdentifierQuery");
        XDS.acceptedTagsXDS.add("ExternalLinkQuery");
        XDS.acceptedTagsXDS.add("ExtrinsicObjectQuery");
        XDS.acceptedTagsXDS.add("FederationQuery");
        XDS.acceptedTagsXDS.add("NotificationQuery");
        XDS.acceptedTagsXDS.add("OrganizationQuery");
        XDS.acceptedTagsXDS.add("PersonQuery");
        XDS.acceptedTagsXDS.add("PersonQuery");
        XDS.acceptedTagsXDS.add("RegistryPackageQuery");
        XDS.acceptedTagsXDS.add("RegistryQuery");
        XDS.acceptedTagsXDS.add("ServiceBindingQuery");
        XDS.acceptedTagsXDS.add("ServiceQuery");
        XDS.acceptedTagsXDS.add("SpecificationLinkQuery");
        XDS.acceptedTagsXDS.add("SubscriptionQuery");
        XDS.acceptedTagsXDS.add("UserQuery");
        XDS.acceptedTagsXDS.add("AdhocQuery");
        XDS.acceptedTagsXDS.add("QueryExpression");
        XDS.acceptedTagsXDS.add("RegistryRequest");
        XDS.acceptedTagsXDS.add("RegistryResponse");

        return XDS.acceptedTagsXDS;
    }

    public static boolean containsXDS(final String fileContent, final MessageContentAnalyzer mca) {
        final List<String> acceptedXDSTags = XDS.getXDSAcceptedTags();

        for (int i = 0; i < acceptedXDSTags.size(); i++) {
            final String goodTag = acceptedXDSTags.get(i);
            if (fileContent.contains(goodTag)) {
                XDS.LOGGER.info(goodTag);
                // Verify if it's just one line
                String xdsReg = "<([^<]+)?" + goodTag + "([\\r|\\n| |>])+.+?>";
                XDS.LOGGER.info(xdsReg);

                Pattern regex = Pattern.compile(xdsReg, Pattern.DOTALL);
                Matcher regexMatcher = regex.matcher(fileContent);
                if (regexMatcher.find()) {
                    XDS.LOGGER.info(regexMatcher.group());
                    if ("/".equals(fileContent.substring(regexMatcher.end() - 2, regexMatcher.end() - 1))) {
                        if (XmlUtils.testValidSchemes(regexMatcher.group(1), regexMatcher.group(1))) {
                            xdsReg = "<([^<]+)?" + goodTag + "([\\r|\\n| |>])+.+?(/>)";
                            XDS.LOGGER.info(xdsReg);

                            regex = Pattern.compile(xdsReg, Pattern.DOTALL);
                            regexMatcher = regex.matcher(fileContent);
                            if (regexMatcher.find()) {
                                XDS.LOGGER.info(regexMatcher.group());
                                mca.setOffsetStart(regexMatcher.start());
                                mca.setOffsetEnd(regexMatcher.end());
                                return true;
                            }
                        }
                    } else {
                        if (XmlUtils.testValidSchemes(regexMatcher.group(1), regexMatcher.group(1))) {
                            String scheme = "";
                            if (regexMatcher.group(1) != null) {
                                scheme = regexMatcher.group(1);
                            }
                            xdsReg = '<' + scheme + goodTag + "([\\r|\\n| |>])+.+?(" + scheme + goodTag + ">)";
                            XDS.LOGGER.info(xdsReg);

                            regex = Pattern.compile(xdsReg, Pattern.DOTALL);
                            regexMatcher = regex.matcher(fileContent);
                            if (regexMatcher.find()) {
                                XDS.LOGGER.info(regexMatcher.group());
                                mca.setOffsetStart(regexMatcher.start());
                                mca.setOffsetEnd(regexMatcher.end());
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
}
