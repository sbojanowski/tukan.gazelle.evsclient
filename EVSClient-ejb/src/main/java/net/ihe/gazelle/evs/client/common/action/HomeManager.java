/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.evs.client.common.model.Home;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;

@Name("homeManagerBean")
@Scope(ScopeType.PAGE)
public class HomeManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1111092307583013570L;

    private Home selectedHome;
    private boolean mainContentEditMode;

    public HomeManager() {
    }

    public Home getSelectedHome() {
        return this.selectedHome;
    }

    public void setSelectedHome(final Home selectedHome) {
        this.selectedHome = selectedHome;
    }

    public boolean isMainContentEditMode() {
        return this.mainContentEditMode;
    }

    public void setMainContentEditMode(final boolean mainContentEditMode) {
        Home.getHomeForSelectedLanguage();
        this.mainContentEditMode = mainContentEditMode;
    }

    public void initializeHome() {
        this.selectedHome = Home.getHomeForSelectedLanguage();
    }

    public void editMainContent() {
        this.mainContentEditMode = true;
    }

    public void save() {
        this.selectedHome = this.selectedHome.save();
        this.mainContentEditMode = false;
    }

    public void cancel() {
        this.mainContentEditMode = false;
    }

}
