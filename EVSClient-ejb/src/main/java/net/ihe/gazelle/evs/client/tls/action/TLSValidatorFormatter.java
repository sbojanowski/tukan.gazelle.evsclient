/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.tls.action;

import net.ihe.gazelle.atna.action.pki.ws.CertificateValidatorError;
import net.ihe.gazelle.atna.action.pki.ws.CertificateValidatorErrorTrace;
import net.ihe.gazelle.atna.action.pki.ws.CertificateValidatorResult;
import net.ihe.gazelle.evs.client.common.model.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import javax.xml.XMLConstants;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TLSValidatorFormatter {

    private static final Logger LOGGER = LoggerFactory.getLogger(TLSValidatorFormatter.class);

    private static final String ROOT_ELEMENT = "detailedResult";
    private static final String OVERVIEW_ROOT = "ValidationResultsOverview";
    private static final String DETAILS_ROOT = "CertificatesValidation";

    private TLSValidatorFormatter() {

    }

    public static String getResultAsXml(final CertificateValidatorResult validationResult, final Date validationDate,
            final ValidationService service, final String validator, final boolean checkRevocation) {
        final AttributesImpl emptyAtt = new AttributesImpl();
        try {
            // initiate the transformer
            final SAXTransformerFactory factory = (SAXTransformerFactory) TransformerFactory.newInstance();

            final TransformerHandler handler = factory.newTransformerHandler();
            handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
            // transformation output will be stored in result
            final StreamResult result = new StreamResult(new ByteArrayOutputStream());
            handler.setResult(result);
            // start XML
            handler.startDocument();
            handler.startElement(XMLConstants.NULL_NS_URI, TLSValidatorFormatter.ROOT_ELEMENT, TLSValidatorFormatter.ROOT_ELEMENT, emptyAtt);
            // ValidationResultOverview
            final SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy, MM dd");
            final SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
            handler.startElement(XMLConstants.NULL_NS_URI, TLSValidatorFormatter.OVERVIEW_ROOT, TLSValidatorFormatter.OVERVIEW_ROOT, emptyAtt);
            TLSValidatorFormatter.addElement(handler, "ValidationDate", sdfDate.format(validationDate));
            TLSValidatorFormatter.addElement(handler, "ValidationTime", sdfTime.format(validationDate));
            TLSValidatorFormatter.addElement(handler, "ValidationServiceName", service.getName());
            TLSValidatorFormatter.addElement(handler, "ValidationMethod", validator);
            TLSValidatorFormatter.addElement(handler, "CheckRevocation", Boolean.toString(checkRevocation));
            TLSValidatorFormatter.addElement(handler, "ValidationTestResult", validationResult.getResult().toString());
            handler.endElement(XMLConstants.NULL_NS_URI, TLSValidatorFormatter.OVERVIEW_ROOT, TLSValidatorFormatter.OVERVIEW_ROOT);
            // end ValidationResultOverview

            // CertificatesValidation
            handler.startElement(XMLConstants.NULL_NS_URI, TLSValidatorFormatter.DETAILS_ROOT, TLSValidatorFormatter.DETAILS_ROOT, emptyAtt);
            final CertificateValidatorError[] errors = validationResult.getErrors();
            if (errors != null && errors.length > 0) {
                for (final CertificateValidatorError error : errors) {
                    handler.startElement(XMLConstants.NULL_NS_URI, "Error", "Error", emptyAtt);
                    if (error.getNormMessage() != null) {
                        TLSValidatorFormatter.addElement(handler, "Test", error.getNormMessage());
                    }
                    if (error.getExceptionMessage() != null) {
                        TLSValidatorFormatter.addElement(handler, "Description", error.getExceptionMessage());
                    }
                    handler.endElement(XMLConstants.NULL_NS_URI, "Error", "Error");
                }
            }
            final CertificateValidatorError[] warnings = validationResult.getWarnings();
            if (warnings != null && warnings.length > 0) {
                for (final CertificateValidatorError warning : warnings) {
                    handler.startElement(XMLConstants.NULL_NS_URI, "Warning", "Warning", emptyAtt);
                    if (warning.getNormMessage() != null) {
                        TLSValidatorFormatter.addElement(handler, "Test", warning.getNormMessage());
                    }
                    if (warning.getExceptionMessage() != null) {
                        TLSValidatorFormatter.addElement(handler, "Description", warning.getExceptionMessage());
                    }
                    handler.endElement(XMLConstants.NULL_NS_URI, "Warning", "Warning");
                }
            }
            handler.endElement(XMLConstants.NULL_NS_URI, TLSValidatorFormatter.DETAILS_ROOT, TLSValidatorFormatter.DETAILS_ROOT);
            // end CertificatesValidation

            // end XML
            handler.endElement(XMLConstants.NULL_NS_URI, TLSValidatorFormatter.ROOT_ELEMENT, TLSValidatorFormatter.ROOT_ELEMENT);
            handler.endDocument();

            final ByteArrayOutputStream baos = (ByteArrayOutputStream) result.getOutputStream();
            return new String(baos.toByteArray(), StandardCharsets.UTF_8);
        } catch (TransformerConfigurationException|SAXException e) {
            LOGGER.error(e.getMessage(),e);
            return null;
        }
    }

    public static String getResultAsText(final CertificateValidatorResult validationResult) {
        final StringBuilder builder = new StringBuilder();
        builder.append("validation status: ").append(validationResult.getResult().getValue()).append("<br/>");
        if (validationResult.getErrors() != null) {
            for (final CertificateValidatorErrorTrace error : validationResult.getErrors()) {
                builder.append("Error: ").append(error.getExceptionMessage()).append("<br/>").append("------<br/>");
            }
        }
        if (validationResult.getWarnings() != null) {
            for (final CertificateValidatorErrorTrace warning : validationResult.getWarnings()) {
                builder.append("Warning: ").append(warning.getExceptionMessage()).append("<br/>").append("------<br/>");
            }
        }
        return builder.toString();
    }

    public static void addElement(final TransformerHandler handler, final String qName, final String value) throws SAXException {
        handler.startElement(XMLConstants.NULL_NS_URI, qName, qName, new AttributesImpl());
        handler.characters(value.toCharArray(), 0, value.length());
        handler.endElement(XMLConstants.NULL_NS_URI, qName, qName);
    }
}
