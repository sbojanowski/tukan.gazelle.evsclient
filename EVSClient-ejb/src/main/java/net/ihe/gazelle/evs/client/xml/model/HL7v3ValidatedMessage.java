/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.util.Util;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;

/**
 * <b>Class Description :  </b>HL7v3ValidatedMessage<br><br>
 * This class describes the HL7v3ValidatedMessage which represents the HL7V3 files uploaded by the user and validated by the selected service
 * <p/>
 * HL7v3ValidatedMessage possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id of the message in database</li>
 * <li><b>messagePath</b> : Path to the file which has been uploaded and stored ON disk -location is given by an application preference-</li>
 * <li><b>description</b> : This attribute is used to store the name of the uploaded file</li>
 * <li><b>schematron</b> : the schematron that has been used by the validator</li>
 * <li><b>detailedResults</b> : detailed results of the validation (error, warnings ...)</li>
 * <li><b>metadata</b> : message metadata (method, language ...)</li>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, July 15th
 * @class HL7v3ValidatedMessage.java
 * @package net.ihe.gazelle.evs.client.hl7v3.model
 * @see Aberge@irisa.fr -  http://www.ihe-europe.org
 */

@Entity
@Name("hl7v3ValidatedMessage")
@Table(name = "hl7v3_validated_message", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "hl7v3_validated_message_sequence", sequenceName = "hl7v3_validated_message_id_seq", allocationSize = 1)
public class HL7v3ValidatedMessage extends AbstractValidatedXMLFile {

    /**
     *
     */
    private static final long serialVersionUID = 475978417841282758L;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(HL7v3ValidatedMessage.class);

    private static final String HL7V3_FILE_PREFIX = "validatedHL7v3_";
    private static final String HL7V3_FILE_SUFFIX = ".xml";

    @Id
    @NotNull
    @GeneratedValue(generator = "hl7v3_validated_message_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "message_file_path")
    private String messagePath;

    @Column(name = "schematron")
    private String schematron;



    public HL7v3ValidatedMessage(final HL7v3ValidatedMessage selectedHL7v3Message) {
        this.messagePath = selectedHL7v3Message.getMessagePath();
        this.description = selectedHL7v3Message.getDescription();
    }

    public HL7v3ValidatedMessage() {

    }

    /**
     * Getters and Setters
     */

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getMessagePath() {
        return this.messagePath;
    }

    public void setMessagePath(final String messagePath) {
        this.messagePath = messagePath;
    }

    @Override
    public String getSchematron() {
        return this.schematron;
    }

    @Override
    public void setSchematron(final String schematron) {
        this.schematron = schematron;
    }

    /**
     * Creates a new HL7V3 Validated object in order to set the file name to use to record the file ON disk
     *
     * @return the newly created object with the messagePath set
     */
    public static HL7v3ValidatedMessage createNewHL7v3ValidatedMessage() {
        HL7v3ValidatedMessage message = new HL7v3ValidatedMessage();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        message = em.merge(message);
        em.flush();
        if (message == null || message.getId() == 0) {
            HL7v3ValidatedMessage.LOGGER.error("An error occurred when creating a new HL7v2ValidatedMessage object");
            return null;
        }
        final String repository = ApplicationPreferenceManager.getStringValue("hl7v3_repository");
        message.setMessagePath(
                repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + HL7v3ValidatedMessage.HL7V3_FILE_PREFIX
                        .concat(message.getId().toString()).concat(
                        HL7v3ValidatedMessage.HL7V3_FILE_SUFFIX));
        return message;
    }

    /**
     * adds or updates a HL7v3ValidatedMessage object
     *
     * @return
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends AbstractValidatedXMLFile> T addOrUpdate(final Class<T> clazz) {
        if (this.getMessagePath() == null || this.getMessagePath().isEmpty()) {
            HL7v3ValidatedMessage.LOGGER.error("The message path is missing, cannot add this object !");
            return (T) this;
        } else {
            final File hl7v3File = new File(this.getMessagePath());
            hl7v3File.getParentFile().mkdirs();

            if (!hl7v3File.exists()) {
                HL7v3ValidatedMessage.LOGGER.error("The given file does not exist: {}", this.getMessagePath());
                return (T) this;
            }
            if (this.getOid() == null || this.getOid().isEmpty()) {
                this.setOid(OIDGenerator.getNewOid());
            }
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            final HL7v3ValidatedMessage validatedMessage = em.merge(this);
            em.flush();

            //Update the cache with the newest values
            GazelleValidationCacheManager
                    .refreshGazelleCacheWebService(validatedMessage.getExternalId(), validatedMessage.getToolOid(),
                            validatedMessage.getOid(), "off");

            return (T) validatedMessage;
        }
    }

    @Override
    public String getFilePath() {
        return this.messagePath;
    }

    @Override
    public void setFilePath(final String filePath) {
        this.messagePath = filePath;
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/detailedResult.seam?type=HL7v3").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }
}
