/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.util.DateDisplayUtil;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.common.model.ValidatedObjectQuery;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.contexts.Lifecycle;

public class GazelleValidationCacheManager {

    public static String getValidationData(final String oid, final String externalId, final String toolOid, final String queryCache, final String objectToQuery) {
        String objectId;
        String objectType;
        if (oid == null && externalId == null){
            return null;
        }
        if (oid == null) {
            objectId = externalId;
            objectType = toolOid;
        } else {
            objectId = oid;
            objectType = "oid";
        }
        switch (objectToQuery){
        case "EVSClient:GetValidationDate" :

            return GazelleCacheManager.queryWithCache(new GazelleCacheRequest() {
                @Override
                public String get(final String id, final String objectType) {
                    return GazelleValidationCacheManager.getValidationDateFromDb(id, objectType);
                }
            }, objectId, objectType, objectToQuery, queryCache);
        case "EVSClient:GetValidationStatusAndPermanentLink" :
            return GazelleCacheManager.queryWithCache(new GazelleCacheRequest() {
                @Override
                public String get(final String id, final String objectType) {
                    return GazelleValidationCacheManager.getValidationStatusAndPermanentLinkFromDb(id, objectType);
                }
            }, objectId, objectType, objectToQuery, queryCache);
        default : //"EVSClient:GetValidationPermanentLink" :

            return GazelleCacheManager.queryWithCache(new GazelleCacheRequest() {
                @Override
                public String get(final String id, final String objectType) {
                    return GazelleValidationCacheManager.getValidationPermanentLinkFromDb(id, objectType);
                }
            }, objectId, objectType, objectToQuery, queryCache);


        }

    }



    public static String getValidationDateService(final String oid, final String externalId, final String toolOid, final String queryCache) {



        return GazelleValidationCacheManager
                .getValidationData( oid,  externalId,  toolOid,  queryCache,  "EVSClient:GetValidationDate");
    }

    public static String getValidationPermanentLinkService(final String oid, final String externalId, final String toolOid,
            final String queryCache) {
        return GazelleValidationCacheManager
                .getValidationData( oid,  externalId,  toolOid,  queryCache,  "EVSClient:GetValidationPermanentLink");
    }

    public static String getValidationStatusService(final String oid, final String externalId, final String toolOid, final String queryCache) {
        String objectId;
        String objectType;
        if (oid == null && externalId == null){
            return null;
        }
        if (oid == null) {
            objectId = externalId;
            objectType = toolOid;
        } else {
            objectId = oid;
            objectType = "oid";
        }
        return GazelleValidationCacheManager.getValidationStatusFromdb(objectId, objectType);
    }

    public static String getValidationStatusAndPermanentLinkService(final String oid, final String externalId, final String toolOid, final String queryCache) {
        return GazelleValidationCacheManager.getValidationData( oid,  externalId,  toolOid,  queryCache,  "EVSClient:GetValidationStatusAndPermanentLink");
    }

    public static boolean refreshGazelleCacheWebService(final String externalId, final String toolOid, final String oid,
            final String queryCache) {
        getValidationDateService(oid, externalId, toolOid, queryCache);
        getValidationStatusService(oid, externalId, toolOid, queryCache);
        getValidationPermanentLinkService(oid, externalId, toolOid, queryCache);
        return true;
    }

    private static ValidatedObject getObject(final String id, final String objectType) {
        final boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
        if (createContexts) {
            Lifecycle.beginCall();
        }
        ValidatedObject vo;
        final ValidatedObjectQuery query = new ValidatedObjectQuery();
        if ("oid".equals(objectType)) {
            query.oid().eq(id);
        } else {
            query.externalId().eq(id);
            query.toolOid().eq(objectType);
            query.validationDate().isNotNull();
            // we might have several entries with the same externalID and we want to retrieve the latest
            query.validationDate().order(false);
        }
        vo = query.getUniqueResult();
        if (createContexts) {
            Lifecycle.endCall();
        }
        return vo;
    }

    private static String getValidationDateFromDb(final String id, final String objectType) {
        final boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
        if (createContexts) {
            Lifecycle.beginCall();
        }
        final ValidatedObject validatedObject = GazelleValidationCacheManager.getObject(id, objectType);
        String result = null;
        if (validatedObject != null) {
            result = DateDisplayUtil.displayDateTime(validatedObject.getValidationDate());
        }
        if (createContexts) {
            Lifecycle.endCall();
        }
        return result;
    }

    private static String getValidationPermanentLinkFromDb(final String id, final String objectType) {
        final boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
        if (createContexts) {
            Lifecycle.beginCall();
        }
        final ValidatedObject validatedObject = GazelleValidationCacheManager.getObject(id, objectType);
        String url = null;
        if (validatedObject != null) {
            final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
            final StringBuilder builder = new StringBuilder(applicationUrl);
            final String result = validatedObject.getBeginPermanentLink();
            builder.append(result);
            url = builder.toString();
        }
        if (createContexts) {
            Lifecycle.endCall();
        }
        return url;
    }

    private static String getValidationStatusFromdb(final String id, final String objectType) {
        final String result;
        final boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
        if (createContexts) {
            Lifecycle.beginCall();
        }
        final ValidatedObject validatedObject = GazelleValidationCacheManager.getObject(id, objectType);
        if (validatedObject != null && validatedObject.getValidationStatus() != null) {
            result = validatedObject.getValidationStatus();
        } else {
            result = null;
        }
        if (createContexts) {
            Lifecycle.endCall();
        }
        return result;
    }

    private static String getValidationStatusAndPermanentLinkFromDb(String id, String objectType) {
        final StringBuilder status = new StringBuilder("STATUS: ");
        final boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
        if (createContexts) {
            Lifecycle.beginCall();
        }
        final ValidatedObject validatedObject = GazelleValidationCacheManager.getObject(id, objectType);
        if (validatedObject != null && validatedObject.getValidationStatus() != null) {
            status.append(validatedObject.getValidationStatus());
        }

        final StringBuilder url = new StringBuilder("PERMLINK: ");
        if (validatedObject != null) {
            final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
            final StringBuilder builder = new StringBuilder(applicationUrl);
            final String permanentLink = validatedObject.getBeginPermanentLink();
            builder.append(permanentLink);
            url.append(builder.toString());
        }
        if (createContexts) {
            Lifecycle.endCall();
        }
        return new StringBuilder(status).append("\n").append(url).toString();
    }
}
