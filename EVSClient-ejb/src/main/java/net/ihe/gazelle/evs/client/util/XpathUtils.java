/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.util;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class XpathUtils {

    public static String evaluateByString(final String string, final String nodeName, final String expression) throws Exception {
        final Node clin = XpathUtils.getNodeFromString(string, nodeName);
        return XpathUtils.evaluateByNode(clin, expression);
    }

    public static String evaluateByNode(final Node node, final String expression) throws Exception {
        String b;
        final XPathFactory fabrique = XPathFactory.newInstance();
        final XPath xpath = fabrique.newXPath();
        xpath.setNamespaceContext(new DatatypesNamespaceContext());
        b = (String) xpath.evaluate(expression, node, XPathConstants.STRING);
        return b;
    }

    private static Node getNodeFromString(final String string, final String nodeName) throws Exception {
        final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);

        final DocumentBuilder db = dbf.newDocumentBuilder();
        final Document doc = db.parse(new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8)));
        final NodeList dd = doc.getElementsByTagName(nodeName);
        return dd.item(0);
    }

    public static List<String> evaluatesByString(final String string, final String nodeName, final String expression)
            throws IOException, SAXException, ParserConfigurationException {
        final NodeList dd = XpathUtils.getNodesFromString(string, nodeName);
        final List<String> results = new ArrayList<>();
        for (int i = 0; i < dd.getLength(); i++) {
            results.add(((Element) dd.item(i)).getAttribute("root"));
        }
        return results;
    }

    private static NodeList getNodesFromString(final String string, final String nodeName)
            throws ParserConfigurationException, IOException, SAXException {
        final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);

        final DocumentBuilder db = dbf.newDocumentBuilder();
        final Document doc = db.parse(new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8)));
        final NodeList dd = doc.getElementsByTagName(nodeName);
        return dd;
    }

}
