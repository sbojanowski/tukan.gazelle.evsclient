/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.evs.client.common.model.StatisticsPerCountry;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("worldMap")
@Scope(ScopeType.PAGE)
public class WorldMap implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4403023500692873463L;

    public List<StatisticsPerCountry> getStatisticsPerCountry() {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        final HQLQueryBuilder<StatisticsPerCountry> builder = new HQLQueryBuilder<>(entityManager,
                StatisticsPerCountry.class);
        builder.addRestriction(HQLRestrictions.neq("country", "Reserved"));
        return builder.getList();
    }
}
