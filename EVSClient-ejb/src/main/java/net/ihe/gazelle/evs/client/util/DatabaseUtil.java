/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.util;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.jboss.seam.core.ResourceBundle;

import javax.faces.model.SelectItem;
import java.util.*;

public class DatabaseUtil {

    public enum RestrictionType {
        ILIKE,
        IN,
        EQ,
        GE,
        LE,
        LIKE,
        BETWEEN
    }

    public enum When {
        AFTER, ON, BEFORE
    }

    public static List<SelectItem> getWhenValuesAsSelectItems() {
        final List<SelectItem> items = new ArrayList<>();
        items.add(new SelectItem(DatabaseUtil.When.BEFORE, ResourceBundle.instance().getString("gazelle.evs.client.BEFORE")));
        items.add(new SelectItem(DatabaseUtil.When.ON, ResourceBundle.instance().getString("gazelle.evs.client.on")));
        items.add(new SelectItem(DatabaseUtil.When.AFTER, ResourceBundle.instance().getString("gazelle.evs.client.AFTER")));
        return items;
    }


    public static Criterion addCriterion(final Criterion criterion, final DatabaseUtil.RestrictionType type, final String attribute, final Object value1,
            final Object value2) {
        if (type != null) {
            Criterion newCriterion = null;
            switch (type) {
            case ILIKE:
                newCriterion = Restrictions.ilike(attribute, value1);
                break;
            case EQ:
                newCriterion = Restrictions.eqOrIsNull(attribute, value1);
                break;
            case GE:
                newCriterion = Restrictions.ge(attribute, value1);
                break;
            case LE:
                newCriterion = Restrictions.le(attribute, value1);
                break;
            case LIKE:
                newCriterion = Restrictions.like(attribute, value1);
                break;
            case BETWEEN:
                newCriterion = Restrictions.between(attribute, value1, value2);
                break;
            default:
                break;
            }
            if (newCriterion != null) {
                if (criterion == null) {
                    return newCriterion;
                } else {
                    return Restrictions.and(criterion, newCriterion);
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**

     */
    public static StringBuilder addANDorWHERE(final StringBuilder queryString) {
        if (queryString.length() > 0) {
            return queryString.append(" AND ");
        } else {
            return queryString.append(" WHERE ");
        }
    }

    /**
     * When the user select "ON date" as search criteria, the restriction ON date must be between 'day at midnight' and 'day + 1 at midnight'
     *
     * @param inDate : date selected in rich:calendar component
     * @return a map of dates (with keys "begin" and "end") which correspond to this two dates
     */
    public static Map<String, Date> getBeginAndEnd(final Date inDate) {
        if (inDate == null) {
            return null;
        }

        final Map<String, Date> dates = new HashMap<>();
        final Calendar date = Calendar.getInstance();
        date.setTime(inDate);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        dates.put("begin", date.getTime());
        date.add(Calendar.DAY_OF_MONTH, +1);
        dates.put("end", date.getTime());
        return dates;
    }

}
