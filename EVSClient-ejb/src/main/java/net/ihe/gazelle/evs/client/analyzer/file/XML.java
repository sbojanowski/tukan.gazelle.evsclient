/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file;

import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.analyzer.file.xml.*;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.analyzer.utils.XmlUtils;
import net.ihe.gazelle.evs.client.common.action.FileToValidate;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.util.ValidationContextHandler;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XML implements IAnalyzableContent {

    private static final Logger LOGGER = LoggerFactory.getLogger(XML.class);

    /**
     *
     */
    @Override
    public boolean analyze(final File f, final FileToValidate parent, final FileToValidate fileToValidateInput, final MessageContentAnalyzer mca,
            final int startOffset, final int endOffset) {
        FileToValidate ftv = fileToValidateInput;
        final FileToValidate ftv1 = new FileToValidate(parent);
        final FileToValidate ftv2 = new FileToValidate(parent);
        final FileToValidate ftv3 = new FileToValidate(parent);
        final FileToValidate ftv4 = new FileToValidate(parent);
        // If the entire file is well formed. This is only done one the full document analyzed.
        mca.setWellFormedFirstTime(true);

        // Don't add another time XML for body
        if (!"Body".equals(parent.getDocType())) {
            ftv.setDocType("XML");
            ftv.setMessageContentAnalyzerOid(OIDGenerator.getNewOid());

            if (parent.getDocType().equals(B64.BASE64)) {
                ftv.setStartOffset(mca.getBase64OffsetStart());
                ftv.setEndOffset(mca.getBase64OffsetEnd());
            } else {
                ftv.setStartOffset(startOffset);
                ftv.setEndOffset(endOffset);
            }
            parent.getFileToValidateList().add(ftv);
        } else {
            ftv = parent;
        }

        // Contains elements envelope ?
        if (this.containsEnvelopElement(f, Integer.valueOf(startOffset), Integer.valueOf(endOffset), mca)) {

            ftv2.setDocType("Envelope");
            ftv2.setStartOffset(mca.getOffsetStart() + mca.getSaveStartOffset());
            ftv2.setEndOffset(mca.getOffsetEnd() + mca.getSaveStartOffset());
            ftv2.setMessageContentAnalyzerOid(OIDGenerator.getNewOid());

            ftv2.setParent(ftv);
            ftv.getFileToValidateList().add(ftv2);

            XML.LOGGER.info("Splitting Header and body");
            // Separate Header and body
            this.separateHeaderBody(f, startOffset, endOffset, mca);

            mca.setHeaderValue(mca.getHeader());
            if (mca.getHeaderValue() != null) {
                ftv3.setDocType("Header");
                ftv3.setStartOffset(mca.getHeaderOffsetStart());
                ftv3.setEndOffset(mca.getHeaderOffsetEnd());
                ftv3.setParent(ftv2);
                ftv3.setMessageContentAnalyzerOid(OIDGenerator.getNewOid());
                ftv2.getFileToValidateList().add(ftv3);
                mca.messageContentAnalyzer(f, mca.getHeaderOffsetStart(), mca.getHeaderOffsetEnd(), ftv3);
                mca.setHeaderValue(null);
                mca.setHeader(null);
                mca.setBodyValue(mca.getBody());
            }
            if (mca.getBodyValue() != null) {
                ftv4.setDocType("Body");
                ftv4.setParent(ftv2);
                ftv4.setStartOffset(mca.getBodyOffsetStart());
                ftv4.setEndOffset(mca.getBodyOffsetEnd());
                ftv4.setListOfNamespacesForftv(mca.getListOfNamespaces());
                ftv4.setMessageContentAnalyzerOid(OIDGenerator.getNewOid());
                ftv2.getFileToValidateList().add(ftv4);
                mca.messageContentAnalyzer(f, mca.getBodyOffsetStart(), mca.getBodyOffsetEnd(), ftv4);
                mca.setBody(null);
                mca.setBodyValue(null);
            }
            // END
        } else {
            final IAnalyzable detectedType = this.detectFileType(f, Integer.valueOf(startOffset), Integer.valueOf(endOffset), parent, mca);
            detectedType.analyze(ftv, ftv1, mca);

            // Check if there is SAML
            mca.containsSAMLOverride(f, startOffset, endOffset, ftv1);

        }
        return true;
    }

    /**

     */
    public IAnalyzable detectFileType(final File file, final Integer startOffsetArgument, final Integer endOffsetArgument, final FileToValidate parent,
            final MessageContentAnalyzer mca) {
        final File f;
        Integer startOffset = startOffsetArgument;
        Integer endOffset = endOffsetArgument;
        final Map<String, String> namespacesList = parent.getListOfNamespacesForftv();
        if (parent.getDocType().equals(B64.BASE64)) {
            startOffset = Integer.valueOf(0);
            endOffset = Integer.valueOf(0);
        }
        if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
            mca.setSaveStartOffset(0);
            f = mca.fileIntoOffsets(file, startOffset, endOffset);
            mca.setSaveStartOffset(mca.getSaveStartOffset() + startOffset);
            final IAnalyzable result = this.detectFileType(f, Integer.valueOf(0), Integer.valueOf(0), parent, mca);
            AnalyzerFileUtils.deleteTmpFile(f);
            return result;
        } else {

            if (CDA.containsCDA(file, mca)) {
                return new CDA();
            } else if (HL7V3.containsHl7v3MessageId(file, mca)) {
                return new HL7V3();
            } else if (DSUB.containsDSUB(file, namespacesList, mca)) {
                return new DSUB();
            } else if (XDS.containsXDS(file, mca)) {
                return new XDS();
            } else if (XDW.containsXDW(file, mca)) {
                return new XDW();
            } else if (AUDIT_MESSAGE.containsAuditMessage(file, mca)) {
                return new AUDIT_MESSAGE();
            } else if (HPD.containsHPD(file, mca)) {
                return new HPD();
            } else if (SVS.containsSVS(file, mca)) {
                return new SVS();
            } else if (XACML.containsXACML(file, mca)) {
                return new XACML();
            } else if (WSTrust.containsWS_Trust(file, mca)) {
                return new WSTrust();
            }

            // TODO There is a problem with this method which do not return a valid SAML start/end part when SAML is present in the body
            // Currently SAML can be found in body only in a XUA request which is handled line 106
            //When SAML is in header, it is handled by the method analyzeHeader in the file MessageContentAnalyzer
            //}
            // else if (SAML.containsSAML(file, mca)) {
            //	return new SAML();
            //}

        }
        return new NullAnalyzable();
    }

    /**

     */
    public boolean containsEnvelopElement(final File file, final Integer startOffsetArgument, final Integer endOffsetArgument, final MessageContentAnalyzer mca) {
        final File f;
        final Integer startOffset = startOffsetArgument;
        final Integer endOffset = endOffsetArgument;
        try {
            if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
                mca.setSaveStartOffset(0);
                f = mca.fileIntoOffsets(file, startOffset, endOffset);
                mca.setSaveStartOffset(mca.getSaveStartOffset() + startOffset);
                final boolean result = this.containsEnvelopElement(f, Integer.valueOf(0), Integer.valueOf(0), mca);
                AnalyzerFileUtils.deleteTmpFile(f);
                return result;
            } else {
                String fileContent ;
                fileContent = FileUtils.readFileToString(file);
                final String envReg = "<([^<]+)?Envelope[\\r| |>].+</([^<]+)?Envelope>";

                final ValidationContextHandler vch = new ValidationContextHandler(fileContent);

                Pattern regex = Pattern.compile(envReg, Pattern.DOTALL);
                Matcher regexMatcher = regex.matcher(fileContent);
                if (regexMatcher.find()) {
                    XML.LOGGER.info(regexMatcher.group());
                    if (XmlUtils.testValidSchemes(regexMatcher.group(1), regexMatcher.group(2))) {

                        regex = Pattern.compile(envReg, Pattern.DOTALL);
                        regexMatcher = regex.matcher(fileContent);
                        if (regexMatcher.find()) {
                            if (regexMatcher.group(1) != null) {
                                vch.setPatternEnvelope(regexMatcher.group(1) + "Envelope");
                            } else {
                                vch.setPatternEnvelope("Envelope");
                            }
                            vch.parseMessageContext();
                        }
                        vch.parseMessageContext();
                    }
                }

                if (vch.isEnvelope()) {
                    XML.LOGGER.info(regexMatcher.group());
                    mca.setOffsetStart(regexMatcher.start());
                    mca.setOffsetEnd(regexMatcher.end());
                    return true;
                }
                return false;
            }
        } catch (final IOException e) {
            XML.LOGGER.error(e.getMessage());
            return false;
        }

    }

    /**

     */
    public boolean separateHeaderBody(final File file, final int startOffset, final int endOffset, final MessageContentAnalyzer mca) {
        final File f;
        try {
            if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
                mca.setSaveStartOffset(0);
                f = mca.fileIntoOffsets(file, startOffset, endOffset);
                mca.setSaveStartOffset(mca.getSaveStartOffset() + startOffset);
                final boolean result = this.separateHeaderBody(f, 0, 0, mca);
                AnalyzerFileUtils.deleteTmpFile(f);
                return result;
            } else {
                String fileContent ;
                boolean result = false;
                fileContent = FileUtils.readFileToString(file);
                final String headerReg = "<([^<]+)?Header([\\r|\\n| |>])+(.+)</([^<]+)?Header>";
                final String bodyReg = "<([^<]+)?Body([\\r|\\n| |>])+(.+)</([^<]+)?Body>";

                Pattern regex = Pattern.compile(headerReg, Pattern.DOTALL);
                Matcher regexMatcher = regex.matcher(fileContent);
                if (regexMatcher.find()) {
                    if (XmlUtils.testValidSchemes(regexMatcher.group(1), regexMatcher.group(4))) {
                        XML.LOGGER.info(regexMatcher.group());
                        mca.setHeader(regexMatcher.group());
                        result = true;
                        if (mca.getSaveStartOffset() != 0) {
                            mca.setHeaderOffsetStart(regexMatcher.start() + mca.getSaveStartOffset());
                            mca.setHeaderOffsetEnd(regexMatcher.end() + mca.getSaveStartOffset());
                        } else {
                            mca.setHeaderOffsetStart(regexMatcher.start());
                            mca.setHeaderOffsetEnd(regexMatcher.end());
                        }
                    }
                }

                regex = Pattern.compile(bodyReg, Pattern.DOTALL);
                regexMatcher = regex.matcher(fileContent);
                if (regexMatcher.find()) {
                    if (XmlUtils.testValidSchemes(regexMatcher.group(1), regexMatcher.group(4))) {
                        XML.LOGGER.info(regexMatcher.group());
                        mca.setBody(regexMatcher.group());
                        result = true;
                        if (mca.getSaveStartOffset() != 0) {
                            mca.setBodyOffsetStart(regexMatcher.start() + mca.getSaveStartOffset());
                            mca.setBodyOffsetEnd(regexMatcher.end() + mca.getSaveStartOffset());
                        } else {
                            mca.setBodyOffsetStart(regexMatcher.start());
                            mca.setBodyOffsetEnd(regexMatcher.end());
                        }
                    }
                }
                return result;
            }
        } catch (final IOException e) {
            XML.LOGGER.error(e.getMessage());
            return false;
        }
    }

}