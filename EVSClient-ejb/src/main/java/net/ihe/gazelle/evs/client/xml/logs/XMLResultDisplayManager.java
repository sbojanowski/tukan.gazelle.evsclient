/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.logs;

import net.ihe.gazelle.com.templates.Template;
import net.ihe.gazelle.com.templates.TemplateId;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.model.ApplicationPreference;
import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.common.util.DocumentFileUpload;
import net.ihe.gazelle.evs.client.cdascorecard.ScorecardManager;
import net.ihe.gazelle.evs.client.common.action.AbstractResultDisplayManager;
import net.ihe.gazelle.evs.client.common.action.ApplicationConfiguration;
import net.ihe.gazelle.evs.client.common.model.DocStyleSheet;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.common.model.ValidationService;
import net.ihe.gazelle.evs.client.pdf.model.ValidatedPdf;
import net.ihe.gazelle.evs.client.util.JHiglightUtil;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.evs.client.util.ValidatorType;
import net.ihe.gazelle.evs.client.xml.model.*;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.validation.DetailedResult;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.core.Interpolator;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.faces.Redirect;
import org.jboss.seam.international.LocaleSelector;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.richfaces.component.UIDataTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.xml.XMLConstants;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.jboss.seam.core.ResourceBundle;

@Name("resultDisplayManager")
@Scope(ScopeType.PAGE)
public class XMLResultDisplayManager extends AbstractResultDisplayManager<AbstractValidatedXMLFile> implements Serializable {

    public static final String MB_CDA = "MB-CDA";


    /**
     *
     */
    private static final long serialVersionUID = 1725659452942403578L;


    private static final Logger LOGGER = LoggerFactory.getLogger(XMLResultDisplayManager.class);

    private ValidatorType validatorType;

    /**
     * Used for displaying the tree of templates for CDA
     */
    private transient DetailedResult detailedResult;
    private Template selectedTemplate;
    private Boolean showTemplates = Boolean.FALSE;

    private Boolean viewLineNumber = Boolean.TRUE;

    private Boolean indentFile = Boolean.FALSE;

    private Boolean scanTemplates = Boolean.TRUE;

    private DocStyleSheet selectedDocStyleSheet;

    public String wadoUrl = "Unable to parse the document";

    public XMLResultDisplayManager() {
    }

    public DocStyleSheet getSelectedDocStyleSheet() {
        return this.selectedDocStyleSheet;
    }

    public void setSelectedDocStyleSheet(final DocStyleSheet selectedDocStyleSheet) {
        this.selectedDocStyleSheet = selectedDocStyleSheet;
    }

    public Boolean getViewLineNumber() {
        return this.viewLineNumber;
    }

    public void setViewLineNumber(final Boolean viewLineNumber) {
        this.viewLineNumber = viewLineNumber;
    }

    public Boolean getIndentFile() {
        return this.indentFile;
    }

    public void setIndentFile(final Boolean indentFile) {
        this.indentFile = indentFile;
    }

    public Boolean getShowTemplates() {
        return this.showTemplates;
    }

    public void setShowTemplates(final Boolean showTemplates) {
        this.showTemplates = showTemplates;
    }

    public boolean displayScorecardTab() {
        return (selectedObject.getMbValidationService() != null && MB_CDA.equals(selectedObject.getMbValidationService().getKeyword()));
    }

    public boolean isTemplateTreeAvailable() {
        return (this.detailedResult != null && this.detailedResult.getTemplateDesc() != null);
    }

    public Template getSelectedTemplate() {
        return this.selectedTemplate;
    }

    public Boolean getScanTemplates() {
        this.scanTemplates = Boolean.valueOf(ApplicationConfiguration.isTemplateScanned());
        return this.scanTemplates;
    }

    public void setScanTemplates(final Boolean scanTemplates) {
        final ApplicationConfiguration ac = new ApplicationConfiguration();
        ac.setSelectedPreference(ApplicationPreference.get_preference_from_db("templates_scanned"));
        ac.getSelectedPreference().setPreferenceValue(scanTemplates.toString());
        ac.save();

        this.scanTemplates = scanTemplates;
    }

    public void setWadoUrl(final String wadoUrl) {
        this.wadoUrl = wadoUrl;
    }

    public String getWadoUrl() {
        if (this.isWado()) {
            final ValidatedWADO wado = ValidatedObject
                    .getObjectByOID(ValidatedWADO.class, this.selectedObject.getOid(), null);
            if (wado.getValidatedUrl() != null) {
                this.wadoUrl = new String(DatatypeConverter.parseBase64Binary(wado.getValidatedUrl()), StandardCharsets.UTF_8);

            } else {
                XMLResultDisplayManager.LOGGER.error("wadoUrl empty in DB");
                FacesMessages.instance().add(Severity.ERROR, "Error with register value to display");
            }
        } else if (selectedObject instanceof ValidatedFHIR) {
            final ValidatedFHIR fhirObj = (ValidatedFHIR) selectedObject;
            if (FhirObjectType.URL.equals(fhirObj.getValidatedObjectType())) {
                this.wadoUrl = new String(DatatypeConverter.parseBase64Binary(fhirObj.getRequestUrl()), StandardCharsets.UTF_8);
            }
        }
        return this.wadoUrl;
    }

    public DetailedResult getDetailedResult() {
        return this.detailedResult;
    }

    public void setDetailedResult(final DetailedResult detailedResult) {
        this.detailedResult = detailedResult;
    }

    @Override
    public void initFromUrl() {
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String objectOid = null;
        String validationType = null;
        if (urlParams != null && !urlParams.isEmpty()) {
            if (urlParams.containsKey("type")) {
                validationType = urlParams.get("type");
            }
            if (urlParams.containsKey("oid")) {
                objectOid = urlParams.get("oid");
            }
            this.init(objectOid, validationType, urlParams.get("privacyKey"));
        }
    }

    public void initSimple(final CDAValidatedFile f, final String validationType) {
        this.showTemplates = Boolean.FALSE;
        this.selectedTemplate = null;
        if (validationType.equals(ValidatorType.CDA.getFriendlyName())) {
            this.selectedObject = f;
            this.validatorType = ValidatorType.CDA;
        }
    }

    public void init(final String objectOid, final String validationType, final String privacyKey) {
        this.showTemplates = Boolean.FALSE;
        this.selectedTemplate = null;
        if (objectOid != null && validationType != null) {
            this.validatorType = ValidatorType.getByFriendlyName(validationType);
            if (validatorType != null) {
                switch (validatorType) {
                    case ATNA:
                        this.selectedObject = ValidatedObject.getObjectByOID(ValidatedAuditMessage.class, objectOid, privacyKey);
                        break;
                    case CDA:
                        this.selectedObject = ValidatedObject.getObjectByOID(CDAValidatedFile.class, objectOid, privacyKey);
                        break;
                    case HL7V3:
                        this.selectedObject = ValidatedObject.getObjectByOID(HL7v3ValidatedMessage.class, objectOid, privacyKey);
                        break;
                    case XDS:
                        this.selectedObject = ValidatedObject.getObjectByOID(ValidatedXDS.class, objectOid, privacyKey);
                        break;
                    case XDW:
                        this.selectedObject = ValidatedObject.getObjectByOID(ValidatedXDW.class, objectOid, privacyKey);
                        break;
                    case SAML:
                        this.selectedObject = ValidatedObject.getObjectByOID(ValidatedAssertion.class, objectOid, privacyKey);
                        break;
                    case SVS:
                        this.selectedObject = ValidatedObject.getObjectByOID(ValidatedSVS.class, objectOid, privacyKey);
                        break;
                    case HPD:
                        this.selectedObject = ValidatedObject.getObjectByOID(ValidatedHPD.class, objectOid, privacyKey);
                        break;
                    case DSUB:
                        this.selectedObject = ValidatedObject.getObjectByOID(ValidatedDSUB.class, objectOid, privacyKey);
                        break;
                    case XML:
                        this.selectedObject = ValidatedObject.getObjectByOID(ValidatedXML.class, objectOid, privacyKey);
                        break;
                    case DICOM_WEB:
                        this.selectedObject = ValidatedObject.getObjectByOID(ValidatedDICOMWeb.class, objectOid, privacyKey);
                        break;
                    case FHIR:
                        this.selectedObject = ValidatedObject.getObjectByOID(ValidatedFHIR.class, objectOid, privacyKey);
                        break;
                    default:
                        this.selectedObject = null;
                        FacesMessages.instance()
                                .addFromResourceBundle("net.ihe.gazelle.evs.xml.ObjectTypeInvalid", validationType);
                }
            }
        } else {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "net.ihe.gazelle.evs.xml.noObjectOIDProvided");
            this.selectedObject = null;
        }

    }

    public void downloadResultAsXml(final String engine) {
        String content = null;
        String fileName = null;
        if (engine.equals(ValidationService.EngineType.MODEL_BASED.getKeyword())) {
            fileName = "mbvResult.xml";
            content = this.selectedObject.getMbvDetailedResult();
        } else if (engine.equals(ValidationService.EngineType.SCHEMATRON.getKeyword())) {
            fileName = "schValidationResult.xml";
            content = this.selectedObject.getDetailedResult();
        }
        ReportExporterManager.exportToFile("text/xml", content, fileName);
    }

    public void downloadReportAsPdf(final String engine, boolean withScorecard) {
        StringBuilder content = new StringBuilder();
        String reportSource = null;
        StringBuilder startTag = new StringBuilder("<CombinedOutcome>");
        StringBuilder endTag = new StringBuilder("</CombinedOutcome>");

        if (engine.equals(ValidationService.EngineType.MODEL_BASED.getKeyword())) {
            content.append(this.selectedObject.getMbvDetailedResult());
            reportSource = this.selectedObject.getMbValidationService().getReportName();
            if (withScorecard) {
                content = getScorecardInContent(content, startTag, endTag);
            }

        } else if (engine.equals(ValidationService.EngineType.SCHEMATRON.getKeyword())) {
            content.append(this.selectedObject.getDetailedResult());
            reportSource = this.selectedObject.getService().getReportName();
        }

        Map<String, Object> parameters = initParams();

        // We make the validation report neutral so that the same jasper report can be used for the result transformation into pdf.
        String xmlReport = content.toString()
                .replaceAll("SchematronValidation", "Validation")
                .replaceAll("MDAValidation", "Validation");

        if (reportSource != null && !reportSource.isEmpty()) {
            String subdirReportsPath = ApplicationPreferenceManager.instance().getGazelleReportsPath();
            reportSource = subdirReportsPath + File.separatorChar + reportSource;
            String fileNameDestination = "report.pdf";
            ReportExporterManager.exportFromXmlToPDF(reportSource, subdirReportsPath, xmlReport, fileNameDestination, parameters);
        } else {
            LOGGER.error("The report is not defined in validation service");
        }

    }

    private StringBuilder getScorecardInContent(StringBuilder content, StringBuilder startTag, StringBuilder endTag) {
        addScorecardContent(content);
        startTag.append(content);
        startTag.append(endTag);
        content = startTag;
        return content;
    }

    // These are the parameter for the jasperreport calls
    private Map<String, Object> initParams() {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("APPLICATION_URL", getAppUrlWithoutLastSlash());
        parameters.put("VALIDATION_TYPE", getValidatorType());
        parameters.put("OID", this.selectedObject.getOid());
        parameters.put("VALIDATED_FILE_NAME", this.selectedObject.getDescription());
        return parameters;
    }

    private void addScorecardContent(StringBuilder content) {
        ScorecardManager sm = new ScorecardManager();
        String scorecardContent = null;
        if (sm.isScorecardAvailable(this.selectedObject)) {
            sm.processScorecard();
            scorecardContent = sm.getScorecard();
            if (scorecardContent.contains("<?xml version=")) {
                scorecardContent = scorecardContent.substring(scorecardContent.indexOf('\n') + 1);
            }
        }
        content.append(scorecardContent);
    }

    public String getAppUrlWithoutLastSlash() {
        String appUrl = ApplicationPreferenceManager.instance().getApplicationUrl();
        if (appUrl.endsWith("/")) {
            appUrl = appUrl.substring(0, appUrl.length() - 1);
        }
        return appUrl;
    }

    public String getValidationType() {
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String validationType = null;
        if (urlParams != null && !urlParams.isEmpty()) {
            if (urlParams.containsKey("type")) {
                validationType = urlParams.get("type");
            }
        }
        return validationType;
    }


    /**
     * used for PDF embedded in CDA documents
     */
    public void validatePdf(final File pdfFile) {
        ValidatedPdf validatedPdf = ValidatedPdf.createNewValidatedPdf();
        if (validatedPdf != null) {
            try {
                FileUtils.copyFile(pdfFile, new File(validatedPdf.getPdfPath()));
                validatedPdf.setDescription(pdfFile.getName());
                validatedPdf = ValidatedObject.save(ValidatedPdf.class, validatedPdf);
                final Redirect redirect = Redirect.instance();
                final String viewId = ValidatorType.PDF.getView();
                redirect.setViewId(viewId);
                redirect.setParameter("id", validatedPdf.getId());
                redirect.execute();
            } catch (final IOException e) {
                FacesMessages.instance().addFromResourceBundle("net.ihe.gazelle.evs.xml.pdfValidationFailed", e.getMessage());
            }
        }

    }

    public void showPDF(final File pdfFile, final boolean download) {
        InputStream is = null;
        try {
            is = new FileInputStream(pdfFile.getAbsoluteFile());
            DocumentFileUpload.showFile(is, pdfFile.getName(), download);
        } catch (final FileNotFoundException e) {
            XMLResultDisplayManager.LOGGER.error("{}", e);
        } catch (final IOException e) {
            XMLResultDisplayManager.LOGGER.error("", e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (final IOException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * redirect to the validator page with the object oid as parameter
     */
    @Override
    public String revalidate() {
        final StringBuilder builder = new StringBuilder(this.validatorType.getView());
        setValidationDone(false);
        builder.append("?oid=").append(this.selectedObject.getOid());
//        builder.append("?oid=").append(OIDGenerator.getNewOid());
        if (this.selectedObject.getStandard() != null && this.selectedObject.getStandard().getExtension() != null
                && !this.validatorType.getView().contains("extension")) {
            builder.append("&extension=").append(this.selectedObject.getStandard().getExtension());
        }
        return builder.toString();
    }

    public String performAnotherValidation() {
        StringBuilder builder = new StringBuilder(this.validatorType.getView());
        if (this.selectedObject.getStandard() != null && this.selectedObject.getStandard().getExtension() != null
                && !this.validatorType.getView().contains("extension")) {
            builder.append("?extension=").append(this.selectedObject.getStandard().getExtension());
        }
        return builder.toString();
    }

    @Override
    protected Class<AbstractValidatedXMLFile> getEntityClass() {
        return AbstractValidatedXMLFile.class;
    }

    @Override
    protected String getValidatorUrl() {
        return null;
    }

    /**
     *
     */
    @Override
    public void downloadValidatedObject() {
        downloadFileContent("text/xml");
    }

    /**
     */
    public String getPrettyXmlContent() {
        if (this.selectedObject == null) {
            return "";
        }
        if (this.selectedDocStyleSheet != null && this.selectedDocStyleSheet.getPath() != null) {
            if (this.selectedDocStyleSheet.getName() != null) {
                return Util.transformXMLFileToHTML(this.selectedObject.getFilePath(), this.selectedDocStyleSheet.getPath());
            }
        }
        return null;
    }


    public String getRawXmlContent() {
        if (this.selectedObject == null) {
            return "";
        }
        final Map<Integer, String> listErrors = new HashMap<>();
        final Map<Integer, String> listWarnings = new HashMap<>();
        if (selectedObject instanceof ValidatedFHIR) {
            ValidatedFHIR fhirObj = (ValidatedFHIR) selectedObject;
            switch (fhirObj.getValidatedObjectType()) {
                case XML:
                    break;
                case JSON:
                    return ValidatedFHIR.formatJsonContent(fhirObj, getIndentFile());
                case URL:
                    return getWadoUrl();
                default:
                    return "";
            }
        }
        this.extractListXMLErrorsAndWarnings(this.selectedObject.getMbvDetailedResult(), listErrors, listWarnings);
        return JHiglightUtil.getHighLightedDocumentString(this.selectedObject.getFilePath(), listErrors, listWarnings, this.getViewLineNumber(),
                this.getIndentFile());
    }


    private void extractListXMLErrorsAndWarnings(final String validationResult, final Map<Integer, String> listXMLErrors, final Map<Integer,
            String> listXMLWarnings) {
        if (validationResult != null && listXMLErrors != null) {
            try {
                final SAXBuilder sxb = new SAXBuilder();
                final Document dom = sxb.build(new ByteArrayInputStream(validationResult.getBytes(StandardCharsets.UTF_8)));

                final Element rac = dom.getRootElement();
                if (rac != null) {
                    Element xsd = rac.getChild("DocumentValidXSD");
                    if (xsd == null) {
                        xsd = rac.getChild("DocumentWellFormed");
                    }
                    if (xsd != null) {
                        final List<Element> mess = xsd.getChildren("XSDMessage");
                        if (mess != null && (!mess.isEmpty())) {
                            for (final Element mm : mess) {
                                if (StringUtils.isNumeric(mm.getChildText("lineNumber"))) {
                                    if ("warning".equalsIgnoreCase(mm.getChildText("Severity"))) {
                                        listXMLWarnings.put(
                                                Integer.valueOf(mm.getChildText("lineNumber")),
                                                String.valueOf(mm.getChildText("Message")));
                                    }
                                    if ("error".equalsIgnoreCase(mm.getChildText("Severity"))) {
                                        listXMLErrors.put(
                                                Integer.valueOf(mm.getChildText("lineNumber")),
                                                String.valueOf(mm.getChildText("Message")));
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (JDOMException | IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }


    /**
     */
    public String getTransformedDetailedResult(final String engine) {
        if (this.selectedObject == null) {
            return "";
        }
        String detailedResult = null;
        String xsl = null;
        if (engine.equals(ValidationService.EngineType.MODEL_BASED.getKeyword())) {
            detailedResult = this.selectedObject.getMbvDetailedResult();
            if (this.selectedObject.getMbValidationService() != null) {
                xsl = this.selectedObject.getMbValidationService().getXslLocation();
            }
        } else if (engine.equals(ValidationService.EngineType.SCHEMATRON.getKeyword())) {
            detailedResult = this.selectedObject.getDetailedResult();
            if (this.selectedObject.getService() != null) {
                xsl = this.selectedObject.getService().getXslLocation();
            }
        }
        if (detailedResult != null && !detailedResult.isEmpty()) {
            if (detailedResult.contains("?>")) {
                detailedResult = detailedResult.substring(detailedResult.indexOf("?>") + 2);
            }
            return Util.transformXMLStringToHTML(detailedResult, xsl, LocaleSelector.instance().getLanguage());
        }
        return null;
    }

    public String getValidationStatus(final String engine) {
        String detailedResult = null;
        if (engine.equals(ValidationService.EngineType.MODEL_BASED.getKeyword())) {
            detailedResult = this.selectedObject.getMbvDetailedResult();
        } else if (engine.equals(ValidationService.EngineType.SCHEMATRON.getKeyword())) {
            detailedResult = this.selectedObject.getDetailedResult();
        }
        if (detailedResult != null) {
            final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            String  FEATURE = "http://apache.org/xml/features/nonvalidating/load-external-dtd";
            try {
                 dbFactory.setFeature(FEATURE, false);
            } catch (ParserConfigurationException e) {
                // This should catch a failed setFeature feature
                LOGGER.info("ParserConfigurationException was thrown. The feature '" +
                        FEATURE + "' is probably not supported by your XML processor.");
            }
            final DocumentBuilder dBuilder;
            try {
                dbFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
                dbFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
                dBuilder = dbFactory.newDocumentBuilder();
                final org.w3c.dom.Document doc = dBuilder.parse(new ByteArrayInputStream(detailedResult.getBytes(StandardCharsets.UTF_8)));

                final NodeList res = doc.getDocumentElement().getElementsByTagName("ValidationResultsOverview");
                if (res.getLength() > 0) {
                    final Node nNode = res.item(0);
                    final NodeList cn = nNode.getChildNodes();
                    if (cn.getLength() > 0) {
                        for (int i = 0; i < cn.getLength(); i++) {
                            final Node nNode2 = cn.item(i);
                            if ("ValidationTestResult".equals(nNode2.getNodeName())) {
                                return nNode2.getTextContent();
                            }
                        }
                    }
                    return nNode.getTextContent();
                } else {
                    return "NOT AVAILABLE";
                }
            } catch (ParserConfigurationException | SAXException | IOException e) {
                return "NOT AVAILABLE";
            }
        } else {
            return "N/A";
        }
    }

    public void setSelectedObject(final AbstractValidatedXMLFile selectedObject) {
        this.selectedObject = selectedObject;
    }

    /**
     */
    public Integer getPdfFileListSize() {
        final List<File> files = this.getPdfFileList();
        if (files == null) {
            return Integer.valueOf(0);
        } else {
            return Integer.valueOf(files.size());
        }
    }

    /**

     */
    public List<File> getPdfFileList() {
        if (this.selectedObject != null && this.selectedObject instanceof CDAValidatedFile) {
            final CDAValidatedFile selectedCDA = (CDAValidatedFile) this.selectedObject;
            return selectedCDA.getPdfFiles();
        } else {
            return null;
        }
    }

    public String getValidatorType() {
        return this.validatorType.getFriendlyName();
    }

    public GazelleTreeNodeImpl<Template> getListTemplatesArchitecture() {
        final GazelleTreeNodeImpl<Template> treeNodeImpl = new GazelleTreeNodeImpl<>();
        if (this.detailedResult == null) {
            return null;
        }
        if (this.detailedResult.getTemplateDesc() == null) {
            return null;
        }
        final Template temp = this.detailedResult.getTemplateDesc();
        this.updateTreeTemplate(temp, treeNodeImpl);

        List<ObjectIndexer> objectIndexers = new ArrayList<>();
        objectIndexers = this.addTemplatesInDB(treeNodeImpl.getData(), objectIndexers);
        this.save(objectIndexers);
        return treeNodeImpl;
    }

    public List<ObjectIndexer> addTemplatesInDB(final Template template, final List<ObjectIndexer> objectIndexersList) {

        List<ObjectIndexer> list = objectIndexersList;
        final ObjectIndexerManager objectIndexerManager = new ObjectIndexerManager();

        final List<TemplateId> nodeList = template.getTemplateId();
        final String extension = this.getSelectedObject().getStandard().getExtension();
        list = objectIndexerManager.addTemplatesIfNotExist(nodeList, list, extension);

        for (final Template temp : template.getTemplate()) {
            this.addTemplatesInDB(temp, list);
        }

        list = objectIndexerManager.removeDuplicate(list);
        return list;
    }

    public void save(final List<ObjectIndexer> objectIndexersList) {

        final EntityManager em = EntityManagerService.provideEntityManager();
        for (ObjectIndexer oi : objectIndexersList) {
            if (this.selectedObject != null && this.selectedObject instanceof CDAValidatedFile && oi.getIdentifier() != null) {

                List<Integer> validatedCdaFiles = new ArrayList<>();
                final List<Integer> validatedCdaFilesToAdd = new ArrayList<>();

                final int oiId = this.objectIndexerAlreadyExist(oi);
                if (oiId != 0) {
                    oi = em.find(ObjectIndexer.class, Integer.valueOf(oiId));
                    LOGGER.debug("Object identifier already exist : {}", oi.getIdentifier());
                    final ObjectIndexerQuery query = new ObjectIndexerQuery();
                    query.id().eq(Integer.valueOf(oiId));
                    validatedCdaFiles = query.validatedCdaFiles().id().getListDistinct();
                }

                if (!validatedCdaFiles.contains(((CDAValidatedFile) this.selectedObject).getId())) {

                    validatedCdaFilesToAdd.add(((CDAValidatedFile) this.selectedObject).getId());
                } else {
                    LOGGER.debug("OID :{} already in DB", this.selectedObject.getOid());
                }

                this.performSaving(oi, validatedCdaFilesToAdd);
                LOGGER.debug("Object identifier Saved ");

            }
        }
    }

    public void performSaving(final ObjectIndexer oi, final List<Integer> validatedCdaFilesToAdd) {
        final int max = validatedCdaFilesToAdd.size();
        int startIndex = 0;
        final int step = 100;
        boolean finished = false;
        while (!finished) {
            final int toIndex;
            if (startIndex + step <= max) {
                toIndex = startIndex + step - 1;
            } else {
                toIndex = max;
                finished = true;
            }
            final List<Integer> subList = validatedCdaFilesToAdd.subList(startIndex, toIndex);

            final CDAValidatedFileQuery query = new CDAValidatedFileQuery();
            query.id().in(subList);
            final List<CDAValidatedFile> res = query.getListDistinct();
            oi.addValidatedCdaFiles(res);

            this.saveObjectIndexer(oi);
            startIndex += step;
        }
    }

    public void saveObjectIndexerListInCDA(final List<ObjectIndexer> objectIndexersList) {
        if (this.selectedObject instanceof CDAValidatedFile) {
            for (final ObjectIndexer oi : objectIndexersList) {
                int oiId = this.objectIndexerAlreadyExist(oi);
                if (oiId == 0) {
                    oiId = this.saveObjectIndexer(oi);
                }
                final ObjectIndexerQuery query = new ObjectIndexerQuery();
                query.id().eq(Integer.valueOf(oiId));
                final List<ObjectIndexer> res = query.getList();
                ((CDAValidatedFile) this.selectedObject).addObjectIndexer(res);
            }

            ((CDAValidatedFile) this.selectedObject).setTemplatesListed(true);
            CDAValidatedFile.save(CDAValidatedFile.class, ((CDAValidatedFile) this.selectedObject));
        }
    }

    public boolean selectedObjectIsInTable(final List<CDAValidatedFile> validatedCdaFiles, final CDAValidatedFile file) {
        final int id = file.getId();
        for (int i = 0; i < validatedCdaFiles.size(); i++) {
            if (id == validatedCdaFiles.get(i).getId()) {
                return true;
            }
        }
        return false;
    }

    private int objectIndexerAlreadyExist(final ObjectIndexer oi) {

        int resultId = 0;
        final ObjectIndexerQuery q = new ObjectIndexerQuery();

        q.identifier().eq(oi.getIdentifier());
        final List<ObjectIndexer> result = q.getList();
        if (!result.isEmpty()) {
            resultId = result.get(0).getId();
        }

        return resultId;
    }

    private int saveObjectIndexer(final ObjectIndexer object) {
        final EntityManager em = EntityManagerService.provideEntityManager();
        final ObjectIndexer o = em.merge(object);
        em.flush();
        return o.getId();
    }

    private void updateTreeTemplate(final Template temp, final GazelleTreeNodeImpl<Template> treeNodeImpl) {
        treeNodeImpl.setData(temp);
        int i = 0;
        for (final Template tempchild : temp.getTemplate()) {
            final GazelleTreeNodeImpl<Template> treeNodeImplChild = new GazelleTreeNodeImpl<>();
            this.updateTreeTemplate(tempchild, treeNodeImplChild);
            treeNodeImpl.addChild(Integer.valueOf(i), treeNodeImplChild);
            i++;
        }
    }

    public void initTemplatePanel() throws JAXBException {
        if (this.selectedObject == null || this.selectedObject.getMbvDetailedResult() == null) {
            return;
        }
        final JAXBContext jax = JAXBContext.newInstance(DetailedResult.class);
        final Unmarshaller unm = jax.createUnmarshaller();
        try {
            this.detailedResult = (DetailedResult) unm
                    .unmarshal(new ByteArrayInputStream(this.selectedObject.getMbvDetailedResult().getBytes(StandardCharsets.UTF_8)));
        } catch (final JAXBException e) {
            this.detailedResult = null;
        }
    }

    public String getIconClassForTemplate(final Template template) {
        String res = "gzl-icon-times";
        if (template != null) {
            final String validationOutcome = template.getValidation();
            if (validationOutcome != null) {
                if ("Warning".equals(validationOutcome)) {
                    res = "gzl-icon-exclamation-triangle";
                } else if ("Error".equals(validationOutcome)) {
                    res = "gzl-icon-times-circle";
                } else if ("Success".equals(validationOutcome)) {
                    res = "gzl-icon-checked";
                } else if ("Report".equals(validationOutcome)) {
                    res = "gzl-icon-checked";
                }
            }
        }
        return res;
    }

    public void initSelectedDocStyleSheet() {
        this.selectedDocStyleSheet = null;
        if (this.selectedObject != null && this.selectedObject.getStandard() != null) {
            ReferencedStandard standard = EntityManagerService.provideEntityManager().find(ReferencedStandard.class, this.selectedObject
                    .getStandard().getId());
            if (standard.getListDocStyleSheet() != null && !standard.getListDocStyleSheet().isEmpty()) {
                this.selectedDocStyleSheet = standard.getListDocStyleSheet().get(0);
                if (standard.getListDocStyleSheet().size() > 1) {
                    final Comparator<DocStyleSheet> comp = new Comparator<DocStyleSheet>() {
                        @Override
                        public int compare(final DocStyleSheet o1, final DocStyleSheet o2) {
                            return o1.getId().compareTo(o2.getId());
                        }
                    };
                    Collections.sort(standard.getListDocStyleSheet(), comp);
                    Collections.sort(standard.getListDocStyleSheet());
                }
            }
        }
    }

    public boolean verifyThatListDocStyleSheetNotEmpty() {
        boolean result;
        // No check if it's wado or fhir
        if (this.isWado() || this.selectedObject instanceof ValidatedFHIR) {
            result = false;
        } else if (this.selectedObject.getStandard().getListDocStyleSheet() != null
                && !this.selectedObject.getStandard().getListDocStyleSheet().isEmpty()) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    public boolean isWado() {
        return this.selectedObject instanceof ValidatedWADO;
    }

    public void updateTemplateList() {

        final TemplatesUpdaterJob job = new TemplatesUpdaterJob();
        job.templatesUpdaterJob(this.getAllCdaIds());
    }

    public int getlastIndex() {
        final CDAValidatedFile f = new CDAValidatedFile();
        return f.getLastIndex();
    }

    public List<Integer> getAllCdaIds() {
        final CDAValidatedFileQuery q = new CDAValidatedFileQuery();
        q.templatesListed().eq(Boolean.FALSE);
        return q.id().getListDistinct();
    }

    public List<CDAValidatedFile> getAllCdaFiles() {
        final CDAValidatedFileQuery q = new CDAValidatedFileQuery();
        return q.getList();
    }

    public void displaySelection(final String dataTableId) {
        final UIDataTable dataTable = this.dataTable(dataTableId);
        this.displayTemplate(dataTable, true);
    }

    public void hideSelection(final String dataTableId) {
        final UIDataTable dataTable = this.dataTable(dataTableId);
        this.displayTemplate(dataTable, false);
    }

    public UIDataTable dataTable(final String dataTableId) {
        // Gets the datatable
        final UIDataTable dataTable = (UIDataTable) FacesContext.getCurrentInstance().getViewRoot()
                .findComponent(dataTableId);

        if (dataTable == null) {
            throw new RuntimeException(
                    Interpolator.instance().interpolate("Could not find data table with id #0", dataTableId));
        }
        return dataTable;
    }

    public void displayTemplate(final UIDataTable dataTable, final boolean displayTemplate) {
        for (int i = 0; i < dataTable.getRowCount(); i++) {
            /*RowIndex doesn't exist in richfaces 4*/
            //			dataTable.setRowIndex(i);
            dataTable.setRowKey(Integer.valueOf(i));
            final Object object = dataTable.getRowData();
            if (object instanceof ObjectIndexer) {
                ((ObjectIndexer) object).setDisplay(displayTemplate);
            }
        }
    }

    public String getDefaultTab() {
        if (this.selectedObject.getMbValidationService() != null) {
            if (!this.selectedObject.getMbValidationService().getKeyword().isEmpty()) {
                return "MbvTab";
            }
        }
        return "SchematronTab";
    }

    public String getMainContentTabTitle() {
        if (isWado()) {
            return "Validated URL";
        } else if (this.selectedObject instanceof ValidatedFHIR) {
            switch (((ValidatedFHIR) selectedObject).getValidatedObjectType()) {
                case XML:
                    break;
                case JSON:
                    return "Raw JSON Content";
                case URL:
                    return "Request URL";
                default:
                    return "";
            }
        }
        return ResourceBundle.instance().getString("net.ihe.gazelle.EVSClient-ui.RawXMLContent");
    }

    public boolean displayViewOptions(){
        if (selectedObject instanceof ValidatedDICOMWeb || selectedObject instanceof ValidatedWADO){
            return false;
        } else if (selectedObject instanceof ValidatedFHIR){
            ValidatedFHIR fhirObj = (ValidatedFHIR) selectedObject;
            return (!FhirObjectType.URL.equals(fhirObj.getValidatedObjectType()));
        } else {
            return true;
        }
    }
}
