/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.util.Util;
import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Name("validatedDICOMWeb")
@Table(name = "validated_dicomweb", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "validated_dicomweb_sequence", sequenceName = "validated_dicomweb_id_seq", allocationSize = 1)
public class ValidatedDICOMWeb extends AbstractValidatedXMLFile {

    private static final long serialVersionUID = -1443811873360866107L;
    private static final String FILE_PREFIX = "dicom_web_";
    private static final String FILE_EXTENSION = ".txt";
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidatedDICOMWeb.class);

    @Id
    @NotNull
    @GeneratedValue(generator = "validated_dicomweb_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "validated_url")
    @Lob
    @Type(type = "text")
    private String validatedUrl;

    @Column(name = "file_path")
    private String filePath;

    public ValidatedDICOMWeb() {

    }

    @Override
    public void setFilePath(final String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String getFilePath() {
        return this.filePath;
    }

    public String getValidatedUrl() {
        return this.validatedUrl;
    }

    public void setValidatedUrl(final String validatedUrl) {
        this.validatedUrl = validatedUrl;
    }

    public ValidatedDICOMWeb(final ValidatedDICOMWeb inDICOMWebObject) {
        this.validatedUrl = inDICOMWebObject.getValidatedUrl();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    /**

     */
    public static ValidatedDICOMWeb createNewValidatedDicomWeb() {
        ValidatedDICOMWeb message = new ValidatedDICOMWeb();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        message = em.merge(message);
        em.flush();
        if (message == null || message.getId() == 0) {
            ValidatedDICOMWeb.LOGGER.error("An error occurred while creating a Validated object");
            return null;
        }

        final String repository = ApplicationPreferenceManager.getStringValue("dicom_web_repository");

        message.setFilePath(
                repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ValidatedDICOMWeb.FILE_PREFIX
                        .concat(message.getId().toString()).concat(
                                ValidatedDICOMWeb.FILE_EXTENSION));
        return message;
    }

    /**
     * @return
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends AbstractValidatedXMLFile> T addOrUpdate(final Class<T> clazz) {
        if (this.getOid() == null || this.getOid().length() == 0) {
            this.setOid(OIDGenerator.getNewOid());
        }
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        final ValidatedDICOMWeb validatedFile = em.merge(this);
        em.flush();

        // Update the cache with the newest values
        GazelleValidationCacheManager
                .refreshGazelleCacheWebService(validatedFile.getExternalId(), validatedFile.getToolOid(),
                        validatedFile.getOid(), "off");

        return (T) validatedFile;
    }

    @Override
    public String getSchematron() {
        return null;
    }

    @Override
    public void setSchematron(final String schematron) {
        return;
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/dicomWebResult.seam?type=DICOM_WEB").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

}
