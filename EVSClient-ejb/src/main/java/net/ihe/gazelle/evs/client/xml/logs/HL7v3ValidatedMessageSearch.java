/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.logs;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.action.AbstractLogBrowser;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.xml.model.HL7v3ValidatedMessage;
import net.ihe.gazelle.evs.client.xml.model.HL7v3ValidatedMessageQuery;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("hl7v3validatedMessageSearch")
@Scope(ScopeType.PAGE)
public class HL7v3ValidatedMessageSearch extends AbstractLogBrowser<HL7v3ValidatedMessage> {

    /**
     *
     */
    private static final long serialVersionUID = -3252575715670580720L;

    @Override
    protected HL7v3ValidatedMessage getObjectByOID(final String oid, final String privacyKey) {
        return ValidatedObject.getObjectByOID(HL7v3ValidatedMessage.class, oid, privacyKey);
    }

    @Override
    protected Class<HL7v3ValidatedMessage> getEntityClass() {
        return HL7v3ValidatedMessage.class;
    }

    @Override
    protected HQLCriterionsForFilter<HL7v3ValidatedMessage> getCriterions() {
        final HL7v3ValidatedMessageQuery hl7v3ValidatedMessageQueryQuery = new HL7v3ValidatedMessageQuery();
        final HQLCriterionsForFilter<HL7v3ValidatedMessage> result = hl7v3ValidatedMessageQueryQuery
                .getHQLCriterionsForFilter();
        result.addPath("standard", hl7v3ValidatedMessageQueryQuery.standard().label());
        result.addPath("validationStatus", hl7v3ValidatedMessageQueryQuery.validationStatus());
        result.addPath("privateUser", hl7v3ValidatedMessageQueryQuery.privateUser());
        result.addPath("institutionKeyword", hl7v3ValidatedMessageQueryQuery.institutionKeyword());
        result.addPath("mbvalidator", hl7v3ValidatedMessageQueryQuery.mbvalidator());
        result.addPath("schematron", hl7v3ValidatedMessageQueryQuery.schematron());
        result.addPath("validationDate", hl7v3ValidatedMessageQueryQuery.validationDate());
        return result;
    }

    @Override
    public FilterDataModel<HL7v3ValidatedMessage> getDataModel() {
        return new FilterDataModel<HL7v3ValidatedMessage>(HL7v3ValidatedMessageSearch.this.getFilter()) {
            @Override
            protected Object getId(final HL7v3ValidatedMessage t) {
                return t.getId();
            }
        };
    }
}
