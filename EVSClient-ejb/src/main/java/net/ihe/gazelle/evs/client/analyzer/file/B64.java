/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file;

import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.common.action.FileToValidate;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class B64 implements IAnalyzableContent {
    public static final String BASE64 = "BASE64";
    private static final Logger LOGGER = LoggerFactory.getLogger(B64.class);

    @Override
    public boolean analyze(final File f, final FileToValidate parent, final FileToValidate ftv, final MessageContentAnalyzer vd,
            final int startOffset, final int endOffset) {
        B64.LOGGER.info("Decode base 64");
        // Decode base 64
        final File file;
        if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
            vd.setSaveStartOffset(0);
            file = vd.fileIntoOffsets(f, startOffset, endOffset);
            this.base64Decoding(file, file);

            vd.setBase64OffsetStart(startOffset + vd.getSaveStartOffset());
            vd.setBase64OffsetEnd(endOffset);

            ftv.setDocType(B64.BASE64);
            ftv.setStartOffset(vd.getBase64OffsetStart());
            ftv.setEndOffset(vd.getBase64OffsetEnd());

            // Restart test with decoded file
            vd.messageContentAnalyzer(file, 0, 0, ftv);
            AnalyzerFileUtils.deleteTmpFile(file);
        } else {
            this.base64Decoding(f, f);
            B64.LOGGER.info("Restart test with decoded file");

            ftv.setStartOffset(startOffset);
            ftv.setEndOffset(endOffset);
            ftv.setDocType(B64.BASE64);
            // Restart test with decoded file
            vd.messageContentAnalyzer(f, startOffset, endOffset, ftv);
        }
        parent.getFileToValidateList().add(ftv);
        return false;
    }

    public boolean base64Decoding(final File fileToDecode, final File decodedFile) {

        byte[] decoded;

        try {
            String todecode = String.valueOf(Files.readAllLines(Paths.get(fileToDecode.getAbsolutePath()), StandardCharsets.UTF_8));
            decoded = DatatypeConverter.parseBase64Binary(todecode);
            FileUtils.writeByteArrayToFile(decodedFile, decoded);
            return true;
        } catch (final FileNotFoundException e) {
            B64.LOGGER.error("{}", e);
            return false;
        } catch (final IOException e) {
            B64.LOGGER.error("{}", e);
            return false;
        }
    }
}
