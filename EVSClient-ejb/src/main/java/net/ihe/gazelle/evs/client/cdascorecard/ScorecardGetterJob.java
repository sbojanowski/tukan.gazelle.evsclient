package net.ihe.gazelle.evs.client.cdascorecard;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.evs.client.xml.model.CDAValidatedFile;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.annotations.async.Asynchronous;
import org.jboss.seam.async.QuartzDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * <b>Class Description : </b>ScorecardGetterJob<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 07/12/16
 * @class ScorecardGetterJob
 * @package net.ihe.gazelle.evs.client.cdascorecard
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */

@Name("scorecardUpdaterJob")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("ScorecardUpdaterJobLocal")
public class ScorecardGetterJob implements Serializable, ScorecardUpdaterJobLocal {
    private static final long serialVersionUID = 1;
    private static Logger log = LoggerFactory.getLogger(ScorecardGetterJob.class);
    private static Map<String, ScorecardStatus> statuses;

    static{
        statuses = new HashMap<String, ScorecardStatus>();
    }

    @Override
    public void getScorecardJob(final CDAValidatedFile validatedFile) {
        QuartzDispatcher.instance().scheduleAsynchronousEvent("getScorecard", validatedFile);
    }

    @Override
    @Observer("getScorecard")
    @Transactional
    @Asynchronous
    public void getScorecard(CDAValidatedFile validatedFile) {
        statuses.put(validatedFile.getOid(), validatedFile.getScorecardStatus());
        ScorecardStatus newStatus = ScorecardStatus.BEING_COMPUTED;
        String wsdl = validatedFile.getMbValidationService().getWsdl();
        int index = wsdl.indexOf("-ejb");
        String validatorUrl;
        if (index > 0) {
            validatorUrl = wsdl.substring(0, index);
            try {
                String scorecard = ScorecardServiceClient.getScorecardFromValidator(validatedFile.getOid(), validatedFile.getMbvDetailedResult(), validatorUrl);
                if (scorecard != null) {
                    newStatus = storeScorecardOnFileSystem(scorecard, validatedFile.getScorecardPath());
                }
            } catch (IOException e){
                log.error(e.getMessage(), e);
                newStatus = ScorecardStatus.FAILED_ON_SERVER_SIDE;
            }
            catch (Exception e) {
                log.error("Cannot get scorecard for CDA with OID " + validatedFile.getOid() + ": " + e.getMessage());
                newStatus = ScorecardStatus.FAILED_ON_SERVER_SIDE;
            }
        } else {
           log.error("Cannot get CDA Validator URL");
            newStatus = ScorecardStatus.FAILED_ON_SERVER_SIDE;
        }
        setNewStatus(validatedFile.getOid(), newStatus);
    }

    private void setNewStatus(String oid, ScorecardStatus newStatus) {
        for(Map.Entry<String, ScorecardStatus> entry: statuses.entrySet()){
            if (entry.getKey().equals(oid)){
                entry.setValue(newStatus);
                break;
            } else {
                continue;
            }
        }
    }

    public static ScorecardStatus getStatusForScorecard(String oid){
        ScorecardStatus status = statuses.get(oid);
        if (!(ScorecardStatus.BEING_COMPUTED == status)) {
            statuses.remove(oid);
        }
        return status;
    }

    private ScorecardStatus storeScorecardOnFileSystem(String scorecard, String scorecardPath) {
        File scorecardFile = new File(scorecardPath);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(scorecardFile);
            fos.write(scorecard.getBytes(Charset.forName("UTF-8")));
            return ScorecardStatus.AVAILABLE;
        } catch (final IOException e) {
            log.error(e.getMessage());
            return ScorecardStatus.FAILED_ON_CLIENT_SIDE;
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (final IOException e) {
                    log.error("not able to close the FOS", e);
                }
            }
        }
    }


}
