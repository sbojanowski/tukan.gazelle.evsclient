/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.model;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * this class aimed to uniquely identified the tools that can send files to the EVS Client for validation.
 * When the tool send a
 *
 * @author aberge
 */

@Entity
@Name("callingTool")
@Table(name = "evsc_calling_tool", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "label"))
@SequenceGenerator(name = "evsc_calling_tool_sequence", sequenceName = "evsc_calling_tool_id_seq", allocationSize = 1)
public class CallingTool implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6159285638210857211L;


    @Id
    @GeneratedValue(generator = "evsc_calling_tool_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    @NotNull
    private Integer id;

    @Column(name = "label")
    @NotNull
    private String label;

    @Column(name = "oid")
    @NotNull
    //	@Pattern(regex="", message="This attribute must be formatted as a valid OID")
    private String oid;

    @Column(name = "url")
    @NotNull
    @Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$)", message = "This attribute must be formatted as a valid URL")
    private String url;

    @Column(name = "tool_type")
    private CallingTool.ToolType toolType;

    public CallingTool() {
    }

    public void save() {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            entityManager.merge(this);
            entityManager.flush();
    }

    public static CallingTool getToolByOid(final String oid) {
        final CallingToolQuery query = new CallingToolQuery();
        query.oid().eq(oid);
        return query.getUniqueResult();
    }

    public enum ToolType {
        PROXY("Gazelle Proxy"),
        TM("Gazelle Test Management"),
        OTHER("Other");

        protected String label;

        ToolType(final String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public String getOid() {
        return this.oid;
    }

    public void setOid(final String oid) {
        this.oid = oid;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public CallingTool.ToolType getToolType() {
        return this.toolType;
    }

    public void setToolType(final CallingTool.ToolType toolType) {
        this.toolType = toolType;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        return prime * result + (this.label == null ? 0 : this.label.hashCode());
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final CallingTool other = (CallingTool) obj;
        if (this.label == null) {
            if (other.label != null) {
                return false;
            }
        } else if (!this.label.equals(other.label)) {
            return false;
        }
        return true;
    }

    public static boolean toolIsAProxy(final String toolOid) {
        final CallingToolQuery query = new CallingToolQuery();
        query.oid().eq(toolOid);
        final CallingTool tool = query.getUniqueResult();
        return tool != null && tool.getToolType() == CallingTool.ToolType.PROXY;
    }

    public static boolean toolIsTM(final String toolOid) {
        final CallingToolQuery query = new CallingToolQuery();
        query.oid().eq(toolOid);
        final CallingTool tool = query.getUniqueResult();
        return tool != null && tool.getToolType() == CallingTool.ToolType.TM;
    }
}
