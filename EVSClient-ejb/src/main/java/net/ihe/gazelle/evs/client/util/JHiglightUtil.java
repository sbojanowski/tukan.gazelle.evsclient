/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.util;

import com.uwyn.jhighlight.renderer.XhtmlRendererFactory;
import com.uwyn.jhighlight.tools.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JHiglightUtil {




    public static String getHighLightedDocumentStringWithNoLineNumber(final String extension, final String inputString) throws IOException {
        return JHiglightUtil.highlightFile(extension, inputString, "UTF-8", true, false);
    }

    public static String getHighLightedDocumentString(final String fileName, final Map<Integer, String> listErrors, final Map<Integer, String> listWarnings,
            final boolean showLineNumber ) {
        return getHighLightedDocumentString(fileName,  listErrors, listWarnings, showLineNumber, false);
    }

    public static String getHighLightedDocumentString(final String fileName, final Map<Integer, String> listErrors, final Map<Integer, String> listWarnings,
            final boolean showLineNumber, final boolean tidy) {

        String inputString;
        String restmp;

        try {
            inputString = Util.getFileContentToString(fileName);
            if (tidy){
                inputString = Util.prettyFormat(inputString);
            }
            restmp = JHiglightUtil.getHighLightedDocumentStringWithNoLineNumber(FileUtils.getExtension(fileName), inputString);
        } catch (final Exception e) {
            return e.getMessage() + ": " + fileName;
        }

        final StringBuilder res = new StringBuilder();
        if (tidy) {
            res.append("<span>The following content has been modified (indented) for better visualization</span>");
            res.append("<table class=\"xml-indented\">\n<tr>\n");
        }
        else {
            res.append("<table>\n<tr>\n");

        }
        final String[] listline = restmp.split("\\n");
        int i = 0;
        for (final String string : listline) {
            if (i > 0) {
                if (showLineNumber) {
                    res.append("<td class=\"idclass\"><a name=\"line_").append(i).append("\">").append(i)
                            .append("</a></td>\n");
                } else {
                    res.append("<td class=\"idclass\" style=\"display:none;\"><a name=\"line_").append(i).append("\">")
                            .append(i).append("</a></td>\n");
                }
                if (listErrors != null && listErrors.keySet().contains(Integer.valueOf(i))) {
                    res.append("<td><span class=\"xml-bad\" title=\"").append(StringEscapeUtils.escapeHtml(listErrors.get(i))).append("\">").append(string).append("</span></td>\n");
                } else if (listWarnings != null && listWarnings.keySet().contains(Integer.valueOf(i))) {
                    res.append("<td><span class=\"xml-warning\" title=\"").append(StringEscapeUtils.escapeHtml(listWarnings.get(i))).append("\">").append(string).append("</span></td>\n");
                } else
                    res.append("<td>").append(string).append("</td>\n");
            }
            res.append("</tr>\n");
            res.append("<tr>\n");
            i++;
        }
        res.append("</tr>\n</table>\n");
        return res.toString();
    }

    private static String updateTag(final String tag, final Integer numberElement) {
        if (tag == null || numberElement == null) {
            return "";
        }
        final String[] ll = tag.split("<.*?>");

        int number = 0;
        int id = 0;
        for (int i = 0; i < ll.length; i++) {
            String string = ll[i];
            string = string.replaceAll("&.*?;", "s");
            number = number + string.length();
            if (number > numberElement) {
                id = i;
                break;
            }
        }
        if (id == 0) {
            id = ll.length;
        }

        final List<String> listTagg = new ArrayList<>();
        final Pattern pat = Pattern.compile("<.*?>");
        final Matcher m = pat.matcher(tag);
        while (m.find()) {
            listTagg.add(m.group());
        }

        final StringBuilder buf = new StringBuilder();

        for (int i = 0; i < id; i++) {
            buf.append(ll[i]);
            buf.append(listTagg.get(i));
        }
        final String toModify = buf.toString();

        final String restOfTag = tag.substring(toModify.length());

        final Pattern pat2 = Pattern.compile("span class=\"(.*?)\"");
        final Matcher m2 = pat2.matcher(toModify);
        String res = toModify;
        while (m2.find()) {
            res = res.replaceAll(m2.group(), m2.group().subSequence(0, m2.group().length() - 1) + " xml-bad\"");
        }
        return res + restOfTag;
    }

    private static void highlightFile(final String name, final InputStream is, final OutputStream os, final String encoding, final boolean fragment,
            final boolean verbose) throws IOException {

        XhtmlRendererFactory.getRenderer(FileUtils.getExtension(name)).highlight(name, is, os, encoding, fragment);

    }

    private static String highlightFile(final String extension, final String inputString, final String encoding, final boolean fragment,
            final boolean verbose) throws IOException {

        return XhtmlRendererFactory.getRenderer(extension).highlight(extension, inputString, encoding, fragment);

    }
}

