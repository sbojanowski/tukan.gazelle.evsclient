/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.logs;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.action.AbstractLogBrowser;
import net.ihe.gazelle.evs.client.xml.model.CDAValidatedFile;
import net.ihe.gazelle.evs.client.xml.model.CDAValidatedFileQuery;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@Name("cdaSearch")
@Scope(ScopeType.PAGE)
public class CDASearch extends AbstractLogBrowser<CDAValidatedFile> {

    private static final long serialVersionUID = -3989216645433570741L;
    private static final Logger LOGGER = LoggerFactory.getLogger(CDASearch.class);

    @Override
    protected CDAValidatedFile getObjectByOID(final String oid, final String privacyKey) {
        return CDAValidatedFile.getCDAValidatedFileByOid(oid, privacyKey);
    }

    @Override
    protected Class<CDAValidatedFile> getEntityClass() {
        return CDAValidatedFile.class;
    }

    @Override
    protected HQLCriterionsForFilter<CDAValidatedFile> getCriterions() {
        final CDAValidatedFileQuery cdaValidatedFileQuery = new CDAValidatedFileQuery();
        final HQLCriterionsForFilter<CDAValidatedFile> result = cdaValidatedFileQuery.getHQLCriterionsForFilter();

        cdaValidatedFileQuery.objectIndexers().display().eq(Boolean.TRUE);

        result.addPath("standard", cdaValidatedFileQuery.standard().label());
        result.addPath("validationStatus", cdaValidatedFileQuery.validationStatus());
        result.addPath("privateUser", cdaValidatedFileQuery.privateUser());
        result.addPath("institutionKeyword", cdaValidatedFileQuery.institutionKeyword());
        result.addPath("mbvalidator", cdaValidatedFileQuery.mbvalidator());
        result.addPath("schematron", cdaValidatedFileQuery.schematron());
        result.addPath("validationDate", cdaValidatedFileQuery.validationDate());
        result.addPath("templateName", cdaValidatedFileQuery.objectIndexers().name());
        result.addPath("templateOid", cdaValidatedFileQuery.objectIndexers().identifier());
        result.addPath("templateExtension", cdaValidatedFileQuery.objectIndexers().extension());

        result.addQueryModifierForCriterion("templateName", this.queryModifierForCDA());
        result.addQueryModifierForCriterion("templateOid", this.queryModifierForCDA());
        result.addQueryModifierForCriterion("templateExtension", this.queryModifierForCDA());
        return result;
    }

    public QueryModifier<CDAValidatedFile> queryModifierForCDA() {
        return new QueryModifier<CDAValidatedFile>() {

            @Override
            public void modifyQuery(final HQLQueryBuilder<CDAValidatedFile> queryBuilder,
                    final Map<String, Object> filterValuesApplied) {
                final CDAValidatedFileQuery q = new CDAValidatedFileQuery(queryBuilder);
                q.objectIndexers().display().eq(Boolean.TRUE);
            }
        };
    }
    @Override
    public FilterDataModel<CDAValidatedFile> getDataModel() {
        return new FilterDataModel<CDAValidatedFile>(CDASearch.this.getFilter()) {
            @Override
            protected Object getId(final CDAValidatedFile t) {
                return t.getId();
            }
        };
    }




}
