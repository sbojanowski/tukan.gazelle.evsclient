package net.ihe.gazelle.evs.client.cdascorecard;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.jboss.seam.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Set;

/**
 * <b>Class Description : </b>ScorecardServiceClient<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 07/12/16
 * @class ScorecardServiceClient
 * @package net.ihe.gazelle.evs.client.cdascorecard
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */
public final class ScorecardServiceClient {

    private static final ApacheHttpClient4Executor CLIENT_EXECUTOR = new ApacheHttpClient4Executor();
    private static Logger LOGGER = LoggerFactory.getLogger(ScorecardServiceClient.class);
    public static final String UTF_8 = "UTF-8";
    private static final int OK = 200;

    private ScorecardServiceClient() {

    }

    public static String getScorecardFromValidator(String oid, String detailedResult, String validatorBaseUrl) throws Exception {
        if (validatorBaseUrl == null || validatorBaseUrl.isEmpty()) {
            LOGGER.error("Validator URL is null or empty, cannot contact validator");
            return null;
        }
        String b64DetailedResult;
        if (detailedResult != null && !detailedResult.isEmpty()) {
            b64DetailedResult = Base64.encodeBytes(detailedResult.getBytes(Charset.forName(UTF_8)));
        } else {
            LOGGER.error("Detailed result is either null or empty, cannot get scorecard");
            return null;
        }
        StringBuilder validatorUrl = new StringBuilder(validatorBaseUrl);
        if (!validatorBaseUrl.endsWith("/")) {
            validatorUrl.append('/');
        }
        validatorUrl.append("rest/scorecard/computeAndReturnImmediately");
        ClientRequest request = new ClientRequest(validatorUrl.toString(), CLIENT_EXECUTOR);
        request.accept(MediaType.APPLICATION_XML);
        request.body("text/plain", b64DetailedResult);
        request.queryParameter("oid", oid);
        ClientResponse<String> response;
        try {
            response = request.post(String.class);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
        if (OK == response.getStatus()) {
            String scorecard = response.getEntity();
            response.releaseConnection();
            return scorecard;
        } else {
            MultivaluedMap<String, String> headers = response.getHeaders();
            Set<String> headernames = headers.keySet();
            for (String head: headernames){
                LOGGER.info(head);
            }
            List<String> warnings = response.getHeaders().get("Warning");
            if (warnings != null && !warnings.isEmpty()) {
                StringBuilder errorMsg = new StringBuilder();
                for (String warning : warnings) {
                    errorMsg.append(warning);
                    errorMsg.append('\n');
                }
                response.releaseConnection();
                throw new Exception(errorMsg.toString());
            } else {
                LOGGER.error(response.getResponseStatus().name());
                response.releaseConnection();
                throw new Exception("Unable to get the scorecard. Tool returned error code: " + response.getResponseStatus());
            }
        }
    }

}
