/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.util.Util;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;

@Entity
@Name("validatedXDW")
@Table(name = "validated_xdw", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "validated_xdw_sequence", sequenceName = "validated_xdw_id_seq", allocationSize = 1)
public class ValidatedXDW extends AbstractValidatedXMLFile {

    private static final long serialVersionUID = -1443811873360866107L;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ValidatedXDW.class);

    private static final String XDW_FILE_PREFIX = "validatedXDW_";
    private static final String XDW_FILE_SUFFIX = ".xml";

    @Id
    @NotNull
    @GeneratedValue(generator = "validated_xdw_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "xdw_path")
    private String xdwPath;


    public ValidatedXDW(final ValidatedXDW inXDWObject) {
        this.xdwPath = inXDWObject.getXDWPath();
        this.description = inXDWObject.getDescription();
    }

    public ValidatedXDW() {

    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getXDWPath() {
        return this.xdwPath;
    }

    public void setXDWPath(final String xdwPath) {
        this.xdwPath = xdwPath;
    }

    @Override
    public void setFilePath(final String xdwPath) {
        this.xdwPath = xdwPath;
    }

    /**
     */
    public static ValidatedXDW createNewValidatedXDW() {
        ValidatedXDW message = new ValidatedXDW();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        message = em.merge(message);
        em.flush();
        if (message == null || message.getId() == 0) {
            ValidatedXDW.LOGGER.error("An error occurred when creating a new ValidatedAuditMessage object");
            return null;
        }
        final String repository = ApplicationPreferenceManager.getStringValue("xdw_repository");
        message.setXDWPath(
                repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ValidatedXDW.XDW_FILE_PREFIX
                        .concat(message.getId().toString()).concat(
                        ValidatedXDW.XDW_FILE_SUFFIX));
        return message;
    }

    /**
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends AbstractValidatedXMLFile> T addOrUpdate(final Class<T> clazz) {
        if (this.getXDWPath() == null || this.getXDWPath().isEmpty()) {
            ValidatedXDW.LOGGER.error("The file path is missing, cannot add this object !");
            return (T) this;
        } else {
            final File uploadedFile = new File(this.getXDWPath());
            if (!uploadedFile.exists()) {
                ValidatedXDW.LOGGER.error("The given file does not exist: {}", this.getXDWPath());
                return (T) this;
            }
            if (this.getOid() == null || this.getOid().length() == 0) {
                this.setOid(OIDGenerator.getNewOid());
            }
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            final ValidatedXDW validatedFile = em.merge(this);
            em.flush();

            //Update the cache with the newest values
            GazelleValidationCacheManager
                    .refreshGazelleCacheWebService(validatedFile.getExternalId(), validatedFile.getToolOid(),
                            validatedFile.getOid(), "off");

            return (T) validatedFile;
        }
    }

    @Override
    public String getFilePath() {
        return this.xdwPath;
    }

    @Override
    public String getSchematron() {
        return null;
    }

    @Override
    public void setSchematron(final String schematron) {
        return;
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/detailedResult.seam?type=XDW").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

}
