/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;

public enum Pages implements Page {

    SCHEMATRONS("/administration/schematrons.xhtml", "fa-file-text", "gazelle.evs.client.cda.schematrons.title",
            Authorizations.SCHEMATRON),

    STATISTICS("/administration/statistics.xhtml", "fa-bar-chart", "gazelle.evs.client.statistics",
            Authorizations.ALL),

    VALIDATION_SERVICES("/administration/validationServices.xhtml", "fa-wrench",
            "gazelle.evs.client.admin.menu.services", Authorizations.ADMIN),

    REFERENCED_STANDARDS("/administration/referencedStandards.xhtml", "fa-wrench",
            "gazelle.evs.client.admin.menu.standards", Authorizations.ADMIN),

    CONFIGURATION("/administration/applicationConfiguration.xhtml", "fa-wrench",
            "gazelle.evs.client.admin.menu.preferences", Authorizations.ADMIN),

    CALLING_TOOLS("/administration/callingTools.xhtml", "fa-wrench",
            "gazelle.evs.client.admin.menu.callingTools", Authorizations.ADMIN),

    ADMIN_TEMPLATES("/administration/adminTemplates.xhtml", "fa-wrench",
            "gazelle.evs.client.admin.menu.adminTemplates", Authorizations.ADMIN),

    MENU_CONFIG("/administration/menuConfiguration.xhtml", "fa-wrench",
            "gazelle.evs.client.admin.menu.menuConfiguration", Authorizations.ADMIN);

    private String link;

    private final Authorization[] authorizations;

    private String label;

    private final String icon;

    Pages(final String link, final String icon, final String label, final Authorization... authorizations) {
        this.link = link;
        this.authorizations = authorizations.clone();
        this.label = label;
        this.icon = icon;
    }

    @Override
    public String getId() {
        return this.name();
    }

    @Override
    public String getLink() {
        return this.link;
    }


    @Override
    public String getIcon() {
        return this.icon;
    }

    @Override
    public Authorization[] getAuthorizations() {

        if(this.authorizations != null) {
            return this.authorizations.clone();
        }
        else {
            return null;
        }
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public String getMenuLink() {
        return this.link.replace(".xhtml", ".seam");
    }

    private void setLink(final String link) {
        this.link = link;
    }

    private void setLabel(final String label) {
        this.label = label;
    }

}
