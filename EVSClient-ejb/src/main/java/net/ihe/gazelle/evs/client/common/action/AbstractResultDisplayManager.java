/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.evs.client.common.model.CallingTool;
import net.ihe.gazelle.evs.client.common.model.CallingTool.ToolType;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.apache.james.mime4j.util.CharsetUtil;
import org.jboss.seam.faces.FacesManager;
import org.jboss.seam.faces.FacesMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.xml.bind.DatatypeConverter;
import java.util.Map;

public abstract class AbstractResultDisplayManager<T extends ValidatedObject>{

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractResultDisplayManager.class);

    protected T selectedObject;
    private boolean validationDone;

    public boolean getValidationDone() {
        return this.validationDone;
    }

    public void setValidationDone(final boolean value) {
        this.validationDone = value;
    }

    public void downloadResult() {
        ReportExporterManager.exportToFile("text/xml", this.selectedObject.getDetailedResult(), "validationResult.xml");
    }

    public void downloadResult(final String oid) {
        this.init(oid, null);
        this.downloadResult();
    }

    public void initFromUrl() {
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String objectOid = null;
        if (urlParams != null && !urlParams.isEmpty()) {
            if (urlParams.containsKey("oid")) {
                objectOid = urlParams.get("oid");
            }
            this.init(objectOid, urlParams.get("privacyKey"));
        }

    }

    public void init(final String objectOid, final String privacyKey) {
        this.selectedObject = ValidatedObject.getObjectByOID(this.getEntityClass(), objectOid, privacyKey);
    }

    public String revalidate(final String oid) {
        this.init(oid, null);
        return this.revalidate();
    }

    /**
     * redirect to the validator page with the object oid as parameter
     */

    public String revalidate() {
        final StringBuilder url = new StringBuilder(this.getValidatorUrl());
        url.append("?oid=").append(this.selectedObject.getOid());
        final ReferencedStandard standard = this.selectedObject.getStandard();
        if (standard != null) {
            final String extension = standard.getExtension();
            if (extension != null && !extension.isEmpty()) {
                url.append("&extension=").append(extension);
            }
        }
        return url.toString();
    }

    protected abstract Class<T> getEntityClass();

    protected abstract String getValidatorUrl();

    public void downloadValidatedObject() {
        downloadFileContent(GetValidationInfo.TEXT_PLAIN);
    }

    protected void downloadFileContent(String mimeType){
        final String content = Util.getFileContentToString(this.selectedObject.getFilePath());
        final String fileName = this.selectedObject.getDescription();
        ReportExporterManager.exportToFile(mimeType, content, fileName);
    }

    public T getSelectedObject() {
        if (this.selectedObject == null){
            this.initFromUrl();
        }
        return this.selectedObject;
    }

    public void makeResultPublic() {
        this.selectedObject.setIsPublic(Boolean.TRUE);
        this.selectedObject.setPrivacyKey(null);
        final EntityManager entityManager = EntityManagerService.provideEntityManager();
        this.selectedObject = entityManager.merge(this.selectedObject);
        entityManager.flush();
    }

    public void makeResultPrivate() {
        this.selectedObject.setIsPublic(Boolean.FALSE);
        this.selectedObject.setPrivacyKey(null);
        final EntityManager entityManager = EntityManagerService.provideEntityManager();
        this.selectedObject = entityManager.merge(this.selectedObject);
        entityManager.flush();
    }

    public void shareResult() {
        this.selectedObject.generatePrivacyKey();
        final EntityManager entityManager = EntityManagerService.provideEntityManager();
        this.selectedObject = entityManager.merge(this.selectedObject);
        entityManager.flush();
    }

    public void sendPermaLink() {
        if (this.selectedObject.getOid() != null && this.selectedObject.getExternalId() != null
                && this.selectedObject.getToolOid() != null) {
            final CallingTool tool = CallingTool.getToolByOid(this.selectedObject.getToolOid());
            if (tool != null && tool.getUrl() != null && !tool.getUrl().isEmpty()) {
                final StringBuilder builder = new StringBuilder(tool.getUrl());
                //final String obj = Base64.encodeBase64String(this.selectedObject.getOid().getBytes(CharsetUtil.UTF_8));
                final String obj = DatatypeConverter.printBase64Binary(this.selectedObject.getOid().getBytes(CharsetUtil.UTF_8));

                if (tool.getToolType() == ToolType.PROXY) {
                    builder.append(this.redirectToProxy(this.selectedObject.getProxyType())).append("?id=");
                } else if (tool.getToolType() == ToolType.TM) {
                    builder.append("?fileId=");
                }
                builder.append(this.selectedObject.getExternalId()).append("&oid=").append(obj);
                FacesManager.instance().redirectToExternalURL(builder.toString());
            } else {
                FacesMessages.instance()
                        .add("OID " + this.selectedObject.getToolOid() + " does not match any known tool");
            }
        }
    }

    public String redirectToProxy(final String type) {

        if (type.contains("HL7")) {
            return "/messages/hl7.seam";
        }
        if (type.contains("RAW")) {
            return "/messages/raw.seam";
        }
        if (type.contains("DICOM")) {
            return "/messages/dicom.seam";
        }
        if (type.contains("SYSLOG")) {
            return "/messages/syslog.seam";
        }
        if (type.contains("HTTP")) {
            return "/messages/http.seam";
        } else {
            LOGGER.error("Message type is undefined for {}", type);
            return null;
        }
    }

    public boolean sendBackResultToTool() {
        if (this.selectedObject ==  null || this.selectedObject.getExternalId() == null) {
            return(false);
        } else {
            final Boolean returnedValue = (!CallingTool.toolIsTM(this.selectedObject.getToolOid())
                    || this.selectedObject.getExternalId().startsWith("sample_"));
            AbstractResultDisplayManager.LOGGER.info("The returned value from sendBackResultToTool is {}", returnedValue);
            return returnedValue;
        }
    }

    public abstract String performAnotherValidation();
}
