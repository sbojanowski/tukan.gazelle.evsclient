/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.model;

import org.ajax4jsf.model.DataVisitor;
import org.ajax4jsf.model.ExtendedDataModel;
import org.ajax4jsf.model.Range;
import org.richfaces.model.Arrangeable;
import org.richfaces.model.ArrangeableState;

import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;

public abstract class JPADataModel<T> extends ExtendedDataModel<T> implements Arrangeable {
  
    private Object rowKey;
    private ArrangeableState arrangeableState;
    private final Class<T> entityClass;
    public List<T> list= new ArrayList<>();
    public JPADataModel(final Class<T> entityClass) {
        super();
        this.entityClass = entityClass;
    }
 
    @Override
    public void arrange(final FacesContext context, final ArrangeableState state) {
        this.arrangeableState = state;
    }
 
    @Override
    public void setRowKey(final Object key) {
        this.rowKey = key;
    }
 
    @Override
    public Object getRowKey() {
        return this.rowKey;
    }
 


 

 
    protected ArrangeableState getArrangeableState() {
        return this.arrangeableState;
    }
 
    protected Class<T> getEntityClass() {
        return this.entityClass;
    }
 

 
   
 
    @Override
    public void walk(final FacesContext context, final DataVisitor visitor, final Range range, final Object argument) {
    
    	
 
        
 
        final List<T> data = this.list;
        for (final T t : data) {
            visitor.process(context, this.getId(t), argument);
        }
    }
 
    @Override
    public boolean isRowAvailable() {
        return this.rowKey != null;
    }
 
    @Override
    public int getRowCount() {
    	return this.list.size();
    }
 
    @Override
    public T getRowData() {
        return this.list.get((Integer) this.rowKey);
    }
 
    @Override
    public int getRowIndex() {
        return -1;
    }
 
    @Override
    public void setRowIndex(final int rowIndex) {
        throw new UnsupportedOperationException();
    }
 
    @Override
    public Object getWrappedData() {
        throw new UnsupportedOperationException();
    }
 
    @Override
    public void setWrappedData(final Object data) {
        throw new UnsupportedOperationException();
    }
 
    // TODO - implement using metadata
    protected abstract Object getId(T t);
}