/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.logs;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.evs.client.util.XpathUtils;
import net.ihe.gazelle.evs.client.xml.model.CDAValidatedFile;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.apache.commons.io.FileUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.async.Asynchronous;
import org.jboss.seam.async.QuartzDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

@Name("templatesUpdaterJob")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("templatesUpdaterJobLocal")
public class TemplatesUpdaterJob implements templatesUpdaterJobLocal, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 839966950305942919L;

    private static final Logger LOGGER = LoggerFactory.getLogger(TemplatesUpdaterJob.class);

    private final XMLResultDisplayManager xmlResultManager = new XMLResultDisplayManager();

    @Override
    public void templatesUpdaterJob(final List<Integer> iDsList) {

        LOGGER.warn("TemplatesUpdaterJob perform job");
        QuartzDispatcher.instance().scheduleAsynchronousEvent("updateList", iDsList);
    }

    @Override
    public void extractTemplatesJob(final CDAValidatedFile f) {

        LOGGER.warn("extractTemplatesJob perform job");
        QuartzDispatcher.instance().scheduleAsynchronousEvent("extractTemplates", f);
    }

    @Override
    @Observer("updateList")
    @Transactional
    @Asynchronous
    public void updateList(final List<Integer> iDsList) {
        final int size = iDsList.size();
        if (size > 0) {
            final int i = iDsList.get(0);
            iDsList.remove(0);
            try {
                final EntityManager em = EntityManagerService.provideEntityManager();

                CDAValidatedFile file = em.find(CDAValidatedFile.class, Integer.valueOf(i));
                this.xmlResultManager.initSimple(file, "CDA");
                this.xmlResultManager.initTemplatePanel();
                this.xmlResultManager.getListTemplatesArchitecture();

                file.setTemplatesListed(true);
                LOGGER.info("File id :{}, oid : {} updated.", Integer.valueOf(i), file.getOid());
                LOGGER.info("---------------------");
                LOGGER.info("{} cda files need to be checked", Integer.valueOf(size));
                LOGGER.info("---------------------");

            } catch (final JAXBException|RuntimeException e) {
                LOGGER.error("Error when adding templates in database : {}", e);
            } finally {
                QuartzDispatcher.instance().scheduleAsynchronousEvent("updateList", iDsList);
            }
        } else {
            LOGGER.warn("end update list");
        }
    }

    @Override
    @Observer("extractTemplates")
    @Transactional
    @Asynchronous
    public void extractTemplateFromXpath(final CDAValidatedFile f) {
        final File file = new File(f.getCdaFilePath());
        this.xmlResultManager.setSelectedObject(f);

        try {
            final String fileContent = FileUtils.readFileToString(file);
            final List<String> results = XpathUtils
                    .evaluatesByString(fileContent, "templateId", "//cda:ClinicalDocument/cda:templateId/@root");

            final ObjectIndexerManager objectIndexerManager = new ObjectIndexerManager();
            final String extension = this.xmlResultManager.getSelectedObject().getStandard().getExtension();
            List<ObjectIndexer> objectIndexersList = objectIndexerManager.addTemplatesIfNotExistFromXpath(results, extension);
            objectIndexersList = objectIndexerManager.removeDuplicate(objectIndexersList);
            this.xmlResultManager.saveObjectIndexerListInCDA(objectIndexersList);

            LOGGER.warn("end extract Template From Xpath");
        } catch (final ParserConfigurationException e) {
            LOGGER.error("Error when parsing the document : {}", e);
        } catch (final FileNotFoundException e) {
            LOGGER.error("File not found : {}", e);
        } catch (final SAXException e) {
            LOGGER.error("SAXError : {}", e);
        } catch (final IOException e) {
            LOGGER.error("IOException : {}", e);
        } catch (final OutOfMemoryError e) {
            LOGGER.error("OutOfMemoryError : {}", e);
        }

    }
}
