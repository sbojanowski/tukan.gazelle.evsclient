package net.ihe.gazelle.evs.client.xml.model;

/**
 * <p>FhirObjectType enum.</p>
 *
 * @author abe
 * @version 1.0: 14/12/17
 */
public enum FhirObjectType {

    JSON("JSON"),
    XML("XML"),
    URL("URL");

    private String label;

    FhirObjectType(String inLabel){
        this.label = inLabel;
    }

    public String getLabel() {
        return label;
    }

}
