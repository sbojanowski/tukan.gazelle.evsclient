/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.pdf.action;

import edu.harvard.hul.ois.jhove.HandlerBase;
import edu.harvard.hul.ois.jhove.JhoveBase;
import edu.harvard.hul.ois.jhove.JhoveException;
import edu.harvard.hul.ois.jhove.RepInfo;
import edu.harvard.hul.ois.jhove.handler.TextHandler;
import edu.harvard.hul.ois.jhove.handler.XmlHandler;
import edu.harvard.hul.ois.jhove.module.PdfModule;
import edu.harvard.hul.ois.jhove.module.pdf.PdfProfile;
import net.ihe.gazelle.evs.client.common.action.EmailNotificationManager;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.common.model.ValidationService;
import net.ihe.gazelle.evs.client.pdf.model.ValidatedPdf;
import net.ihe.gazelle.evs.client.tls.action.StringUtil;
import net.ihe.gazelle.evs.client.util.Util;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.io.*;
import java.util.*;

@Name("pdfValidationManagerBean")
@Scope(ScopeType.PAGE)
public class PdfValidationManager implements Serializable {

    private static final long serialVersionUID = 7920478668366520753L;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PdfValidationManager.class);


    private ValidatedPdf validatedPdf;
    private transient File uploadedFile;
    private boolean displayResult;
    private ValidationService service;
    private String standardDescription;
    private boolean validationDone;
    private static final String ACCEPTED_FILE_TYPES = "pdf, PDF";

    public String getAcceptedFileTypes() {
        return ACCEPTED_FILE_TYPES;
    }
    public String getStandardDescription() {
        return this.standardDescription;
    }

    public void setStandardDescription(final String standardDescription) {
        this.standardDescription = standardDescription;
    }

    public void init() {
        final Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.containsKey("id")) {
            this.displayResult = false;
            final Integer id =  Integer.valueOf(params.get("id"));
            this.validatedPdf = ValidatedObject.getObjectByID(ValidatedPdf.class, id, null);
        } else if (params.containsKey("oid")) {
            this.displayResult = false;
            final ValidatedPdf pdfToValidateAgain = ValidatedObject
                    .getObjectByOID(ValidatedPdf.class, params.get("oid"), null);
            if (pdfToValidateAgain != null) {
                this.validatedPdf = new ValidatedPdf(pdfToValidateAgain);
            } else {
                FacesMessages.instance()
                        .addFromResourceBundle("net.ihe.gazelle.evs.pdf.OIDDoesnotMatchObjectInDBErrorMessage");
            }
        }
        this.service = ValidationService.getValidationServiceByKeyword("JHOVE");

        final ReferencedStandard selectedStandard = this.service.getStandards().get(0);
        if (selectedStandard != null && selectedStandard.getDescription() != null && !selectedStandard.getDescription()
                .isEmpty()) {
            this.setStandardDescription(selectedStandard.getDescription());
        }
        if (this.validatedPdf != null && this.validatedPdf.isLucky()) {
            this.validateMessage();
        }

    }

    public ValidatedPdf getValidatedPdf() {
        return this.validatedPdf;
    }

    public void setValidatedPdf(final ValidatedPdf validatedPdf) {
        this.validatedPdf = validatedPdf;
    }

    public void reset() {
        this.displayResult = false;
        this.uploadedFile = null;
        this.validatedPdf = null;
        this.setValidationDone(false);
    }

    public void uploadEventListener(final FileUploadEvent event) {
        FileOutputStream fos = null;
        try {
            this.validatedPdf = ValidatedPdf.createNewValidatedPdf();
            final UploadedFile item = event.getUploadedFile();
            this.uploadedFile = new File(this.validatedPdf.getPdfPath());
            final boolean folderExisted = this.uploadedFile.getParentFile().exists() || this.uploadedFile.getParentFile().mkdirs();
            if (!folderExisted) {
                throw new IOException("Unable to create path");
            }
            // when uploading a file from Windows the whole path is contained in
            // item.filename
            // we need to split this string according the \ character and take
            // only the last part
            // String[] splitFilename = item.getName().split("\\");
            // selectedAuditMessage.setDescription(splitFilename[splitFilename.length
            // - 1]);
            this.validatedPdf.setDescription(item.getName());
            if (item.getData() != null && item.getData().length > 0) {
                fos = new FileOutputStream(this.uploadedFile);
                fos.write(item.getData());
            } else {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.evs.client.cda.file.empty");
                this.validatedPdf = null;
            }
        } catch (final IOException e) {
            LOGGER.error("uploadEventListener: an error occurred - {}", e.getMessage());
            return;
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (final IOException e) {
                    LOGGER.error("not able to close the FOS", e);
                }
            }
        }
    }

    public void validateMessage() {
        if (this.validatedPdf == null) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.evs.client.pdf.error.no.file");
            this.displayResult = false;
            return;
        } else if (this.validatedPdf.getPdfPath() == null || this.validatedPdf.getPdfPath().isEmpty()) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.evs.client.cda.file.empty");
            this.displayResult = false;
            this.validatedPdf = null;
            return;
        } else {
            this.validatePdf();
            this.displayResult = true;
        }
    }

    private static class PdfModuleProfiles extends PdfModule {
        public List getProfiles() {
            return this._profile;
        }
    }

    private void validatePdf() {
        // Comment this line because it adds a clone line in DB. Don't know why !
        // validatedPdf = new ValidatedPdf(validatedPdf);

        this.validatedPdf.setUserIp(Util.getUserIpAddress());
        this.validatedPdf.updateCountryStatistics();
        this.validatedPdf.setStandard(null);
        this.validatedPdf.setService(this.service);

        try {
            // Analyze
            final File pdfFile = new File(this.validatedPdf.getPdfPath());

            final boolean folderExisted = pdfFile.getParentFile().exists() || pdfFile.getParentFile().mkdirs();
            if (!folderExisted) {
                throw new IOException("Unable to create path");
            }

            final PdfModuleProfiles module = new PdfModuleProfiles();
            module.setDefaultParams(new ArrayList<String>());
            final RandomAccessFile raf = new RandomAccessFile(pdfFile, "r");
            final RepInfo info = new RepInfo("");
            module.parse(raf, info);
            raf.close();

            // Export
            final String resultTxt = this.getResult(info, new TextHandler());
            final String resultXml = this.getResult(info, new XmlHandler());
            this.validatedPdf.setValidationServiceVersion(info.getVersion());
            final List<String> profiles = info.getProfile();
            String prefix = "PDF ";
            String suffix = " (not PDF/A)";
            this.validatedPdf.setPdfProfileColor("FAILED");
            for (final String profile : profiles) {
                if (profile.contains("PDF/A")) {
                    prefix = "PDF/A ";
                    suffix = "";
                    this.validatedPdf.setPdfProfileColor("PASSED");

                }
            }
            this.validatedPdf.setValidationStatus("FAILED");
            switch (info.getValid()) {
            case RepInfo.TRUE:
                this.validatedPdf.setPdfValid("Valid");
                this.validatedPdf.setPdfValidColor("PASSED");
                break;
            case RepInfo.FALSE:
                this.validatedPdf.setPdfValid("Not valid");
                this.validatedPdf.setPdfValidColor("FAILED");
                break;
            default:
                this.validatedPdf.setPdfValid("Unknown");
                this.validatedPdf.setPdfValidColor("UNKNOWN");
                break;
            }
            switch (info.getWellFormed()) {
            case RepInfo.TRUE:
                this.validatedPdf.setPdfWellFormed("Well-formed");
                this.validatedPdf.setPdfWellFormedColor("PASSED");
                switch (info.getValid()) {
                case RepInfo.TRUE:
                   // validatedPdf.setValidationStatus(prefix + "well-formed and valid" + suffix);
                    if ("PDF/A".equals(prefix)) {
                        this.validatedPdf.setValidationStatus("PASSED");
                    }
                    break;
                case RepInfo.FALSE:
                  //  validatedPdf.setValidationStatus(prefix + "well-formed, but not valid" + suffix);
                    break;
                default:
                 //   validatedPdf.setValidationStatus(prefix + "well-formed" + suffix);
                    break;
                }
                break;
            case RepInfo.FALSE:
                this.validatedPdf.setPdfWellFormed("Not well-formed");
                this.validatedPdf.setPdfWellFormedColor("FAILED");
                break;
            default:
                this.validatedPdf.setPdfWellFormed("Unknown");
                this.validatedPdf.setPdfWellFormedColor("UNKNOWN");
                break;
            }

            final List<PdfProfile> moduleProfiles = module.getProfiles();
            final List<String> moduleTxtProfiles = new ArrayList<>(moduleProfiles.size());
            for (final Object object : moduleProfiles) {
                final PdfProfile profile = (PdfProfile) object;
                moduleTxtProfiles.add(profile.getText());
            }

            Collections.sort(moduleTxtProfiles);
            Collections.sort(profiles);

            this.validatedPdf.setTestedPdfProfiles(StringUtils.join(moduleTxtProfiles, "; "));
            this.validatedPdf.setPdfProfiles(StringUtils.join(profiles, "; "));

            this.validatedPdf.setDetailedResult("Validated using " + this.service.getName() + ": " + resultTxt);
            this.validatedPdf.setResultXml(resultXml);

            this.validatedPdf.setValidationDate(new Date());
        } catch (JhoveException|IOException e) {
            LOGGER.error(String.format("an unexpected error occurred during the validation: %s", e.getMessage()));
            this.updateAbortedValidation(e);
        }
        LOGGER.info("validation done");
        this.setValidationDone(true);
        this.validatedPdf = ValidatedPdf.addOrUpdate(this.validatedPdf);
    }

    private String getResult(final RepInfo info, final HandlerBase handler) throws JhoveException {
        final JhoveBase je = new JhoveBase();
        je.setSignatureFlag(false);
        handler.setBase(je);
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);
        handler.reset();
        handler.setWriter(pw);
        info.show(handler);
        pw.close();
        final String result = sw.toString();
        return result;
    }

    private void updateAbortedValidation(final Exception exception) {
        LOGGER.info("validation cannot be performed: recording result");
        EmailNotificationManager.send(exception, "TLS Validation cannot be performed", null);
        this.validatedPdf.setValidationStatus("VALIDATION NOT PERFORMED");
        this.validatedPdf.setValidationDate(new Date());
    }

    public File getUploadedFile() {
        return this.uploadedFile;
    }

    public void setUploadedFile(final File uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public boolean isDisplayResult() {
        return this.displayResult;
    }

    public void setDisplayResult(final boolean displayResult) {
        this.displayResult = displayResult;
    }

    public static String rnTobr(final String result) {
        final String splitted = StringUtil.wrap(result, 80);
        return splitted.replaceAll(" ", "&nbsp;").replaceAll("\r\n", "<br \\>").replaceAll("\r", "<br \\>")
                .replaceAll("\n", "<br \\>");
    }

    public boolean getValidationDone() {
        return this.validationDone;
    }

    public void setValidationDone(final boolean validationDone) {
        this.validationDone = validationDone;
    }

    public void setMessageInvalidFileType(){
        FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Invalid file type. Expected file types are : "+ this
                .getAcceptedFileTypes());
    }

}
