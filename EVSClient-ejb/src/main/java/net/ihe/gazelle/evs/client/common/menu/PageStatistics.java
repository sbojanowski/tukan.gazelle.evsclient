/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;

public class PageStatistics implements Page {

    private final String url;

    public PageStatistics(final String type) {
        this.url = "/administration/statisticsByType.seam?type=" + type;
    }

    public PageStatistics(final String validatorFriendlyName, final String extension, String standardLabel) {
        final StringBuilder builder = new StringBuilder("/administration/statisticsByType.seam?");
        builder.append(EVSMenu.TYPE_PARAM);
        builder.append('=');
        builder.append(validatorFriendlyName);

        if (extension != null && !extension.isEmpty()) {
            builder.append('&');
            builder.append(EVSMenu.EXTENSION_PARAM);
            builder.append('=');
            builder.append(extension);
        }

        if (standardLabel != null && !standardLabel.isEmpty()){
            builder.append('&');
            builder.append(EVSMenu.STANDARD_PARAM);
            builder.append('=');
            builder.append(standardLabel);
        }
        this.url = builder.toString();
    }

    @Override
    public String getId() {
        return this.url;
    }

    @Override
    public String getLabel() {
        return "gazelle.evs.client.statistics";
    }

    @Override
    public String getLink() {
        return this.url;
    }

    @Override
    public String getIcon() {
        return "fa fa-bar-chart";
    }

    @Override
    public Authorization[] getAuthorizations() {
        return new Authorization[] { Authorizations.ALL };
    }

    @Override
    public String getMenuLink() {
        return this.getLink().replace(".xhtml", ".seam");
    }

}
