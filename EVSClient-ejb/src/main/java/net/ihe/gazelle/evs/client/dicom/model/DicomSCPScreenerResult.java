/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.dicom.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.users.action.SSOIdentity;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.apache.commons.lang.RandomStringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Date;

@Entity
@Name("dicomSCPScreenerResult")
@Table(name = "dcm_scp_screener_result", schema = "public")
@SequenceGenerator(name = "dcm_scp_screener_result_sequence", sequenceName = "dcm_scp_screener_result_id_seq", allocationSize = 1)
public class DicomSCPScreenerResult implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7673617575717502145L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DicomSCPScreenerResult.class);

    @Id
    @GeneratedValue(generator = "dcm_scp_screener_result_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "hostname")
    @NotNull
    private String hostname;

    @Column(name = "port")
    @NotNull
    private Integer port;

    @Column(name = "aet")
    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9_ ]{0,16}$", message = "AE Title is invalid: no more than 16 characters, alphanumerical characters and or '_' ")
    private String aETitle;

    private String username;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "timestamp")
    private Date timestamp;

    @Column(name = "result")
    private byte[] result;

    @Column(name = "oid")
    private String oid;

    @Column(name = "private_user")
    private String privateUser;


    @Column(name = "institution_keyword")
    private String institutionKeyword;

    /**
     * this attribute is already set to true when the user is not logged when the user is logged, the default is false but the user is allowed to change it to make it public
     */
    @Column(name = "is_public")
    private Boolean isPublic;

    /**
     * when isPublic = false, the user can choose to share this validation result with some others. A privacy key is generated and will be provided in the permanent link as an authentication process
     */
    @Column(name = "privacy_key")
    private String privacyKey;


    public String getPrivateUser() {
        return this.privateUser;
    }

    public void setPrivateUser(final String privateUser) {
        this.privateUser = privateUser;
    }

    public String getOid() {
        return this.oid;
    }

    protected String validationStatus = "N/A";

    public void setOid(final String oid) {
        this.oid = oid;
    }

    public Date getTimestamp() {
        if (this.timestamp != null){
            return (Date) this.timestamp.clone();
        }
        else {
            return null;
        }
    }

    public void setTimestamp(final Date ts) {
        if(ts != null){
            this.timestamp = (Date) ts.clone();
        }
        else {
            this.timestamp = null;
        }
    }

    public void setResult(final byte[] result) {
        final byte[] resultTmp = result.clone();
        this.result = resultTmp;
    }

    public DicomSCPScreenerResult() {
        this.timestamp = new Date();
    }

    public DicomSCPScreenerResult(final DicomSCPScreenerResult inResult) {
        this();
        this.aETitle = inResult.getaETitle();
        this.hostname = inResult.getHostname();
        this.port = inResult.getPort();
    }

    public DicomSCPScreenerResult save() {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        final DicomSCPScreenerResult dicomResult = entityManager.merge(this);
        entityManager.flush();
        return dicomResult;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getResult() {
        if (this.result == null) {
            return null;
        }
        return new String(this.result, StandardCharsets.UTF_8);
    }

    public void setResult(final String result) {
        if (result != null) {
            this.result = result.getBytes(StandardCharsets.UTF_8);
        }
    }

    public String getHostname() {
        return this.hostname;
    }

    public void setHostname(final String hostname) {
        this.hostname = hostname;
    }

    public Integer getPort() {
        return this.port;
    }

    public void setPort(final Integer port) {
        this.port = port;
    }

    public String getaETitle() {
        return this.aETitle;
    }

    public void setaETitle(final String aETitle) {
        this.aETitle = aETitle;
    }

    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/dicomSCPScreenerResult.seam?").append("oid=").append(this.getOid());
        if (this.getPrivacyKey() != null && !this.getPrivacyKey().isEmpty()) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

    public static DicomSCPScreenerResult getObjectByOID(final String inOID, final String privacyKey) {
        if (inOID == null || inOID.length() == 0) {
            return null;
        }

        final DicomSCPScreenerResultQuery query = new DicomSCPScreenerResultQuery();
        query.oid().like(inOID);
        if (privacyKey != null && !privacyKey.isEmpty()) {
            query.privacyKey().eq(privacyKey);
        } else {
            boolean monitor = false;
            String login = null;
            String institutionKeyword = null;

            final SSOIdentity instance = SSOIdentity.instance();
            if (instance != null && instance.isLoggedIn()) {
                login = instance.getCredentials().getUsername();
                institutionKeyword = instance.getInstitutionKeyword();
                if (instance.hasRole("monitor_role") || instance.hasRole("admin_role")) {
                    monitor = true;
                }
            }

            // show all messages if monitor else show only public results and those performed by the logged in user
            if (!monitor) {
                if (login == null) {
                    query.addRestriction(DicomSCPScreenerResult.getIsPublicRestriction(query));
                } else {
                    query.addRestriction(HQLRestrictions
                            .or(DicomSCPScreenerResult.getIsPublicRestriction(query),
                                    query.privateUser().eqRestriction(login),
                                    query.institutionKeyword().eqRestriction(institutionKeyword)));
                }
            }
        }
        return query.getUniqueResult();
    }

    private static HQLRestriction getIsPublicRestriction(final DicomSCPScreenerResultQuery query) {
        return HQLRestrictions.or(query.isPublic().eqRestriction(Boolean.TRUE), query.isPublic().eqRestriction(null));
    }

    public Integer getId() {
        return this.id;
    }

    public Boolean getIsPublic() {
        return this.isPublic;
    }

    public void setIsPublic(final Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public String getPrivacyKey() {
        return this.privacyKey;
    }

    public void setPrivacyKey(final String privacyKey) {
        this.privacyKey = privacyKey;
    }

    /**
     * generate a privacy key made of 16 alphanumrical characters
     */
    public void generatePrivacyKey() {
        this.setPrivacyKey(RandomStringUtils.randomAlphanumeric(16));
    }

    public String getValidationStatus() {
        return this.validationStatus;
    }

    public void setValidationStatus(final String validationStatus) {
        this.validationStatus = validationStatus;
    }

    public Boolean isDicomValidatedObject() {
        return Boolean.FALSE;
    }
    public String getInstitutionKeyword() {
        return this.institutionKeyword;
    }

    public void setInstitutionKeyword(String institutionKeyword) {
        this.institutionKeyword = institutionKeyword;
    }
}
