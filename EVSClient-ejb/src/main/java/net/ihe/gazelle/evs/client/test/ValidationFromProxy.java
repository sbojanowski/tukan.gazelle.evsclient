/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.test;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.*;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.Pattern;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Name("validationFromProxy")
@Scope(ScopeType.PAGE)
public class ValidationFromProxy implements Serializable {

    private static final long serialVersionUID = -2702947973129989361L;
    private String fileName;
    @Pattern(regexp = "HTTP|SYSLOG|RAW|DICOM|HL7", message="Possible values are HTTP, SYSLOG, RAW, DICOM or HL7" )
    private String fileType;


    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationFromProxy.class);


    private static final String APPLICATION_URL = ApplicationPreferenceManager.getStringValue("application_url") +(ApplicationPreferenceManager.getStringValue("application_url").endsWith("/")?"":"/");

    public void validate() {

        // First send the file to EVSClient
        final HttpClient client = new HttpClient();
        String url = ValidationFromProxy.APPLICATION_URL +"error.seam";
        final PostMethod filePost = new PostMethod(ValidationFromProxy.APPLICATION_URL +"upload");
       // final HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance()
       //         .getExternalContext().getResponse();

        ValidationFromProxy.LOGGER.info("filename is{}", this.fileName);
        ValidationFromProxy.LOGGER.info("fileType is{}", this.fileType);
        // use the type as file name
        final File file = new File(this.fileName);

        int status;

        try {
            final PartSource partSource = new FilePartSource(this.fileType, file);
            final Part[] parts = { new FilePart("message", partSource) };
            filePost.setRequestEntity(new MultipartRequestEntity(parts, filePost.getParams()));
            status = client.executeMethod(filePost);


        } catch (final IOException e) {
            status = -1;
            ValidationFromProxy.LOGGER.error(e.getMessage());
        }
        try {

            // redirect the user to the good page
            if (status == HttpStatus.SC_OK) {

                final String key = filePost.getResponseBodyAsString();
                final String encodedKey = URLEncoder.encode(key, StandardCharsets.UTF_8.toString());
                  url = ValidationFromProxy.APPLICATION_URL + "validate.seam?file=" + encodedKey;
               // response.sendRedirect(url);
                ValidationFromProxy.LOGGER.info(url);
            }

        } catch (final IOException e) {
            ValidationFromProxy.LOGGER.error(e.getMessage());
        }
        FacesManager.instance().redirectToExternalURL(url);
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileType(final String fileType) {
        this.fileType = fileType;
    }

    public String getFileType() {
        return this.fileType;
    }
}
