/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.logs;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.action.AbstractLogBrowser;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.xml.model.ValidatedFHIR;
import net.ihe.gazelle.evs.client.xml.model.ValidatedFHIRQuery;
import net.ihe.gazelle.evs.client.xml.model.ValidatedXML;
import net.ihe.gazelle.evs.client.xml.model.ValidatedXMLQuery;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("fhirSearch")
@Scope(ScopeType.PAGE)
public class FHIRSearch extends AbstractLogBrowser<ValidatedFHIR> {

    private static final long serialVersionUID = -3989216645433570741L;

    @Override
    protected ValidatedFHIR getObjectByOID(final String oid, final String privacyKey) {
        return ValidatedObject.getObjectByOID(ValidatedFHIR.class, oid, privacyKey);
    }

    @Override
    protected Class<ValidatedFHIR> getEntityClass() {
        return ValidatedFHIR.class;
    }

    @Override
    protected HQLCriterionsForFilter<ValidatedFHIR> getCriterions() {
        final ValidatedFHIRQuery fhirValidatedFileQuery = new ValidatedFHIRQuery();
        final HQLCriterionsForFilter<ValidatedFHIR> result = fhirValidatedFileQuery.getHQLCriterionsForFilter();

        result.addPath("standard", fhirValidatedFileQuery.standard().label());
        result.addPath("validationStatus", fhirValidatedFileQuery.validationStatus());
        result.addPath("privateUser", fhirValidatedFileQuery.privateUser());
        result.addPath("institutionKeyword", fhirValidatedFileQuery.institutionKeyword());
        result.addPath("mbvalidator", fhirValidatedFileQuery.mbvalidator());
        result.addPath("schematron", fhirValidatedFileQuery.schematron());
        result.addPath("validationDate", fhirValidatedFileQuery.validationDate());
        return result;
    }

    @Override
    public FilterDataModel<ValidatedFHIR> getDataModel() {
        return new FilterDataModel<ValidatedFHIR>(FHIRSearch.this.getFilter()) {
            @Override
            protected Object getId(final ValidatedFHIR t) {
                return t.getId();
            }
        };
    }
}
