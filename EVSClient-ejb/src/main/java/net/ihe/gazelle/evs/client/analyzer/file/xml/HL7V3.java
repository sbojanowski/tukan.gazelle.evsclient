/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file.xml;

import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.analyzer.utils.XmlUtils;
import net.ihe.gazelle.evs.client.util.ValidationContextHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HL7V3 extends Analyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(HL7V3.class);

    @Override
    public Logger getLog() {
        return HL7V3.LOGGER;
    }

    @Override
    public String getType() {
        return "HL7V3";
    }

    public static boolean containsHl7v3MessageId(final String fileContent, final MessageContentAnalyzer mca) {
        if (fileContent != null && !fileContent.isEmpty()) {
            // Verify second part of pattern (match PRPA_...)
            String hl7v3Reg = "([A-Z]{4}_[A-Z]{2}[0-9]{6})([A-Z]{2}[0-9]{2})";
            final ValidationContextHandler vch = new ValidationContextHandler(fileContent);

            Pattern regex = Pattern.compile(hl7v3Reg, Pattern.DOTALL);
            Matcher regexMatcher = regex.matcher(fileContent);
            if (regexMatcher.find()) {
                HL7V3.LOGGER.info("HL7V3 :\n{}", regexMatcher.group());
                hl7v3Reg = '(' + XmlUtils.SCHEME_REGEX + "?[A-Z]{4}_[A-Z]{2}[0-9]{6})([A-Z]{2}[0-9]{2})";

                regex = Pattern.compile(hl7v3Reg, Pattern.DOTALL);
                regexMatcher = regex.matcher(fileContent);
                if (regexMatcher.find()) {
                    // if second part of pattern is good add it !!
                    vch.setPatternHl7v3(regexMatcher.group(1) + regexMatcher.group(4));
                    vch.parseMessageContext();
                }
            }

            if (vch.isHl7v3()) {
                hl7v3Reg = "(<" + XmlUtils.SCHEME_REGEX
                        + "?[A-Z]{4}_[A-Z]{2}[0-9]{6})([A-Z]{2}[0-9]{2})(.+)([A-Z]{4}_[A-Z]{2}[0-9]{6})([A-Z]{2}[0-9]{2}>)";
                regex = Pattern.compile(hl7v3Reg, Pattern.DOTALL);
                regexMatcher = regex.matcher(fileContent);
                if (regexMatcher.find()) {
                    HL7V3.LOGGER.info("hl7v3 :\n{}", regexMatcher.group());
                    mca.setOffsetStart(regexMatcher.start());
                    mca.setOffsetEnd(regexMatcher.end());
                }
                return true;
            }
        } else {
            HL7V3.LOGGER.error("fileContent is null !");
        }
        return false;
    }

    public static boolean containsHl7v3MessageId(final File file, final MessageContentAnalyzer mca) {
        final String fileContent = AnalyzerFileUtils.convertFileToString(file);
        return HL7V3.containsHl7v3MessageId(fileContent, mca);
    }
}
