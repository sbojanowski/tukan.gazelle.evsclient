/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.util.Util;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;

@Entity
@Name("validatedDSUB")
@Table(name = "validated_dsub", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "validated_dsub_sequence", sequenceName = "validated_dsub_id_seq", allocationSize = 1)
public class ValidatedDSUB extends AbstractValidatedXMLFile {

    private static final long serialVersionUID = -1443811873360866107L;


    private static final Logger LOGGER = LoggerFactory.getLogger(ValidatedDSUB.class);

    private static final String DSUB_FILE_PREFIX = "validatedDSUB_";
    private static final String DSUB_FILE_SUFFIX = ".xml";

    @Id
    @NotNull
    @GeneratedValue(generator = "validated_dsub_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "dsub_path")
    private String dsubPath;


    public ValidatedDSUB(final ValidatedDSUB inDSUBObject) {
        this.dsubPath = inDSUBObject.getDSUBPath();
        this.description = inDSUBObject.getDescription();
    }

    public ValidatedDSUB() {

    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getDSUBPath() {
        return this.dsubPath;
    }

    public void setDSUBPath(final String dsubPath) {
        this.dsubPath = dsubPath;
    }

    @Override
    public void setFilePath(final String dsubPath) {
        this.dsubPath = dsubPath;
    }

    /**
     */
    public static ValidatedDSUB createNewValidatedDSUB() {
        ValidatedDSUB message = new ValidatedDSUB();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        message = em.merge(message);
        em.flush();
        if (message == null || message.getId() == 0) {
            ValidatedDSUB.LOGGER.error("An error occurred when creating a new ValidatedAuditMessage object");
            return null;
        }
        final String repository = ApplicationPreferenceManager.getStringValue("dsub_repository");
        message.setDSUBPath(
                repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ValidatedDSUB.DSUB_FILE_PREFIX
                        .concat(message.getId().toString()).concat(
                        ValidatedDSUB.DSUB_FILE_SUFFIX));
        return message;
    }

    /**
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends AbstractValidatedXMLFile> T addOrUpdate(final Class<T> clazz) {
        if (this.getDSUBPath() == null || this.getDSUBPath().isEmpty()) {
            ValidatedDSUB.LOGGER.error("The file path is missing, cannot add this object !");
            return (T) this;
        } else {
            final File uploadedFile = new File(this.getDSUBPath());
            if (!uploadedFile.exists()) {
                ValidatedDSUB.LOGGER.error("The given file does not exist: {}", this.getDSUBPath());
                return (T) this;
            }
            if (this.getOid() == null || this.getOid().length() == 0) {
                this.setOid(OIDGenerator.getNewOid());
            }
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            final ValidatedDSUB validatedFile = em.merge(this);
            em.flush();

            //Update the cache with the newest values
            GazelleValidationCacheManager
                    .refreshGazelleCacheWebService(validatedFile.getExternalId(), validatedFile.getToolOid(),
                            validatedFile.getOid(), "off");

            return (T) validatedFile;
        }
    }

    @Override
    public String getFilePath() {
        return this.dsubPath;
    }

    @Override
    public String getSchematron() {
        return null;
    }

    @Override
    public void setSchematron(final String schematron) {
        return;
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/detailedResult.seam?type=DSUB").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

}
