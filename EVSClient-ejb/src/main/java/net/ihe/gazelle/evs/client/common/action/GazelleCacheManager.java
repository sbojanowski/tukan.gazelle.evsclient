/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

/**
 * @author jlabbe
 * @version %I%, %G%
 */
public class GazelleCacheManager {

    private static final int TIME_TO_LIVE_IN_CACHE_IN_SECONDS = 3600;
    private static final CacheManager CACHE_MANAGER = CacheManager.newInstance();

    /**
     * Method queryWithCache. Perform the request using cache enhancements. It can return the requested value by performing a method call or performing a cache request
     * <p/>
     * <pre>
     * Behavior If the Value is not found in the cache or if queryCache parameter is "off" then it performs the request and stores the result in the cache <!--  -->
     * If the Value is found in then the value is returned
     * </pre>
     *
     * @param request     GazelleCacheRequest
     * @param objectId    String specifying the id of the object to query
     * @param objectType  String specifying the object type to query
     * @param request_url String in the form EVSClient:GetValidationStatusAndPermanentLink
     * @param queryCache  A String that specifies if the cache as to be used for the query (if the value is "off" do not use the cache. Otherwize use it.
     * @return String
     */
    public static String queryWithCache(final GazelleCacheRequest request, final String objectId, final String objectType,
                                        final String request_url, final String queryCache) {
        String result;

        final Ehcache cache = GazelleCacheManager.CACHE_MANAGER.getEhcache("resteasyCache");
        final String cacheId = GazelleCacheManager.cacheId(request_url, objectType, objectId);
        if (cache != null) {

            final Element element = cache.get(cacheId);
            if (null == element || "off".equals(queryCache)) {
                result = getResultAndCacheIt(request, objectId, objectType, cache, cacheId);
            } else {
                result = (String) element.getObjectValue();
                if (result == null) {
                    result = getResultAndCacheIt(request, objectId, objectType, cache, cacheId);
                }
            }

        } else {
            result = request.get(objectId, objectType);
        }
        return result;
    }

    private static String getResultAndCacheIt(GazelleCacheRequest request, String objectId, String objectType, Ehcache cache, String cacheId) {
        String result;
        result = request.get(objectId, objectType);
        GazelleCacheManager.cacheElement(result, cache, cacheId);
        return result;
    }

    private static void cacheElement(final String result, final Ehcache cache, final String cacheId) {
        final Element elm = new Element(cacheId, result);
        elm.setTimeToLive(GazelleCacheManager.TIME_TO_LIVE_IN_CACHE_IN_SECONDS);
        cache.put(elm);
    }

    /**
     * Method cacheId. Returns an id build with the given infos. Ids must be unique, so here we have a concatenation of: objectType:request_url:objectId separated by dash.
     */
    private static String cacheId(final String request_url, final String objectType, final String objectId) {
        return objectType + ':' + request_url + ':' + objectId;
    }
}
