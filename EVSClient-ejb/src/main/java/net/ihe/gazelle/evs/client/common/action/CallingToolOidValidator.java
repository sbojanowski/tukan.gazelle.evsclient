/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;
import java.util.regex.Pattern;

/**
 * Created by epoiseau ON 14/06/2016.
 */

@Name("callingToolOidValidator")
@Scope(ScopeType.PAGE)
public class CallingToolOidValidator implements Validator, Serializable {

    private static final Pattern UID_PATTERN = Pattern.compile("([0-9]+\\.)*[0-9]+");
    private static final long serialVersionUID = 6870103477502177830L;

    @Override
    public void validate(final FacesContext context, final UIComponent component,
            final Object object) throws ValidatorException {
        final String oid = String.valueOf(object);
        final CallingToolManager manager = new CallingToolManager();
        FacesMessage message =  new FacesMessage(
                FacesMessage.SEVERITY_ERROR, "Error in the validator 'callingToolOidValidator'",
                "Case not handled in validator 'callingToolOidValidator'");
        boolean valid = true;
        if (!manager.checkOidIsUnique(oid)) {
            valid = false;
            message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "OID already exist",
                    "OID must be unique. '" + oid + "' is already in use.");
        } else if (!CallingToolOidValidator.UID_PATTERN.matcher(oid).matches()) {
            valid = false;
            message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Value is not a valid OID",
                    "The string '" + oid + "' does not match the UID pattern.");
        }
        if (!valid) {
            throw new ValidatorException(message);
        }
    }
}

