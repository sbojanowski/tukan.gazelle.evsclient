/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.util.Arrays;

@Entity
@Name("validatedFHIR")
@Table(name = "validated_fhir", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "validated_fhir_sequence", sequenceName = "validated_fhir_id_seq", allocationSize = 1)
public class ValidatedFHIR extends AbstractValidatedXMLFile {

    private static final long serialVersionUID = -1443811873360866107L;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ValidatedFHIR.class);


    private static final String XML_FILE_PREFIX = "validatedFHIR_";
    private static final String XML_FILE_SUFFIX = ".xml";
    private static final String JSON_FILE_SUFFIX = ".json";

    @Id
    @NotNull
    @GeneratedValue(generator = "validated_fhir_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "resource_path")
    private String resourcePath;

    @Column(name = "schematron")
    private String schematron;

    @Column(name = "request_url")
    private String requestUrl;

    @Column(name = "validated_object_type")
    private FhirObjectType validatedObjectType;


    public ValidatedFHIR(final ValidatedFHIR inXMLObject) {
        this.resourcePath = inXMLObject.getResourcePath();
        this.description = inXMLObject.getDescription();
        this.requestUrl = inXMLObject.getRequestUrl();
        this.validatedObjectType = inXMLObject.getValidatedObjectType();
    }

    public ValidatedFHIR() {

    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    @Override
    public void setFilePath(final String path) {
        this.resourcePath = path;
    }

    /**
     */
    public static ValidatedFHIR createNewValidatedFHIR(FhirObjectType objectType) {
        ValidatedFHIR message = new ValidatedFHIR();
        message.setValidatedObjectType(objectType);
        final EntityManager em = EntityManagerService.provideEntityManager();
        message = em.merge(message);
        em.flush();
        if (message == null || message.getId() == 0) {
            ValidatedFHIR.LOGGER.error("An error occurred when creating a new instance of ValidatedFHIR object");
            return null;
        }
        if (!FhirObjectType.URL.equals(objectType)) {
            final String repository = ApplicationPreferenceManager.getStringValue("fhir_repository");
            final String suffixToUse;
            if (FhirObjectType.XML.equals(objectType)) {
                suffixToUse = XML_FILE_SUFFIX;
            } else {
                suffixToUse = JSON_FILE_SUFFIX;
            }
            message.setResourcePath(
                    repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ValidatedFHIR.XML_FILE_PREFIX
                            .concat(message.getId().toString()).concat(
                                    suffixToUse));
        } else {
            message.setDescription("Manually entered URL");
        }
        return message;
    }

    /**
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends AbstractValidatedXMLFile> T addOrUpdate(final Class<T> clazz) {
        if ((this.getResourcePath() == null || this.getResourcePath().isEmpty())
                && (this.requestUrl == null || this.requestUrl.isEmpty())) {
            ValidatedFHIR.LOGGER.error("No data to store (path to resource or request ULR is empty) !");
            return (T) this;
        } else {
            if (!FhirObjectType.URL.equals(this.validatedObjectType)) {
                final File uploadedFile = new File(this.getResourcePath());
                if (!uploadedFile.exists()) {
                    ValidatedFHIR.LOGGER.error("The given file does not exist: {}", this.getResourcePath());
                    return (T) this;
                }
            }
            if (this.getOid() == null || this.getOid().length() == 0) {
                this.setOid(OIDGenerator.getNewOid());
            }
            final EntityManager em = EntityManagerService.provideEntityManager();
            final ValidatedFHIR validatedFile = em.merge(this);
            em.flush();

            //Update the cache with the newest values
            GazelleValidationCacheManager
                    .refreshGazelleCacheWebService(validatedFile.getExternalId(), validatedFile.getToolOid(),
                            validatedFile.getOid(), "off");

            return (T) validatedFile;
        }
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public FhirObjectType getValidatedObjectType() {
        return validatedObjectType;
    }

    public void setValidatedObjectType(FhirObjectType validatedObjectType) {
        this.validatedObjectType = validatedObjectType;
    }

    @Override
    public String getFilePath() {
        return this.resourcePath;
    }

    @Override
    public String getSchematron() {
        return this.schematron;
    }

    @Override
    public void setSchematron(final String schematron) {
        this.schematron = schematron;
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/detailedResult.seam?type=FHIR").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ValidatedFHIR)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        ValidatedFHIR that = (ValidatedFHIR) o;

        if (resourcePath != null ? !resourcePath.equals(that.resourcePath) : that.resourcePath != null) {
            return false;
        }
        if (schematron != null ? !schematron.equals(that.schematron) : that.schematron != null) {
            return false;
        }
        if (requestUrl != null ? !requestUrl.equals(that.requestUrl) : that.requestUrl != null) {
            return false;
        }
        return validatedObjectType == that.validatedObjectType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (resourcePath != null ? resourcePath.hashCode() : 0);
        result = 31 * result + (schematron != null ? schematron.hashCode() : 0);
        result = 31 * result + (requestUrl != null ? requestUrl.hashCode() : 0);
        result = 31 * result + (validatedObjectType != null ? validatedObjectType.hashCode() : 0);
        return result;
    }

    public static String formatJsonContent(ValidatedFHIR validatedFile, Boolean indent) {
        String jsonString = validatedFile.getFileContent();
        if (indent != null && indent) {
            JsonParser parser = new JsonParser();
            JsonObject json = parser.parse(jsonString).getAsJsonObject();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            return gson.toJson(json);
        } else {
            return jsonString;
        }
    }
}
