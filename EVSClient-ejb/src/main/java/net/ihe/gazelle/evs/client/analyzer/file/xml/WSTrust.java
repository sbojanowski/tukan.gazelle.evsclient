/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file.xml;

import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerUtils;
import net.ihe.gazelle.evs.client.analyzer.utils.XmlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WSTrust extends Analyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(WSTrust.class);

    @Override
    public Logger getLog() {
        return WSTrust.LOGGER;
    }

    @Override
    public String getType() {
        return "WSTrust";
    }

    public static boolean containsWS_Trust(final File file, final MessageContentAnalyzer mca) {
        final String fileContent = AnalyzerFileUtils.convertFileToString(file);
        return WSTrust.containsWS_Trust(fileContent, mca);
    }

    public static boolean containsWS_Trust(final String fileContent, final MessageContentAnalyzer mca) {
        return WSTrust.containsRST(fileContent, mca) || WSTrust.containsRSTR(fileContent, mca);
    }

    public static boolean containsRST(final String fileContent, final MessageContentAnalyzer mca) {
        if (fileContent != null && !fileContent.isEmpty()) {
            final String rstReg = "<([^<]+)?RequestSecurityToken([\\r|\\n| |>])+.+</([^<]+)?RequestSecurityToken>";

            Pattern regex = Pattern.compile(rstReg, Pattern.DOTALL);
            Matcher regexMatcher = regex.matcher(fileContent);
            if (regexMatcher.find()) {
                WSTrust.LOGGER.info(regexMatcher.group());
                if (XmlUtils.testValidSchemes(regexMatcher.group(1), regexMatcher.group(3))) {

                    regex = Pattern.compile(rstReg, Pattern.DOTALL);
                    regexMatcher = regex.matcher(fileContent);
                    if (regexMatcher.find()) {
                        WSTrust.LOGGER.info(regexMatcher.group());
                        mca.setOffsetStart(regexMatcher.start());
                        mca.setOffsetEnd(regexMatcher.end());
                        return true;
                    }
                }
            }
        } else {
            WSTrust.LOGGER.error(AnalyzerUtils.EMPTY_ERROR_MESSAGE);
        }
        return false;
    }

    public static boolean containsRSTR(final String fileContent, final MessageContentAnalyzer mca) {
        if (fileContent != null && !fileContent.isEmpty()) {
            final String rstrReg = "<([^<]+)?RequestSecurityTokenResponseCollection([\\r|\\n| |>])+.+</([^<]+)?RequestSecurityTokenResponseCollection>";

            Pattern regex = Pattern.compile(rstrReg, Pattern.DOTALL);
            Matcher regexMatcher = regex.matcher(fileContent);
            if (regexMatcher.find()) {
                WSTrust.LOGGER.info(regexMatcher.group());
                if (XmlUtils.testValidSchemes(regexMatcher.group(1), regexMatcher.group(3))) {

                    regex = Pattern.compile(rstrReg, Pattern.DOTALL);
                    regexMatcher = regex.matcher(fileContent);
                    if (regexMatcher.find()) {
                        WSTrust.LOGGER.info(regexMatcher.group());
                        mca.setOffsetStart(regexMatcher.start());
                        mca.setOffsetEnd(regexMatcher.end());
                        return true;
                    }
                }
            }
        } else {
            WSTrust.LOGGER.error(AnalyzerUtils.EMPTY_ERROR_MESSAGE);
        }
        return false;
    }
}