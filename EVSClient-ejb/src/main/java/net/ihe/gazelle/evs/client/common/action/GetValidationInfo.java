/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.evs.client.analyzer.AnalyzerBean;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Stateless
@Name("getValidationInfo")
public class GetValidationInfo implements GetValidationInfoLocal {

    public static final String CACHE = "cache";
    public static final String OID = "oid";
    public static final String TEXT_PLAIN = "text/plain";

    @Override
    @GET
    @Path("/GetValidationDate")
    @Produces(GetValidationInfo.TEXT_PLAIN)
    public String getValidationDate(@QueryParam(GetValidationInfo.OID)
    final String oid,
            @DefaultValue("") @QueryParam(GetValidationInfo.CACHE)
            final String queryCache) {
        return GazelleValidationCacheManager.getValidationDateService(oid, null, null, queryCache);
    }

    @Override
    @GET
    @Path("/GetValidationPermanentLink")
    @Produces(GetValidationInfo.TEXT_PLAIN)
    public String getValidationPermanentLink(@QueryParam(GetValidationInfo.OID)
    final String oid,
            @DefaultValue("") @QueryParam(GetValidationInfo.CACHE)
            final String queryCache) {
        return GazelleValidationCacheManager.getValidationPermanentLinkService(oid, null, null, queryCache);
    }

    @Override
    @GET
    @Path("/GetValidationStatus")
    @Produces(GetValidationInfo.TEXT_PLAIN)
    public String getValidationStatus(@QueryParam(GetValidationInfo.OID)
    final String oid,
            @DefaultValue("") @QueryParam(GetValidationInfo.CACHE)
            final String queryCache) {
        return GazelleValidationCacheManager.getValidationStatusService(oid, null, null, queryCache);
    }

    @Override
    @GET
    @Path("/GetLastResultStatusByExternalId")
    @Produces(GetValidationInfo.TEXT_PLAIN)
    public String getLastResultStatusByExternalId(@QueryParam("externalId")
    final String externalId,
            @QueryParam(AnalyzerBean.TOOL_OID)
            final String toolOid, @DefaultValue("") @QueryParam(GetValidationInfo.CACHE)
    final String queryCache) {
        return GazelleValidationCacheManager.getValidationStatusService(null, externalId, toolOid, queryCache);
    }

    @Override
    @GET
    @Path("/GetValidationPermanentLinkByExternalId")
    @Produces(GetValidationInfo.TEXT_PLAIN)
    public String getValidationPermanentLinkByExternalId(@QueryParam("externalId")
    final String externalId,
            @QueryParam(AnalyzerBean.TOOL_OID)
            final String toolOid, @DefaultValue("") @QueryParam(GetValidationInfo.CACHE)
    final String queryCache) {
        return GazelleValidationCacheManager.getValidationPermanentLinkService(null, externalId, toolOid, queryCache);
    }

    @Override
    @GET
    @Path("/GetLastResultStatusAndPermanentLinkByExternalId")
    @Produces(GetValidationInfo.TEXT_PLAIN)
    public String getLastResultStatusAndPermanentLinkByExternalId(@QueryParam("externalId")
    final String externalId,
            @QueryParam(AnalyzerBean.TOOL_OID)
            final String toolOid, @DefaultValue("") @QueryParam(GetValidationInfo.CACHE)
    final String queryCache) {
        return GazelleValidationCacheManager.getValidationStatusAndPermanentLinkService(null, externalId, toolOid, queryCache);
    }

    @Override
    @GET
    @Path("/GetLastResultStatusAndPermanentLink")
    @Produces(GetValidationInfo.TEXT_PLAIN)
    public String getLastResultStatusAndPermanentLink(@QueryParam(GetValidationInfo.OID)
    final String oid,
             @DefaultValue("") @QueryParam(GetValidationInfo.CACHE)
             final String queryCache) {
        return GazelleValidationCacheManager.getValidationStatusAndPermanentLinkService(oid, null, null, queryCache);
    }
}