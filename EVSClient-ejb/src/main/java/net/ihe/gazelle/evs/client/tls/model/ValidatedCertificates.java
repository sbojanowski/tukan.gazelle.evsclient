/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.tls.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.util.Util;
import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;

@Entity
@Name("validatedCertificate")
@Table(name = "tls_validated_certificate", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "tls_validated_certificate_sequence", sequenceName = "tls_validated_certificate_id_seq", allocationSize = 1)
public class ValidatedCertificates extends ValidatedObject {

    private static final long serialVersionUID = 732366639349862863L;

    private static final String FILE_PREFIX = "tlsCertificates_";
    private static final String FILE_EXTENSION = ".pem";

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ValidatedCertificates.class);

    @Id
    @NotNull
    @Column(name = "id")
    @GeneratedValue(generator = "tls_validated_certificate_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "pem_file_path")
    private String pemPath;

    @Lob
    @Type(type = "text")
    @Column(name = "certificates")
    private String certificates;

    /**
     * Constructors
     */
    public ValidatedCertificates() {

    }

    public ValidatedCertificates(final ValidatedCertificates message) {
        this.pemPath = message.getPemPath();
        this.setDescription(message.getDescription());
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getPemPath() {
        return this.pemPath;
    }

    public void setPemPath(final String pemPath) {
        this.pemPath = pemPath;
    }

    public String getCertificates() {
        return this.certificates;
    }

    public void setCertificates(final String certificates) {
        this.certificates = certificates;
    }

    /**
     * Static methods
     */

    /**
     * creation of a new object in order to compute the name of uploaded file
     */
    public static ValidatedCertificates createNewValidatedCertificates() {
        ValidatedCertificates message = new ValidatedCertificates();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        message = em.merge(message);
        em.flush();
        if (message == null || message.getId() == 0) {
            ValidatedCertificates.LOGGER.error("An error occurred when creating a new ValidatedAuditMessage object");
            return null;
        }
        final String repository = ApplicationPreferenceManager.getStringValue("tls_repository");
        message.setPemPath(repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ValidatedCertificates.FILE_PREFIX
                .concat(message.getId().toString()).concat(
                ValidatedCertificates.FILE_EXTENSION));
        return message;
    }

    /**
     * @param inMessage
     */
    public static ValidatedCertificates addOrUpdateCertificates(ValidatedCertificates inMessage) {
        if (inMessage == null) {
            ValidatedCertificates.LOGGER.error("the given ValidatedAuditMessage is null");
            return null;
        }
        if (inMessage.getPemPath() == null || inMessage.getPemPath().length() == 0) {
            ValidatedCertificates.LOGGER.error("The file path is missing, cannot add this object !");
            return inMessage;
        } else {
            final File uploadedFile = new File(inMessage.getPemPath());
            if (!uploadedFile.exists()) {
                ValidatedCertificates.LOGGER.error("The given file does not exist: {}", inMessage.getPemPath());
                return inMessage;
            }
            if (inMessage.getOid() == null || inMessage.getOid().length() == 0) {
                inMessage.setOid(OIDGenerator.getNewOid());
            }
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            inMessage = em.merge(inMessage);
            em.flush();

            //Update the cache with the newest values
            GazelleValidationCacheManager
                    .refreshGazelleCacheWebService(inMessage.getExternalId(), inMessage.getToolOid(),
                            inMessage.getOid(), "off");

            return inMessage;
        }
    }

    @Override
    public String getFilePath() {
        return this.pemPath;
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/tlsResult.seam?").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

    @Override
    public boolean equals(final Object obj) {
        if (! super.equals(obj)) {
            return false;
        }

        final ValidatedCertificates other = (ValidatedCertificates) obj;
        if (!pemPath.equals(((ValidatedCertificates) obj).pemPath)){
            return false;
        }
        if (!certificates.equals(((ValidatedCertificates) obj).certificates)){
            return false;
        }

        return true;
    }

}
