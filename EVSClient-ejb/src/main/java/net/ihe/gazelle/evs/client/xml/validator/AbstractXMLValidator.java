/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.validator;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.evs.client.common.action.ApplicationConfiguration;
import net.ihe.gazelle.evs.client.common.action.EmailNotificationManager;
import net.ihe.gazelle.evs.client.common.menu.EVSMenu;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.common.model.ValidationService;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.evs.client.util.ValidatorType;
import net.ihe.gazelle.evs.client.util.XmlContentDecode;
import net.ihe.gazelle.evs.client.util.XmlContentGeneration;
import net.ihe.gazelle.evs.client.xml.logs.TemplatesUpdaterJob;
import net.ihe.gazelle.evs.client.xml.model.AbstractValidatedXMLFile;
import net.ihe.gazelle.evs.client.xml.model.CDAValidatedFile;
import net.ihe.gazelle.evs.client.xml.model.FhirObjectType;
import net.ihe.gazelle.evs.client.xml.model.ValidatedDICOMWeb;
import net.ihe.gazelle.evs.client.xml.model.ValidatedFHIR;
import net.ihe.gazelle.mb.validator.client.MBValidator;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub;
import net.ihe.gazelle.sch.validator.ws.client.SOAPExceptionException;
import org.apache.axis2.Constants.Configuration;
import org.apache.commons.io.FileUtils;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.util.Base64;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.faces.context.FacesContext;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.util.*;

@SuppressWarnings("serial")
public abstract class AbstractXMLValidator<T extends AbstractValidatedXMLFile> implements Serializable {

    private static final String TMP_FILE_SUFFIX = ".tmp";

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractXMLValidator.class);

    protected T validatedFile;
    private transient List<String> availableSchematrons;
    protected String selectedSchematron;
    private transient List<String> availableMBValidations;
    protected String selectedMBValidation;
    protected ReferencedStandard selectedReferenceStandard;
    private File uploadedFile;
    private transient GazelleObjectValidatorServiceStub stub;
    private String validationType;
    protected ValidationService modelBasedValidationService;
    protected ValidationService schematronBasedValidationService;
    protected transient MBValidator mbValidator;
    private boolean validationDone;
    private String standardDescription;
    private String modelBasedValidationServiceDescription;
    private String schematronBasedValidationServiceDescription;
    private String urlToValidate;
    private String extension;
    private Boolean showMessageInGUI = Boolean.FALSE;
    private Boolean indentMessageInGUI = Boolean.FALSE;
    private static final String ACCEPTED_FILE_TYPES =  "xml, XML, cda, CDA, saml, SAML";

    public String getAcceptedFileTypes() {

        String result = ApplicationPreferenceManager.getStringValue(validationType + "_file_types");
        if (result != null) {
            return (result);
        }
        else {
            return AbstractXMLValidator.ACCEPTED_FILE_TYPES;
        }

    }

    public String getModelBasedValidationServiceDescription() {
        return modelBasedValidationServiceDescription;
    }

    public void setModelBasedValidationServiceDescription(String modelBasedValidationServiceDescription) {
        this.modelBasedValidationServiceDescription = modelBasedValidationServiceDescription;
    }

    public String getSchematronBasedValidationServiceDescription() {
        return schematronBasedValidationServiceDescription;
    }

    public void setSchematronBasedValidationServiceDescription(String schematronBasedValidationServiceDescription) {
        this.schematronBasedValidationServiceDescription = schematronBasedValidationServiceDescription;
    }

    public String getUrlToValidate() {
        return this.urlToValidate;
    }

    public void setUrlToValidate(final String urlToValidate) {
        this.urlToValidate = urlToValidate;
    }

    public void saveUrl(final String urlToValidate) {
        try {
            this.validatedFile = this.newInstance();

            final String urlB64 = DatatypeConverter.printBase64Binary(urlToValidate.getBytes(
                    StandardCharsets.UTF_8));
            final File uploadedFile = File.createTempFile("temp-file", AbstractXMLValidator.TMP_FILE_SUFFIX);
            FileUtils.writeStringToFile(uploadedFile, urlToValidate, StandardCharsets.UTF_8.toString());
            this.validatedFile.setFilePath(uploadedFile.getPath());
            this.validatedFile.setDescription(uploadedFile.getName());

            this.validatedFile = this.validatedFile.addOrUpdate(this.getEntityClass());

            if (this.validatedFile instanceof ValidatedDICOMWeb) {
                final ValidatedDICOMWeb vw = ValidatedObject
                        .getObjectByID(ValidatedDICOMWeb.class, ((ValidatedDICOMWeb) this.validatedFile).getId(), null);
                if (vw != null) {
                    vw.setValidatedUrl(urlB64);
                    this.validatedFile = vw.addOrUpdate(this.getEntityClass());
                }
            } else if (this.validatedFile instanceof ValidatedFHIR){
                final ValidatedFHIR fhirObj = ValidatedObject.getObjectByID(ValidatedFHIR.class, ((ValidatedFHIR) this.validatedFile).getId(), null);
                if (fhirObj != null){
                    fhirObj.setRequestUrl(urlB64);
                    this.validatedFile = fhirObj.addOrUpdate(this.getEntityClass());
                }
            }

        } catch (final IOException e) {
            AbstractXMLValidator.LOGGER.error("{}", e);
        }
    }

    public String getStandardDescription() {
        return this.standardDescription;
    }

    public void setStandardDescription(final String standardDescription) {
        this.standardDescription = standardDescription;
    }
    public String getExtension() {
        return this.extension;
    }

    public void setExtension(final String extension) {
        this.extension = extension;
    }

    private Exception generatedException;

    public void init(final String validatorType) {

        this.validationType = validatorType;
        String standardLabel;
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        if (urlParams.containsKey(EVSMenu.STANDARD_PARAM)){
            standardLabel = urlParams.get(EVSMenu.STANDARD_PARAM);
        } else {
            standardLabel = null;
        }
        if (validatorType == null && urlParams.containsKey(EVSMenu.TYPE_PARAM)) {
            this.validationType = urlParams.get(EVSMenu.TYPE_PARAM);
        } else if (validatorType == null) {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "net.ihe.gazelle.evs.xml.ParameterMissingInUrl");
        }
        if (urlParams.containsKey(EVSMenu.EXTENSION_PARAM)) {
            this.extension = urlParams.get(EVSMenu.EXTENSION_PARAM);
        }
        if (urlParams.containsKey(EVSMenu.OID_PARAM)) {
            this.validatedFile = this.getObjectByOid(urlParams.get(EVSMenu.OID_PARAM));
        }
        if (urlParams.containsKey(EVSMenu.ID_PARAM)) {
            final Integer id = Integer.valueOf(urlParams.get(EVSMenu.ID_PARAM));
            this.validatedFile = ValidatedObject.getObjectByID(this.getEntityClass(), id, null);
        }
        if (this.validatedFile != null && this.validationType.equals(ValidatorType.DICOM_WEB.getFriendlyName())) {
            this.setUrlToValidate(new String(Base64.decode(((ValidatedDICOMWeb) this.validatedFile).getValidatedUrl()), StandardCharsets.UTF_8));
        } if (this.validatedFile != null && this.validationType.equals(ValidatorType.FHIR.getFriendlyName())){
            ValidatedFHIR fhirObj = (ValidatedFHIR) validatedFile;
            if (FhirObjectType.URL.equals(fhirObj.getValidatedObjectType())){
                this.setUrlToValidate(new String(Base64.decode(fhirObj.getRequestUrl()), StandardCharsets.UTF_8));
            }
        }
        List<ReferencedStandard> standards = ReferencedStandard
                .getReferencedStandardFiltered(this.validationType, null, this.extension, standardLabel);
        if (standards != null && !standards.isEmpty()) {
            this.selectedReferenceStandard = standards.get(0);
            if (this.selectedReferenceStandard.getDescription() != null && !this.selectedReferenceStandard.getDescription()
                    .isEmpty()) {
                this.setStandardDescription(this.selectedReferenceStandard.getDescription());
            }
            List<ValidationService> services = this.selectedReferenceStandard.getServices();
            if (services != null && !services.isEmpty()) {
                for (final ValidationService service : services) {
                    if (service != null && service.isAvailable() && service.getEngine() != null) {
                        if (service.getEngine().equals(ValidationService.EngineType.SCHEMATRON.getKeyword())) {
                            this.schematronBasedValidationService = service;
                            setSchematronBasedValidationServiceDescription(schematronBasedValidationService.getDescription());
                            if (this.stub == null) {
                                try {
                                    this.stub = new GazelleObjectValidatorServiceStub(this.schematronBasedValidationService.getWsdl());
                                    this.stub._getServiceClient().getOptions().setProperty(Configuration.DISABLE_SOAP_ACTION,
                                            Boolean.TRUE);

                                    final GazelleObjectValidatorServiceStub.GetSchematronsForAGivenType params = new GazelleObjectValidatorServiceStub.GetSchematronsForAGivenType();
                                    params.setFileType(this.selectedReferenceStandard.getLabel());
                                    final GazelleObjectValidatorServiceStub.GetSchematronsForAGivenTypeE paramsE = new GazelleObjectValidatorServiceStub.GetSchematronsForAGivenTypeE();
                                    paramsE.setGetSchematronsForAGivenType(params);
                                    final GazelleObjectValidatorServiceStub.Schematron[] schematrons = this.stub.getSchematronsForAGivenType(paramsE)
                                            .getGetSchematronsForAGivenTypeResponse().getSchematrons();
                                    if (schematrons != null && schematrons.length > 0) {
                                        this.availableSchematrons = new ArrayList<>();
                                        for (final GazelleObjectValidatorServiceStub.Schematron schematron : schematrons) {
                                            this.availableSchematrons.add(schematron.getLabel());
                                        }
                                        Collections.sort(this.availableSchematrons);
                                    }
                                } catch (final RemoteException|SOAPExceptionException e) {
                                    this.availableSchematrons = null;
                                    EmailNotificationManager
                                            .send(e, "Unable to retrieve the list of schematrons for Validation using label " +this.selectedReferenceStandard.getLabel(),
                                                    null);
                                    FacesMessages.instance()
                                            .addFromResourceBundle("gazelle.evs.client.service.seems.unavailable");

                                }
                            }
                        } else if (service.getEngine().equals(ValidationService.EngineType.MODEL_BASED.getKeyword())) {
                            this.modelBasedValidationService = service;
                            setModelBasedValidationServiceDescription(modelBasedValidationService.getDescription());
                            this.availableMBValidations = this.getModelBasedValidators();
                        }
                    }
                }
            }
        } else {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "net.ihe.gazelle.evs.xml.noStandardFound");
        }
    }

    public void uploadEventListener(final FileUploadEvent event) {
        this.resetSimple();
        FileOutputStream fos = null;
        try {
            this.validatedFile = this.newInstance();
            final UploadedFile item = event.getUploadedFile();
            this.uploadedFile = new File(this.validatedFile.getFilePath());
            this.uploadedFile.getParentFile().mkdirs();
            this.validatedFile.setDescription(item.getName());
            if (item.getData() != null && item.getData().length > 0) {
                fos = new FileOutputStream(this.uploadedFile);
                fos.write(item.getData());
            } else {
                FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "gazelle.evs.client.cda.file.empty");
                this.validatedFile.setFilePath(null);
                this.validatedFile = this.validatedFile.addOrUpdate(this.getEntityClass());
            }
        } catch (final IOException e) {
            AbstractXMLValidator.LOGGER.error(e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (final IOException e) {
                    AbstractXMLValidator.LOGGER.error("not able to close the FOS", e);
                }
            }
        }
    }


    /**
     * By fge
     *
     */
    public void wroteEventListener(final FileUploadEvent event) {
        this.resetSimple();
        this.validatedFile = this.newInstance();
        final UploadedFile item = event.getUploadedFile();
        this.uploadedFile = new File(this.validatedFile.getFilePath());
        this.validatedFile.setDescription(item.getName());

        FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "gazelle.evs.client.cda.file.empty");
        this.validatedFile.setFilePath(null);
        this.validatedFile = this.validatedFile.addOrUpdate(this.getEntityClass());
    }

    /**
     * returns a new instance of T
     *
     */
    protected abstract T newInstance();

    /**
     * Gets an object by its OID
     *
     */
    protected T getObjectByOid(final String string) {
        return ValidatedObject.getObjectByOID(this.getEntityClass(), string, null);
    }

    public void validateFile() {
        this.setValidationDone(false);
        this.generatedException = null;

        if (this.urlToValidate != null && !this.urlToValidate.isEmpty()) {
            // URL does not have carriage return in it. Cleaning it
            this.saveUrl(this.urlToValidate.replaceAll("(?:\\n|\\r)", ""));
        }
        if ("".equals(this.selectedSchematron)) {
            this.selectedSchematron = null;
        }
        if ("".equals(this.selectedMBValidation)) {
            this.selectedMBValidation = null;
        }

        if (this.validatedFile == null) {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "gazelle.evs.client.cda.error.no.file");
            return;
        } else if (this.validatedFile.getFilePath() == null || this.validatedFile.getFilePath().isEmpty()) {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "gazelle.evs.client.cda.file.empty");
            return;
        } else if (this.selectedSchematron == null && this.selectedMBValidation == null) {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "gazelle.evs.client.cda.error.no.method");
            return;
        } else if (this.availableSchematrons != null && this.selectedSchematron != null && !this.availableSchematrons
                .contains(this.selectedSchematron)) {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "The specified schematron is not ON the list of available schematrons ! Please choose a schematron validator from the checkList.");
            return;
        } else if (this.availableMBValidations != null && this.selectedMBValidation != null
                && !this.availableMBValidations
                .contains(this.selectedMBValidation)) {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "The specified model based validator is not ON the list of available validator ! Please choose amodel based validator from the checkList.");
            return;
        } else {
            this.validatedFile.setUserIp(Util.getUserIpAddress());
            this.validatedFile.updateCountryStatistics();
            this.validatedFile.setStandard(this.selectedReferenceStandard);
            this.validatedFile.setValidationContext(null);
            this.validatedFile.setSchematron(this.selectedSchematron);
            this.validatedFile.setMbvalidator(this.selectedMBValidation);

            if (this.selectedMBValidation != null) {
                this.setValidationDone(true);
                this.validatedFile.setMbValidationService(this.modelBasedValidationService);
                this.validateWithModelBased();
            }

            if (this.selectedSchematron != null) {
                this.setValidationDone(true);
                this.validatedFile.setService(this.schematronBasedValidationService);
                final Thread scht = new Thread(new SchematronValidator(this.schematronBasedValidationService.getXslLocation()));
                scht.start();
                try {
                    scht.join();
                } catch (final InterruptedException e) {
                    this.updateAbortedValidation(e);
                }
            }

            this.validatedFile.setValidationDate(new Date());
            this.validatedFile = this.validatedFile.addOrUpdate(this.getEntityClass());
            this.setValidationDone(true);
            if ("VALIDATION NOT PERFORMED".equals(this.validatedFile.getValidationStatus())) {
                EmailNotificationManager.send(this.generatedException, "unable to perform Validation", null);
                FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "gazelle.evs.client.service.seems.unavailable");
            }
            else {
                FacesMessages.instance().addFromResourceBundle(Severity.INFO, "Validation Performed");

            }
        }

        this.updateTemplateList(this.validatedFile);
    }

    public void updateTemplateList(final T f) {
        if (ApplicationConfiguration.isTemplateScanned() && f instanceof CDAValidatedFile && !((CDAValidatedFile) f)
                .isTemplatesListed()) {
            final TemplatesUpdaterJob job = new TemplatesUpdaterJob();
            job.extractTemplatesJob((CDAValidatedFile) f);
        }
    }

    /**
     * Calls the SchematronValidation web service API to validate the file against the selected schematron Sets the validation result in detailedResult attribute of T
     */
    protected void validateWithSchematron(final String xslLocationString) {
        // build metadata content
        final Map<String, String> metadataContent = new HashMap<>();
        metadataContent.put("SchematronLabel", this.selectedSchematron);
        synchronized (this.validatedFile) {
            this.validatedFile.setMetadata(XmlContentGeneration.generateMetadata(metadataContent));
        }
        try {
            final GazelleObjectValidatorServiceStub.ValidateObject params = new GazelleObjectValidatorServiceStub.ValidateObject();
            params.setXmlMetadata(this.selectedSchematron);
            params.setXmlReferencedStandard(this.validatedFile.getStandard().getName());
            params.setBase64ObjectToValidate(DatatypeConverter.printBase64Binary(Files.readAllBytes(Paths.get(this.validatedFile.getFilePath()))));

            final GazelleObjectValidatorServiceStub.ValidateObjectE paramsE = new GazelleObjectValidatorServiceStub.ValidateObjectE();
            paramsE.setValidateObject(params);
            final GazelleObjectValidatorServiceStub.ValidateObjectResponseE responseE = this.stub.validateObject(paramsE);
            String detailedResult = responseE.getValidateObjectResponse().getValidationResult();

            if (detailedResult != null) {
                if (detailedResult.length() > 0 && xslLocationString != null) {
                    detailedResult = detailedResult.replace("XSLT_LOCATION", xslLocationString);
                }
            }

            synchronized (this.validatedFile) {
                if (responseE.getValidateObjectResponse().getValidationResult() != null) {
                    this.validatedFile.setDetailedResult(detailedResult);
                    this.validatedFile.setValidationStatus(XmlContentDecode.getValidationResult(detailedResult));
                    this.validatedFile.setValidationDate(new Date());
                } else {
                    this.validatedFile.setDetailedResult(null);
                    this.validatedFile.setValidationStatus("ABORTED");
                    this.validatedFile.setValidationDate(new Date());
                }
            }

        } catch (final RemoteException|SOAPExceptionException e) {
            this.updateAbortedValidation(e);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR,e.getMessage());
        }
    }

    /**
     * validates the file against the selected model based validator This method may be overriden if another validator than ModelBasedValidator must be used Sets the validation result in
     * mbvDetailedResult attribute of T
     */
    protected void validateWithModelBased() {
        if (this.mbValidator == null) {
            this.mbValidator = new MBValidator(this.modelBasedValidationService.getWsdl());
        }
        //       final String result = this.mbValidator
        //               .validate(Base64.encodeFromFile(this.validatedFile.getFilePath()), this.selectedMBValidation, true);

        final String result;
        try {
            result = this.mbValidator
                    .validate(DatatypeConverter.printBase64Binary(Files.readAllBytes(Paths.get(this.validatedFile.getFilePath()))), this.selectedMBValidation, true);


            synchronized (this.validatedFile) {
                // the condition is suspended for the moment for rewriting new validation status
                // if ((validatedFile.getValidationStatus() == null) || validatedFile.getValidationStatus().equals("PASSED")) {
                this.validatedFile.setValidationStatus(XmlContentDecode.getValidationResult(result));
                // }
                this.validatedFile.setMbvDetailedResult(result);
                // validatedFile = validatedFile.addOrUpdate(getEntityClass());
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR,e.getMessage());

        }
    }

    /**
     * Returns the list of available validators for the selected model based validation service This method may be overriden if another validator than ModelBasedValidator must be used
     *
     */
    protected List<String> getModelBasedValidators() {
        List<String> validators;
        try {
            if (this.mbValidator == null) {
                this.mbValidator = new MBValidator(this.modelBasedValidationService.getWsdl());
            }

            validators = this.mbValidator.listAvailableValidators(this.selectedReferenceStandard.getExtension());
            if (validators != null) {
                Collections.sort(validators);
            }
            return validators;
        } catch (final Exception e) {
            EmailNotificationManager.send(e, "Unable to retrieve the list of model based validators", null);
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "net.ihe.gazelle.evs.xml.failToValidateWithMBValidator");
            return null;
        }
    }

    public void reset() {
        this.resetSimple();
        this.selectedSchematron = null;
        this.selectedMBValidation = null;
        this.init(this.validationType);
    }

    public void resetSimple() {
        this.validatedFile = null;
        this.uploadedFile = null;
        this.setValidationDone(false);
        this.setUrlToValidate(null);
    }

    @Destroy
    @Remove
    public void destroy() {

    }

    protected void updateAbortedValidation(final Exception exception) {
        this.generatedException = exception;
        synchronized (this.validatedFile) {
            this.validatedFile.setValidationStatus("VALIDATION NOT PERFORMED");
            this.validatedFile.setValidationDate(new Date());
        }
    }

    public void downloadValidatedFile() {
        final String content = this.getValidatedFile().getFileContent();
        final String fileName = this.getValidatedFile().getDescription();
        ReportExporterManager.exportToFile("text/xml", content, fileName);
    }

    public String getFileContent() {
        if (this.indentMessageInGUI){
            return Util.prettyFormat(getValidatedFile().getFileContent());
        }
        return this.getValidatedFile().getFileContent();
    }

    public T getValidatedFile() {
        return this.validatedFile;
    }

    public void setValidatedFile(final T validatedFile) {
        this.validatedFile = validatedFile;
    }

    public List<String> getAvailableSchematrons() {
        return this.availableSchematrons;
    }

    public void setAvailableSchematrons(final List<String> availableSchematrons) {
        if (availableSchematrons != null) {
            this.availableSchematrons = new ArrayList<String>(availableSchematrons);
        }
        else {
            this.availableSchematrons = null;
        }
    }

    public String getSelectedSchematron() {
        return this.selectedSchematron;
    }

    public void setSelectedSchematron(final String selectedSchematron) {
        this.selectedSchematron = selectedSchematron;
    }

    public List<String> getAvailableMBValidations() {
        return this.availableMBValidations;
    }

    public void setAvailableMBValidations(final List<String> availableMBValidations) {
        if (availableMBValidations != null) {
            this.availableMBValidations = new ArrayList<String>(availableMBValidations);
        }
        else {
            this.availableMBValidations = null;
        }
    }

    public String getSelectedMBValidation() {
        return this.selectedMBValidation;
    }

    public void setSelectedMBValidation(final String selectedMBValidation) {
        this.selectedMBValidation = selectedMBValidation;
    }

    public abstract Class<T> getEntityClass();

    public ValidationService getModelBasedValidationService() {
        return this.modelBasedValidationService;
    }

    public ValidationService getSchematronBasedValidationService() {
        return this.schematronBasedValidationService;
    }

    public boolean getValidationDone() {
        return this.validationDone;
    }

    public void setValidationDone(final boolean value) {
        this.validationDone = value;
    }

    class SchematronValidator implements Runnable {

        String xslLocationString;

        public SchematronValidator(final String xslLocationString) {
            this.xslLocationString = xslLocationString;
        }

        @Override
        public void run() {
            AbstractXMLValidator.this.validateWithSchematron(this.xslLocationString);
        }

    }

    class ModelBasedValidator implements Runnable {

        @Override
        public void run() {
            AbstractXMLValidator.this.validateWithModelBased();
        }

    }
    public Boolean getShowMessageInGUI() {
        return this.showMessageInGUI;
    }

    public void setShowMessageInGUI(final Boolean showMessageInGUI) {
        this.showMessageInGUI = showMessageInGUI;
    }

    public Boolean getIndentMessageInGUI() {
        return this.indentMessageInGUI;
    }

    public void setIndentMessageInGUI(final Boolean indentMessageInGUI) {
        this.indentMessageInGUI = indentMessageInGUI;
    }
    public void setMessageInvalidFileType(){
        FacesMessages.instance().add(Severity.ERROR,"Invalid file type. Expected file types are : "+ this
                .getAcceptedFileTypes());
    }

}
