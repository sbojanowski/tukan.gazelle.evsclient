/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evs.client.hl7.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * <b>Class Description : </b>HL7ValidatedMessage<br>
 * <br>
 * <p>
 * This class describes the ValidatedMessage object which represents the HL7v2 message which has been validated and the result of this validation This class extends the abstract class ValidatedObject
 * <p/>
 * ValidatedMessage possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id of the message in database</li>
 * <li><b>message</b> : HL7v2 message which has been validated (ER7 format)</li>
 * <li><b>profileOid</b> : OID of the profile the message has been validated against</li>
 * <li><b>detailedResults</b> : detailed results of the validation (error, warnings ...)</li>
 * <li><b>messageMetadata</b> : message metadata (profile OID, language ...)</li>
 * <li><b>referencedStandard</b> : referenced standard used to validate the message</li>
 * <li><b>messageType</b> : message type</li>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, May 28th
 * @class HL7ValidatedMessage.java
 * @package net.ihe.gazelle.evs.client.hl7.model
 * @see Aberge@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Name("hl7ValidatedMessage")
@Table(name = "hl7_validated_message", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "hl7_validated_message_sequence", sequenceName = "hl7_validated_message_id_seq", allocationSize = 1)
public class HL7ValidatedMessage extends ValidatedObject {

    /**
     *
     */
    private static final long serialVersionUID = -3435397913635134710L;

  

    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hl7_validated_message_sequence")
    private Integer id;

    @Lob
    @Type(type = "text")
    @Column(name = "validated_message")
    private String validatedMessage;

    @Column(name = "profile_oid")
    private String profileOid;

    /**
     * Constructor
     */
    public HL7ValidatedMessage() {
        this.setDescription("no description");
    }



    public HL7ValidatedMessage(final HL7ValidatedMessage inMessage) {
        if (inMessage != null) {
            this.validatedMessage = inMessage.getValidatedMessage();
        }
    }

    /**
     * Getters and Setters
     */

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getValidatedMessage() {
        // log.info("getValidatedMessage " + validatedMessage);
        return this.validatedMessage;
    }

    public void setValidatedMessage(final String validatedMessage) {
        // log.info("setValidatedMessage " + validatedMessage);
        this.validatedMessage = validatedMessage;
    }

    public String getProfileOid() {
        return this.profileOid;
    }

    public void setProfileOid(final String profileOid) {
        this.profileOid = profileOid;
    }

    /**
     * Get the HL7v2.x message for the specfied OID

     */
    public static HL7ValidatedMessage getMessageByOID(final String inOID, final String privacyKey) {
        return ValidatedObject.getObjectByOID(HL7ValidatedMessage.class, inOID, privacyKey);
    }

    public static HL7ValidatedMessage storeHL7Message(HL7ValidatedMessage messageToStore) {
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        if (messageToStore.getOid() == null) {
            messageToStore.setOid(OIDGenerator.getNewOid());
        }
        messageToStore = em.merge(messageToStore);
        em.flush();

        //Update the cache with the newest values
        GazelleValidationCacheManager
                .refreshGazelleCacheWebService(messageToStore.getExternalId(), messageToStore.getToolOid(),
                        messageToStore.getOid(), "off");
        return messageToStore;
    }

    public static HL7ValidatedMessage getHL7ValidatedMessageById(final int messageId, final String privacyKey) {
        return ValidatedObject.getObjectByID(HL7ValidatedMessage.class, Integer.valueOf(messageId), privacyKey);
    }

    @Override
    public String getFilePath() {
        return null;
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/hl7v2Result.seam?").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (! super.equals(obj)) {
            return false;
        }
        HL7ValidatedMessage fobj = (HL7ValidatedMessage) obj;

        return id.equals(fobj.getId()) && validatedMessage.equals((fobj.getValidatedMessage()))
                && profileOid
                .equals(fobj.getProfileOid()) ;

    }
}
