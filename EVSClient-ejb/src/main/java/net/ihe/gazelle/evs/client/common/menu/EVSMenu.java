/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.menu.Menu;
import net.ihe.gazelle.common.pages.menu.MenuEntry;
import net.ihe.gazelle.common.pages.menu.MenuGroup;
import net.ihe.gazelle.evs.client.common.model.EVSMenuGroup;
import net.ihe.gazelle.evs.client.common.model.EVSMenuGroupQuery;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class EVSMenu {

    private static final Logger LOGGER = LoggerFactory.getLogger(EVSMenu.class);
    public static final String STANDARD_PARAM = "standard";
    public static final String EXTENSION_PARAM = "extension";
    private static final String NO_EXTENSION = "None";
    public static final String TYPE_PARAM = "type";
    public static final String OID_PARAM = "oid";
    public static final String ID_PARAM = "id";
    public static final String PRIVACY_KEY_PARAM = "privacyKey";

    private static MenuGroup MENU;

    public static synchronized Menu getMenu() {
        if (EVSMenu.MENU == null) {
            EVSMenu.MENU = EVSMenu.computeMenu();
        }
        return EVSMenu.MENU;
    }

    public static void forceUpdate() {
        synchronized (EVSMenu.class) {
            EVSMenu.MENU = EVSMenu.computeMenu();
        }
    }

    private static MenuGroup computeMenu() {
        // all elements
        final ArrayList<Menu> children = new ArrayList<>();




        // Customized menu begins here
        final EVSMenuGroupQuery query = new EVSMenuGroupQuery();
        query.available().eq(Boolean.TRUE);
        query.orderInGui().order(true);
        final List<EVSMenuGroup> menusFromDb = query.getListNullIfEmpty();

        if (menusFromDb != null) {
            for (final EVSMenuGroup group : menusFromDb) {
                final List<Menu> groupItems = new ArrayList<>();
                if (group.getStandards() != null && !group.getStandards().isEmpty()) {
                    Collections.sort(group.getStandards(), EVSMenu.COMPARATOR);
                    for (final ReferencedStandard standard : group.getStandards()) {
                        if ("DICOM".equals(standard.getName())) {
                            EVSMenu.addDICOM(groupItems);
                        } else {
                            final List<Menu> standardItems = new ArrayList<>();
                            standardItems.add(new MenuEntry(
                                    new PageValidate(standard)));
                            standardItems.add(new MenuEntry(new PageLogs(standard)));
                            EVSMenu.addStatistics(standardItems, standard);
                            EVSMenu.addMenu(groupItems, standardItems, standard.getMenuDisplayName(),
                                    standard.getIconName());
                        }
                    }
                }
                EVSMenu.addMenu(children, groupItems, group.getLabel(), group.getIconPath(), group.getVisibleToAll()?Authorizations.ALL:Authorizations.LOGGED);
            }
        }

        // Add-ons menu
        if (PreferenceService.getBoolean("add_ons_enabled")) {
            final List<Menu> addOnsMenu = new ArrayList<>();

            // Validator detector Menu
            final List<Menu> detectorItems = new ArrayList<>();
            EVSMenu.addValidate(detectorItems, "/messageContentAnalyzer.xhtml", "Message Content Analyzer");
            EVSMenu.addLogs(detectorItems, "/common/messageContentAnalyzerResultLogs.seam",
                    "Message Content Analyzer Result Logs");

            // Dicom SCP Screener Menu
            final List<Menu> dicomTools = new ArrayList<>();
            EVSMenu.addValidate(dicomTools, "/dicom/SCPValidator.xhtml", "DICOM SCP Screener");
            EVSMenu.addLogs(dicomTools, "/dicom/SCPValidatorResult.xhtml", "DICOM SCP Screener Logs");

            EVSMenu.addMenu(addOnsMenu, detectorItems, "Message Content Analyzer", "fa fa-barcode", Authorizations.ALL);
            EVSMenu.addMenu(addOnsMenu, dicomTools, "Dicom Tools", "gzl-font gzl-dicom", Authorizations.ALL);

            // Cross Validator
            final List<Menu> xvalidation = new ArrayList<>();
            xvalidation.add(new MenuEntry(XValidationPages.VAL_VALIDATION));
            xvalidation.add(new MenuEntry(XValidationPages.VAL_LOGS));
            xvalidation.add(new MenuEntry(XValidationPages.DOC_HOME));
            EVSMenu.addMenu(addOnsMenu, xvalidation, "Gazelle Cross Validation", "fa fa-sitemap", Authorizations.CROSS_VALIDATION);
            addOnsMenu.add(new MenuEntry(Pages.SCHEMATRONS));
            addOnsMenu.add(new MenuEntry(Pages.STATISTICS));

            EVSMenu.addMenu(children, addOnsMenu, "Add-ons", "fa fa-wrench", Authorizations.ALL);
        }

        // Administration Menu
        final List<Menu> adminItems = new ArrayList<>();
        adminItems.add(new MenuEntry(Pages.VALIDATION_SERVICES));
        adminItems.add(new MenuEntry(Pages.REFERENCED_STANDARDS));
        adminItems.add(new MenuEntry(Pages.CONFIGURATION));
        adminItems.add(new MenuEntry(Pages.CALLING_TOOLS));
        adminItems.add(new MenuEntry(Pages.MENU_CONFIG));
        adminItems.add(new MenuEntry(Pages.ADMIN_TEMPLATES));
        if (PreferenceService.getBoolean("x_validation_enabled")) {
            adminItems.add(new MenuEntry(XValidationPages.ADMIN_IMPORT_VALIDATORS));
            adminItems.add(new MenuEntry(XValidationPages.ADMIN_VALIDATORS));
        }
        EVSMenu.addMenu(children, adminItems, "gazelle.evs.client.admin.main.menu", "fa fa-television",
                Authorizations.ADMIN);

        return new MenuGroup(null, children);
    }

    /**
     * This comparator is used to sort the statistics by decreasing result
     */
    protected static final Comparator<ReferencedStandard> COMPARATOR = new Comparator<ReferencedStandard>() {

        @Override
        public int compare(final ReferencedStandard o1, final ReferencedStandard o2) {
            return o1.getMenuDisplayName().compareTo(o2.getMenuDisplayName());
        }

    };

    private static void addDICOM(final List<Menu> iheItems) {
        final List<Menu> dicomItems = new ArrayList<>();
        EVSMenu.addValidate(dicomItems, "/dicom/validator.xhtml", "gazelle.evs.client.dicom.menu.validator");
        EVSMenu.addLogs(dicomItems, "/dicom/allLogs.xhtml", "gazelle.evs.client.dicom.menu.logger");
        EVSMenu.addStatistics(dicomItems, "DICOM");
        EVSMenu.addMenu(iheItems, dicomItems, "DICOM", "gzl-font gzl-dicom");
    }

    private static void addValidate(final List<Menu> items, final String url, final String label) {
        items.add(new MenuEntry(new PageValidate(url, label, null, null, null)));
    }

    private static void addLogs(final List<Menu> items, final String link, final String label) {
        items.add(new MenuEntry(new PageLogs(link, label, null, null)));
    }

    private static void addStatistics(final List<Menu> items, final ReferencedStandard standard) {
        items.add(new MenuEntry(new PageStatistics(standard.getName(), standard.getExtension(), standard.getLabel())));
    }

    private static void addStatistics(final List<Menu> items, final String type) {
        items.add(new MenuEntry(new PageStatistics(type)));
    }

    private static void addMenu(final List<Menu> parentItems, final List<Menu> items, final String title, final String img,
            final Authorization... authorizations) {
        EVSMenu.LOGGER.debug("adding menu :{} {}", title, img);
        parentItems.add(new MenuGroup(new PageMenu(title, img, authorizations), items));
    }

    public static void appendUrlParameters(StringBuilder builder, String standardLabel, String extension) {
        boolean firstparam = true;
        if (standardLabel != null && !standardLabel.isEmpty()) {
            builder.append('?');
            builder.append(STANDARD_PARAM);
            builder.append('=');
            builder.append(standardLabel);
            firstparam = false;
        }
        if (extension != null && !NO_EXTENSION.equalsIgnoreCase(extension)) {
            if (firstparam) {
                builder.append('?');
            } else {
                builder.append('&');
            }
            builder.append(EXTENSION_PARAM);
            builder.append('=');
            builder.append(extension);
        }
    }
}
