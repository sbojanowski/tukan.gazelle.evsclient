/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evs.client.dicom.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.util.Util;
import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;

/**
 * <b>Class Description : </b>DicomValidatedObject<br>
 * <br>
 * This class describes the DicomValidatedObject which represents the DICOM objects uploaded by the user and validated by the selected service
 * <p/>
 * DicomValidatedObject possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id of the message in database</li>
 * <li><b>objectPath</b> : Path to the DICOM object which has been uploaded and stored ON disk -location is given by an application preference-</li>
 * <li><b>description</b> : This attribute can be used be the user to describe its CDA file and thus retrieve it later more easily</li>
 * <li><b>detailedResults</b> : detailed results of the validation (error, warnings ...)</li>
 * <li><b>metadata</b> : DICOM metadata (method, language ...)</li>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, October 21st
 * @class DicomValidatedObject.java
 * @package net.ihe.gazelle.evs.client.dicom.model
 * @see Aberge@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Name("dicomValidatedObject")
@Table(name = "dicom_validated_object", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "dicom_validated_object_sequence", sequenceName = "dicom_validated_object_id_seq", allocationSize = 1)
public class DicomValidatedObject extends ValidatedObject {

    /**
     *
     */
    private static final long serialVersionUID = 3435376463635134710L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DicomValidatedObject.class);


    private static final String DICOM_FILE_PREFIX = "validatedDICOM_";
    private static final String DICOM_FILE_SUFFIX = ".dcm";

    @Id
    @NotNull
    @GeneratedValue(generator = "dicom_validated_object_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "object_path")
    private String objectPath;

    @Column(name = "dicom_operation")
    private String dicomOperation;

    @Column(name = "dicom_service_version")
    private String dicomServiceVersion;

    @Column(name = "file_content")
    @Lob
    @Type(type = "text")
    private String dicomFileContent;

    @Column(name = "dicomToolDumper")
    private String dicomToolDumper;

    @Column(name = "dicomToolValidator")
    private String dicomToolValidator;

    public DicomValidatedObject() {

    }

    public DicomValidatedObject(final DicomValidatedObject inDicomObject) {
        this.objectPath = inDicomObject.getObjectPath();
        this.description = inDicomObject.getDescription();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getObjectPath() {
        return this.objectPath;
    }

    public void setObjectPath(final String objectPath) {
        this.objectPath = objectPath;
    }

    public String getDicomServiceVersion() {
        return this.dicomServiceVersion;
    }

    public void setDicomServiceVersion(final String dicomServiceVersion) {
        this.dicomServiceVersion = dicomServiceVersion;
    }

    public String getDicomFileContent() {
        return this.dicomFileContent;
    }

    public void setDicomFileContent(final String dicomFileContent) {
        this.dicomFileContent = dicomFileContent;
    }

    @Override
    public String getFilePath() {
        return this.objectPath;
    }

    public void setDicomOperation(final String dicomOperation) {
        this.dicomOperation = dicomOperation;
    }

    public String getDicomOperation() {
        return this.dicomOperation;
    }

    public String getDicomToolDumper() {
        return this.dicomToolDumper;
    }

    public void setDicomToolDumper(final String dicomToolDumper) {
        this.dicomToolDumper = dicomToolDumper;
    }

    public String getDicomToolValidator() {
        return this.dicomToolValidator;
    }

    public void setDicomToolValidator(final String dicomToolValidator) {
        this.dicomToolValidator = dicomToolValidator;
    }

    /**
     * @param inDicomValidatedObject
     */
    public static DicomValidatedObject createNewDicomObject() {
        DicomValidatedObject dicomObject = new DicomValidatedObject();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        dicomObject = em.merge(dicomObject);
        em.flush();
        if (dicomObject == null || dicomObject.getId() == 0) {
            DicomValidatedObject.LOGGER.error("cannot create DICOM Object");
            return null;
        } else {
            final String dicomRepository = ApplicationPreferenceManager.getStringValue("dicom_repository");
            if (dicomRepository == null || dicomRepository.isEmpty()) {
                DicomValidatedObject.LOGGER.error("The repository for dicom objects (dicom_repository) has not been set");
                return null;
            } else {
                dicomObject.setObjectPath(
                        dicomRepository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + DicomValidatedObject.DICOM_FILE_PREFIX
                                .concat(dicomObject.getId().toString())
                                .concat(DicomValidatedObject.DICOM_FILE_SUFFIX));
                return dicomObject;
            }
        }
    }

    /**
     */
    public static DicomValidatedObject mergeDicomValidatedObject(DicomValidatedObject inDicomObject) {
        if (inDicomObject == null) {
            DicomValidatedObject.LOGGER.error("The given DicomValidatedObject is null !!!");
            return null;
        } else if (inDicomObject.getObjectPath() == null || inDicomObject.getObjectPath().isEmpty()) {
            DicomValidatedObject.LOGGER.error("Cannot merge the given DicomValidatedObject because objectPath is null");
            return null;
        } else {
            final File uploadedFile = new File(inDicomObject.getObjectPath());
            if (!uploadedFile.exists()) {
                DicomValidatedObject.LOGGER.error("The given file does not exist: {}", inDicomObject.getObjectPath());
                return null;
            }
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            inDicomObject = em.merge(inDicomObject);
            em.flush();

            // Update the cache with the newest values
            GazelleValidationCacheManager
                    .refreshGazelleCacheWebService(inDicomObject.getExternalId(), inDicomObject.getToolOid(),
                            inDicomObject.getOid(), "off");

            return inDicomObject;
        }
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/dicomResult.seam?").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null && !this.getPrivacyKey().isEmpty()) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (! super.equals(obj)) {
            return false;
        }
        DicomValidatedObject fobj = (DicomValidatedObject) obj;

        return id.equals(fobj.getId()) && objectPath.equals((fobj.getObjectPath())) && dicomServiceVersion
                .equals(fobj.getDicomServiceVersion()) && dicomOperation.equals(fobj.getDicomOperation());

    }

}
