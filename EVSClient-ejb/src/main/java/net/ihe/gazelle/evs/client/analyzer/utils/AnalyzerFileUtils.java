/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.utils;

import org.apache.commons.io.FileUtils;
import org.apache.tika.io.IOUtils;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class AnalyzerFileUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnalyzerFileUtils.class);


    public static void deleteTmpFile(final File f) {
        if (f.exists()) {
            if (f.isDirectory()) {
                // directory is empty, then delete it
                final String[] list = f.list();
                if(list !=null){

                    if (list.length == 0) {
                        if ( f.delete()) {
                            AnalyzerFileUtils.LOGGER.debug("deleting directory : {}", f.getAbsolutePath());
                        }
                        else {
                            AnalyzerFileUtils.LOGGER.error("Unable to delete directory : {}", f.getAbsolutePath());
                        }
                    } else {
                        // list all the directory contents
                        final String[] files = list;
                        for (final String temp : files) {
                            // construct the file structure
                            final File fileDelete = new File(f, temp);
                            // recursive delete
                            AnalyzerFileUtils.deleteTmpFile(fileDelete);
                        }
                        // check the directory again, if empty then delete it
                        if (list.length == 0) {
                            if ( f.delete()) {
                                AnalyzerFileUtils.LOGGER.debug("deleting directory : {}", f.getAbsolutePath());
                            }
                            else {
                                AnalyzerFileUtils.LOGGER.error("Unable to delete directory : {}", f.getAbsolutePath());
                            }
                        }
                    }
                }
            } else {
                if (f.delete()) {
                    AnalyzerFileUtils.LOGGER.debug("deleting file : {}", f.getName());
                } else {
                    AnalyzerFileUtils.LOGGER.error("deletion operation failed.");
                }
            }
        } else {
            AnalyzerFileUtils.LOGGER.error("Delete operation is already done!");
        }
    }

    // Set a string with the content of a file
    public static String fileToString(final File file) {
        String result = null;
        DataInputStream in = null;
        FileInputStream fileInputStream = null;

        try {
            final byte[] buffer = new byte[(int) file.length()];
            fileInputStream = new FileInputStream(file);
            in = new DataInputStream(fileInputStream);
            in.readFully(buffer);
            result = new String(buffer, StandardCharsets.UTF_8);
        } catch (final IOException e) {
            FacesMessages.instance().add(Severity.ERROR, e.getMessage());
            AnalyzerFileUtils.LOGGER.error("IO problem in fileToString : {}", e.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (final IOException e) {
                AnalyzerFileUtils.LOGGER.error("unable to close the opened stream", e.getMessage());
            }

        }
        return result;
    }

    public static boolean base64Encoding(final File fileToEncode, final File encodedFile) {

        byte[] fileByte;
        String encoded;
        try {
            fileByte = IOUtils.toByteArray(new FileInputStream(fileToEncode));
            // encoding byte array into base 64
         //   encoded = Base64.encodeBase64(fileByte);
            encoded = DatatypeConverter.printBase64Binary(fileByte);
            FileUtils.writeByteArrayToFile(encodedFile, encoded.getBytes());
            return true;
        } catch (final FileNotFoundException e) {
            AnalyzerFileUtils.LOGGER.error("{}", e);
            return false;
        } catch (final IOException e) {
            AnalyzerFileUtils.LOGGER.error("{}", e);
            return false;
        }
    }

    public static String convertFileToString(final File file) {
        String fileContent = null;

        try {
            fileContent = FileUtils.readFileToString(file);
        } catch (final IOException e) {
            AnalyzerFileUtils.LOGGER.error(e.getMessage());
        }
        return fileContent;
    }

}
