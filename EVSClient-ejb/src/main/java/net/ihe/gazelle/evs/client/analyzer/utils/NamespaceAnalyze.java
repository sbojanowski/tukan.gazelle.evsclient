/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class NamespaceAnalyze {

    private static final Logger LOGGER = LoggerFactory.getLogger(NamespaceAnalyze.class);

    public NamespaceAnalyze() {
    }

    /**
     */
    public Map<String, String> getListOfPrefixAndURIForNamespaces(final File _FilePath) throws SAXException {
        Document document;
        final Map<String, String> mapsPrefixUri = new HashMap<>();
        try {

            final DocumentBuilderFactory dbfact = DocumentBuilderFactory.newInstance();
            dbfact.setAttribute("http://apache.org/xml/features/nonvalidating/load-external-dtd", Boolean.FALSE);
            dbfact.setFeature("http://apache.org/xml/features/disallow-doctype-decl", Boolean.TRUE);
            dbfact.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            dbfact.setNamespaceAware(true);
            final DocumentBuilder builder = dbfact.newDocumentBuilder();
            document = builder.parse(_FilePath);

            final XPathFactory fabrique = XPathFactory.newInstance();
            final XPath xpath = fabrique.newXPath();

            final NodeList nodeList;
            try {
                nodeList = (NodeList) xpath.evaluate("//namespace::*", document, XPathConstants.NODESET);

                for (int i = 0; i < nodeList.getLength(); i++) {
                    mapsPrefixUri.put(nodeList.item(i).getNodeName(), nodeList.item(i).getNodeValue());
                }
            } catch (final XPathExpressionException e) {
                NamespaceAnalyze.LOGGER.error("Error when evaluate xpath : {}", e);
            }
        } catch (final ParserConfigurationException e) {
            NamespaceAnalyze.LOGGER.error("Error when creating document builder : {}", e.getMessage());
        } catch (final IOException e) {
            NamespaceAnalyze.LOGGER.error("Error when reading file : {}", e.getMessage());
        }

        return mapsPrefixUri;
    }
}