/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file;

import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.common.action.FileToValidate;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class DICOM implements IAnalyzableContent {

    private static final Logger LOGGER = LoggerFactory.getLogger(DICOM.class);

    @Override
    public boolean analyze(final File f, final FileToValidate parent, final FileToValidate ftv, final MessageContentAnalyzer mca,
            final int startOffset, final int endOffset) {
        DICOM.LOGGER.info("Launch dicom validation");
        if (parent.getDocType().equals(B64.BASE64)) {
            ftv.setStartOffset(mca.getBase64OffsetStart());
            ftv.setEndOffset(mca.getBase64OffsetEnd());
        } else {
            ftv.setStartOffset(startOffset);
            ftv.setEndOffset(endOffset);
        }
        mca.setSaveStartOffset(0);
        ftv.setDocType(MessageContentAnalyzer.DICOM);
        ftv.setMessageContentAnalyzerOid(OIDGenerator.getNewOid());

        ftv.setValidationType(MessageContentAnalyzer.DICOM);
        parent.getFileToValidateList().add(ftv);
        return true;
    }

}
