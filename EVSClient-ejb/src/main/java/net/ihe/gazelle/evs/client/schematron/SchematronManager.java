/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.schematron;

import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.evs.client.common.model.ValidationService;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub;
import net.ihe.gazelle.sch.validator.ws.client.SOAPExceptionException;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants.Configuration;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


@Name("schematronManagerBean")
@Scope(ScopeType.PAGE)
public class SchematronManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SchematronManager.class);

    private List<Schematron> availableSchematrons;
    private Schematron selectedSchematron;
    private String selectedObjectType;
    private transient GazelleObjectValidatorServiceStub stub;

    public SchematronManager() {
    }

    public Schematron getSelectedSchematron() {
        return this.selectedSchematron;
    }

    public void setSelectedSchematron(final Schematron selectedSchematron) {
        this.selectedSchematron = selectedSchematron;
    }

    public String getSelectedObjectType() {
        return this.selectedObjectType;
    }

    public void setSelectedObjectType(final String selectedObjectType) {
        this.selectedObjectType = selectedObjectType;
    }

    @Create
    public void init() {
        final List objectTypes = this.getListOfObjectTypes();
        if (!objectTypes.isEmpty()){
            final SelectItem type = (SelectItem) objectTypes.get(0);
            this.setSelectedObjectType(type.getValue().toString());
        }
        this.getListOfSchematrons();
    }

    public List<SelectItem> getListOfObjectTypes() {
        final List<SelectItem> items = new ArrayList<>();
        final GazelleObjectValidatorServiceStub.GetAllAvailableObjectTypes gaaot = new GazelleObjectValidatorServiceStub.GetAllAvailableObjectTypes();
        final GazelleObjectValidatorServiceStub.GetAllAvailableObjectTypesE gaaotE = new GazelleObjectValidatorServiceStub.GetAllAvailableObjectTypesE();
        gaaotE.setGetAllAvailableObjectTypes(gaaot);
        if (this.stub == null) {
            if (!this.createStub()) {
                this.availableSchematrons = null;
                SchematronManager.LOGGER.error("getListOfObjectTypes() : Failed to create stub");
                return new ArrayList<>();
            }
        }
        try {
            final GazelleObjectValidatorServiceStub.GetAllAvailableObjectTypesResponseE resp = this.stub.getAllAvailableObjectTypes(gaaotE);
            final GazelleObjectValidatorServiceStub.ObjectType[] listOfObjectTypes = resp.getGetAllAvailableObjectTypesResponse().getObjectType();
            for (final GazelleObjectValidatorServiceStub.ObjectType ot : listOfObjectTypes) {
                items.add(new SelectItem(ot.getKeyword(), ot.getLabelToDisplay()));
            }

        } catch (final RemoteException e) {
            SchematronManager.LOGGER.error("unable to retrieve the list of schematrons :{}", e.getMessage());
        }
        return items;
    }

    public void getListOfSchematrons() {

        if (this.selectedObjectType != null) {
            if (this.stub == null) {
                if (!this.createStub()) {
                    this.availableSchematrons = null;
                    SchematronManager.LOGGER.error("getListOfSchematrons() : Failed to create stub");
                    return;
                }
            }
            try {
                final GazelleObjectValidatorServiceStub.GetSchematronsForAGivenType params = new GazelleObjectValidatorServiceStub.GetSchematronsForAGivenType();
                params.setFileType(this.selectedObjectType);
                final GazelleObjectValidatorServiceStub.GetSchematronsForAGivenTypeE paramsE = new GazelleObjectValidatorServiceStub.GetSchematronsForAGivenTypeE();
                paramsE.setGetSchematronsForAGivenType(params);
                final GazelleObjectValidatorServiceStub.GetSchematronsForAGivenTypeResponseE responseE = this.stub.getSchematronsForAGivenType(paramsE);
                final GazelleObjectValidatorServiceStub.Schematron[] schematrons = responseE
                        .getGetSchematronsForAGivenTypeResponse().getSchematrons();
                if (schematrons != null && schematrons.length > 0) {
                    this.availableSchematrons = new ArrayList<>();
                    for (final GazelleObjectValidatorServiceStub.Schematron schematron : schematrons) {
                        this.availableSchematrons.add(new Schematron(schematron.getLabel(), schematron.getName(),
                                schematron.getVersion(), schematron.getDescription()));
                    }
                }
            } catch (final RemoteException|SOAPExceptionException e) {
                SchematronManager.LOGGER.error("unable to retrieve the list of schematrons :{}", e.getMessage());
            }

        }

    }

    public void downloadSelectedSchematron(final Schematron inSchematron) {
        if (inSchematron != null) {
            this.selectedSchematron = inSchematron;
            if (this.stub == null) {
                if (!this.createStub()) {
                    return;
                }
            }
            try {
                final GazelleObjectValidatorServiceStub.GetSchematronByName params = new GazelleObjectValidatorServiceStub.GetSchematronByName();
                params.setSchematronName(this.selectedSchematron.getLabel());
                final GazelleObjectValidatorServiceStub.GetSchematronByNameE paramsE = new GazelleObjectValidatorServiceStub.GetSchematronByNameE();
                paramsE.setGetSchematronByName(params);
                final GazelleObjectValidatorServiceStub.GetSchematronByNameResponseE responseE = this.stub.getSchematronByName(paramsE);
             //   final String schematronContent = new String(
                   //     Base64.decode(responseE.getGetSchematronByNameResponse().getSchematron()), StandardCharsets.UTF_8);
                final String schematronContent = new String(
                        DatatypeConverter.parseBase64Binary(responseE.getGetSchematronByNameResponse().getSchematron()),StandardCharsets.UTF_8);
                final String fileName = this.selectedSchematron.getLabel().replace(" ", "").concat(".sch");
                SchematronManager.LOGGER.info("filename{}", fileName);
                ReportExporterManager.exportToFile("text/xml", schematronContent, fileName);
            } catch (final RemoteException|SOAPExceptionException e) {
                SchematronManager.LOGGER.error("unable to retrieve selected schematron : {}", e.getMessage());
            }
        }

    }

    public void addFileToZipFile(final String zipFileName) throws IOException, SOAPExceptionException {
        String label;
        try (
                FileOutputStream fos = new FileOutputStream(zipFileName);
                BufferedOutputStream bos = new BufferedOutputStream(fos);
                ZipOutputStream zos = new ZipOutputStream(bos)) {

            for (final Schematron schematron : this.availableSchematrons) {
                final GazelleObjectValidatorServiceStub.GetSchematronByName params = new GazelleObjectValidatorServiceStub.GetSchematronByName();
                params.setSchematronName(schematron.getLabel());
                final GazelleObjectValidatorServiceStub.GetSchematronByNameE paramsE = new GazelleObjectValidatorServiceStub.GetSchematronByNameE();
                paramsE.setGetSchematronByName(params);
                final GazelleObjectValidatorServiceStub.GetSchematronByNameResponseE responseE = this.stub.getSchematronByName(paramsE);
              //  final String schematronContent = new String(
              //          Base64.decode(responseE.getGetSchematronByNameResponse().getSchematron()),StandardCharsets.UTF_8);
                final String schematronContent = new String(
                        DatatypeConverter.parseBase64Binary(responseE.getGetSchematronByNameResponse().getSchematron()),StandardCharsets.UTF_8);
                // the following replaceAll call is to clean-up the filename in the zip file.
                label = schematron.getLabel();
                label = label.replaceAll("[^a-zA-Z0-9.-]", "_").concat(".sch");
                zos.putNextEntry(new ZipEntry(label));
                zos.write(schematronContent.getBytes(StandardCharsets.UTF_8));
                zos.closeEntry();
                zos.flush();
            }
        }

    }


    public void downloadSelectionAsZip() {
        if (this.selectedObjectType != null && this.availableSchematrons != null && !this.availableSchematrons.isEmpty()) {
            final String zipFileName = this.selectedObjectType + "_schematrons.zip";
            try {
                this.addFileToZipFile(zipFileName);

                // export
                final FacesContext context = FacesContext.getCurrentInstance();
                final HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                response.setContentType("application/zip");
                response.setHeader("Content-Disposition", "attachment;filename=" + zipFileName);
                final ServletOutputStream servletOutputStream = response.getOutputStream();

                // get bytes from zip file
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(zipFileName);
                    final Integer zipLength = Integer.valueOf(fis.available());
                    final byte[] buffer = new byte[zipLength];
                    fis.read(buffer);
                    servletOutputStream.write(buffer);
                }
                finally {
                    if (fis != null) {
                        fis.close();
                    }
                }

                servletOutputStream.flush();
                servletOutputStream.close();
                context.responseComplete();

            } catch (IOException|SOAPExceptionException e) {
                FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "Error in this application : " + e.getMessage());
            }

        } else {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "net.ihe.gazelle.evs.schematron.selectAnObjectType");
        }
    }

    public GazelleListDataModel<Schematron> getAvailableSchematrons() {
        if (this.availableSchematrons != null){
            return new GazelleListDataModel<Schematron>(this.availableSchematrons);
        } else {
            return null;
        }
    }

    private boolean createStub() {
        final ValidationService service = ValidationService.getValidationServiceByKeyword("Gazelle");
        if (service != null && service.getWsdl() != null && service.getWsdl().length() > 0) {
            try {
                this.stub = new GazelleObjectValidatorServiceStub(service.getWsdl());
                this.stub._getServiceClient().getOptions().setProperty(Configuration.DISABLE_SOAP_ACTION, Boolean.TRUE);
                return true;
            } catch (final AxisFault e) {
                SchematronManager.LOGGER.error("unable to create stub at {}", service.getWsdl());
                return false;
            }
        } else {
            return false;
        }
    }
}
