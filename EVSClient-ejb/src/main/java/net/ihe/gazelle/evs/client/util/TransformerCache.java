package net.ihe.gazelle.evs.client.util;

import com.sun.org.apache.xerces.internal.impl.Constants;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.XMLConstants;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class TransformerCache {
    private static Logger LOGGER = LoggerFactory.getLogger(TransformerCache.class);
    private static Map<String,Transformer > transformerMap = new HashMap<>();
    private static TransformerCache instance;

    public static TransformerCache instance(){
        if (instance == null){
            instance = new TransformerCache();
        }
        return instance;
    }


    public Transformer getTransformerFromCache(String inXslPath) {

        Transformer transformer = null;
        if (inXslPath == null || inXslPath.isEmpty()) {
            return null;
        }
        if (transformerMap.containsKey(inXslPath)) {
            transformer = transformerMap.get(inXslPath);
            return transformer;
        } else {
            try {
           //     final TransformerFactory tFactory = TransformerFactory.newInstance();
                final TransformerFactory tFactory = TransformerFactory.newInstance("org.apache.xalan.processor.TransformerFactoryImpl", null);
                try {
                    tFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, Boolean.TRUE);
                 //   tFactory.setAttribute(Constants.DISALLOW_DOCTYPE_DECL_FEATURE, Boolean.TRUE);
                 //   tFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
                    tFactory.setAttribute(Constants.LOAD_DTD_GRAMMAR_FEATURE, Boolean.FALSE);
                    tFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
                }
                catch (IllegalArgumentException e)
                {
                    LOGGER.error(e.getMessage());
                }
                if (inXslPath.startsWith("http")) {

                   transformer = tFactory.newTransformer(new StreamSource(inXslPath));

                } else {
                    transformer = tFactory.newTransformer(new StreamSource(
                            ApplicationPreferenceManager.instance().getGazelleBinPath() + File.separatorChar + "xsl"
                                    + File.separatorChar + inXslPath));
                }
                transformerMap.put(inXslPath, transformer);
                return transformer;
            } catch (TransformerConfigurationException e) {
                e.printStackTrace();

            }
            return null;
        }
    }
}