/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.menu.EVSMenu;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.common.model.ValidatedObjectQuery;
import net.ihe.gazelle.evs.client.users.action.SSOIdentity;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Map;

public abstract class AbstractLogBrowser<T extends ValidatedObject> implements QueryModifier<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractLogBrowser.class);

    private static final long serialVersionUID = 5651865677305923944L;

    protected FilterDataModel<T> dataModel;
    protected Filter<T> filter;
    protected T selectedObject;
    private Date startDate;
    private Date endDate;
    private String objectType;
    private String extension;
    private String standardLabel;

    protected abstract Class<T> getEntityClass();

    protected abstract HQLCriterionsForFilter<T> getCriterions();

    public void getObjectFromUrlParams() {
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        final String privacyKey = urlParams.get(EVSMenu.PRIVACY_KEY_PARAM);
        if (urlParams.containsKey(EVSMenu.OID_PARAM)) {
            this.selectedObject = this.getObjectByOID(urlParams.get(EVSMenu.OID_PARAM), privacyKey);
        } else if (urlParams.containsKey(EVSMenu.TYPE_PARAM)) {
            this.setObjectType(urlParams.get(EVSMenu.TYPE_PARAM));
        }
    }

    /**
     * This method must select an object according to its oid in the database

     */
    protected abstract T getObjectByOID(String oid, String privacyKey);

    public void reset() {
        if (this.filter != null) {
            this.filter.clear();
        }
        this.resetDate();
    }

    // public abstract String getImageSrcForStatus(String status);

    public void resetDate() {
        this.startDate = null;
        this.endDate = null;
        if (this.filter != null) {
            this.filter.getPossibleValues();
        }
        this.setCurrentSelectDate("--");
    }

    //TODO EP : This code cannot work. OID is a string and Id is Integer !!!

    public FilterDataModel<T> getDataModel() {
        return new FilterDataModel<T>(AbstractLogBrowser.this.getFilter()) {
            @Override
            protected Object getId(final T t) {
                return t.getOid();
            }
        };
    }

    public Filter<T> getFilter() {
        ValidatedObject.removeNullEntries(this.getEntityClass());
        if (this.filter == null) {
            Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            extension = urlParams.get(EVSMenu.EXTENSION_PARAM);
            standardLabel = urlParams.get(EVSMenu.STANDARD_PARAM);
            final HQLCriterionsForFilter<T> criterions = this.getCriterions();
            criterions.addQueryModifier(this);
            this.filter = new Filter<>(criterions, urlParams);
        }
        return this.filter;
    }

    public void setFilter(final Filter<T> filter) {
        this.filter = filter;
    }

    public T getSelectedObject() {
        return this.selectedObject;
    }

    public void setSelectedObject(final T selectedObject) {
        this.selectedObject = selectedObject;
    }

    public void deleteSelectedObject() {
        deleteSelectedObject(this.getSelectedObject());
    }

    public void deleteSelectedObject( T object){
        String message ;
        try {
            String path = object.getFilePath();
            if (null != path){
                Files.deleteIfExists(Paths.get(object.getFilePath()));
            }
        } catch (NoSuchFileException x) {
            message = "%s: no such" + " file %n" + object.getFilePath();
            LOGGER.error(message);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, message);
        } catch (IOException x) {
            // File permission problems are caught here.
            message = "Insufficient permission to delete file" + object.getFilePath();
            LOGGER.error(message);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, message);

        }
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        object = entityManager.merge(object);
        entityManager.remove(object);
        entityManager.flush();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Log ("+ object.getOid()+") successfully deleted ");
    }

    public Date getStartDate() {
        if (this.startDate == null){
            return  null;
        }
        else {
            return (Date) this.startDate.clone();
        }
    }

    public final void setStartDate(final Date startDate) {
        this.startDate = (Date) startDate.clone();
        this.getFilter().modified();
    }

    public Date getEndDate() {
        if (this.endDate == null){
            return null;
        }else {
            return (Date) this.endDate.clone();
        }
    }

    public final void setEndDate(final Date endDate) {
        if(endDate != null) {
            this.endDate = (Date) endDate.clone();
            this.getFilter().modified();
        }
    }

    @Override
    public void modifyQuery(final HQLQueryBuilder<T> queryBuilder, final Map<String, Object> filterValuesApplied) {
        final ValidatedObjectQuery validatedObjectQuery = new ValidatedObjectQuery(queryBuilder);
        if (this.startDate != null) {
            validatedObjectQuery.validationDate().ge(this.startDate);
        }
        if (this.endDate != null) {
            validatedObjectQuery.validationDate().lt(this.endDate);
        }
        if (this.extension != null && !this.extension.isEmpty()){
            validatedObjectQuery.standard().extension().eq(extension);
        }
        if (this.standardLabel != null && !this.standardLabel.isEmpty()){
            validatedObjectQuery.standard().label().eq(standardLabel);
        }

        boolean monitor = false;
        String login = null;
        String institutionKeyword = null;
        final SSOIdentity instance = SSOIdentity.instance();

        // Check if user is logged in, get the username, the role  and the organization name

        if (instance != null && Identity.instance().isLoggedIn()) {

            login = instance.getCredentials().getUsername();
            institutionKeyword = instance.getInstitutionKeyword();

            if (instance.hasRole("monitor_role")|| instance.hasRole("admin_role")) {
                monitor = true;
            }
        }

        // login is null user is not logged in. Can only see the public logs
        if (login == null){
            queryBuilder.addRestriction(HQLRestrictions.eq("isPublic", Boolean.TRUE));
        }
        // Login is not null user is logged in.
        // if the user is not monitor, then he/she can see public logs, his logs and the ones from his/her colleagues
        else if (!monitor) {
            if (ApplicationPreferenceManager.getBooleanValue("show_logs_from_colleagues")){
                queryBuilder.addRestriction(HQLRestrictions
                        .or(HQLRestrictions.eq("privateUser", login),
                                HQLRestrictions.eq("isPublic", Boolean.TRUE),
                                HQLRestrictions.eq("institutionKeyword", institutionKeyword)
                        ));
            } else {
                queryBuilder.addRestriction(HQLRestrictions
                        .or(HQLRestrictions.eq("privateUser", login),
                                HQLRestrictions.eq("isPublic", Boolean.TRUE)
                        ));
            }

        }
        // User is logged in. He/She is a monitor or an admin. He she can see all the logs. No need to add any restrictions

    }

    private String currentSelectDate;

    public String getCurrentSelectDate() {
        return this.currentSelectDate;
    }

    public void setCurrentSelectDate(final String currentSelectDate) {
        this.currentSelectDate = currentSelectDate;
    }


    public String getObjectType() {
        return this.objectType;
    }

    public void setObjectType(final String objectType) {
        this.objectType = objectType;
    }

    public String getExtension() {
        return extension;
    }

}
