/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.common.util.ZipUtility;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.common.action.FileToValidate;
import net.ihe.gazelle.evs.client.common.action.FileType;
import net.ihe.gazelle.evs.client.common.action.GetValidationInfo;
import net.ihe.gazelle.evs.client.common.model.ObjectForValidatorDetector;
import net.ihe.gazelle.evs.client.common.model.ObjectForValidatorDetectorQuery;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.common.model.ValidatedObjectQuery;
import net.ihe.gazelle.evs.client.users.action.SSOIdentity;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.evs.client.util.ValidatorType;
import net.ihe.gazelle.evs.client.util.ViewFileUtils;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.paths.HQLSafePathBasic;
import net.ihe.gazelle.hql.paths.HQLSafePathBasicDate;
import net.ihe.gazelle.hql.paths.HQLSafePathBasicString;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.version.ws.Interface.VersionProvider;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.tika.io.IOUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.contexts.Lifecycle;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.richfaces.component.UITree;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.event.TreeSelectionChangeEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 *
 */
@Name("analyzerBean")
@Scope(ScopeType.PAGE)
public class AnalyzerBean implements QueryModifier<ObjectForValidatorDetector> {

    /**
     *
     */
    private static final long serialVersionUID = 1835607594127751143L;
    public static final String TOOL_OID = "toolOid";

    private static final Logger LOGGER = LoggerFactory.getLogger(AnalyzerBean.class);

    private static final String TMP_FILE_SUFFIX = ".tmp";
    private static final String HQL_VISIBILITY = "isPublic";
    private static final String DICOM = "DICOM";
    private static final String TLS = "TLS";
    private static final String DSUB = "DSUB";
    public static final String NOT_PERFORMED = "not performed";


    private String toolOID;
    private String externalId;
    private String proxyType;

    private String messagePart;

    private ObjectForValidatorDetector analyzedObject;

    private FileToValidate parentFileToValidate = new FileToValidate(null);

    private final Boolean adviseNodeOpened = Boolean.TRUE;

    private FilterDataModel<ObjectForValidatorDetector> dataModel;

    private List<String> fileList = new ArrayList<>();

    private boolean selectedMessagePartEdited;
    private Boolean showMessageInGUI = Boolean.FALSE;
    private Boolean indentMessageInGUI = Boolean.FALSE;

    public AnalyzerBean() {
    }

    public List<String> getFileList() {
        return this.fileList;
    }

    public void setFileList(final List<String> fileList) {
        if (fileList != null) {
            this.fileList = new ArrayList<String>(fileList);
        }
    }

    public String getMessagePart() {
        if (this.indentMessageInGUI) {
            this.messagePart = Util.prettyFormat(this.messagePart);
        }
        return this.messagePart;
    }


    public void setMessagePart(final String messagePart) {
        this.messagePart = messagePart;
    }

    public boolean isSelectedMessagePartEdited() {
        return this.selectedMessagePartEdited;
    }

    public void setSelectedMessagePartEdited(final boolean selectedMessagePartEdited) {
        this.selectedMessagePartEdited = selectedMessagePartEdited;
    }

    public FilterDataModel<ObjectForValidatorDetector> getDataModel() {
        if (this.dataModel == null) {
            this.dataModel = new FilterDataModel<ObjectForValidatorDetector>(
                    new Filter<>(AnalyzerBean.this.getCriterions())) {
                @Override
                protected Object getId(final ObjectForValidatorDetector t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }
        return this.dataModel;
    }


    public String getFileContent() {

        if (this.indentMessageInGUI){
            return Util.prettyFormat(this.analyzedObject.getFileContent());
        }
        return this.analyzedObject.getFileContent();
    }

    private HQLCriterionsForFilter<ObjectForValidatorDetector> getCriterions() {
        final ObjectForValidatorDetectorQuery objectForValidatorDetectorQuery = new ObjectForValidatorDetectorQuery();
        final HQLCriterionsForFilter<ObjectForValidatorDetector> result = objectForValidatorDetectorQuery
                .getHQLCriterionsForFilter();
        result.addQueryModifier(this);
        final HQLSafePathBasicDate<Date> timestamp = objectForValidatorDetectorQuery.validationDate();
        timestamp.setCriterionWithoutTime(TimeZone.getDefault());
        result.addPath("validation_date", timestamp);
        final HQLSafePathBasicString<String> privateUser = objectForValidatorDetectorQuery.privateUser();
        result.addPath("private_user", privateUser);
        final HQLSafePathBasicString<String> institutionKeyword = objectForValidatorDetectorQuery.institutionKeyword();
        result.addPath("institution_keyword", institutionKeyword);
        final HQLSafePathBasic<FileType> ft = objectForValidatorDetectorQuery.validationTypeList().ft();
        result.addPath("validation_type_list", ft);
        return result;
    }

    public void resetFilter() {
        final Filter filter = this.dataModel.getFilter();
        if (filter != null) {
            filter.clear();
        }
    }

    @Override
    public void modifyQuery(final HQLQueryBuilder<ObjectForValidatorDetector> queryBuilder,
            final Map<String, Object> filterValuesApplied) {
        boolean monitor = false;
        String login = null;
        String institutionKeyword = null;

        final SSOIdentity instance = SSOIdentity.instance();
        if (instance != null && instance.isLoggedIn()) {
            login = instance.getCredentials().getUsername();
            institutionKeyword = instance.getInstitutionKeyword();
            if (instance.hasRole("monitor_role")|| instance.hasRole("admin_role")) {
                monitor = true;
            }
        }
        // show all messages if monitor
        if (!monitor) {
            if (login == null) {
                queryBuilder.addRestriction(HQLRestrictions
                        .or(HQLRestrictions.eq(AnalyzerBean.HQL_VISIBILITY, null),
                                HQLRestrictions.eq(AnalyzerBean.HQL_VISIBILITY, Boolean.TRUE)));
            } else {
                queryBuilder.addRestriction(HQLRestrictions
                        .or(HQLRestrictions.eq(AnalyzerBean.HQL_VISIBILITY, null),
                                HQLRestrictions.eq(AnalyzerBean.HQL_VISIBILITY, Boolean.TRUE),
                                HQLRestrictions.eq("privateUser", login),
                                HQLRestrictions.eq("institutionKeyword", institutionKeyword)
                                ));
            }
        }
    }

    public Boolean adviseNodeOpened(final UITree tree) {
        return this.adviseNodeOpened;
    }

    public ObjectForValidatorDetector getAnalyzedObject() {
        return this.analyzedObject;
    }

    public void setAnalyzedObject(final ObjectForValidatorDetector analyzedObject) {
        this.analyzedObject = analyzedObject;
    }

    public FileToValidate getParentFileToValidate() {
        return this.parentFileToValidate;
    }

    public void setParentFileToValidate(final FileToValidate parentFileToValidate) {
        this.parentFileToValidate = parentFileToValidate;
    }

    private String getToolOID() {
        return this.toolOID;
    }

    private void setToolOID(final String toolOID) {
        this.toolOID = toolOID;
    }

    public String getExternalId() {
        return this.externalId;
    }

    private void setExternalId(final String externalId) {
        this.externalId = externalId;
    }

    private String getProxyType() {
        return this.proxyType;
    }

    private void setProxyType(final String proxyType) {
        this.proxyType = proxyType;
    }

    public void init() {
        final Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.get("oid") != null) {
            final ObjectForValidatorDetector objectToValidateAgain = ValidatedObject
                    .getObjectByOID(ObjectForValidatorDetector.class, params.get("oid"), null);
            if (objectToValidateAgain != null) {
                this.analyzedObject = new ObjectForValidatorDetector(objectToValidateAgain);
                // When we try to Re-Analyze, store the new object and relaunch the execution
                this.setAnalyzedObject(ObjectForValidatorDetector.storeObjectForValidatorDetector(this.analyzedObject));
                this.execute(this.analyzedObject.getFilePath());
            } else {
                this.analyzedObject = null;
                FacesMessages.instance()
                        .add(Severity.ERROR,"Application has not been able to retrieve the object because of a wrong OID or because you do not have enough right to access it");
            }

        } else if (params.get("id") != null) {
            try {
                this.analyzedObject = ValidatedObject
                        .getObjectByID(ObjectForValidatorDetector.class, Integer.valueOf(params.get("id")), null);
            } catch (final NumberFormatException e) {
                FacesMessages.instance().add(Severity.ERROR,
                        "Application has not been able to retrieve the object because of a wrong OID or because you do not have enough right to access it");
                this.analyzedObject = null;
            }
        }

        // Get the tool oid if exist (Proxy oid to send back the result)
        if (params.get(AnalyzerBean.TOOL_OID) != null) {
            this.setToolOID(params.get(AnalyzerBean.TOOL_OID));
        }
        // Get the tool externalId if exist (To send back the result)
        if (params.get("externalId") != null) {
            this.setExternalId(params.get("externalId"));
        }
        // Get the proxy type if exist and add it(To send back the result)
        if (params.get("proxyType") != null) {
            this.setProxyType(params.get("proxyType"));
        }
    }

    /**
     * Untar an input file into an output file.
     * <p/>
     * The output file is created in the output folder, having the same name as the input file, minus the '.tar' extension.
     *
     * @param inputFile the input .tar file
     * @param outputDir the output directory file.
     * @return The {@link List} of {@link File}s with the untared content.
     * @throws IOException
     * @throws FileNotFoundException
     * @throws ArchiveException
     */
    public List<File> unTar(final File inputFile, final File outputDir) throws IOException, ArchiveException {
        List<File> untaredFiles = null;
        if (inputFile != null && outputDir != null) {
            AnalyzerBean.LOGGER.info(String.format("Untaring %s to dir %s.", inputFile.getAbsolutePath(), outputDir.getAbsolutePath()));

            untaredFiles = new LinkedList<>();
            final InputStream is = new FileInputStream(inputFile);
            final TarArchiveInputStream debInputStream = (TarArchiveInputStream) new ArchiveStreamFactory()
                    .createArchiveInputStream("tar", is);
            TarArchiveEntry entry;
            while ((entry = (TarArchiveEntry) debInputStream.getNextEntry()) != null) {
                final File outputFile = new File(outputDir, entry.getName());
                if (entry.isDirectory()) {
                    AnalyzerBean.LOGGER.info(String.format("Attempting to write output directory %s.", outputFile.getAbsolutePath()));
                    if (!outputFile.exists()) {
                        AnalyzerBean.LOGGER.info(String.format("Attempting to create output directory %s.",
                                outputFile.getAbsolutePath()));
                        if (!outputFile.mkdirs()) {
                            AnalyzerBean.LOGGER.error(String.format("Couldn't create directory %s.", outputFile.getAbsolutePath()));
                            return null;
                        }
                    }
                } else {
                    AnalyzerBean.LOGGER.info(String.format("Creating output file %s.", outputFile.getAbsolutePath()));
                    final OutputStream outputFileStream = new FileOutputStream(outputFile);
                    IOUtils.copy(debInputStream, outputFileStream);
                    outputFileStream.close();
                }
                untaredFiles.add(outputFile);
            }
            debInputStream.close();
            this.listFilesForFolder(outputDir);
        } else {
            AnalyzerBean.LOGGER.error("inputFile or outputDir is null");
        }
        return untaredFiles;

    }

    // Call by validate.xhtml
    public void executeWithString(final String fileContent) {
        final File file;
        try {
            this.setAnalyzedObject(ObjectForValidatorDetector.createNewObjectForValidatorDetector());
            this.setAnalyzedObject(ObjectForValidatorDetector.storeObjectForValidatorDetector(this.analyzedObject));
            file = new File(this.getAnalyzedObject().getObjectPath());
            FileUtils.writeStringToFile(file, fileContent);
            this.execute(file.getPath());
        } catch (final IOException e) {
            AnalyzerBean.LOGGER.error(e.getMessage());
        }
    }

    public String executeOnEditedPart() {
        //  final String file = this.getMessagePart();
        final String file = this.getMessagePart();
        this.executeWithString(file);
        return this.getAnalyzedObject().getBeginPermanentLink();
    }

    private void saveObject(final ObjectForValidatorDetector objectForValidatorDetector, final FileToValidate ftv) {
        try {
            objectForValidatorDetector.setMessageToAnalyze(FileToValidate.serialize(ftv));
            this.setAnalyzedObject(ObjectForValidatorDetector.storeObjectForValidatorDetector(objectForValidatorDetector));
        } catch (final IOException e) {
            AnalyzerBean.LOGGER.error(e.getMessage());
        }
    }

    public void execute(final String objectPath) {
        if ( "".equals(objectPath)){
            FacesMessages.instance().add(Severity.ERROR, "Please select a file BEFORE requesting the analysis");
            return;
        }
        this.parentFileToValidate = new FileToValidate(null);

        final File f = new File(objectPath);

        // Add global file to display it
        final FileToValidate ftv = new FileToValidate("Entire file", AnalyzerFileUtils.fileToString(f), "",
                this.parentFileToValidate);
        ftv.setDocType("DOCUMENT");

        // Start analyze
        this.messageContentAnalyzer(f, ftv);

        // Add info in parent
        this.parentFileToValidate.setDocType("Files/Folder/Archive");
        this.parentFileToValidate.setEntireLayout("");
        this.parentFileToValidate.getFileToValidateList().add(ftv);

        // Show arbo in terminal
        this.parentFileToValidate.print();
        final String test = this.parentFileToValidate.getEntireLayout(this.parentFileToValidate.getTextLayout());
        AnalyzerBean.LOGGER.info("\n{}", test);

        // Add result values for each ftv
        this.parentFileToValidate.printInString();

        // Add all previous result and concatenate it in parent result.
        // Start html transformation
        this.parentFileToValidate.getEntireResult(this.parentFileToValidate.getResult());

        // Get the version of the anaylzer (EVSClient Version)
        final String contentAnalyzerVersion = VersionProvider.get();
        this.analyzedObject.setContentAnalyzerVersion(contentAnalyzerVersion);

        // Get the tool oid if exist and add it(Proxy oid to send back the result)
        if (this.getToolOID() != null) {
            this.analyzedObject.setToolOid(this.getToolOID());
        }
        // Get the external id if exist and add it(To send back the result)
        if (this.getExternalId() != null) {
            this.analyzedObject.setExternalId(this.getExternalId());
        }
        // Get the proxy type if exist and add it(To send back the result)
        if (this.getProxyType() != null) {
            this.analyzedObject.setProxyType(this.getProxyType());
        }

        // Save the file and result
        this.saveObject(this.analyzedObject, this.parentFileToValidate);

        // Add all validation type in result object
        this.analyzedObject.addAllValidationTypesFromResult(this.parentFileToValidate);
    }

    private void messageContentAnalyzer(final File f, final FileToValidate parent) {
        final MessageContentAnalyzer m = new MessageContentAnalyzer(this.analyzedObject);
        m.messageContentAnalyzer(f, 0, 0, parent);
    }

    public void uploadListener(final FileUploadEvent event) {
        FileOutputStream fos = null;
        final File uploadedFile;
        try {
            this.setAnalyzedObject(ObjectForValidatorDetector.createNewObjectForValidatorDetector());
            final UploadedFile item = event.getUploadedFile();
            uploadedFile = new File(this.analyzedObject.getObjectPath());
            final File parentFile = uploadedFile.getParentFile() ;
            if (!parentFile.exists() && !parentFile.mkdirs()) {
                FacesMessages.instance()
                        .addFromResourceBundle(Severity.ERROR, "Unable to create directory.");
            }

            this.analyzedObject.setDescription(item.getName());

            if (item.getData() != null && item.getData().length > 0) {
                fos = new FileOutputStream(uploadedFile);
                fos.write(item.getData());
                fos.close();
            } else {
                FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "file is empty");
                this.analyzedObject.setObjectPath(null);
            }
            this.setAnalyzedObject(ObjectForValidatorDetector.storeObjectForValidatorDetector(this.analyzedObject));
        } catch (final IOException e) {
            FacesMessages.instance().add(Severity.ERROR, e.getMessage());
            AnalyzerBean.LOGGER.error("uploadListener: an error occurred: {}", e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (final IOException e) {
                    AnalyzerBean.LOGGER.error("not able to close the FOS{}", e.getMessage());
                }
            }
        }

    }

    public void reset() {
        this.analyzedObject = null;
        this.parentFileToValidate = new FileToValidate(null);
        this.setMessagePart(null);
    }

    public void downloadFile(final String objectPath, final String validationType, final int startOffset, final int endOffset) {
        File f = new File(objectPath);
        String fileContent = AnalyzerFileUtils.fileToString(f);
        final ViewFileUtils vfu = new ViewFileUtils();
        final MessageContentAnalyzer vd = new MessageContentAnalyzer();
        final String fileToDisplay ;

        if (fileContent == null) {
            FacesMessages.instance().add(Severity.ERROR, "Unable to read file");
            return;
        }

        if (validationType.equals(AnalyzerBean.DSUB)) {
            final String editedObjectPath = objectPath.replace(".xml", "_edited.xml");
            f = new File(editedObjectPath);
            fileContent = AnalyzerFileUtils.fileToString(f);
        }
        if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
            fileToDisplay = fileContent.substring(startOffset, endOffset);
        } else {
            fileToDisplay = fileContent;
        }
        if (validationType.equals(AnalyzerBean.DICOM)) {
            // Test base 64
            final boolean result = vd.base64Detection(fileContent, startOffset, endOffset);
            if (result) {
                //            final byte[] bytes = Base64.decodeBase64(fileToDisplay);
                final byte[] bytes = DatatypeConverter.parseBase64Binary(fileToDisplay);

                if (bytes != null) {
                    final File file = this.writeBytesInFile("temp-file-viewFile-decodeBase64", bytes);
                    vfu.viewFile(null, file, validationType, FacesContext.getCurrentInstance());
                    AnalyzerFileUtils.deleteTmpFile(file);
                }
            }
        } else {
            vfu.viewFile(fileToDisplay, null, validationType, FacesContext.getCurrentInstance());

        }
    }

    public void viewMessagePart(final String objectPath, final String validationType, final int startOffset, final int endOffset) {
        File f = new File(objectPath);
        String fileContent = AnalyzerFileUtils.fileToString(f);
        String fileToDisplay;
        final MessageContentAnalyzer vd = new MessageContentAnalyzer();
        final File file;
        final ViewFileUtils vfu = new ViewFileUtils();

        if (fileContent == null) {
            FacesMessages.instance().add(Severity.ERROR, "Unable to read file");
            return;
        }

  /* This is actually not needed
       if (validationType.equals(AnalyzerBean.DSUB)) {
            final String editedObjectPath = objectPath.replace(ObjectForValidatorDetector.OVD_FILE_SUFFIX, "_" + startOffset + '_' + endOffset + "_edited" + ObjectForValidatorDetector.OVD_FILE_SUFFIX);
            f = new File(editedObjectPath);
            fileContent = AnalyzerFileUtils.fileToString(f);
        }*/

        if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
            fileToDisplay = fileContent.substring(startOffset, endOffset);
        } else {
            fileToDisplay = fileContent;
        }

        final boolean result = vd.base64Detection(fileContent, startOffset, endOffset);
        byte[] bytes = null;

        if (DICOM.equals(validationType)) {
            // Test base 64
            if (result) {
                //  bytes = Base64.decodeBase64(fileToDisplay);
                bytes = DatatypeConverter.parseBase64Binary(fileToDisplay);
            } else {
                // Get the file bytes for dcm2txt
                try {
                    bytes = FileUtils.readFileToByteArray(f);
                    if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
                        bytes = Arrays.copyOfRange(bytes, startOffset, endOffset);
                    }
                } catch (final IOException e) {
                    AnalyzerBean.LOGGER.error(e.getMessage());
                }
            }

            if(bytes != null) {
                file = this.writeBytesInFile("temp-file-viewMessagePart-decodeBase64", bytes);


                bytes = vfu.dicom2txt(file);
                AnalyzerFileUtils.deleteTmpFile(file);
                fileToDisplay = new String(bytes, StandardCharsets.UTF_8);
                this.setMessagePart(fileToDisplay);

            }
        } else if (TLS.equals(validationType)) {
            this.setMessagePart(fileToDisplay);
        } else if (result) {
            // bytes = Base64.decodeBase64(fileToDisplay);
            bytes = DatatypeConverter.parseBase64Binary(fileToDisplay);


            fileToDisplay = new String(bytes, StandardCharsets.UTF_8);
            this.setMessagePart(fileToDisplay);

        } else {
            this.setMessagePart(fileToDisplay);
        }

    }

    private File writeBytesInFile(final String fileName, final byte[] bytes) {
        FileOutputStream fos = null;
        File file = null;
        try {
            file = File.createTempFile(fileName, AnalyzerBean.TMP_FILE_SUFFIX);
            fos = new FileOutputStream(file.getAbsolutePath());
            fos.write(bytes);
            fos.close();
        } catch (final IOException e) {
            AnalyzerBean.LOGGER.error(e.getMessage());
            FacesMessages.instance().add(Severity.ERROR, "Error in conversion byte to file!");
        } finally {
            // Always close output streams. Doing this closes
            // the channels associated with them as well.
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (final IOException e) {
                AnalyzerBean.LOGGER.error("Unable to close output stream. {}", e.getMessage());
            }
        }
        return file;
    }

    // Forward to validation page : validate.xhtml
    public String validate(final String objectPath, final String validationType, final int startOffset, final int endOffset, final String oid) {
        final String encodedObjectPath = this.isDSUBEncodedObjectPath(objectPath, validationType, startOffset, endOffset);
        return  "/validate.seam?startOffset=" + startOffset +
                "&endOffset=" + endOffset +
                "&validationType=" + ValidatorType.getByProxyType(validationType) +
                "&messageContentAnalyzerOid=" + oid +
                "&filePath=" + encodedObjectPath ;

    }

    // Forward directly ON validator object page : validator.xhtml
    public String validateLucky(final String objectPath, final String validationType, final int startOffset, final int endOffset, final String oid) {
        final String encodedObjectPath = this.isDSUBEncodedObjectPath(objectPath, validationType, startOffset, endOffset);

        return "/validate.seam?filePath=" + encodedObjectPath + "&startOffset=" + startOffset + "&endOffset="
                + endOffset + "&validationType=" + validationType + "&messageContentAnalyzerOid=" + oid + "&lucky=true";
    }

    private String isDSUBEncodedObjectPath(final String objectPath, final String validationType, final int startOffset, final int endOffset) {
        final String encodedObjectPath;
        if (AnalyzerBean.DSUB.equals(validationType)) {
            final String editedObjectPath = objectPath.replace(".xml", "_" + startOffset + '_' + endOffset + "_edited.xml");
            encodedObjectPath = this.getEncodedObjectPath(editedObjectPath, validationType);

        } else {
            encodedObjectPath = this.getEncodedObjectPath(objectPath, validationType);
        }
        return encodedObjectPath;
    }

    private String getEncodedObjectPath(final String objectPath, final String validationType) {
        if (validationType == null || validationType.isEmpty()) {
            FacesMessages.instance().add(Severity.WARN, "This part has no validation type !");
            AnalyzerBean.LOGGER.warn("User try to validate a document part with no validation type (probably the entire document).");
        }

        String encodedObjectPath = "";
        //      encodedObjectPath = Base64.encodeBase64String(objectPath.getBytes(StandardCharsets.UTF_8));
        try {
            encodedObjectPath = DatatypeConverter.printBase64Binary(objectPath.getBytes(StandardCharsets.UTF_8));
            AnalyzerBean.LOGGER.info(encodedObjectPath);
        } catch (RuntimeException e) {
            AnalyzerBean.LOGGER.error(e.getMessage());
        }
        return encodedObjectPath;
    }

    // Tree creation
    public GazelleTreeNodeImpl<FileToValidate> buildTree() {
        final GazelleTreeNodeImpl<FileToValidate> tree = new GazelleTreeNodeImpl<>();
        tree.setData(this.parentFileToValidate);
        int index = 0;
        for (final FileToValidate file : this.parentFileToValidate.getFileToValidateList()) {
            final List tmpList = file.getFileToValidateList();
            if (tmpList != null && !tmpList.isEmpty()) {
                tree.addChild(Integer.valueOf(index), this.processFileList(file));
            }
            else {
                final GazelleTreeNodeImpl<FileToValidate> node = new GazelleTreeNodeImpl<>();
                node.setData(file);
                tree.addChild(Integer.valueOf(index), node);
            }
            index++;
        }

        return tree;
    }

    private GazelleTreeNodeImpl<FileToValidate> processFileList(final FileToValidate file) {
        final GazelleTreeNodeImpl<FileToValidate> tree = new GazelleTreeNodeImpl<>();
        tree.setData(file);
        int index = 0;
        for (final FileToValidate childFile : file.getFileToValidateList()) {
            final List tmpList = childFile.getFileToValidateList();
            if (tmpList != null && !tmpList.isEmpty()) {
                tree.addChild(Integer.valueOf(index), this.processFileList(childFile));
            } else {
                final GazelleTreeNodeImpl<FileToValidate> node = new GazelleTreeNodeImpl<>();
                node.setData(childFile);
                tree.addChild(Integer.valueOf(index), node);
            }
            index++;
        }
        return tree;
    }

    public void nodeSelectListener(final TreeSelectionChangeEvent event) {
        final FileToValidate node = (FileToValidate) event.getNewSelection();
        this.viewMessagePart(this.analyzedObject.getObjectPath(), node.getValidationType(), node.getStartOffset(),
                node.getEndOffset());
    }

    // Used in xhtml
    public String getIconForNode(final FileToValidate node) {
        String res = "gzl-icon-circle-red";
        final GetValidationInfo gvi = new GetValidationInfo();
        if (node != null && node.getDocType() != null) {
            if (node.getMessageContentAnalyzerOid() == null || node.getMessageContentAnalyzerOid().isEmpty()) {
                res = "gzl-icon-globe";
            } else if (AnalyzerBean.getValidationPermanentLinkFromDb(node.getMessageContentAnalyzerOid()).equals(
                    AnalyzerBean.NOT_PERFORMED)) {
                res = "gzl-icon-circle-orange";
            } else {
                final String url = AnalyzerBean.getValidationPermanentLinkFromDb(node.getMessageContentAnalyzerOid());
                final String oidArg = "&oid=";
                final int index = url.indexOf(oidArg);
                final String oid = url.substring(index + oidArg.length());

                if (gvi.getValidationStatus(oid, "") == null) {
                    res = "gzl-icon-circle-blue";
                } else if ("FAILED".equals(gvi.getValidationStatus(oid, ""))) {
                    res = "gzl-icon-circle-red";
                } else if ("PASSED".equals(gvi.getValidationStatus(oid, ""))) {
                    res = "gzl-icon-circle-green";
                } else {
                    res = "gzl-icon-circle-blue";
                }
            }
        }
        return res;
    }

    public static String getValidationPermanentLinkFromDb(final String messageContentAnalyzerOid) {
        if (messageContentAnalyzerOid.length() == 0 ){
            return AnalyzerBean.NOT_PERFORMED;
        }
        final boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
        if (createContexts) {
            Lifecycle.beginCall();
        }
        String result;
        final ValidatedObjectQuery query = new ValidatedObjectQuery();
        query.validationDate().isNotNull();
        query.messageContentAnalyzerOid().eq(messageContentAnalyzerOid);
        query.validationDate().order(false);
        final List<ValidatedObject> vo = query.getList();
        if (vo.isEmpty()) {
            result = AnalyzerBean.NOT_PERFORMED;
        }
        else {
            int i = 0;

            for (int j = 1; j < vo.size(); j++) {
                if (vo.get(i).getValidationDate().compareTo(vo.get(j).getValidationDate()) > 0) {
                    j++;
                } else {
                    i = j;
                }
            }
            final ValidatedObject object = vo.get(i);

            if (object != null) {
                final String response = object.getBeginPermanentLink();
                result = ApplicationPreferenceManager.getStringValue("application_url");
                result = result.concat(response);
            } else {
                result = AnalyzerBean.NOT_PERFORMED;
            }
        }
        if (createContexts) {
            Lifecycle.endCall();
        }
        return result;

    }

    public File unzip(final File fileToUnzip, final File unzipFolder) {
        try {
            ZipUtility.unzip(fileToUnzip, unzipFolder);
            AnalyzerBean.LOGGER.info("{} is unzipped in {}", fileToUnzip, unzipFolder);
            this.listFilesForFolder(unzipFolder);
            return unzipFolder;
        } catch (final IOException e) {
            AnalyzerBean.LOGGER.error(e.getMessage());
        }
        return unzipFolder;
    }

    // List all files of a folder
    public boolean listFilesForFolder(final File folder) {
        if (folder != null && folder.exists()) {
            final File[] listOfFiles = folder.listFiles();
            if (listOfFiles != null) {
                for (final File fileEntry : listOfFiles) {
                    if (fileEntry.isDirectory()) {
                        this.listFilesForFolder(fileEntry);
                    } else {
                        this.fileList.add(fileEntry.getName());
                    }
                }
            }
            else {
                AnalyzerBean.LOGGER.error("Error access list of files in folder !");
                return false;
            }
        } else {
            AnalyzerBean.LOGGER.error("Folder is null or doesn't exist !");
            return false;
        }
        return true;
    }
    public Boolean getShowMessageInGUI() {
        return this.showMessageInGUI;
    }

    public void setShowMessageInGUI(final Boolean showMessageInGUI) {
        this.showMessageInGUI = showMessageInGUI;
    }

    public Boolean getIndentMessageInGUI() {
        return this.indentMessageInGUI;
    }

    public void setIndentMessageInGUI(final Boolean indentMessageInGUI) {
        this.indentMessageInGUI = indentMessageInGUI;
    }


}