/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.util.Util;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;

@Entity
@Name("validatedAssertion")
@Table(name = "saml_validated_assertion", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "saml_validated_assertion_sequence", sequenceName = "saml_validated_assertion_id_seq", allocationSize = 1)
public class ValidatedAssertion extends AbstractValidatedXMLFile {

    /**
     *
     */
    private static final long serialVersionUID = 3435397463635134560L;
    private static final String FILE_PREFIX = "assertion_";
    private static final String FILE_EXTENSION = ".xml";

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidatedAssertion.class);

    @Id
    @NotNull
    @Column(name = "id")
    @GeneratedValue(generator = "saml_validated_assertion_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "file_path")
    private String filePath;

    @Column(name = "schematron")
    private String schematron;

    /**
     *
     * @param inAssertion : the assertion to be loaded when calling the constructor
     */
    public ValidatedAssertion(final ValidatedAssertion inAssertion) {
        if (inAssertion != null) {
            this.filePath = inAssertion.getFilePath();
            this.description = inAssertion.getDescription();
        }
    }

    /**
     * Constructor
     */
    public ValidatedAssertion() {

    }

    /**
     * Getters and Setters
     */
    @Override
    public void setSchematron(final String schematron) {
        this.schematron = schematron;
    }

    @Override
    public String getSchematron() {
        return this.schematron;
    }

    @Override
    public void setFilePath(final String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String getFilePath() {
        return this.filePath;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    /**
     * Static methods
     */

    public static ValidatedAssertion createNewAssertion() {
        ValidatedAssertion assertion = new ValidatedAssertion();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        assertion = em.merge(assertion);
        em.flush();
        if (assertion == null || assertion.getId() == 0) {
            ValidatedAssertion.LOGGER.error("An error occurred when creating a new ValidatedAssertion object");
            return null;
        }
        final String repository = ApplicationPreferenceManager.getStringValue("saml_repository");
        assertion.setFilePath(
                repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ValidatedAssertion.FILE_PREFIX
                        .concat(assertion.getId().toString()).concat(
                        ValidatedAssertion.FILE_EXTENSION));
        return assertion;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends AbstractValidatedXMLFile> T addOrUpdate(final Class<T> entityClass) {
        if (this.getFilePath() == null || this.getFilePath().isEmpty()) {
            ValidatedAssertion.LOGGER.error("The file path is missing, cannot add this object !");
            return (T) this;
        } else {
            final File uploadedFile = new File(this.getFilePath());
            if (!uploadedFile.exists()) {
                ValidatedAssertion.LOGGER.error("The given file does not exist: {}", this.getFilePath());
                return (T) this;
            }
            if (this.getOid() == null || this.getOid().isEmpty()) {
                this.setOid(OIDGenerator.getNewOid());
            }
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            final ValidatedAssertion assertion = em.merge(this);
            em.flush();

            //Update the cache with the newest values
            GazelleValidationCacheManager
                    .refreshGazelleCacheWebService(assertion.getExternalId(), assertion.getToolOid(),
                            assertion.getOid(), "off");

            return (T) assertion;
        }
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/detailedResult.seam?type=SAML").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }
}
