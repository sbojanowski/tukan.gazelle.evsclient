/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.dicom.action;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.dicom.test.DicomEcho;
import net.ihe.gazelle.dicom.test.TestDicom;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.dicom.model.DicomSCPScreenerResult;
import net.ihe.gazelle.evs.client.dicom.model.DicomSCPScreenerResultQuery;
import net.ihe.gazelle.evs.client.users.action.SSOIdentity;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.paths.HQLSafePathBasicDate;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.international.StatusMessages;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

@Name("dicomSCPScreenerManagerBean")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 60000)
public class DicomSCPScreenerManager implements QueryModifier<DicomSCPScreenerResult> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DicomSCPScreenerManager.class);
    /**
     *
     */
    private static final long serialVersionUID = 5858502355206962888L;

    private DicomSCPScreenerResult screenerResult;

    private FilterDataModel<DicomSCPScreenerResult> dataModel;

    public FilterDataModel<DicomSCPScreenerResult> getDataModel() {
        if (this.dataModel == null) {
            this.dataModel = new FilterDataModel<DicomSCPScreenerResult>(
                    new Filter<>(DicomSCPScreenerManager.this.getCriterions())) {
                @Override
                protected Object getId(final DicomSCPScreenerResult t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }
        return this.dataModel;
    }

    private HQLCriterionsForFilter<DicomSCPScreenerResult> getCriterions() {
        final DicomSCPScreenerResultQuery dicomSCPScreenerResultQuery = new DicomSCPScreenerResultQuery();
        final HQLCriterionsForFilter<DicomSCPScreenerResult> result = dicomSCPScreenerResultQuery.getHQLCriterionsForFilter();
        result.addQueryModifier(this);
        final HQLSafePathBasicDate<Date> timestamp = dicomSCPScreenerResultQuery.timestamp();
        timestamp.setCriterionWithoutTime(TimeZone.getDefault());
        result.addPath("timestamp", timestamp);
        return result;
    }

    public DicomSCPScreenerResult getScreenerResult() {
        return this.screenerResult;
    }

    public void setScreenerResult(final DicomSCPScreenerResult screenerResult) {
        this.screenerResult = screenerResult;
    }

    public void reset() {
        this.screenerResult = new DicomSCPScreenerResult();
        this.screenerResult.setOid(OIDGenerator.getNewOid());
    }

    @Create
    public void init() {
        if (this.screenerResult != null && this.screenerResult.getId() != null) {
            this.screenerResult = new DicomSCPScreenerResult(this.screenerResult);
            this.screenerResult.setOid(OIDGenerator.getNewOid());
        } else {
            this.reset();
        }
        if (SSOIdentity.instance().isLoggedIn()) {
            this.screenerResult.setPrivateUser(SSOIdentity.instance().getCredentials().getUsername());
            this.screenerResult.setInstitutionKeyword(SSOIdentity.instance().getInstitutionKeyword());
            this.screenerResult.setIsPublic(Boolean.FALSE);
            this.screenerResult.setPrivacyKey(null);
        } else {
            this.screenerResult.setPrivateUser(null);
            this.screenerResult.setInstitutionKeyword(null);
            this.screenerResult.setIsPublic(Boolean.TRUE);
        }
    }

    public void reExecute() {
        this.init();
        this.execute();
    }

    public boolean validateIP() {
        final String validIpAddressRegex = "^[1-9][0-9]{0,2}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$";
        final String validHostnameRegex = "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$";

        final String hostname = this.screenerResult.getHostname();
        if (hostname.matches(validHostnameRegex) ||hostname.matches(validIpAddressRegex) ) {
            return true;
        } else {
            DicomSCPScreenerManager.LOGGER.error("You must enter a valid IP address !");
            StatusMessages.instance()
                    .addToControlFromResourceBundleOrDefault("ip", Severity.ERROR, "test",
                            "You must enter a valid IP address !");

            return false;
        }
    }

    public boolean validateAET() {
        final String aetPattern = "^[a-zA-Z0-9_]{0,16}$";
        final String aETitle = this.screenerResult.getaETitle();
        if (aETitle.length() > 16) {
            DicomSCPScreenerManager.LOGGER.error("Your AETitile has more than 16 characters !!");
            StatusMessages.instance()
                    .addToControlFromResourceBundleOrDefault("AET", Severity.ERROR, "test",
                            "Your AETitile has more than 16 characters !");
            return false;
        } else if (!aETitle.matches(aetPattern)) {
            DicomSCPScreenerManager.LOGGER.error("You must enter a valid AETitile !");
            StatusMessages.instance()
                    .addToControlFromResourceBundleOrDefault("AET", Severity.ERROR, "test",
                            "You must enter a valid AETitile !");
            return false;
        }
        return true;
    }

    @SuppressWarnings("static-access")
    public void execute() {
        try {
            final String hostname = this.screenerResult.getHostname();
            final Integer port = this.screenerResult.getPort();
            final String aETitle = this.screenerResult.getaETitle();
            final String ipPattern = "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$";

            // Database verification
            if (this.verifLocation()) {

                // Verify entered values BEFORE test
                if (hostname == null || "".equals(hostname)) {
                    FacesMessages.instance().add(Severity.ERROR,"You need to provide an IP address !");
                    return;
                } else if (!hostname.matches(ipPattern)) {
                    FacesMessages.instance().add(Severity.ERROR,"Format of IP address is invalid !");
                    return;
                }
                if (port == null || port.equals(Integer.valueOf(0))) {
                    FacesMessages.instance().add(Severity.ERROR,"You need to provide a port number !");
                    return;
                }
                if (aETitle == null || "".equals(aETitle)) {
                    FacesMessages.instance().add(Severity.ERROR, "You need to provide an AETitile !");
                    return;
                } else if (aETitle.length() > 15) {
                    FacesMessages.instance().add(Severity.ERROR,"The AETitile shall not exceed 16 characters !");
                    return;
                }
                this.screenerResult.setOid(OIDGenerator.getNewOid());
                // Make echo (like ping) BEFORE send somthing to host
                final DicomEcho de = new DicomEcho(hostname, port.toString(), aETitle);

                // Connection establish
                if (de.result) {
                    DicomSCPScreenerManager.LOGGER.info("\nWAITING PLEASE ...\nTEST IS RUNNING");
                    final TestDicom td = new TestDicom(hostname, port, aETitle, "TEST");
                    // Store results in XML files
                    this.screenerResult.setResult(td.dicomResultatString());
                    this.screenerResult = this.screenerResult.save();
                } else {
                    DicomSCPScreenerManager.LOGGER.error("CONNECTION FAILED !");
                    FacesMessages.instance().add(Severity.ERROR,"CONNECTION FAILED ! Verify IP, Port and AETitle values.");
                }
            }
        } catch (final JAXBException e) {
            DicomSCPScreenerManager.LOGGER.error("", e);
            FacesMessages.instance().add(Severity.ERROR, e.getMessage());
            throw new IllegalStateException("Failed to process ", e);
        } catch (final IllegalArgumentException e) {
            DicomSCPScreenerManager.LOGGER.error("", e);
            FacesMessages.instance().add(Severity.ERROR, e.getMessage());
            throw new IllegalStateException("Failed to process ", e);
        }

    }

    public boolean verifLocation() {
        final String xslLocation = ApplicationPreferenceManager.getStringValue("dicom_scp_screener_xsl");
        if (xslLocation == null || xslLocation.isEmpty()) {
            DicomSCPScreenerManager.LOGGER.error("The repository for xsl file (dicom_scp_screener_xsl) has not been set");
            FacesMessages.instance().add(Severity.ERROR, "The repository for xsl file (dicom_scp_screener_xsl) has not been set");
            return false;
        } else {
            DicomSCPScreenerManager.LOGGER.info("xslLocation : {}", xslLocation);
            final String appName = ApplicationPreferenceManager.getStringValue("application_url_basename");
            if (appName == null || appName.isEmpty()) {
                DicomSCPScreenerManager.LOGGER.error("The repository for the application base name (application_url_basename) has not been set");
                FacesMessages.instance()
                        .add("The repository for the application base name (application_url_basename) has not been set");
                return false;
            }
            return true;
        }
    }

    public String resultAsHtml() {
        final String xslLocation = ApplicationPreferenceManager.getStringValue("dicom_scp_screener_xsl");
        if (xslLocation == null || xslLocation.isEmpty()) {
            DicomSCPScreenerManager.LOGGER.error("The repository for xsl file (dicom_scp_screener_xsl) has not been set");
            FacesMessages.instance().add(Severity.ERROR, "The repository for xsl file (dicom_scp_screener_xsl) has not been set");
            return null;
        } else {
            DicomSCPScreenerManager.LOGGER.info("xslLocation : {}", xslLocation);
            final String appName = ApplicationPreferenceManager.getStringValue("application_url_basename");
            if (appName == null || appName.isEmpty()) {
                DicomSCPScreenerManager.LOGGER.error("The repository for the application base name (application_url_basename) has not been set");
                FacesMessages.instance()
                        .add("The repository for the application base name (application_url_basename) has not been set");
                return null;
            } else {
                DicomSCPScreenerManager.LOGGER.info("appName : {}", appName);
                final String htmlResult = Util.resultTransformation(this.screenerResult.getResult(), xslLocation, appName);
                return htmlResult;
            }
        }
    }

    public void downloadResultAsXml() {
        String content;
        String fileName;
        fileName = "DicomSCPScreenerResult.xml";
        content = this.screenerResult.getResult();
        if (!"".equals(content)) {
            ReportExporterManager.exportToFile("text/xml", content, fileName);
            DicomSCPScreenerManager.LOGGER.info("XML download");
        } else {
            DicomSCPScreenerManager.LOGGER.error("No result to download");
            FacesMessages.instance().add(Severity.ERROR,"No result to download");
        }
    }

    public void makeResultPublic() {
        this.screenerResult.setIsPublic(Boolean.TRUE);
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        this.screenerResult = entityManager.merge(this.screenerResult);
        entityManager.flush();
    }

    public void makeResultPrivate() {
        this.screenerResult.setIsPublic(Boolean.FALSE);
        this.screenerResult.setPrivacyKey(null);
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        this.screenerResult = entityManager.merge(this.screenerResult);
        entityManager.flush();
    }

    public void shareResult() {
        this.screenerResult.generatePrivacyKey();
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        this.screenerResult = entityManager.merge(this.screenerResult);
        entityManager.flush();
    }

    @Override
    public void modifyQuery(final HQLQueryBuilder<DicomSCPScreenerResult> queryBuilder,
            final Map<String, Object> filterValuesApplied) {
        boolean monitor = false;
        String login = null;
        String institutionKeyword = null;

        final SSOIdentity instance = SSOIdentity.instance();
        if (instance != null && instance.isLoggedIn()) {
            login = instance.getCredentials().getUsername();
            institutionKeyword = instance.getInstitutionKeyword();
            if (instance.hasRole("monitor_role")|| instance.hasRole("admin_role")) {
                monitor = true;
            }
        }
        // show all messages if monitor
        if (!monitor) {
            if (login == null) {
                queryBuilder.addRestriction(
                        HQLRestrictions.or(
                                HQLRestrictions.eq("isPublic", null),
                                HQLRestrictions.eq("isPublic", Boolean.TRUE)));
            } else {
                queryBuilder.addRestriction(HQLRestrictions
                        .or(HQLRestrictions.eq("isPublic", null),
                                HQLRestrictions.eq("isPublic", Boolean.TRUE),
                                HQLRestrictions.eq("privateUser", login),
                                HQLRestrictions.eq("institutionKeyword", institutionKeyword)
                                ));
            }
        }
    }
}
