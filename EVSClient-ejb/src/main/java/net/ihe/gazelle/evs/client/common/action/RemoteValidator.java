/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.evs.client.analyzer.AnalyzerBean;
import net.ihe.gazelle.evs.client.analyzer.file.B64;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandardQuery;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.common.model.ValidationService;
import net.ihe.gazelle.evs.client.dicom.model.DicomValidatedObject;
import net.ihe.gazelle.evs.client.hl7.model.HL7ValidatedMessage;
import net.ihe.gazelle.evs.client.pdf.model.ValidatedPdf;
import net.ihe.gazelle.evs.client.servlet.Upload;
import net.ihe.gazelle.evs.client.tls.model.ValidatedCertificates;
import net.ihe.gazelle.evs.client.util.ValidatorType;
import net.ihe.gazelle.evs.client.xml.model.*;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.FileUtils;
import org.apache.tika.Tika;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;


@Name("remoteValidator")
@Scope(ScopeType.PAGE)
public class RemoteValidator implements Serializable {

    private static final long serialVersionUID = 7920478668366260753L;

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteValidator.class);


    @In("#{facesContext}")
    private transient FacesContext facesContext;

    private static final String FILE_DESCRIPTION = "Uploaded.xml";

    private String fileName;
    private int startOffset;
    private int endOffset;

    private ValidatorType validator;

    private boolean lucky;

    private String proxyType;

    private String externalId;

    private String toolOid;

    private String messageContentAnalyzerOid;

    private String selectedExtension;

    private List<String> extensions;

    private SelectItem[] validators;

    private String messageStr;

    private boolean displayPanel = true;
    private boolean base64Decoded;

    private Map<String, List<String>> authorizedExtensions = new HashMap<>();

    private File tmpFileBase64Decoded;

    public boolean isBase64Decoded() {
        return this.base64Decoded;
    }

    public void setBase64Decoded(final boolean base64Decoded) {
        this.base64Decoded = base64Decoded;
    }

    public Map<String, List<String>> getAuthorizedExtensions() {
        return this.authorizedExtensions;
    }

    public void setAuthorizedExtensions(final Map<String, List<String>> authorizedExtensions) {
        this.authorizedExtensions = authorizedExtensions;
    }

    public boolean isDisplayPanel() {
        return this.displayPanel;
    }

    public void setDisplayPanel(final boolean displayPanel) {
        this.displayPanel = displayPanel;
    }

    public int getStartOffset() {
        return this.startOffset;
    }

    public void setStartOffset(final int startOffset) {
        this.startOffset = startOffset;
    }

    public int getEndOffset() {
        return this.endOffset;
    }

    public void setEndOffset(final int endOffset) {
        this.endOffset = endOffset;
    }

    public String getMessageStr() {
        return this.messageStr;
    }

    public void setMessageStr(final String messageStr) {
        this.messageStr = messageStr;
    }

    public RemoteValidator() {
    }

    @Create
    public void retrieveFilename() {
        B64 b64 = new B64();
        final Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        final String key = params.get("file");
        String path = params.get("filePath");
        final String validationType = params.get("validationType");
        final String sOffset = params.get("startOffset");
        final String eOffset = params.get("endOffset");
        final String luckyStr = params.get("lucky");
        this.toolOid = params.get(AnalyzerBean.TOOL_OID);
        this.externalId = params.get("externalId");

        this.proxyType = params.get("proxyType");
        this.messageContentAnalyzerOid = params.get("messageContentAnalyzerOid");
        this.lucky = Boolean.parseBoolean(luckyStr);

        // For validator detector
        if (path != null) {
            // Padding of the B64 encoded path may be lost in the URL encoding. Need to add "=" removed from the URL
            while (path.length() % 4 != 0) {
                path += "=";
            }
            final byte[] decodedObjectPathBytes = DatatypeConverter.parseBase64Binary(path);

            final String decodedObjectPath;
            decodedObjectPath = new String(decodedObjectPathBytes, StandardCharsets.UTF_8);
            LOGGER.info(decodedObjectPath);
            this.fileName = decodedObjectPath;
            this.setStartOffset(Integer.valueOf(sOffset));
            this.setEndOffset(Integer.valueOf(eOffset));
            this.setDisplayPanel(false);
        } else { // Here we are coming from the proxy or TM "
            this.fileName = Upload.getFile(key);
        }
        FileOutputStream fos = null;
        try {
            if (this.externalId != null) {
                // Decode base64 file from an external tool
                b64.base64Decoding(new File(this.fileName), new File(this.fileName));
                this.setBase64Decoded(true);
            }

            // Decode for example base 64 dicom file part BEFORE send it to Dicom validation (we are not Dicom and we are not TLS
            else if (this.proxyType == null || !"DICOM".equals(this.proxyType) || validationType == null || !"TLS"
                    .equals(validationType)) {
                final FileInputStream fileStream = new FileInputStream(new File(this.fileName));
                String str = Streams.asString(fileStream);
                fileStream.close();

                // Split file if offset
                if (this.getStartOffset() != 0 && this.getEndOffset() != 0) {
                    str = str.substring(this.getStartOffset(), this.getEndOffset());
                }
                this.setBase64Decoded(true);
            }


            if (this.fileName != null) {
                // Detection of message type when validating using Proxy->EVSClient
                final String fileExtension = this.fileName.substring(this.fileName.lastIndexOf('.') + 1);
                this.displayMessage(validationType);
                this.validator = ValidatorType.getByProxyType(fileExtension);
                if (validationType == null || validationType.isEmpty() || this.lucky) {
                    this.initializeValidatorList();
                    if (validationType != null) {
                        this.setValidatorsForMessagStr(validationType);
                    } else if (ValidatorType.getByProxyType(fileExtension) != null) {
                        this.setValidator(ValidatorType.getByProxyType(fileExtension));
                        this.getExtensionsForSelectedValidator();
                    }
                }
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No file provided or file size is &gt; 15Mo");
                LOGGER.info("No file provided or file size is &gt; 15Mo");
            }
            if (this.validators == null || this.validators.length == 0) {
                this.initializeValidatorList();
            }
            if (validationType != null) {
                this.setValidatorsForMessagStr(validationType);
            }
        } catch (final IOException e) {
            LOGGER.error("error to decoded file : ", e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (final IOException e) {
                    LOGGER.error("not able to close the FOS", e.getMessage());
                }
            }
        }
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    public ValidatorType getValidator() {
        return this.validator;
    }

    public void setValidator(final ValidatorType validator) {
        this.validator = validator;
    }

    public void getExtensionsForSelectedValidator() {
        if (this.validator != null) {
            final ReferencedStandardQuery query = new ReferencedStandardQuery();
            query.addRestriction(HQLRestrictions.or(query.name().eqRestriction(this.validator.getProxyType()),
                    query.name().eqRestriction(this.validator.getFriendlyName())));
            //			query.name().eq(validator.getProxyType());
            query.extension().order(true);
            this.extensions = query.extension().getListDistinct();
            if (!this.getAuthorizedExtensions().isEmpty() && this.getAuthorizedExtensions() != null && this.extensions != null
                    && !this.extensions.isEmpty()) {
                this.verifyExtensions(this.authorizedExtensions, this.validator);
            }
        } else {
            this.extensions = null;
            this.selectedExtension = null;
        }

    }

    public SelectItem[] getValidators() {
        if (this.validator != null) {
            return this.validators.clone();
        } else {
            return null;
        }
    }

    public void setValidatorsForMessagStr(final String validator) {
        try {
            this.setValidator(ValidatorType.valueOf(validator));
        } catch (IllegalArgumentException e) {
            this.setValidator(null);
        }
        this.getExtensionsForSelectedValidator();
    }

    public void initializeValidatorList() {
        final List<ValidatorType> list = Arrays.asList(ValidatorType.values());
        final List<SelectItem> validatorsList = new ArrayList<>();

        final ValidationServiceManager vsm = new ValidationServiceManager();
        vsm.initValidationServiceDisplay();
        final List<ReferencedStandard> availableStandards = vsm.getAvailableStandards();
        for (final ValidatorType validatorValue : list) {
            final ArrayList<String> extensionsForValidator = new ArrayList<>();
            for (int i = 0; i < availableStandards.size(); i++) {
                final List<ValidationService> services = availableStandards.get(i).getServices();
                for (int j = 0; j < services.size(); j++) {
                    final boolean available = services.get(j).isAvailable();
                    if (available && validatorValue.getFriendlyName()
                            .equalsIgnoreCase(availableStandards.get(i).getName())) {
                        validatorsList.add(new SelectItem(validatorValue, validatorValue.getFriendlyName()));
                        extensionsForValidator.add(availableStandards.get(i).getExtension());

                    }
                }
                removeDuplicatesExtensions(extensionsForValidator);
            }
            this.authorizedExtensions.put(validatorValue.getFriendlyName(), extensionsForValidator);
        }
        removeDuplicatesValidators(validatorsList);

        if (this.validator == null) {
            this.validator = (ValidatorType) validatorsList.get(0).getValue();
        }
        this.getExtensionsForSelectedValidator();
        this.validators = validatorsList.toArray(new SelectItem[validatorsList.size()]);
    }

    // Remove form extensions list, extensions which are not allowed
    public void verifyExtensions(final List<String> authorizeExtensions, final List<String> extensions) {
        int size = extensions.size();
        for (int i = 0; i < size - 1; i++) {
            for (int j = i + 1; j < size; j++) {
                if (!authorizeExtensions.contains(extensions.get(i))) {
                    extensions.remove(i);
                    size--;
                }
            }
        }
    }

    public void verifyExtensions(final Map<String, List<String>> authorizedExtensions, final ValidatorType validators) {
        if (authorizedExtensions.containsKey(validators.getFriendlyName())) {
            this.extensions = authorizedExtensions.get(validators.getFriendlyName());
            if (this.extensions != null && this.extensions.size() == 1) {
                this.selectedExtension = this.extensions.get(0);
            } else {
                this.selectedExtension = null;
            }
        }
    }

    // Remove same name extensions
    public static int removeDuplicatesExtensions(final List<String> extensions) {
        int size = extensions.size();
        int duplicates = 0;
        for (int i = 0; i < size - 1; i++) {
            for (int j = i + 1; j < size; j++) {
                if (!extensions.get(j).equals(extensions.get(i))) {
                    continue;
                }
                duplicates++;
                extensions.remove(j);
                j--;
                size--;
            }
        }
        return duplicates;
    }

    // Remove same name validators
    public static int removeDuplicatesValidators(final List<SelectItem> result) {
        int size = result.size();
        int duplicates = 0;
        for (int i = 0; i < size - 1; i++) {
            for (int j = i + 1; j < size; j++) {
                if (!result.get(j).getLabel().equals(result.get(i).getLabel())) {
                    continue;
                }
                duplicates++;
                result.remove(j);
                j--;
                size--;
            }
        }
        return duplicates;
    }

    /**
     * Verify if the file contains extension and extension type is xml or txt If it is ok the content of the file is display
     */
    public void displayMessage(final String validationType) {
        if (this.fileName != null) {
            final String fileExtension = this.fileName.substring(this.fileName.lastIndexOf('.') + 1);
            InputStream fileStream;
            if (!fileExtension.isEmpty() && !"DICOM".equals(fileExtension)) {
                try {
                    final File tmpFile = new File(this.fileName);
                    fileStream = new FileInputStream(tmpFile);
                    String str = Streams.asString(fileStream);

                    // For validator detector
                    if (this.getStartOffset() != 0 && this.getEndOffset() != 0) {
                        str = str.substring(this.getStartOffset(), this.getEndOffset());
                        //   if (Base64.isArrayByteBase64(str.getBytes(StandardCharsets.UTF_8)) )
                        // no need to decode if already decoded
                        if (this.base64Decoded == false && validationType != null
                                && !"DICOM".equals(validationType) && !"TLS".equals(validationType)) {
                            //   final byte[] decodedStrBytes = Base64.decodeBase64(str);
                            final byte[] decodedStrBytes = DatatypeConverter.parseBase64Binary(str);

                            str = new String(decodedStrBytes, StandardCharsets.UTF_8);
                        }
                    }
                    final Tika t = new Tika();
                    final String result = t.detect(new FileInputStream(tmpFile));
                    if (result.contains("text") || result.contains("xml") || result.contains("html") || result
                            .contains("xhtml") || str.startsWith("--")) {
                        this.setMessageStr(str);
                    } else {
                        FacesMessages.instance().add(StatusMessage.Severity.WARN, "File content does not allow to show it");
                        LOGGER.warn("File extension does not allow to show it");
                    }
                } catch (final FileNotFoundException e) {
                    LOGGER.error(e.getMessage());
                } catch (final IOException e) {
                    LOGGER.error(e.getMessage());
                }
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.WARN, "No file provided or file extension is empty or it's TLS");
                LOGGER.info("No file provided or file extension is empty or it's TLS");
            }
        }
    }

    /**
     * Write content in File
     */
    public void writeInFile(final String content, final String fileName) {
        FileOutputStream fop = null;
        final File file;
        try {
            file = new File(fileName);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                final Boolean outcome = Boolean.valueOf(file.createNewFile());
                if (outcome.equals(Boolean.FALSE)) {
                    throw new IOException("Unable to create file");
                }
            }

            fop = new FileOutputStream(file);

            // get the content in bytes
            final byte[] contentInBytes = content.getBytes(StandardCharsets.UTF_8);

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

            LOGGER.info("Done");

        } catch (final IOException e) {
            LOGGER.error(e.getMessage());
        } finally {
            try {
                if (fop != null) {
                    fop.close();
                }
            } catch (final IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }

    public void forwardToValidation() {

        try {
            final ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();

            FacesMessages.instance().clear();
            String viewId;
            // initialize URL
            if (this.validator.getView().contains("?")) {
                viewId = this.validator.getView().concat("&");
            } else {
                viewId = this.validator.getView().concat("?");
            }
            // append selected extension (if any selected, None is not an extension)
            if (this.selectedExtension != null && !"None".equals(this.selectedExtension)) {
                viewId = viewId.concat("extension=");
                viewId = viewId.concat(this.selectedExtension);
            }
            // append id parameter
            if (viewId.contains("?")) {
                if (viewId.endsWith("&")) {
                    viewId = viewId.concat("id=");
                } else {
                    viewId = viewId.concat("&id=");
                }
            } else {
                viewId = viewId.concat("?id=");
            }

            // if message content Analyzer oid exist else null
            String decodeMessageContentAnalyzerOid;
            decodeMessageContentAnalyzerOid = this.messageContentAnalyzerOid;

            // If file exist and was base 64 encoded
            if (this.tmpFileBase64Decoded != null) {
                final FileInputStream fileStream = new FileInputStream(new File(this.tmpFileBase64Decoded.getAbsolutePath()));
                final String str = Streams.asString(fileStream);

                // Replace encoded string by decoded and
                // the name of the encoded file by the new decoded file name
                if (this.isBase64Decoded()) {
                    this.setMessageStr(str);
                    this.fileName = this.tmpFileBase64Decoded.getAbsolutePath();
                }
            }

            final File file = File.createTempFile("temp-file-name", ".tmp");
            final String tmpFilePath = file.getAbsolutePath();

            switch (this.validator) {
                case ATNA:
                    ValidatedAuditMessage validatedAuditMessage = ValidatedAuditMessage.createNewValidatedAuditMessage();
                    validatedAuditMessage.setExternalId(this.externalId);
                    validatedAuditMessage.setToolOid(this.toolOid);
                    validatedAuditMessage.setProxyType(this.proxyType);
                    this.writeInFile(this.messageStr, tmpFilePath);
                    FileUtils.copyFile(new File(tmpFilePath), new File(validatedAuditMessage.getMessagePath()));
                    validatedAuditMessage.setDescription(FILE_DESCRIPTION);
                    validatedAuditMessage.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    validatedAuditMessage = ValidatedObject.save(ValidatedAuditMessage.class, validatedAuditMessage);
                    viewId = viewId + validatedAuditMessage.getId();
                    break;
                case CDA:
                    CDAValidatedFile cdaValidatedFile = CDAValidatedFile.createNewCDAValidatedFile();
                    cdaValidatedFile.setProxyType(this.proxyType);
                    cdaValidatedFile.setExternalId(this.externalId);
                    cdaValidatedFile.setToolOid(this.toolOid);
                    this.writeInFile(this.messageStr, tmpFilePath);
                    FileUtils.copyFile(new File(tmpFilePath), new File(cdaValidatedFile.getCdaFilePath()));
                    cdaValidatedFile.setDescription(FILE_DESCRIPTION);
                    cdaValidatedFile.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    cdaValidatedFile = ValidatedObject.save(CDAValidatedFile.class, cdaValidatedFile);
                    viewId = viewId + cdaValidatedFile.getId();
                    break;
                case DICOM:
                    DicomValidatedObject dicomMessage = DicomValidatedObject.createNewDicomObject();
                    dicomMessage.setProxyType(this.proxyType);
                    dicomMessage.setExternalId(this.externalId);
                    dicomMessage.setToolOid(this.toolOid);
                    final File uploadedFile = new File(dicomMessage.getFilePath());
                    FileUtils.copyFile(new File(this.fileName), uploadedFile);
                    dicomMessage.setDescription("Uploaded.dcm");
                    dicomMessage.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    dicomMessage = ValidatedObject.save(DicomValidatedObject.class, dicomMessage);
                    viewId = viewId + dicomMessage.getId();
                    break;
                case DSUB:
                    ValidatedDSUB validateddsub = ValidatedDSUB.createNewValidatedDSUB();
                    validateddsub.setExternalId(this.externalId);
                    validateddsub.setToolOid(this.toolOid);
                    validateddsub.setProxyType(this.proxyType);
                    this.writeInFile(this.messageStr, tmpFilePath);
                    FileUtils.copyFile(new File(tmpFilePath), new File(validateddsub.getFilePath()));
                    validateddsub.setDescription(FILE_DESCRIPTION);
                    validateddsub.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    validateddsub = ValidatedObject.save(ValidatedDSUB.class, validateddsub);
                    viewId = viewId + validateddsub.getId();
                    break;
                case HL7V2:
                    HL7ValidatedMessage hl7Message = new HL7ValidatedMessage();
                    hl7Message.setLucky(this.lucky);
                    hl7Message.setExternalId(this.externalId);
                    hl7Message.setToolOid(this.toolOid);
                    hl7Message.setProxyType(this.proxyType);
                    hl7Message.setValidatedMessage(this.messageStr);
                    hl7Message.setDescription("receivedFromGazelleProxy.txt");
                    hl7Message.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    hl7Message = ValidatedObject.save(HL7ValidatedMessage.class, hl7Message);
                    viewId = viewId + hl7Message.getId();
                    break;
                case HL7V3:
                    HL7v3ValidatedMessage hl7v3Message = HL7v3ValidatedMessage.createNewHL7v3ValidatedMessage();
                    hl7v3Message.setExternalId(this.externalId);
                    hl7v3Message.setToolOid(this.toolOid);
                    hl7v3Message.setProxyType(this.proxyType);
                    this.writeInFile(this.messageStr, tmpFilePath);
                    FileUtils.copyFile(new File(tmpFilePath), new File(hl7v3Message.getMessagePath()));
                    hl7v3Message.setDescription(FILE_DESCRIPTION);
                    hl7v3Message.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    hl7v3Message = ValidatedObject.save(HL7v3ValidatedMessage.class, hl7v3Message);
                    viewId = viewId + hl7v3Message.getId();
                    break;
                case HPD:
                    ValidatedHPD validatedhpd = ValidatedHPD.createNewValidatedHPD();
                    validatedhpd.setExternalId(this.externalId);
                    validatedhpd.setToolOid(this.toolOid);
                    validatedhpd.setProxyType(this.proxyType);
                    this.writeInFile(this.messageStr, tmpFilePath);
                    FileUtils.copyFile(new File(tmpFilePath), new File(validatedhpd.getFilePath()));
                    validatedhpd.setDescription(FILE_DESCRIPTION);
                    validatedhpd.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    validatedhpd = ValidatedObject.save(ValidatedHPD.class, validatedhpd);
                    viewId = viewId + validatedhpd.getId();
                    break;
                case PDF:
                    ValidatedPdf validatedPdf = ValidatedPdf.createNewValidatedPdf();
                    validatedPdf.setLucky(this.lucky);
                    validatedPdf.setExternalId(this.externalId);
                    validatedPdf.setToolOid(this.toolOid);
                    validatedPdf.setProxyType(this.proxyType);
                    FileUtils.copyFile(new File(this.fileName), new File(validatedPdf.getPdfPath()));
                    validatedPdf.setDescription("Uploaded.pdf");
                    validatedPdf.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    validatedPdf = ValidatedObject.save(ValidatedPdf.class, validatedPdf);
                    viewId = viewId + validatedPdf.getId();
                    break;
                case SAML:
                    ValidatedAssertion validatedAss = ValidatedAssertion.createNewAssertion();
                    validatedAss.setExternalId(this.externalId);
                    validatedAss.setToolOid(this.toolOid);
                    validatedAss.setProxyType(this.proxyType);
                    this.writeInFile(this.messageStr, tmpFilePath);
                    FileUtils.copyFile(new File(tmpFilePath), new File(validatedAss.getFilePath()));
                    validatedAss.setDescription(FILE_DESCRIPTION);
                    validatedAss.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    validatedAss = ValidatedObject.save(ValidatedAssertion.class, validatedAss);
                    viewId = viewId + validatedAss.getId();
                    break;
                case SVS:
                    ValidatedSVS validatedSVS = ValidatedSVS.createNewValidatedSVS();
                    validatedSVS.setExternalId(this.externalId);
                    validatedSVS.setToolOid(this.toolOid);
                    validatedSVS.setProxyType(this.proxyType);
                    this.writeInFile(this.messageStr, tmpFilePath);
                    FileUtils.copyFile(new File(tmpFilePath), new File(validatedSVS.getFilePath()));
                    validatedSVS.setDescription(FILE_DESCRIPTION);
                    validatedSVS.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    validatedSVS = ValidatedObject.save(ValidatedSVS.class, validatedSVS);
                    viewId = viewId + validatedSVS.getId();
                    break;
                case TLS:
                    ValidatedCertificates validatedCertificates = ValidatedCertificates.createNewValidatedCertificates();
                    validatedCertificates.setLucky(this.lucky);
                    validatedCertificates.setExternalId(this.externalId);
                    validatedCertificates.setToolOid(this.toolOid);
                    validatedCertificates.setProxyType(this.proxyType);
                    FileUtils.copyFile(new File(this.fileName), new File(validatedCertificates.getPemPath()));
                    validatedCertificates.setDescription("Uploaded.pem");
                    validatedCertificates.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    validatedCertificates = ValidatedObject.save(ValidatedCertificates.class, validatedCertificates);
                    viewId = viewId + validatedCertificates.getId();
                    break;
                case XDS:
                    ValidatedXDS validatedXDS = ValidatedXDS.createNewValidatedXDS();
                    validatedXDS.setExternalId(this.externalId);
                    validatedXDS.setToolOid(this.toolOid);
                    validatedXDS.setProxyType(this.proxyType);
                    this.writeInFile(this.messageStr, tmpFilePath);
                    FileUtils.copyFile(new File(tmpFilePath), new File(validatedXDS.getXdsPath()));
                    validatedXDS.setDescription(FILE_DESCRIPTION);
                    validatedXDS.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    validatedXDS = ValidatedObject.save(ValidatedXDS.class, validatedXDS);
                    viewId = viewId + validatedXDS.getId();
                    break;
                case XDW:
                    ValidatedXDW validatedXDW = ValidatedXDW.createNewValidatedXDW();
                    validatedXDW.setExternalId(this.externalId);
                    validatedXDW.setToolOid(this.toolOid);
                    validatedXDW.setProxyType(this.proxyType);
                    this.writeInFile(this.messageStr, tmpFilePath);
                    FileUtils.copyFile(new File(tmpFilePath), new File(validatedXDW.getXDWPath()));
                    validatedXDW.setDescription(FILE_DESCRIPTION);
                    validatedXDW.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    validatedXDW = ValidatedObject.save(ValidatedXDW.class, validatedXDW);
                    viewId = viewId + validatedXDW.getId();
                    break;
                case XML:
                    ValidatedXML validatedXML = ValidatedXML.createNewValidatedXML();
                    validatedXML.setExternalId(this.externalId);
                    validatedXML.setToolOid(this.toolOid);
                    validatedXML.setProxyType(this.proxyType);
                    this.writeInFile(this.messageStr, tmpFilePath);
                    FileUtils.copyFile(new File(tmpFilePath), new File(validatedXML.getXMLPath()));
                    validatedXML.setDescription(FILE_DESCRIPTION);
                    validatedXML.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    validatedXML = ValidatedObject.save(ValidatedXML.class, validatedXML);
                    viewId = viewId + validatedXML.getId();
                    break;
                case FHIR:
                    ValidatedFHIR validatedFHIR = ValidatedFHIR.createNewValidatedFHIR(FhirObjectType.XML);
                    validatedFHIR.setExternalId(this.externalId);
                    validatedFHIR.setToolOid(this.toolOid);
                    validatedFHIR.setProxyType(this.proxyType);
                    validatedFHIR.setValidatedObjectType(FhirObjectType.XML);
                    this.writeInFile(this.messageStr, tmpFilePath);
                    FileUtils.copyFile(new File(tmpFilePath), new File(validatedFHIR.getResourcePath()));
                    validatedFHIR.setDescription(FILE_DESCRIPTION);
                    validatedFHIR.setMessageContentAnalyzerOid(decodeMessageContentAnalyzerOid);
                    validatedFHIR = ValidatedObject.save(ValidatedFHIR.class, validatedFHIR);
                    viewId = viewId + validatedFHIR.getId();
                    break;
                default:
                    break;
            }

            if (file.exists()) {
                if (!file.delete()) {
                    RemoteValidator.LOGGER.error("Unable to delete file");
                }
            }

            if (this.tmpFileBase64Decoded != null) {
                if (!this.tmpFileBase64Decoded.delete()) {
                    LOGGER.error("Something went wrong when deleting temporary file");
                }

            }

            ec.redirect(ec.getRequestContextPath() + viewId);


        } catch (final IOException e) {
            LOGGER.error("Failed to transform file...", e.getMessage());
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Failure: " + e.getMessage());
        }
    }

    public String getSelectedExtension() {
        return this.selectedExtension;
    }

    public void setSelectedExtension(final String selectedExtension) {
        this.selectedExtension = selectedExtension;
    }

    public List<String> getExtensions() {
        if (this.extensions != null) {
            Collections.sort(this.extensions, String.CASE_INSENSITIVE_ORDER);
        }
        return this.extensions;
    }
}
