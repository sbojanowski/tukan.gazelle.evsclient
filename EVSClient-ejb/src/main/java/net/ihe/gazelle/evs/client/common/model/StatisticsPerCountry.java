/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import org.hibernate.annotations.IndexColumn;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Name("statisticsPerCountry")
@Table(name = "statistics_per_country", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "country"))
@SequenceGenerator(name = "statistics_per_country_sequence", sequenceName = "statistics_per_country_id_seq", allocationSize = 1)
public class StatisticsPerCountry implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2920527039289642801L;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(StatisticsPerCountry.class);

    @Id
    @GeneratedValue(generator = "statistics_per_country_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id")
    private Integer id;

    @Column(name = "country")
    private String country;

    @Column(name = "number_of_calls")
    private Integer numberOfCalls;

    @ElementCollection(targetClass = String.class)
    @IndexColumn(name = "ip_address")
    private List<String> ipAddresses;

    public String getCountry() {
        return this.country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    public Integer getNumberOfCalls() {
        return this.numberOfCalls;
    }

    public void setNumberOfCalls(final Integer numberOfCalls) {
        this.numberOfCalls = numberOfCalls;
    }

    public List<String> getIpAddresses() {
        return this.ipAddresses;
    }

    public void setIpAddresses(final List<String> ipAddresses) {
        if (ipAddresses != null) {
            this.ipAddresses = new ArrayList<String>(ipAddresses);
        }
        else {
            this.ipAddresses = null;
        }
    }

    public StatisticsPerCountry() {

    }

    public StatisticsPerCountry(final String country, final String ipAddress) {
        this.country = country;
        this.numberOfCalls = Integer.valueOf(1);
        this.ipAddresses = new ArrayList<>();
        this.ipAddresses.add(ipAddress);
    }

    public void incrementNumberOfCalls() {
        this.numberOfCalls = Integer.valueOf(this.numberOfCalls + 1);
        this.save();
    }

    public void save() {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.merge(this);
        entityManager.flush();
    }

    public static void updateCountryStatistics(final String ipAddress) {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        StatisticsPerCountry country;

        // We do not need to query freegeoIP in order to get the country of origin, if we already have the information in the database.
        // Search for the IP in the database first.
        final Query query = entityManager.createQuery(
                "select c from StatisticsPerCountry c join c.ipAddresses addresses where addresses = :param");
        query.setParameter("param", ipAddress);
        List<StatisticsPerCountry> countries = (List<StatisticsPerCountry>) query.getResultList();
        if (countries != null && !countries.isEmpty()) {
            country = countries.get(0);
            country.incrementNumberOfCalls();
        }
        // If we have not found the IP in the database, then we need to query outside tools.
        else {
            final ClientRequest request = new ClientRequest("http://freegeoip.net/xml/" + ipAddress);
            try {
                final ClientResponse<String> response = request.get(String.class);
                if (response.getStatus() == 200 && response.getEntity() != null && !response.getEntity().isEmpty()) {
                    final String info = response.getEntity();
                    String countryName;
                    final int pos = info.indexOf("<CountryName>");
                    if (pos > 0) {
                        final int end = info.indexOf("</CountryName");
                        countryName = info.substring(pos + 13, end);
                        if (countryName.contains("Korea")) {
                            countryName = "Republic of Korea";
                        }
                        final HQLQueryBuilder<StatisticsPerCountry> builder = new HQLQueryBuilder<>(entityManager,
                                StatisticsPerCountry.class);
                        builder.addEq("country", countryName);
                        countries = builder.getList();
                        if (countries != null && !countries.isEmpty()) {
                            country = countries.get(0);
                            country.getIpAddresses().add(ipAddress);
                            country.incrementNumberOfCalls();
                        } else {
                            country = new StatisticsPerCountry(countryName, ipAddress);
                            country.save();
                        }
                    }
                } else {
                    StatisticsPerCountry.LOGGER.warn("Quota has been reached, IP address will not be computed");
                }
            } catch (final Exception e) {
                StatisticsPerCountry.LOGGER
                        .error("an error occurred when retrieving the country for ip address: {}", ipAddress, e);
            }
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.country == null ? 0 : this.country.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final StatisticsPerCountry other = (StatisticsPerCountry) obj;
        if (this.country == null) {
            if (other.country != null) {
                return false;
            }
        } else if (!this.country.equals(other.country)) {
            return false;
        }
        return true;
    }
}
