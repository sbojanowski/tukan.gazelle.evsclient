/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.schematron;

import java.io.Serializable;

public class Schematron implements Serializable, Comparable<Schematron> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String label;

    private String name;

    private String version;

    private String description;

    public Schematron(final String inLabel, final String inName, final String inVersion, final String inDescription) {
        this.setLabel(inLabel);
        this.setName(inName);
        this.setVersion(inVersion);
        this.setDescription(inDescription);
    }

    public Schematron() {

    }

    public final void setLabel(final String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public final void setVersion(final String version) {
        this.version = version;
    }

    public String getVersion() {
        return this.version;
    }

    public final void setDescription(final String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.label == null ? 0 : this.label.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Schematron other = (Schematron) obj;
        if (this.label == null) {
            if (other.label != null) {
                return false;
            }
        } else if (!this.label.equals(other.label)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(final Schematron o) {
        if (this.getLabel() != null && o != null && o.getLabel() != null) {
            return this.getLabel().compareTo(o.getLabel());
        } else {
            return 1;

        }
    }
}
