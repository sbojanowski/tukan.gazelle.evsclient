/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.dicom.action;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.dicom.evs.api.*;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.common.model.ValidationService;
import net.ihe.gazelle.evs.client.dicom.model.DicomOperation;
import net.ihe.gazelle.evs.client.dicom.model.DicomTools;
import net.ihe.gazelle.evs.client.dicom.model.DicomValidatedObject;
import net.ihe.gazelle.evs.client.util.Util;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.xml.transform.TransformerConfigurationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

@Synchronized(timeout = 100000)
@Name("dicomValidationManagerBean")
@Scope(ScopeType.PAGE)
public class DicomValidationManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(DicomValidationManager.class);


    private DicomValidatedObject selectedDicomFile;
    private ValidationService selectedService;
    private ReferencedStandard selectedStandard;
    private DicomTools selectedTool;
    private DicomOperation selectedOperation;
    private List<ValidationService> services;
    private String standardDescription;
    private String selectedDicomToolDumper;
    private String selectedDicomToolValidator;
    private boolean validationDone;
    private static final String ACCEPTED_FILE_TYPES = "dcm, DCM";

    public String getAcceptedFileTypes() {

        String result = ApplicationPreferenceManager.getStringValue("DICOM_file_types");
        if (result != null) {
            return (result);
        }
        else {
            return DicomValidationManager.ACCEPTED_FILE_TYPES;
        }
    }
    public String getSelectedDicomToolDumper() {
        return this.selectedDicomToolDumper;
    }

    public void setSelectedDicomToolDumper(final String selectedDicomToolDumper) {
        this.selectedDicomToolDumper = selectedDicomToolDumper;
    }

    public String getSelectedDicomToolValidator() {
        return this.selectedDicomToolValidator;
    }

    public void setSelectedDicomToolValidator(final String selectedDicomToolValidator) {
        this.selectedDicomToolValidator = selectedDicomToolValidator;
    }

    public String getStandardDescription() {
        return this.standardDescription;
    }

    public void setStandardDescription(final String standardDescription) {
        this.standardDescription = standardDescription;
    }

    public DicomOperation getSelectedOperation() {
        return this.selectedOperation;
    }

    public void setSelectedOperation(final DicomOperation selectedOperation) {
        this.selectedOperation = selectedOperation;
    }

    public DicomValidatedObject getSelectedDicomFile() {
        return this.selectedDicomFile;
    }

    public ValidationService getSelectedService() {
        return this.selectedService;
    }

    public void setSelectedService(final ValidationService selectedService) {
        this.selectedService = selectedService;
        if (selectedService != null) {
            this.selectedTool = this.selectToolForService(selectedService.getKeyword());
        } else {
            this.selectedTool = null;
        }
        this.selectedOperation = null;
    }

    private DicomTools selectToolForService(final String keyword) {
        final List<DicomTools> all = Arrays.asList(DicomTools.values());
        for (final DicomTools tool : all) {
            if (tool.getValue().equalsIgnoreCase(keyword)) {
                return tool;
            }
        }
        return null;
    }

    public void init() {
        final List<ReferencedStandard> standards = ReferencedStandard
                .getReferencedStandardFiltered("DICOM", null, null, null);
        if (standards != null && !standards.isEmpty()) {
            this.selectedStandard = standards.get(0);
            this.services = ValidationService.getListOfAvailableValidationServicesForStandard(this.selectedStandard);

            if (this.selectedStandard.getDescription() != null && !this.selectedStandard.getDescription().isEmpty()) {
                this.setStandardDescription(this.selectedStandard.getDescription());
            }

        } else {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "gazelle.evs.client.dicom.standard.notdefined");
            DicomValidationManager.LOGGER.error("DICOM referenced standard has not been defined !");
            return;
        }
        final Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.get("oid") != null) {
            final DicomValidatedObject objectToValidateAgain = ValidatedObject
                    .getObjectByOID(DicomValidatedObject.class, params.get("oid"), null);
            if (objectToValidateAgain != null) {
                this.selectedDicomFile = new DicomValidatedObject(objectToValidateAgain);
            } else {
                this.selectedDicomFile = null;
                FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "net.ihe.gazelle.evs.CantRetrieveDICOMFileErrorMessage");
            }
        } else if (params.get("id") != null) {
            try {
                this.selectedDicomFile = ValidatedObject
                        .getObjectByID(DicomValidatedObject.class, Integer.valueOf(params.get("id")), null);
            } catch (final NumberFormatException e) {
                FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "net.ihe.gazelle.evs.CantRetrieveDICOMFileErrorMessage");
                this.selectedDicomFile = null;
            }
        }
    }

    public void reset() {
        this.selectedService = null;
        this.selectedDicomFile = null;
        this.selectedOperation = null;
        this.selectedTool = null;
        this.setValidationDone(false);
    }

    public void uploadListener(final FileUploadEvent event) {
        FileOutputStream fos = null;
        try {
            this.selectedDicomFile = DicomValidatedObject.createNewDicomObject();
            final File uploadedFile = new File(this.selectedDicomFile.getFilePath());
            if (uploadedFile.getParentFile().mkdirs()) {
                DicomValidationManager.LOGGER.info("mkdir successful {}", this.selectedDicomFile.getFilePath());
            }

            final UploadedFile item = event.getUploadedFile();
            this.selectedDicomFile.setDescription(item.getName());
            if (item.getData() != null && item.getData().length > 0) {
                fos = new FileOutputStream(uploadedFile);
                fos.write(item.getData());
                fos.close();
            } else {
                FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "gazelle.evs.client.dicom.file.empty");
                this.selectedDicomFile.setObjectPath(null);
                this.selectedDicomFile = DicomValidatedObject.mergeDicomValidatedObject(this.selectedDicomFile);
            }
        } catch (final IOException e) {
            FacesMessages.instance().add(Severity.ERROR, e.getMessage());
            DicomValidationManager.LOGGER.error("uploadListener: an error occurred: {}", e.getMessage());
            return;
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (final IOException e) {
                    DicomValidationManager.LOGGER.error("not able to close the FIS", e.getMessage());
                }
            }
        }

    }

    public List<DicomOperation> getListOperations() {
        if (this.selectedTool != null) {
            final List<DicomOperation> res = new ArrayList<>();
            final List<DicomOperation> all = Arrays.asList(DicomOperation.values());
            for (final DicomOperation dicomOperation : all) {
                if (dicomOperation != null && dicomOperation.getTool() != null) {
                    if (dicomOperation.getTool().equals(this.selectedTool)) {
                        res.add(dicomOperation);
                    }
                }
            }
            return res;
        } else {
            return null;
        }
    }

    public void execute() {
        if (this.selectedOperation != null) {

            final String result = this.executeDicomOperation(this.selectedOperation, this.selectedDicomFile.getFilePath());
            this.merge(this.selectedDicomFile, this.selectedStandard, this.selectedService, this.selectedOperation, result);

        } else {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "net.ihe.gazelle.evs.NoDicomOperationSelected");
        }
    }

    private void   merge(final DicomValidatedObject dicomFile, final ReferencedStandard standard, final ValidationService service, final DicomOperation operation, final String result) {

        dicomFile.setStandard(standard);
        dicomFile.setValidationDate(new Date());
        dicomFile.setValidationContext(null);
        dicomFile.setUserIp(Util.getUserIpAddress());
        dicomFile.updateCountryStatistics();
        dicomFile.setDetailedResult(result);
        dicomFile.setService(service);
        dicomFile.setDicomOperation(operation.getLabel());
        dicomFile.setDicomServiceVersion(operation.getVersion());
        dicomFile.setOid(OIDGenerator.getNewOid());
        dicomFile.setDicomToolDumper(this.getSelectedDicomToolDumper());
        dicomFile.setDicomToolValidator(this.getSelectedDicomToolValidator());
        this.selectedDicomFile = DicomValidatedObject.mergeDicomValidatedObject(dicomFile);

    }

    public void validate() {
        String result;
        final Dicom3tools d3t = new Dicom3tools();
        if (this.selectedService != null) {
            this.setValidationDone(true);
            if ("Pixelmed".equals(this.selectedService.getName())) {
                final Pixelmed pixelmed = new Pixelmed();
                result = this.executeDicomOperation(DicomOperation.PIXELMED_VALIDATION, this.selectedDicomFile.getFilePath());
                this.setSelectedDicomToolValidator(
                        DicomOperation.PIXELMED_VALIDATION.getTool().getValue() + " version : " + pixelmed
                                .getVersion());
            } else if ("dcm4che".equals(this.selectedService.getName())) {
                this.setSelectedService(ValidationService.getValidationServiceByKeyword("dicom3tools"));
                this.setSelectedOperation(DicomOperation.DICOM3TOOLS_VALIDATION);
                result = this.executeDicomOperation(DicomOperation.DICOM3TOOLS_VALIDATION, this.selectedDicomFile.getFilePath());
                this.setSelectedDicomToolValidator(
                        DicomOperation.DICOM3TOOLS_VALIDATION.getTool().getValue() + " version : " + d3t.getVersion());
            } else if ("dcmcheck".equals(this.selectedService.getName())) {
                final DcmCheckValidator dcmcheck = new DcmCheckValidator();
                result = this.executeDicomOperation(DicomOperation.DCMCHECK_VALIDATION, this.selectedDicomFile.getFilePath());
                this.setSelectedDicomToolValidator(
                        DicomOperation.DCMCHECK_VALIDATION.getTool().getValue() + " version : " + dcmcheck
                                .getVersion());
            } else if ("dccheck".equalsIgnoreCase(this.selectedService.getName())){
                result = this.executeDicomOperation(DicomOperation.DCCHECK_VALIDATION, this.selectedDicomFile.getFilePath());
            } else {
                this.setSelectedService(ValidationService.getValidationServiceByKeyword("dicom3tools"));
                this.setSelectedOperation(DicomOperation.DICOM3TOOLS_VALIDATION);

                result = this.executeDicomOperation(DicomOperation.DICOM3TOOLS_VALIDATION, this.selectedDicomFile.getFilePath());
                this.setSelectedDicomToolValidator(
                        DicomOperation.DICOM3TOOLS_VALIDATION.getTool().getValue() + " version : " + d3t.getVersion());
            }

            this.merge(this.selectedDicomFile, this.selectedStandard, this.selectedService, this.selectedOperation, result);

        } else {
            this.setValidationDone(false);
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "net.ihe.gazelle.evs.NoDicomOperationSelected");
        }
    }

    public void validateAndDump() {
        String fileContent;
        if (this.selectedService != null) {
            if ("Pixelmed".equals(this.selectedService.getName())) {
                final Pixelmed pixelmed = new Pixelmed();
                this.setSelectedOperation(DicomOperation.PIXELMED_DUMP);
                fileContent = this.executeDicomOperation(DicomOperation.PIXELMED_DUMP, this.selectedDicomFile.getFilePath());
                this.setSelectedDicomToolDumper(
                        DicomOperation.PIXELMED_DUMP.getTool().getValue() + " version : " + pixelmed.getVersion());

            } else if ("Dicom 3 tools".equals(this.selectedService.getName())) {
                final Dicom3tools d3t = new Dicom3tools();
                this.setSelectedOperation(DicomOperation.DICOM3TOOLS_DUMP);
                fileContent = this.executeDicomOperation(DicomOperation.DICOM3TOOLS_DUMP, this.selectedDicomFile.getFilePath());
                this.setSelectedDicomToolDumper(
                        DicomOperation.DICOM3TOOLS_DUMP.getTool().getValue() + " version : " + d3t.getVersion());
            } else {
                final Dicom4che dcm4che = new Dicom4che();
                if (this.selectedOperation == null) {
                    this.setSelectedOperation(DicomOperation.DCM4CHE_DUMP);
                }
                fileContent = this.executeDicomOperation(DicomOperation.DCM4CHE_DUMP, this.selectedDicomFile.getFilePath());
                this.setSelectedDicomToolDumper(
                        DicomOperation.DCM4CHE_DUMP.getTool().getValue() + " version : " + dcm4che.getVersion());

            }
            this.selectedDicomFile.setDicomFileContent(fileContent);
        }
        this.validate();
    }

    private String executeDicomOperation(final DicomOperation selectedOperation, final String objectPath) {
        String resultValidation = null;
        if (selectedOperation != null) {
            final Pixelmed pixelmed = new Pixelmed();
            final Dicom4che dcm4che = new Dicom4che();
            Dicom3tools d3t = new Dicom3tools();
            DcmCheckValidator dcmcheck = new DcmCheckValidator();
            final DCCheck dccheck = new DCCheck();
            switch (selectedOperation) {
            case PIXELMED_VALIDATION:
                try {
                    resultValidation = pixelmed.validateDicomSRDose(objectPath);
                    selectedOperation.setVersion(pixelmed.getVersion());
                } catch (final Exception e) {
                    resultValidation =
                            "A problem occurred when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                    + selectedOperation.getLabel() + '(' + pixelmed.getVersion() + "). " + e
                                    .getMessage();
                    DicomValidationManager.LOGGER.error(resultValidation);
                }
                break;

            case PIXELMED_DUMP:
                try {
                    resultValidation = pixelmed.getDumpResult(objectPath);
                    selectedOperation.setVersion(pixelmed.getVersion());
                } catch (final Exception e) {
                    resultValidation =
                            "A problem occurred when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                    + selectedOperation.getLabel() + '(' + pixelmed.getVersion() + "). " + e
                                    .getMessage();
                    DicomValidationManager.LOGGER.error(resultValidation);
                }
                break;

            case PIXELMED_DUMP2XML:
                try {
                    resultValidation = pixelmed.dicom2xml(objectPath);
                    selectedOperation.setVersion(pixelmed.getVersion());
                } catch (final Exception e) {
                    resultValidation =
                            "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                    + selectedOperation.getLabel() + '(' + pixelmed.getVersion() + "). " + e
                                    .getMessage();
                    DicomValidationManager.LOGGER.error(resultValidation);
                }
                break;

            case DCM4CHE_DUMP:
                try {
                    resultValidation = dcm4che.dicom2txt(objectPath);
                    selectedOperation.setVersion(dcm4che.getVersion());
                } catch (final IOException e) {
                    resultValidation =
                            "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                    + selectedOperation.getLabel() + '(' + dcm4che.getVersion() + "). " + e
                                    .getMessage();
                    DicomValidationManager.LOGGER.error(resultValidation);
                }
                break;

            case DCM4CHE_DUMP2XML:
                try {
                    resultValidation = dcm4che.dicom2xml(objectPath);
                    selectedOperation.setVersion(dcm4che.getVersion());
                } catch (final TransformerConfigurationException|IOException e) {
                    resultValidation =
                            "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                    + selectedOperation.getLabel() + '(' + dcm4che.getVersion() + "). " + e
                                    .getMessage();
                    DicomValidationManager.LOGGER.error(resultValidation);
                }
                break;

            case DICOM3TOOLS_DUMP:
                try {
                    d3t = new Dicom3tools(this.selectedService.getWsdl());
                    resultValidation = d3t.dicomDump(objectPath);
                    selectedOperation.setVersion(d3t.getVersion());
                } catch (final RuntimeException e) {
                    DicomValidationManager.LOGGER.error(e.getMessage());
                    resultValidation =
                            "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                    + selectedOperation.getLabel() + '(' + d3t.getVersion() + "). " + e
                                    .getMessage();
                }
                break;

            case DICOM3TOOLS_VALIDATION:
                try {
                    d3t = new Dicom3tools(this.selectedService.getWsdl());
                    resultValidation = d3t.validate(objectPath);
                    selectedOperation.setVersion(d3t.getVersion());
                } catch (final RuntimeException e) {
                    DicomValidationManager.LOGGER.error(e.getMessage());
                    resultValidation =
                            "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                    + selectedOperation.getLabel() + '(' + d3t.getVersion() + "). " + e.getMessage();
                }
                break;

            case DCMCHECK_VALIDATION:
                try {
                    dcmcheck = new DcmCheckValidator(this.selectedService.getWsdl());
                    resultValidation = dcmcheck.validate(objectPath);
                } catch (final RuntimeException e) {
                    DicomValidationManager.LOGGER.error(e.getMessage());
                    resultValidation =
                            "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                    + selectedOperation.getLabel() + '(' + dcmcheck.getVersion() + "). " + e
                                    .getMessage();
                }
                break;
            case DCCHECK_VALIDATION:
                try {
                    final String inValidatorEndpoint = this.selectedService.getWsdl();
                    final String xslLocation = this.selectedService.getXslLocation();
                    resultValidation = dccheck.validate(inValidatorEndpoint, xslLocation, objectPath);
                } catch (final RuntimeException e) {
                    DicomValidationManager.LOGGER.error(e.getMessage());
                    resultValidation =
                            "A problem occure when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                    + selectedOperation.getLabel() + ". Error: " + e;
                }
                break;
            case DVTK_VALIDATION:
                try {
                    final String inValidatorEndpoint = this.selectedService.getWsdl();
                    final String xslLocation = this.selectedService.getXslLocation();
                    final DVTK dvtk = new DVTK();
                    resultValidation = dvtk.validate(inValidatorEndpoint, xslLocation, objectPath);
                } catch (final RuntimeException e) {
                    DicomValidationManager.LOGGER.error(e.getMessage());
                    resultValidation =
                            "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                    + selectedOperation.getLabel() + ". Error: " + e;
                }
                break;
            default:
                break;
            }
        }
        if (resultValidation != null) {
            resultValidation = resultValidation.replace("\0", "");
        }
        return resultValidation;
    }

    public boolean displayExecute() {
        return this.selectedDicomFile != null && this.selectedOperation != null;
    }

    public List<ValidationService> getServices() {
        return this.services;
    }

    public boolean getValidationDone() {
        return this.validationDone;
    }

    public void setValidationDone(final boolean validationDone) {
        this.validationDone = validationDone;
    }

    public void setMessageInvalidFileType(){
        FacesMessages.instance().add(Severity.ERROR,"Invalid file type. Expected file types are : "+ this
                .getAcceptedFileTypes());
    }

}
