/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.util;

import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.Map;

public class XmlContentDecode {

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlContentDecode.class);

    public static Map<String, String> decodeMetadata(final String xmlMetadata) {
        return null;
    }

    public static Document buildDocumentFromString(final String inString) {
        Document document;
        if (inString == null || inString.length() == 0) {
            return null;
        }
        try {
            document = DocumentHelper.parseText(inString);
        } catch (final DocumentException e) {
            LOGGER.error(e.getMessage(),e);
            return null;
        }
        return document;
    }

    public static String getValidationResult(final String inDetailedResult) {
        final Document document = XmlContentDecode.buildDocumentFromString(inDetailedResult);
        if (document == null) {
            return null;
        }
        try {
            if (inDetailedResult.contains("ValidationResultsOverview")) {
                return document.getRootElement().selectSingleNode("ValidationResultsOverview").selectSingleNode("ValidationTestResult").getText();
            } else if (inDetailedResult.contains("ValidationReport")) {
                return document.getRootElement().selectSingleNode("ReportHeader").selectSingleNode("ResultOfTest").getText().toUpperCase(Locale.getDefault());
            } else {
                return null;
            }
        } catch (final RuntimeException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }

    }

    public static String getValidationDate(final String inDetailedResult) {

        final Document document = XmlContentDecode.buildDocumentFromString(inDetailedResult);
        if (document == null) {
            return null;
        }
        try {
            return document.getRootElement().selectSingleNode("ValidationResultsOverview")
                    .selectSingleNode("ValidationDate").getText();
        } catch (final RuntimeException e) {
            LOGGER.error(e.getMessage(),e);
            return null;
        }
    }

    public static String getValidationTime(final String inDetailedResult) {

        final Document document = XmlContentDecode.buildDocumentFromString(inDetailedResult);
        try {
            return document.getRootElement().selectSingleNode("ValidationResultsOverview")
                    .selectSingleNode("ValidationTime").getText();
        } catch (final RuntimeException e) {
            LOGGER.error(e.getMessage(),e);
            return null;
        }
    }

    public static boolean isDocumentWellFormed(final String inDetailedResult) {

        final Document document = XmlContentDecode.buildDocumentFromString(inDetailedResult);
        if (document == null) {
            return false;
        }
        try {
            final String result = document.getRootElement().selectSingleNode("ValidationResultsOverview")
                    .selectSingleNode("documentWellFormed").selectSingleNode("result").getText();
            return "yes".equals(result);
        } catch (final RuntimeException e) {
            LOGGER.error(e.getMessage(),e);
            return false;
        }
    }

    /**
     * @param validationKind   : xml tag for the requested validation, for instance documentValidCDA, documentValidEpsos
     * @param inDetailedResult
     */
    public static boolean isDocumentValid(final String validationKind, final String inDetailedResult) {

        if (validationKind == null || validationKind.length() == 0) {
            return false;
        }

        final Document document = XmlContentDecode.buildDocumentFromString(inDetailedResult);
        if (document == null) {
            return false;
        }
        try {
            final String result = document.getRootElement().selectSingleNode("ValidationResultsOverview")
                    .selectSingleNode(validationKind).selectSingleNode("result").getText();
            return "yes".equals(result);
        } catch (final RuntimeException e) {
            LOGGER.error(e.getMessage(),e);
            return false;
        }
    }

    public static String getOid(final String stringResponse) {

        final Document document = XmlContentDecode.buildDocumentFromString(stringResponse);
        if (document == null) {
            return null;
        }
        try {
            return document.getRootElement().selectSingleNode("ValidationResultsOverview").selectSingleNode("Oid")
                    .getText();
        } catch (final RuntimeException e) {
            LOGGER.error(e.getMessage(),e);
            return OIDGenerator.getNewOid();
        }

    }

}
