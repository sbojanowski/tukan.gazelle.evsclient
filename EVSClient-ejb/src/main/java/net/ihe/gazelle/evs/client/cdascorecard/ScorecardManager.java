package net.ihe.gazelle.evs.client.cdascorecard;

import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.evs.client.xml.model.AbstractValidatedXMLFile;
import net.ihe.gazelle.evs.client.xml.model.CDAValidatedFile;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.Serializable;
import java.nio.charset.Charset;

/**
 * <b>Class Description : </b>ScorecardManager<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 07/12/16
 * @class ScorecardManager
 * @package net.ihe.gazelle.evs.client.cdascorecard
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */
@Name("scorecardManager")
@Scope(ScopeType.PAGE)
public class ScorecardManager implements Serializable {
    private static final long serialVersionUID = 1;

    private static Logger log = LoggerFactory.getLogger(ScorecardManager.class);

    private CDAValidatedFile validatedFile;
    private String scorecard;
    private boolean jobRunningInBackground;
    private boolean pollScorecard = false;
    private String errorMessage;

    public ScorecardManager() {
    }

    public boolean isScorecardAvailable(AbstractValidatedXMLFile selectedObject) {
        if (validatedFile == null) {
            if (selectedObject instanceof CDAValidatedFile) {
                this.validatedFile = (CDAValidatedFile) selectedObject;
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.WARN, "This is not a CDA file, cannot compute scorecard");
                return false;
            }
        } else if ((ScorecardStatus.AVAILABLE == validatedFile.getScorecardStatus()) && scorecard != null) {
            processScorecard(validatedFile.getScorecardStatus());
        }
        return (ScorecardStatus.AVAILABLE.equals(validatedFile.getScorecardStatus()));
    }

    public void computeScorecard(AbstractValidatedXMLFile selectedObject) {
        if (selectedObject instanceof CDAValidatedFile) {
            this.validatedFile = (CDAValidatedFile) selectedObject;
            processScorecard(validatedFile.getScorecardStatus());
            pollScorecard = true;
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Please wait, scorecard is being " +
                    "computed. The page will be refreshed automatically until the score card is available");
        }
    }

    public void processScorecard(){
        if (validatedFile != null) {
            processScorecard(validatedFile.getScorecardStatus());
        }
    }

    public boolean displayScorecard(){
        return (validatedFile != null && ScorecardStatus.AVAILABLE.equals(validatedFile.getScorecardStatus()));
    }

    private boolean processScorecard(ScorecardStatus status) {
        log.info("scorecard status: " + validatedFile.getScorecardStatus());
        if (status != null) {
            switch (status) {
                case AVAILABLE:
                    setScorecardFromFile();
                    return true;
                case BEING_COMPUTED:
                    ScorecardStatus newStatus = getNewStatus();
                    if (newStatus.equals(ScorecardStatus.BEING_COMPUTED)) {
                        errorMessage = "Scorecard is being computed, please wait";
                        return false;
                    } else {
                        return processScorecard(newStatus);
                    }
                case NOT_AVAILABLE:
                    errorMessage = "No scorecard is available for this document";
                    return false;
                case FAILED_ON_SERVER_SIDE:
                    errorMessage = "An error occurred while computing the scorecard";
                    pollScorecard = false;
                    return false;
                case FAILED_ON_CLIENT_SIDE:
                    errorMessage = "An error occurred while storing the scorecard";
                    pollScorecard = false;
                    return false;
                case COMPUTE_AGAIN:
                case NEVER_COMPUTED:
                    if (!jobRunningInBackground) {
                        computeScorecardInBackground();
                    }
                    validatedFile.setScorecardStatus(ScorecardStatus.BEING_COMPUTED);
                    return false;
                default:
                    return false;
            }
        } else if (!jobRunningInBackground) {
            computeScorecardInBackground();
            return false;
        } else {
            return false;
        }
    }

    private ScorecardStatus getNewStatus() {
        ScorecardStatus oldStatus = validatedFile.getScorecardStatus();
        ScorecardStatus currentStatus = ScorecardGetterJob.getStatusForScorecard(validatedFile.getOid());
        if ( null != currentStatus  && ! (currentStatus == oldStatus)) {
            validatedFile.setScorecardStatus(currentStatus);
            validatedFile.addOrUpdate(CDAValidatedFile.class);
        } else {
            currentStatus = oldStatus;
        }
        return currentStatus;
    }


    private void setScorecardFromFile() {
        String scorecardPath = validatedFile.getScorecardPath();
        if (scorecardPath != null && !scorecardPath.isEmpty()) {
            File scorecardFile = new File(scorecardPath);
            if (scorecardFile.exists()) {
                scorecard = Util.getFileContentToString(scorecardPath);
            }
        }
    }

    public String getTransformedScorecard() {
        if (scorecard == null && displayScorecard()){
            setScorecardFromFile();
        }
        String xsl4scorecard = PreferenceService.getString("cda_scorecard_xsl_path");
        String htmlScorecard = Util.transformXMLStringToHTML(scorecard, xsl4scorecard);
        pollScorecard = false;
        return htmlScorecard;
    }

    private void computeScorecardInBackground() {
        jobRunningInBackground = true;
        final ScorecardGetterJob job = new ScorecardGetterJob();
        job.getScorecard(validatedFile);
    }

    public boolean isPollScorecard() {
        return pollScorecard;
    }


    public void computeAgain() {
        if (ScorecardStatus.AVAILABLE == validatedFile.getScorecardStatus()) {
            File scorecardFile = new File(validatedFile.getScorecardPath());
            if (scorecardFile.exists()) {
                scorecardFile.delete();
            }
        }
        validatedFile.setScorecardStatus(ScorecardStatus.COMPUTE_AGAIN);
        validatedFile = validatedFile.addOrUpdate(CDAValidatedFile.class);
        pollScorecard = true;
        jobRunningInBackground = false;
        processScorecard(validatedFile.getScorecardStatus());
    }

    public void downloadScorecard() {
        ReportExporterManager.exportToFile("application/xml",
                scorecard.getBytes(Charset.forName("UTF-8")), "scorecard.xml");
    }

    public boolean isScorecardInError() {
        return (validatedFile != null && (ScorecardStatus.FAILED_ON_SERVER_SIDE == validatedFile.getScorecardStatus()));
    }

    public CDAValidatedFile getValidatedFile() {
        return validatedFile;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getScorecard() {
        return scorecard;
    }

}
