/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.logs;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.action.AbstractLogBrowser;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.xml.model.ValidatedAssertion;
import net.ihe.gazelle.evs.client.xml.model.ValidatedAssertionQuery;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("validatedAssertionSearch")
@Scope(ScopeType.PAGE)
public class ValidatedAssertionSearch extends AbstractLogBrowser<ValidatedAssertion> {

    /**
     *
     */
    private static final long serialVersionUID = -8875906135127456062L;

    @Override
    protected ValidatedAssertion getObjectByOID(final String oid, final String privacyKey) {
        return ValidatedObject.getObjectByOID(ValidatedAssertion.class, oid, privacyKey);
    }

    @Override
    protected Class<ValidatedAssertion> getEntityClass() {
        return ValidatedAssertion.class;
    }

    @Override
    protected HQLCriterionsForFilter<ValidatedAssertion> getCriterions() {
        final ValidatedAssertionQuery query = new ValidatedAssertionQuery();
        final HQLCriterionsForFilter<ValidatedAssertion> result = query.getHQLCriterionsForFilter();
        result.addPath("standard", query.standard().label());
        result.addPath("validationStatus", query.validationStatus());
        result.addPath("privateUser", query.privateUser());
        result.addPath("institutionKeyword", query.institutionKeyword());
        result.addPath("mbvalidator", query.mbvalidator());
        result.addPath("schematron", query.schematron());
        result.addPath("validationDate", query.validationDate());
        return result;
    }

    @Override
    public FilterDataModel<ValidatedAssertion> getDataModel() {
        return new FilterDataModel<ValidatedAssertion>(ValidatedAssertionSearch.this.getFilter()) {
            @Override
            protected Object getId(final ValidatedAssertion t) {
                return t.getId();
            }
        };
    }
}
