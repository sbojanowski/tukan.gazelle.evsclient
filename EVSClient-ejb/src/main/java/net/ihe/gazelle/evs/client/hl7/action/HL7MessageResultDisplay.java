/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.hl7.action;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.parser.XMLParser;
import ca.uhn.hl7v2.validation.impl.NoValidation;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.common.tree.XmlTreeDataBuilder;
import net.ihe.gazelle.evs.client.common.action.AbstractResultDisplayManager;
import net.ihe.gazelle.evs.client.common.action.GetValidationInfo;
import net.ihe.gazelle.evs.client.hl7.model.HL7ValidatedMessage;
import net.ihe.gazelle.evs.client.hl7.model.PackageNameForProfileOID;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.evs.client.util.ValidatorType;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.international.LocaleSelector;
import org.richfaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Name("hl7ResultDisplay")
@Scope(ScopeType.PAGE)
public class HL7MessageResultDisplay extends AbstractResultDisplayManager<HL7ValidatedMessage> implements Serializable {

    private static final long serialVersionUID = 7920472368366520753L;

    private static final Logger LOGGER = LoggerFactory.getLogger(HL7MessageResultDisplay.class);

    private String messageContent;
    private transient TreeNode treeMessageContent;

    public HL7MessageResultDisplay() {
    }

    @Override
    public void init(final String objectOid, final String privacyKey) {
        super.init(objectOid, privacyKey);
        this.trunkHL7MessageContentIfTooBig();
    }

    @Override
    protected Class<HL7ValidatedMessage> getEntityClass() {
        return HL7ValidatedMessage.class;
    }

    @Override
    protected String getValidatorUrl() {
        return ValidatorType.HL7V2.getView();
    }

    @Override
    public void downloadValidatedObject() {
        final String content = this.selectedObject.getValidatedMessage();
        final String fileName = this.selectedObject.getDescription();
        ReportExporterManager.exportToFile(GetValidationInfo.TEXT_PLAIN, content, fileName);
    }

    @Override
    public String performAnotherValidation() {
        StringBuilder url = new StringBuilder(ValidatorType.HL7V2.getView());
        if (selectedObject.getStandard() != null && selectedObject.getStandard().getExtension() != null){
            url.append("?extension=");
            url.append(selectedObject.getStandard().getExtension());
        }
        return url.toString();
    }

    public String getDetailedResult() {
        return Util.transformXMLStringToHTML(this.selectedObject.getDetailedResult(), this.selectedObject.getService().getXslLocation(), LocaleSelector
                .instance().getLanguage());
    }

    public String permanentLinkToProfile() {
        String url = ApplicationPreferenceManager.getStringValue("gazelle_hl7v2_validator_url");
        if (url != null) {
            url = url.concat("/viewProfile.seam?oid=");
            url = url.concat(this.selectedObject.getProfileOid());
        }
        return url;
    }

    /**
     * message display
     */
    public String getHighlightedER7() {
        Integer b = Integer.valueOf(0);
        final List<String> listSegment = new ArrayList<>();

        String highlightedMessage = this.messageContent;

        Integer a = Integer.valueOf(highlightedMessage.indexOf('|'));
        highlightedMessage = highlightedMessage.replace("<", "&lt;");
        highlightedMessage = highlightedMessage.replace(">", "&gt;");

        if (a < highlightedMessage.length() && a != -1) {
            String subString = highlightedMessage.substring(b, a);
            highlightedMessage = highlightedMessage
                    .replaceFirst(subString, "<span class=\"hl-reserved\">" + subString + "</span>");

            while (a < highlightedMessage.length() && a > 0 && b >= 0) {
                a = Integer.valueOf(highlightedMessage.indexOf('\n', a + 1));

                if (a + 1 < highlightedMessage.length() && a != -1) {
                    b = Integer.valueOf(highlightedMessage.indexOf('|', a + 1));

                    if (b - a == 4) {
                        subString = highlightedMessage.substring(a, b);

                        if (!listSegment.contains(subString)) {
                            listSegment.add(subString);
                            highlightedMessage = highlightedMessage.replaceAll(subString,
                                    "\n<span class=\"hl-reserved\">" + subString.substring(1, 4) + "</span>");
                        }
                    }
                } else {
                    a = Integer.valueOf(-1);
                }
            }

        }
        highlightedMessage = highlightedMessage.replace("&", "<span class=\"hl-special\">&amp;</span>");
        highlightedMessage = highlightedMessage.replace("#", "<span class=\"hl-special\">&#35;</span>");
        highlightedMessage = highlightedMessage.replace("~", "<span class=\"hl-special\">&#126;</span>");
        highlightedMessage = highlightedMessage.replace("|", "<span class=\"hl-special\">&#124;</span>");
        highlightedMessage = highlightedMessage.replace("^", "<span class=\"hl-brackets\">&#94;</span>");
        highlightedMessage = highlightedMessage.replace("\n", "<span class=\"hl-crlf\">[CR]</span><br/>");

        return highlightedMessage;
    }

    public String getER7() {
        String er7Message = this.messageContent;

        er7Message = er7Message.replace("<", "&lt;");
        er7Message = er7Message.replace(">", "&gt;");
        er7Message = er7Message.replace("&", "&amp;");
        er7Message = er7Message.replace("#", "&#35;");
        er7Message = er7Message.replace("~", "&#126;");
        er7Message = er7Message.replace("|", "&#124;");
        er7Message = er7Message.replace("^", "&#94;");
        return er7Message.replace("\n", "<br/>");
        // return
        // MessageDisplay.getMessageContentAsER7(selectedObject.getValidatedMessage());
    }

    public String getXML() {
        String xmlMessageContent;

        final String processedMessage = this.messageContent.replaceAll("\n", "\r");

        final PipeParser pipeParser = new PipeParser();
        pipeParser.setValidationContext(new NoValidation());
        final XMLParser xmlParser = new DefaultXMLParser();

        try {
            final Message message;
            final String packageName = PackageNameForProfileOID.findPackageNameWithProfileOID(this.selectedObject.getProfileOid());
            if (packageName != null) {
                message = pipeParser.parseForSpecificPackage(processedMessage, packageName);
            } else {
                message = pipeParser.parse(processedMessage);
            }

            xmlMessageContent = xmlParser.encode(message);

            this.treeMessageContent = XmlTreeDataBuilder
                    .build(new InputSource(new ByteArrayInputStream(xmlMessageContent.getBytes(StandardCharsets.UTF_8))));
            return xmlMessageContent;

        } catch (final IOException |HL7Exception|SAXException e) {
            HL7MessageResultDisplay.LOGGER.error(e.getMessage());
            return null;
        }

    }

    public TreeNode getTree() {
        if (this.treeMessageContent == null) {
            this.getXML();
        }
        return this.treeMessageContent;

    }


    private void trunkHL7MessageContentIfTooBig() {
        if (this.selectedObject != null && this.selectedObject.getValidatedMessage() != null) {
            final String stringToTrunk = this.selectedObject.getValidatedMessage();

            int a = stringToTrunk.indexOf('\n');
            int b = 0;

            // int numberOfSegments = getNumberOfSegmentsToDisplay();
            final int numberOfSegmentsToDisplay = 50;

            while (b < numberOfSegmentsToDisplay && b > -1 && a != -1) {
                a = stringToTrunk.indexOf('\n', a + 1);
                b++;
            }

            // In this case, it is possible that the messages don't have any
            // carriage return.
            if (b < 2 && a == -1) {
                if (stringToTrunk.length() > 30 * numberOfSegmentsToDisplay) {
                    this.messageContent = stringToTrunk.substring(0, 30 * numberOfSegmentsToDisplay);
                } else {
                    this.messageContent = stringToTrunk;
                }
            }

            if (a != -1) {
                this.messageContent = stringToTrunk.substring(0, a);
            } else {
                this.messageContent = stringToTrunk;
            }
        }
    }
}
