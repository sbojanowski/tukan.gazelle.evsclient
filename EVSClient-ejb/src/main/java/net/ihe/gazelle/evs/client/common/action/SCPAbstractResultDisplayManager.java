/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.evs.client.dicom.model.DicomSCPScreenerResult;
import net.ihe.gazelle.evs.client.util.Util;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.util.Map;

public abstract class SCPAbstractResultDisplayManager<T extends DicomSCPScreenerResult> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SCPAbstractResultDisplayManager.class);

    private T selectedObject;

    public void downloadResult() {
        ReportExporterManager.exportToFile(GetValidationInfo.TEXT_PLAIN, this.selectedObject.getResult(), "SCPResult.txt");
    }

    public void initFromUrl() {
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String objectOid = null;
        if (urlParams != null && !urlParams.isEmpty()) {
            if (urlParams.containsKey("oid")) {
                objectOid = urlParams.get("oid");
            }
            this.init(objectOid, urlParams.get("privacyKey"));
        }
    }

    public void init(final String objectOid, final String privacyKey) {
        this.selectedObject = (T) DicomSCPScreenerResult.getObjectByOID(objectOid, privacyKey);
    }

    public String revalidate() {
        return this.getValidatorUrl().concat("?oid=").concat(this.selectedObject.getOid());
    }

    protected abstract Class<T> getEntityClass();

    protected abstract String getValidatorUrl();

    public T getSelectedObject() {
        return this.selectedObject;
    }

    public void makeResultPublic() {
        this.selectedObject.setIsPublic(Boolean.TRUE);
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        this.selectedObject = entityManager.merge(this.selectedObject);
        entityManager.flush();
    }

    public void makeResultPrivate() {
        this.selectedObject.setIsPublic(Boolean.FALSE);
        this.selectedObject.setPrivacyKey(null);
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        this.selectedObject = entityManager.merge(this.selectedObject);
        entityManager.flush();
    }

    public void shareResult() {
        this.selectedObject.generatePrivacyKey();
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        this.selectedObject = entityManager.merge(this.selectedObject);
        entityManager.flush();
    }

    public String resultAsHtml() {
        final String xslLocation = ApplicationPreferenceManager.getStringValue("dicom_scp_screener_xsl");
        if (xslLocation == null || xslLocation.isEmpty()) {
            SCPAbstractResultDisplayManager.LOGGER.error("The repository for xsl file (dicom_scp_screener_xsl) has not been set");
            FacesMessages.instance().add(Severity.ERROR, "The repository for xsl file (dicom_scp_screener_xsl) has not been set");
            return null;
        } else {
            SCPAbstractResultDisplayManager.LOGGER.info("xslLocation : {}", xslLocation);
            final String appName = ApplicationPreferenceManager.getStringValue("application_url_basename");
            if (appName == null || appName.isEmpty()) {
                SCPAbstractResultDisplayManager.LOGGER.error("The repository for the application base name (application_url_basename) has not been set");
                FacesMessages.instance().add(Severity.ERROR, "The repository for the application base name (application_url_basename) has not been set");
                return null;
            } else {
                SCPAbstractResultDisplayManager.LOGGER.info("appName : {}", appName);
                return Util.resultTransformation(this.selectedObject.getResult(), xslLocation, appName);
            }
        }
    }

    public void downloadResultAsXml() {
        String content;
        String fileName;
        fileName = "DicomSCPScreenerResult.xml";
        content = this.selectedObject.getResult();

        if (!"".equals(content)) {
            ReportExporterManager.exportToFile("text/xml", content, fileName);
            SCPAbstractResultDisplayManager.LOGGER.info("XML download");
        } else {
            SCPAbstractResultDisplayManager.LOGGER.error("No result to download");
            FacesMessages.instance().add("No result to download");
        }
    }

}
