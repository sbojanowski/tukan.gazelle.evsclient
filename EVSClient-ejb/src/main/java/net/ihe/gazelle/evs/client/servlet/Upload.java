/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.servlet;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//TODO This class need to be fixed. Indeed there map of uploaded file is a memory leak (EVSCLT-662)
public class Upload extends HttpServlet {

    private static final long serialVersionUID = 1434693061336395803L;
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpServlet.class);

    private static long fileSize;//octets

    public static long getFileSize() {
        return Upload.fileSize;
    }

    private void  setFileSize(long fs) {
        fileSize = fs;
    }

    private static final Map<String, String> UPLOADED_FILES = Collections.synchronizedMap(new ConcurrentHashMap<String, String>());

    public static String putFile(final String filePath) {
        final String key = Integer.toHexString(filePath.hashCode());
        Upload.UPLOADED_FILES.put(key, filePath);
        return key;
    }

    public static String getFile(final String key) {
        String filePath = Upload.UPLOADED_FILES.get(key);
        //FIXME : This is probably not the best place where to do it : EVSCLT-662
        deleteFile(key);
        return filePath;
    }

    private static String deleteFile(final String key) {
        return Upload.UPLOADED_FILES.remove(key);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        String result = "";
        InputStream stream = null;
        try {
            Writer writer = resp.getWriter();
            if (ServletFileUpload.isMultipartContent(req)) {
                final ServletFileUpload upload = new ServletFileUpload();
                final FileItemIterator iter = upload.getItemIterator(req);
                while (iter.hasNext()) {
                    FileItemStream item = iter.next();
                    stream = item.openStream();
                    String messageType = item.getName();
                    final File tmpFile = File.createTempFile("evs", '.' + messageType);
                    final OutputStream outputStream = new FileOutputStream(tmpFile);
                    Streams.copy(stream, outputStream, true);
                    result = Upload.putFile(tmpFile.getAbsolutePath());
                    this.setFileSize(tmpFile.length());
                }
            }
            writer.write(result);
        } catch (final FileUploadException|IOException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (final IOException e) {
                    LOGGER.error("not able to close the stream", e.getMessage());
                }
            }
        }
    }

}
