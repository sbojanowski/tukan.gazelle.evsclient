/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.validator;

import net.ihe.gazelle.evs.client.xml.model.ValidatedAuditMessage;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("atnaValidator")
@Scope(ScopeType.PAGE)
public class ATNAValidator extends AbstractXMLValidator<ValidatedAuditMessage> {

    /**
     *
     */
    private static final long serialVersionUID = -7064478009817263361L;

    @Override
    public Class<ValidatedAuditMessage> getEntityClass() {
        return ValidatedAuditMessage.class;
    }

    @Override
    protected ValidatedAuditMessage newInstance() {
        return ValidatedAuditMessage.createNewValidatedAuditMessage();
    }

}
