/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file.xml;

import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.analyzer.utils.XmlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XACML extends Analyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(XACML.class);

    @Override
    public Logger getLog() {
        return XACML.LOGGER;
    }


    @Override
    public String getType() {
        return "XACML";
    }

    public static boolean containsXACML(final File file, final MessageContentAnalyzer mca) {
        final String fileContent = AnalyzerFileUtils.convertFileToString(file);
        return XACML.containsXACML(fileContent, mca);
    }

    public static boolean containsXACML(final String fileContent, final MessageContentAnalyzer mca) {
        final ArrayList<String> acceptedXACMLTags = new ArrayList<>();
        acceptedXACMLTags.add("Response");
        acceptedXACMLTags.add("XACMLAuthzDecisionQuery");
        if (fileContent != null && !fileContent.isEmpty()) {
            for (int i = 0; i < acceptedXACMLTags.size(); i++) {
                final String goodTag = acceptedXACMLTags.get(i);
                if (fileContent.contains(goodTag)) {
                    XACML.LOGGER.info(goodTag);
                    // Verify if it's just one line
                    String xcamlReg = "<([^<]+:)?" + goodTag + "([\\r|\\n| |>])+.+?>";
                    XACML.LOGGER.info(xcamlReg);

                    Pattern regex = Pattern.compile(xcamlReg, Pattern.DOTALL);
                    Matcher regexMatcher = regex.matcher(fileContent);
                    if (regexMatcher.find()) {
                        XACML.LOGGER.info(regexMatcher.group());
                        if ("/".equals(fileContent.substring(regexMatcher.end() - 2, regexMatcher.end() - 1))) {
                            if (XmlUtils.testValidSchemes(regexMatcher.group(1), regexMatcher.group(1))) {
                                xcamlReg = "<([^<]+)?" + goodTag + "([\\r|\\n| |>])+.+?(/>)";
                                XACML.LOGGER.info(xcamlReg);

                                regex = Pattern.compile(xcamlReg, Pattern.DOTALL);
                                regexMatcher = regex.matcher(fileContent);
                                if (regexMatcher.find()) {
                                    XACML.LOGGER.info(regexMatcher.group());
                                    mca.setOffsetStart(regexMatcher.start());
                                    mca.setOffsetEnd(regexMatcher.end());
                                    return true;
                                }
                            }
                        } else {
                            if (XmlUtils.testValidSchemes(regexMatcher.group(1), regexMatcher.group(1))) {
                                String scheme = "";
                                if (regexMatcher.group(1) != null) {
                                    scheme = regexMatcher.group(1);
                                }
                                xcamlReg = '<' + scheme + goodTag + "([\\r|\\n| |>])+.+?(" + scheme + goodTag + ">)";
                                XACML.LOGGER.info(xcamlReg);

                                regex = Pattern.compile(xcamlReg, Pattern.DOTALL);
                                regexMatcher = regex.matcher(fileContent);
                                if (regexMatcher.find()) {
                                    XACML.LOGGER.info(regexMatcher.group());
                                    mca.setOffsetStart(regexMatcher.start());
                                    mca.setOffsetEnd(regexMatcher.end());
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
}
