/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.logs;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.action.AbstractLogBrowser;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.xml.model.ValidatedWADO;
import net.ihe.gazelle.evs.client.xml.model.ValidatedWADOQuery;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("wadoValidatedMessageSearch")
@Scope(ScopeType.PAGE)
public class WadoValidatedMessageSearch extends AbstractLogBrowser<ValidatedWADO> {

    /**
     *
     */
    private static final long serialVersionUID = -3252575715670580720L;

    @Override
    protected ValidatedWADO getObjectByOID(final String oid, final String privacyKey) {
        return ValidatedObject.getObjectByOID(ValidatedWADO.class, oid, privacyKey);
    }

    @Override
    protected Class<ValidatedWADO> getEntityClass() {
        return ValidatedWADO.class;
    }

    @Override
    protected HQLCriterionsForFilter<ValidatedWADO> getCriterions() {
        final ValidatedWADOQuery validatedWadoQuery = new ValidatedWADOQuery();
        final HQLCriterionsForFilter<ValidatedWADO> result = validatedWadoQuery.getHQLCriterionsForFilter();
        result.addPath("standard", validatedWadoQuery.standard().label());
        result.addPath("validationStatus", validatedWadoQuery.validationStatus());
        result.addPath("privateUser", validatedWadoQuery.privateUser());
        result.addPath("institutionKeyword", validatedWadoQuery.institutionKeyword());
        result.addPath("mbvalidator", validatedWadoQuery.mbvalidator());
        result.addPath("validationDate", validatedWadoQuery.validationDate());
        return result;
    }

    @Override
    public FilterDataModel<ValidatedWADO> getDataModel() {
        return new FilterDataModel<ValidatedWADO>(WadoValidatedMessageSearch.this.getFilter()) {
            @Override
            protected Object getId(final ValidatedWADO t) {
                return t.getId();
            }
        };
    }
}
