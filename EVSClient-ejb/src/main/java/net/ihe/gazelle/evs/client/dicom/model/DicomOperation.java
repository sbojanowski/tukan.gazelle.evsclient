/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.dicom.model;

public enum DicomOperation {

    DICOM3TOOLS_VALIDATION("Validation (dciodvfy) of DICOM file", "validation of DICOM file", DicomTools.DICOM3TOOLS),
    DICOM3TOOLS_DUMP("Dump (dcdump)  of DICOM file", "validation of DICOM file", DicomTools.DICOM3TOOLS),

    PIXELMED_VALIDATION("validation of DICOM dose file", "validation of DICOM dose file", DicomTools.PIXELMED),
    PIXELMED_DUMP("dump of DICOM file", "dump of DICOM file", DicomTools.PIXELMED),
    PIXELMED_DUMP2XML("dump of DICOM file to XML", "dump of DICOM file to XML", DicomTools.PIXELMED),
    DCM4CHE_DUMP("dump of DICOM file", "dump of DICOM file", DicomTools.DCM4CHE),
    DCM4CHE_DUMP2XML("dump of DICOM file to XML", "dump of DICOM file to XML", DicomTools.DCM4CHE),
    DCMCHECK_VALIDATION("validation of DICOM file", "validation of DICOM file", DicomTools.DCMCHECK),
    DCCHECK_VALIDATION("validation of DICOM file", "validation of DICOM file", DicomTools.DCCHECK),
    DVTK_VALIDATION("validation of DICOM file", "validation of DICOM file", DicomTools.DVTK),
    DICOMWEB_VALIDATION("validation of DICOM Web URL", "validation of DICOM Web URL", DicomTools.BCOM_DICOM_WEB);


    DicomOperation(final String label, final String description, final DicomTools tool) {
        this.label = label;
        this.description = description;
        this.tool = tool;
    }

    private String label;

    private String description;

    private String version;

    public String getVersion() {
        return this.version;
    }

    public  void setVersion(final String version) {
        this.version = version;
    }

    private DicomTools tool;

    public String getLabel() {
        return this.label;
    }

    public DicomTools getTool() {
        return this.tool;
    }

}
