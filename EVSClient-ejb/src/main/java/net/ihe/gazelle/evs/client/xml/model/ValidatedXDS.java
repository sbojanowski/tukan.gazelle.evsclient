/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;

@Entity
@Name("validatedXDS")
@Table(name = "validated_xds", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "validated_xds_sequence", sequenceName = "validated_xds_id_seq", allocationSize = 1)
public class ValidatedXDS extends AbstractValidatedXMLFile {

    private static final long serialVersionUID = -1443811873360866107L;

    private static final String FILE_PREFIX = "validatedXDS_";
    private static final String FILE_EXTENSION = ".xml";

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidatedXDS.class);

    @Id
    @NotNull
    @GeneratedValue(generator = "validated_xds_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "xds_path")
    private String xdsPath;

    public ValidatedXDS() {
    }

    public ValidatedXDS(final ValidatedXDS inXDSObject) {
        this.setXdsPath(inXDSObject.getXdsPath());
        this.setDescription(inXDSObject.getDescription());
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public final void setXdsPath(final String xdsPath) {
        this.xdsPath = xdsPath;
    }

    public String getXdsPath() {
        return this.xdsPath;
    }

    public static ValidatedXDS store(ValidatedXDS validatedXds) {
        if (validatedXds != null) {
            if (validatedXds.getOid() == null || validatedXds.getOid().length() == 0) {
                validatedXds.setOid(OIDGenerator.getNewOid());
            }
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            validatedXds = em.merge(validatedXds);
            em.flush();
            return validatedXds;
        }
        return null;
    }

    public static String getAbsolutePath(final ValidatedXDS xdwObject) {
        if (xdwObject == null || xdwObject.getId() == 0) {
            ValidatedXDS.LOGGER.error("cannot get absolute path, the object is null or not persisted.");
            return null;
        } else {
            final String xdsRepository = ApplicationPreferenceManager.getStringValue("xds_repository");
            if (xdsRepository == null || xdsRepository.isEmpty()) {
                ValidatedXDS.LOGGER.error("The repository for XDS files (xds_repository) has not been set");
                return null;
            } else {
                final String res = xdsRepository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ValidatedXDS.FILE_PREFIX
                        .concat(xdwObject.getId().toString())
                        .concat(ValidatedXDS.FILE_EXTENSION);
                return res;
            }
        }
    }

    public static ValidatedXDS getObjectByOid(final String oid) {
        final HQLQueryBuilder<ValidatedXDS> query = new HQLQueryBuilder<>(
                (EntityManager) Component.getInstance("entityManager"), ValidatedXDS.class);
        query.addEq("oid", oid);
        return query.getUniqueResult();
    }

    @Override
    public String getFilePath() {
        return this.xdsPath;
    }

    @Override
    public String getSchematron() {
        return null;
    }

    @Override
    public void setFilePath(final String filePath) {
        this.xdsPath = filePath;
    }

    @Override
    public void setSchematron(final String schematron) {
        return;
    }

    /**
     */
    public static ValidatedXDS createNewValidatedXDS() {
        ValidatedXDS xdsObject = new ValidatedXDS();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        xdsObject = em.merge(xdsObject);
        em.flush();
        if (xdsObject == null || xdsObject.getId() == 0) {
            ValidatedXDS.LOGGER.error("cannot create XDS");
            return null;
        } else {
            final String repository = ApplicationPreferenceManager.getStringValue("xds_repository");
            xdsObject.setXdsPath(
                    repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ValidatedXDS.FILE_PREFIX
                            .concat(xdsObject.getId().toString()).concat(
                            ValidatedXDS.FILE_EXTENSION));

            return xdsObject;
        }
    }

    /**
     *
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends AbstractValidatedXMLFile> T addOrUpdate(final Class<T> clazz) {
        if (this.getFilePath() == null || this.getFilePath().isEmpty()) {
            ValidatedXDS.LOGGER.error("The file path is missing, cannot add this object !");
            return (T) this;
        } else {
            final File uploadedFile = new File(this.getFilePath());
            if (!uploadedFile.exists()) {
                ValidatedXDS.LOGGER.error("The given file does not exist: {}", this.getFilePath());
                return (T) this;
            }
            if (this.getOid() == null || this.getOid().length() == 0) {
                this.setOid(OIDGenerator.getNewOid());
            }

            final ValidatedXDS validatedFile = ValidatedXDS.store(this);

            //Update the cache with the newest values
            GazelleValidationCacheManager
                    .refreshGazelleCacheWebService(validatedFile.getExternalId(), validatedFile.getToolOid(),
                            validatedFile.getOid(), "off");

            return (T) validatedFile;
        }
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/detailedResult.seam?type=XDS").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

}
