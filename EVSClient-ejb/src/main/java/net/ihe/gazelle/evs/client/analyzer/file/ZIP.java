/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file;

import net.ihe.gazelle.common.util.ZipUtility;
import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.common.action.FileToValidate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class ZIP implements IAnalyzableContent {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZIP.class);

    @Override
    public boolean analyze(final File f, final FileToValidate parent, final FileToValidate ftv, final MessageContentAnalyzer mca,
            final int startOffset, final int endOffset) {
        final File file;
        if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
            file = mca.fileIntoOffsets(f, startOffset, endOffset);
            mca.setSaveEndOffset(endOffset);
            this.analyze(file, parent, ftv, mca, 0, 0);
            AnalyzerFileUtils.deleteTmpFile(file);
            mca.setSaveEndOffset(0);
        } else {
            ZIP.LOGGER.info("Extract files from zip");

            ftv.setStartOffset(startOffset + mca.getSaveStartOffset());
            if (mca.getSaveEndOffset() != 0) {
                ftv.setEndOffset(mca.getSaveEndOffset());
            } else {
                ftv.setEndOffset(endOffset + mca.getSaveStartOffset());
            }
            ftv.setDocType("ZIP");
            final File unzipFolder = new File ("/tmp/");
            try {
                ZipUtility.unzip(f, unzipFolder);
            } catch (final IOException e) {
                LOGGER.error(e.getMessage(),e);
            }


        }
        return false;
    }
}
