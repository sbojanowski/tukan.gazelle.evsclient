/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.analyzer.file.*;
import net.ihe.gazelle.evs.client.analyzer.file.xml.SAML;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.analyzer.utils.NamespaceAnalyze;
import net.ihe.gazelle.evs.client.common.action.FileToValidate;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.common.model.ObjectForValidatorDetector;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.evs.client.util.ValidationErrorHandler;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.tika.Tika;
import org.apache.tika.detect.MagicDetector;
import org.apache.tika.io.IOUtils;
import org.apache.tika.mime.MediaType;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageContentAnalyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageContentAnalyzer.class);

    private static final String TMP_FILE_SUFFIX = ".tmp";
    public static final String DICOM = "DICOM";


    private int saveStartOffset;
    private int saveEndOffset;
    private int headerOffsetStart;
    private int headerOffsetEnd;
    private int offsetStart;
    private int offsetEnd;
    private int bodyOffsetStart;
    private int bodyOffsetEnd;
    private int base64OffsetStart;
    private int base64OffsetEnd;
    private int httpHeaderOffsetStart;
    private int httpHeaderOffsetEnd;
    private int httpBodyOffsetStart;
    private int httpBodyOffsetEnd;

    private String header;
    private String body;
    private String headerValue, bodyValue;
    private String httpHeader;
    private String httpBody;

    private Map<String, String> listOfNamespaces = new HashMap<>();
    private boolean wellFormedFirstTime = false;
    private Integer missingTagInducedOffsetParent = 0 ;
    private Integer missingTagInducedOffsetChild = 0 ;

    public MessageContentAnalyzer(final ObjectForValidatorDetector obj) {
        this.analyzedObject = obj;
    }


    private ObjectForValidatorDetector analyzedObject;

    public MessageContentAnalyzer() {
        super();
    }

    public ObjectForValidatorDetector getAnalyzedObject() {
        return this.analyzedObject;
    }

    public void setAnalyzedObject(final ObjectForValidatorDetector analyzedObject) {
        this.analyzedObject = analyzedObject;
    }

    public String getHeader() {
        return this.header;
    }

    public void setHeader(final String header) {
        this.header = header;
    }

    public String getBody() {
        return this.body;
    }

    public void setBody(final String body) {
        this.body = body;
    }

    public String getHeaderValue() {
        return this.headerValue;
    }

    public void setHeaderValue(final String headerValue) {
        this.headerValue = headerValue;
    }

    public String getBodyValue() {
        return this.bodyValue;
    }

    public void setBodyValue(final String bodyValue) {
        this.bodyValue = bodyValue;
    }

    public int getSaveStartOffset() {
        return this.saveStartOffset;
    }

    public void setSaveStartOffset(final int saveStartOffset) {
        this.saveStartOffset = saveStartOffset;
    }

    public int getHeaderOffsetStart() {
        return this.headerOffsetStart;
    }

    public void setHeaderOffsetStart(final int headerOffsetStart) {
        this.headerOffsetStart = headerOffsetStart;
    }

    public int getHeaderOffsetEnd() {
        return this.headerOffsetEnd;
    }

    public void setHeaderOffsetEnd(final int headerOffsetEnd) {
        this.headerOffsetEnd = headerOffsetEnd;
    }

    public int getOffsetStart() {
        return this.offsetStart;
    }

    public void setOffsetStart(final int offsetStart) {
        this.offsetStart = offsetStart;
    }

    public int getOffsetEnd() {
        return this.offsetEnd;
    }

    public void setOffsetEnd(final int offsetEnd) {
        this.offsetEnd = offsetEnd;
    }

    public int getBodyOffsetStart() {
        return this.bodyOffsetStart;
    }

    public void setBodyOffsetStart(final int bodyOffsetStart) {
        this.bodyOffsetStart = bodyOffsetStart;
    }

    public int getBodyOffsetEnd() {
        return this.bodyOffsetEnd;
    }

    public void setBodyOffsetEnd(final int bodyOffsetEnd) {
        this.bodyOffsetEnd = bodyOffsetEnd;
    }

    public int getBase64OffsetStart() {
        return this.base64OffsetStart;
    }

    public void setBase64OffsetStart(final int base64OffsetStart) {
        this.base64OffsetStart = base64OffsetStart;
    }

    public int getBase64OffsetEnd() {
        return this.base64OffsetEnd;
    }

    public void setBase64OffsetEnd(final int base64OffsetEnd) {
        this.base64OffsetEnd = base64OffsetEnd;
    }

    public boolean isWellFormedFirstTime() {
        return this.wellFormedFirstTime;
    }

    public void setWellFormedFirstTime(final boolean wellFormedFirstTime) {
        this.wellFormedFirstTime = wellFormedFirstTime;
    }

    public Map<String, String> getListOfNamespaces() {
        return this.listOfNamespaces;
    }

    public void setListOfNamespaces(final Map<String, String> listOfNamespaces) {
        this.listOfNamespaces = listOfNamespaces;
    }

    public String getHttpHeader() {
        return this.httpHeader;
    }

    public void setHttpHeader(final String httpHeader) {
        this.httpHeader = httpHeader;
    }

    public String getHttpBody() {
        return this.httpBody;
    }

    public void setHttpBody(final String httpBody) {
        this.httpBody = httpBody;
    }

    public int getHttpHeaderOffsetStart() {
        return this.httpHeaderOffsetStart;
    }

    public void setHttpHeaderOffsetStart(final int httpHeaderOffsetStart) {
        this.httpHeaderOffsetStart = httpHeaderOffsetStart;
    }

    public int getHttpHeaderOffsetEnd() {
        return this.httpHeaderOffsetEnd;
    }

    public void setHttpHeaderOffsetEnd(final int httpHeaderOffsetEnd) {
        this.httpHeaderOffsetEnd = httpHeaderOffsetEnd;
    }

    public int getHttpBodyOffsetStart() {
        return this.httpBodyOffsetStart;
    }

    public void setHttpBodyOffsetStart(final int httpBodyOffsetStart) {
        this.httpBodyOffsetStart = httpBodyOffsetStart;
    }

    public int getHttpBodyOffsetEnd() {
        return this.httpBodyOffsetEnd;
    }

    public void setHttpBodyOffsetEnd(final int httpBodyOffsetEnd) {
        this.httpBodyOffsetEnd = httpBodyOffsetEnd;
    }

    public int getSaveEndOffset() {
        return this.saveEndOffset;
    }

    public void setSaveEndOffset(final int saveEndtOffset) {
        this.saveEndOffset = saveEndtOffset;
    }


/*
This method adds recursively the namespaces in the selected part.
 */
    public File addMissingNamespaces(final File entryFile, final Map<String, String> namespaces) throws IOException {

        final NamespaceAnalyze na = new NamespaceAnalyze();

        // Get the list of namespaces for a file
        try {
            final Map<String, String> mapsPrefixUri = na.getListOfPrefixAndURIForNamespaces(entryFile);

            if (!mapsPrefixUri.isEmpty()) {
                this.setListOfNamespaces(mapsPrefixUri);
            }
            return entryFile;
        } catch (final SAXException e) {

            final String errMess = e.getMessage();
            final String firstQuoteReg = "(\"([^\"]+)\")";
            final Pattern regex = Pattern.compile(firstQuoteReg, Pattern.DOTALL);
            final Matcher regexMatcher = regex.matcher(errMess);
            String missingNameSpace = null;
            if (regexMatcher.find()) {
                missingNameSpace = regexMatcher.group(2);
                missingNameSpace = "xmlns:" + missingNameSpace;
                LOGGER.info("missingNameSpace : {}", missingNameSpace);
            }
            String missingTag = null;
            if (regexMatcher.find()) {
                missingTag = regexMatcher.group(2);
                LOGGER.info("missingTag : {}", missingTag);
            }
            if (namespaces != null && !namespaces.isEmpty() && !this.xmlIsWellFormed(entryFile, 0, 0)
                    && missingNameSpace != null && missingTag != null) {
                final String nsURI = namespaces.get(missingNameSpace);
                LOGGER.info("{}={}", missingNameSpace, nsURI);

                // We add in the document the missing namespaces. However we need to keep track of the length, so that we
                // know where we are when we need to compute the offsets.
                // Note that the replace below is a "replaceAll" occurence.
                final String path = this.setEditedObjectPathWithRepository();
                if (path != null) {
                    String fts = AnalyzerFileUtils.fileToString(entryFile);
                    String ftsTemp ;
                    missingTag = '<' + missingTag;
                    File f = new File(path);
                    if (fts.contains(missingTag)) {
                        final String newTag = missingTag + ' ' + missingNameSpace + "=\"" + nsURI + '"';
                        ftsTemp = fts.replace(missingTag, newTag);

                        if(getMissingTagInducedOffsetParent() == 0){
                            setMissingTagInducedOffsetParent(ftsTemp.length() - fts.length());
                        }
                        else {
                            setMissingTagInducedOffsetChild(getMissingTagInducedOffsetChild() + ftsTemp.length() - fts.length());
                        }
                        fts = ftsTemp;
                        FileUtils.writeStringToFile(f, fts);
                        f = this.addMissingNamespaces(f, namespaces);
                    }
                    return f;
                } else {
                    LOGGER.error("Analyzed object has no id : {}", e);
                    return entryFile;
                }
            } else {
                LOGGER.info("No missing namespaces");
                return entryFile;
            }
        }
    }

    protected String setEditedObjectPathWithRepository() {
        final String repository = ApplicationPreferenceManager.instance()
                .getStringValue("object_for_validator_detector_repository");
        if (repository == null || repository.isEmpty()) {
            LOGGER.error("Object For Validator Detector repository doesn't exist in preferences");
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Object For Validator Detector repository doesn't exist in preferences");
        }
        if (this.analyzedObject.getId() != null) {
            final String path = repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ObjectForValidatorDetector.OVD_FILE_PREFIX
                    .concat(this.analyzedObject.getId().toString() + "_edited")
                    .concat(ObjectForValidatorDetector.OVD_FILE_SUFFIX);
            this.analyzedObject.setEditedObjectPath(path);
            return path;
        } else {
            return null;
        }
    }

    public String setEditedObjectPathWithRepository(final int offsetStart, final int offsetEnd) {
        final String repository = ApplicationPreferenceManager.instance()
                .getStringValue("object_for_validator_detector_repository");
        if (this.analyzedObject.getId() != null) {
            final String path = repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ObjectForValidatorDetector.OVD_FILE_PREFIX
                    .concat(this.analyzedObject.getId().toString() + '_' + offsetStart + '_' + offsetEnd + "_edited")
                    .concat(ObjectForValidatorDetector.OVD_FILE_SUFFIX);
            this.analyzedObject.setEditedObjectPath(path);
            return path;
        } else {
            return null;
        }
    }

    public boolean dicomDetection(final File file, final int startOffset, final int endOffset) {
        try {
            if ((startOffset >= 0) && (endOffset != 0) && (startOffset < endOffset)) {
                this.setSaveStartOffset(0);
                final File f = this.fileIntoOffsets(file, startOffset, endOffset);
                this.setSaveStartOffset(this.getSaveStartOffset() + startOffset);
                final boolean result = this.dicomDetection(f, 0, 0);
                AnalyzerFileUtils.deleteTmpFile(f);
                return result;
            } else {
                // Dicom bytes pattern
                final byte[] pattern = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 68, 73, 67, 77 };
                final MediaType type = new MediaType("application", "Dicom");
                final MagicDetector detector = new MagicDetector(type, pattern);
                final Tika t = new Tika(detector);
                final String result = t.detect(new FileInputStream(file));
                if ("application/dicom".equals(result)) {
                    return true;
                }
                return false;
            }
        } catch (final IOException e) {
            LOGGER.error("{}", e);
            return false;
        }
    }

    public boolean httpDetection(final File file, final int startOffset, final int endOffset) {
        try {
            if ((startOffset >= 0) && (endOffset != 0) && (startOffset < endOffset)) {
                this.setSaveStartOffset(0);
                final File f = this.fileIntoOffsets(file, startOffset, endOffset);
                this.setSaveStartOffset(this.getSaveStartOffset() + startOffset);
                final boolean result = this.httpDetection(f, 0, 0);
                AnalyzerFileUtils.deleteTmpFile(f);
                return result;
            } else {
                final String fileContent = FileUtils.readFileToString(file);

                final ArrayList<String> firstElements = new ArrayList<>();
                firstElements.add("GET");
                firstElements.add("HEAD");
                firstElements.add("POST");
                firstElements.add("PUT");
                firstElements.add("DELETE");
                firstElements.add("TRACE");
                firstElements.add("CONNECT");

                for (final String s : firstElements) {
                    if (fileContent.startsWith(s)) {
                        return true;
                    }
                }
                return false;
            }
        } catch (final IOException e) {
            LOGGER.error("{}", e);
            return false;
        }
    }

    // Split a file within offset
    public File fileIntoOffsets(final File f, final int startOffset, final int endOffset) {
        File file = null;
        if ((startOffset >= 0) && (endOffset != 0) && (startOffset < endOffset)) {
            String fileContent = AnalyzerFileUtils.fileToString(f);
            fileContent = fileContent.substring(startOffset, endOffset);
            try {
                file = File.createTempFile("temp-file-into-offset", TMP_FILE_SUFFIX);
                final String newline = System.getProperty("line.separator");
                while (!fileContent.startsWith("<?xml") && (fileContent.startsWith(newline) || fileContent
                        .startsWith("\r") || fileContent.startsWith("\t"))) {
                    fileContent = fileContent.substring(1);
                    this.setSaveStartOffset(this.getSaveStartOffset() + 1);
                }

                FileUtils.writeStringToFile(file, fileContent);
            } catch (final IOException e) {
                LOGGER.error(String.format("%s", e));
            }
        }
        return file;
    }

    public boolean base64Detection(final String fileContent, final int startOffset, final int endOffset) {
        File file;
        try {
            file = File.createTempFile("temp-file-base64Detection-string", TMP_FILE_SUFFIX);
            FileUtils.writeStringToFile(file, fileContent);
        } catch (final IOException e) {
            LOGGER.error("{}", e);
            return false;
        }
        final boolean result = this.base64Detection(file, startOffset, endOffset);
        AnalyzerFileUtils.deleteTmpFile(file);
        return result;
    }

    public boolean base64Detection(final File file, final int startOffset, final int endOffset) {
        boolean isBase64 = false;
        try {
            if ((startOffset >= 0) && (endOffset != 0) && (startOffset < endOffset)) {
                this.setSaveStartOffset(0);
                final File f = this.fileIntoOffsets(file, startOffset, endOffset);
                this.setSaveStartOffset(this.getSaveStartOffset() + startOffset);
                final boolean result = this.base64Detection(f, 0, 0);
                AnalyzerFileUtils.deleteTmpFile(f);
                return result;
            } else {
                final String fileContent = FileUtils.readFileToString(file);
                // Test with regex
                final String pattern = "^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$";
                if (fileContent.matches(pattern) || Base64.isArrayByteBase64(fileContent.getBytes(StandardCharsets.UTF_8)) && !fileContent
                        .contains("CERTIFICATE")) {
                    final byte[] fileByte = IOUtils.toByteArray(new FileInputStream(file));
                    // Currently the method treats whitespace as valid.
                    isBase64 = Base64.isArrayByteBase64(fileByte);
                }
                return isBase64;
            }
        } catch (final FileNotFoundException e) {
            MessageContentAnalyzer.LOGGER.error("{}", e);
            return isBase64;
        } catch (final IOException e) {
            MessageContentAnalyzer.LOGGER.error("{}", e);
            return isBase64;
        }
    }

    public boolean mtomDetection(final File file, final int startOffset, final int endOffset) {

        try {
            if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
                this.setSaveStartOffset(0);
                final File f = this.fileIntoOffsets(file, startOffset, endOffset);
                this.setSaveStartOffset(this.getSaveStartOffset() + startOffset);
                final boolean resultat = this.mtomDetection(f, 0, 0);
                AnalyzerFileUtils.deleteTmpFile(f);
                return resultat;
            } else {

                final String fileContent = FileUtils.readFileToString(file);
                final String mtomReg = "^--([^>].+)";
                final Pattern regex = Pattern.compile(mtomReg);
                final Matcher regexMatcher = regex.matcher(fileContent);

                String patternTmp = null;
                if (regexMatcher.find()) {
                    patternTmp = regexMatcher.group(1);
                }
                patternTmp = "--" + patternTmp;
                final byte[] pattern = patternTmp.getBytes(StandardCharsets.UTF_8);

                // Verification of pattern value in LOGGER
                final String value = new String(pattern, StandardCharsets.UTF_8);
                MessageContentAnalyzer.LOGGER.info("mtom pattern :{}", value);

                final MediaType type = new MediaType("application", "mtom");
                final MagicDetector detector = new MagicDetector(type, pattern);
                final Tika t = new Tika(detector);

                final String result = t.detect(new FileInputStream(file));
                return "application/mtom".equals(result);
            }
        } catch (final IOException e) {
            MessageContentAnalyzer.LOGGER.error("{}", e);
            return false;
        }
    }

    public boolean isHL7(final File file, final int startOffset, final int endOffset) {
        try {
            if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
                this.setSaveStartOffset(0);
                final File f = this.fileIntoOffsets(file, startOffset, endOffset);
                this.setSaveStartOffset(this.getSaveStartOffset() + startOffset);
                final boolean result = this.isHL7(f, 0, 0);
                AnalyzerFileUtils.deleteTmpFile(f);
                return result;
            } else {
                final String fileContent = FileUtils.readFileToString(file);
                return fileContent.startsWith("MSH");
            }
        } catch (final IOException e) {
            MessageContentAnalyzer.LOGGER.error("{}", e);
            return false;
        }
    }

    public boolean isSyslog(final File file, final int startOffset, final int endOffset) {
        try {
            if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
                this.setSaveStartOffset(0);
                final File f = this.fileIntoOffsets(file, startOffset, endOffset);
                this.setSaveStartOffset(this.getSaveStartOffset() + startOffset);
                final boolean result = this.isSyslog(f, 0, 0);
                AnalyzerFileUtils.deleteTmpFile(f);
                return result;
            } else {
                final String fileContent = FileUtils.readFileToString(file);
                if (fileContent.startsWith("<")) {
                    final String xmlReg = "(<[0-9]{2,3}>1 .*)<\\?xml";
                    final Pattern regex = Pattern.compile(xmlReg, Pattern.DOTALL);
                    final Matcher regexMatcher = regex.matcher(fileContent);

                    if (regexMatcher.find()) {
                        MessageContentAnalyzer.LOGGER.debug("regexmatch group 1 {}", regexMatcher.group(1));
                        this.setSaveStartOffset(regexMatcher.group(1).length());
                        this.setSaveEndOffset(fileContent.length());
                        return true;
                    }


                }
                return false;
            }
        } catch (final IOException e) {
            MessageContentAnalyzer.LOGGER.error("{}", e);
            return false;
        }
    }

    public boolean xmlIsWellFormed(final File file, final int startOffset, final int endOffset) {
        FileInputStream fis = null;
        boolean result = false;

        try {
            if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
                this.setSaveStartOffset(0);
                final File f = this.fileIntoOffsets(file, startOffset, endOffset);
                this.setSaveStartOffset(this.getSaveStartOffset() + startOffset);
                result = this.xmlIsWellFormed(f, 0, 0);
                AnalyzerFileUtils.deleteTmpFile(f);
                return result;
            } else {

                final SAXParserFactory factoryBASIC = SAXParserFactory.newInstance();
                factoryBASIC.setValidating(false);
                try {
                    factoryBASIC.setFeature("http://xml.org/sax/features/validation", false);
                    factoryBASIC.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
                    factoryBASIC.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
                    factoryBASIC.setFeature("http://xml.org/sax/features/external-general-entities", false);
                    factoryBASIC.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                 //   factoryBASIC.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);

                    factoryBASIC.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
                } catch (final ParserConfigurationException e1) {
                    MessageContentAnalyzer.LOGGER.error(e1.getMessage());
                }
                factoryBASIC.setValidating(false);
                factoryBASIC.setNamespaceAware(true);

                fis = new FileInputStream(file);
                final ValidationErrorHandler err = new ValidationErrorHandler();
                final SAXParser parser = factoryBASIC.newSAXParser();
                final XMLReader reader = parser.getXMLReader();
                reader.setErrorHandler(err);

                reader.parse(new InputSource(fis));
                result = !err.isContainsError();

                if (result) {
                    this.getNamespacesPrefixAndURI(file);
                }
                return result;
            }
        } catch (final IOException e) {
            MessageContentAnalyzer.LOGGER.error("{}", e);
        } catch (final ParserConfigurationException e) {
            MessageContentAnalyzer.LOGGER.error("{}", e);
        } catch (final SAXException e) {
            MessageContentAnalyzer.LOGGER.error("{}", e);
            // Do not show this message to the user as this is confusing
            //       FacesMessages.instance().add(Severity.INFO, "XML is not well formed : " + e.getMessage());
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (final IOException e) {
                    MessageContentAnalyzer.LOGGER.error("not able to close the FIS", e);
                }
            }
        }
        return result;
    }

    private void getNamespacesPrefixAndURI(final File f) {
        final NamespaceAnalyze na = new NamespaceAnalyze();
        try {
            final Map<String, String> mapsPrefixUri = na.getListOfPrefixAndURIForNamespaces(f);

            if (!mapsPrefixUri.isEmpty()) {
                this.setListOfNamespaces(mapsPrefixUri);
            }
        } catch (final SAXException e) {
            MessageContentAnalyzer.LOGGER.error("Failed to parse file :", e);
        }
    }

    /*
    startLikeXML : returns true is the content at startOffset starts like an XML file
     */
    public boolean startLikeXml(final File file, final int startOffset, final int endOffset) {
        try {
            if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
                this.setSaveStartOffset(0);
                final File f = this.fileIntoOffsets(file, startOffset, endOffset);
                this.setSaveStartOffset(this.getSaveStartOffset() + startOffset);
                final boolean result = this.startLikeXml(f, 0, 0);
                AnalyzerFileUtils.deleteTmpFile(f);
                return result;
            } else {
                final String fileContent = FileUtils.readFileToString(file);
                final String xmlReg = "<.+>.+</.+?>";
                final Pattern regex = Pattern.compile(xmlReg, Pattern.DOTALL);
                final Matcher regexMatcher = regex.matcher(fileContent);

                if (fileContent.startsWith("<?xml version=\"")) {
                    return true;
                } else if (regexMatcher.find()) {
                    return regexMatcher.group(0).length() == fileContent.trim().length();
                } else {
                    return false;
                }
            }
        } catch (final IOException e) {
            MessageContentAnalyzer.LOGGER.error("{}", e);
            return false;
        }
    }

    public boolean isCertificate(final File file, final int startOffset, final int endOffset) {
        try {
            if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
                this.setSaveStartOffset(0);
                final File f = this.fileIntoOffsets(file, startOffset, endOffset);
                this.setSaveStartOffset(this.getSaveStartOffset() + startOffset);
                final boolean result = this.isCertificate(f, 0, 0);
                AnalyzerFileUtils.deleteTmpFile(f);
                return result;
            } else {
                final String fileContent = FileUtils.readFileToString(file);
                return fileContent.startsWith("-----BEGIN CERTIFICATE-----");
            }
        } catch (final IOException e) {
            MessageContentAnalyzer.LOGGER.error("{}", e);
            return false;
        }
    }

    public void messageContentAnalyzer(final File f, final int startOffset, final int endOffset, final FileToValidate parent) {

        final FileToValidate ftv = new FileToValidate(parent);
        final Tika t = new Tika();
        this.setSaveStartOffset(this.getSaveStartOffset());

        try {

            final String detectedMimeType = this.getMimeTypeBetweenOffsets(f, startOffset, endOffset, t);

            if (this.getHeaderValue() != null) {
                this.analyzeHeader(f, parent, ftv, this, startOffset, endOffset);
                return;
            }

            final IAnalyzableContent contentDetector = this.contentDetector(this, f, startOffset, endOffset, detectedMimeType);
            contentDetector.analyze(f, parent, ftv, this, startOffset, endOffset);

        } catch (final IOException e) {
            MessageContentAnalyzer.LOGGER.error("{}", e);
        }
    }

    public void containsSAMLOverride(final File f, final int startOffset, final int endOffset, final FileToValidate parent) {

        final FileToValidate ftv = new FileToValidate(parent);
        this.setSaveStartOffset(this.getSaveStartOffset());
        this.analyzeHeader(f, parent, ftv, this, startOffset, endOffset);

    }

    public IAnalyzableContent contentDetector(final MessageContentAnalyzer messageContentAnalyzer, final File file, final int startOffset,
            final int endOffset, final String detectedMimeType) {
        if ("application/pdf".equals(detectedMimeType)) {
            return new PDF();
        }
        if ("application/zip".equals(detectedMimeType)){
            return new ZIP();
        }
        if (messageContentAnalyzer.dicomDetection(file, startOffset, endOffset)) {
            return new DICOM();
        }
        if (messageContentAnalyzer.base64Detection(file, startOffset, endOffset)) {
            return new B64();
        }
        if (messageContentAnalyzer.httpDetection(file, startOffset, endOffset)) {
            return new HTTP();
        }
        if (messageContentAnalyzer.mtomDetection(file, startOffset, endOffset)) {
            return new MTOM();
        }
        if (messageContentAnalyzer.isHL7(file, startOffset, endOffset)) {
            return new HL7V2();
        }
        if (messageContentAnalyzer.isSyslog(file, startOffset, endOffset)) {
            return new SYSLOG();
        }


        if (!("application/xml".equals(detectedMimeType) || messageContentAnalyzer
                .startLikeXml(file, startOffset, endOffset)) && (this.getBodyValue() == null || this.getBodyValue().isEmpty()) && messageContentAnalyzer.isCertificate(file, startOffset, endOffset)) {
            return new Certificate();
        }

        if (this.getBodyValue() == null && this.getHttpBody() == null &&
                !messageContentAnalyzer.xmlIsWellFormed(file, startOffset, endOffset) && !this.isWellFormedFirstTime()) {
            MessageContentAnalyzer.LOGGER.info("XML Not well formed");
            return new NullAnalyzableContent();
        }

        if (messageContentAnalyzer.xmlIsWellFormed(file, startOffset, endOffset) || this.isWellFormedFirstTime()) {
            return new XML();
        }

        return new NullAnalyzableContent();
    }

    private void analyzeHeader(final File f, final FileToValidate parent, final FileToValidate ftv, final MessageContentAnalyzer mca,
            final int startOffset, final int endOffset) {
        final boolean resultAssertion = SAML.containsAssertionToString(f, startOffset, endOffset, mca);
        if (resultAssertion) {
            MessageContentAnalyzer.LOGGER.info("Launch SAML validation");
            ftv.setStartOffset(mca.getOffsetStart() + startOffset + mca.getSaveStartOffset());
            ftv.setEndOffset(mca.getOffsetEnd() + startOffset + mca.getSaveStartOffset());
            ftv.setValidationType("SAML");
            ftv.setMessageContentAnalyzerOid(OIDGenerator.getNewOid());

            ftv.setDocType("SAML");

            if ("WSTrust".equals(parent.getDocType())) {
                ftv.setParent(parent);
            }

            parent.getFileToValidateList().add(ftv);

            // Verify if there is not another assertion
            final FileToValidate ftv1 = new FileToValidate(parent);
            this.analyzeHeader(f, parent, ftv1, mca, ftv.getEndOffset(), endOffset);
        } else {
            MessageContentAnalyzer.LOGGER.info("No assertion");
        }
    }

    private String getMimeTypeBetweenOffsets(final File f, final int startOffset, final int endOffset, final Tika t)
            throws IOException {
        final String value;
        if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
            final File file = this.fileIntoOffsets(f, startOffset, endOffset);

            value = t.detect(new FileInputStream(file));
            AnalyzerFileUtils.deleteTmpFile(file);
        } else {
            value = t.detect(new FileInputStream(f));
        }
        return value;
    }

    public Integer getMissingTagInducedOffsetParent() {
        return missingTagInducedOffsetParent;
    }

    public void setMissingTagInducedOffsetParent(Integer missingTagInducedOffsetParent) {
        this.missingTagInducedOffsetParent = missingTagInducedOffsetParent;
    }
    public Integer getMissingTagInducedOffsetChild() {
        return missingTagInducedOffsetChild;
    }

    public void setMissingTagInducedOffsetChild(Integer missingTagInducedOffsetChild) {
        this.missingTagInducedOffsetChild = missingTagInducedOffsetChild;
    }
}
