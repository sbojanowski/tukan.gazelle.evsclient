/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file;

import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.common.action.FileToValidate;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MTOM implements IAnalyzableContent {

    private static final Logger LOGGER = LoggerFactory.getLogger(MTOM.class);

    @Override
    public boolean analyze(final File f, final FileToValidate parent, final FileToValidate ftv, final MessageContentAnalyzer mca,
            final int startOffset, final int endOffset) {
        final File file;
        if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
            file = mca.fileIntoOffsets(f, startOffset, endOffset);
            mca.setSaveEndOffset(endOffset);
            this.analyze(file, parent, ftv, mca, 0, 0);
            AnalyzerFileUtils.deleteTmpFile(file);
            mca.setSaveEndOffset(0);
        } else {
            MTOM.LOGGER.info("Extract parts from mtom");

            ftv.setStartOffset(startOffset + mca.getSaveStartOffset());
            if (mca.getSaveEndOffset() != 0) {
                ftv.setEndOffset(mca.getSaveEndOffset());
            } else {
                ftv.setEndOffset(endOffset + mca.getSaveStartOffset());
            }
            ftv.setMessageContentAnalyzerOid(OIDGenerator.getNewOid());

            ftv.setDocType("MTOM");

            this.extractMtomParts(f, ftv, mca);
        }
        this.analyzeMtomParts(f, parent, ftv, mca);
        return false;
    }

    public void analyzeMtomParts(final File f, final FileToValidate parent, final FileToValidate ftv, final MessageContentAnalyzer mca) {
        if (mca.getSaveEndOffset() == 0) {
            // Restart test with parts
            final List<FileToValidate> fileListToValidate = ftv.getFileToValidateList();
            final int size = fileListToValidate.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    final FileToValidate tmpFile = fileListToValidate.get(i);
                    mca.messageContentAnalyzer(f, tmpFile.getStartOffset(),
                            tmpFile.getEndOffset(), tmpFile);
                }
            }
            parent.getFileToValidateList().add(ftv);
        }
    }

    /**
     * Extract parts Make mutli parts (AFTER each regex : ^--([^>].+))
     *

     */
    public void extractMtomParts(final File file, final FileToValidate parent, final MessageContentAnalyzer mca) {
        int begin, end;
        String fileContent;
        final ArrayList<String> offsetStartTab = new ArrayList<>();
        final ArrayList<String> offsetEndTab = new ArrayList<>();

        try {
            fileContent = FileUtils.readFileToString(file);
            String firstLineMatch = "^--([^>].+)";

            Pattern regex = Pattern.compile(firstLineMatch);
            Matcher regexMatcher = regex.matcher(fileContent);
            if (regexMatcher.find()) {
                firstLineMatch = regexMatcher.group();

            }
            final String mtomHeader = "^(" + firstLineMatch + ")(((\\n|\\r|\\r\\n)((^Content-.*(\\n|\\r|\\r\\n)*))+)|--)";

            regex = Pattern.compile(mtomHeader, Pattern.MULTILINE);
            regexMatcher = regex.matcher(fileContent);
            while (regexMatcher.find()) {
                MTOM.LOGGER.info("group : {} start :{} end :{}",
                        new Object[] { regexMatcher.group(), Integer.valueOf(regexMatcher.start()), Integer.valueOf(regexMatcher.end()) });

                offsetStartTab.add("" + Integer.toString(regexMatcher.start()));
                offsetEndTab.add("" + Integer.toString(regexMatcher.end()));
            }
            if (offsetStartTab.size() > 1) {
                // Remove the first element with value 0
                offsetStartTab.remove(0);
                // Remove the last element
                offsetEndTab.remove(offsetEndTab.size() - 1);
            }

            for (int j = 0; j < offsetStartTab.size(); j++) {

                begin =  Integer.parseInt(offsetEndTab.get(j));
                end =  Integer.parseInt(offsetStartTab.get(j));
                if (end - begin > 0) {
                    MTOM.LOGGER.info("\nBegining :{}", Integer.valueOf(begin));
                    MTOM.LOGGER.info("\nend :{}", Integer.valueOf(end));
                  //  MTOM.LOGGER.info(fileContent.substring(begin, end));

                    final FileToValidate ftv = new FileToValidate(parent);
                    ftv.setFileName("Mtom_part_" + (j + 1));
                    ftv.setDocType("Mtom_part_" + (j + 1));
                    ftv.setMessageContentAnalyzerOid(OIDGenerator.getNewOid());
                    ftv.setStartOffset(begin + mca.getSaveStartOffset());
                    ftv.setEndOffset(end + mca.getSaveStartOffset());
                    parent.getFileToValidateList().add(ftv);
                }
            }
        } catch (final IOException e) {
            MTOM.LOGGER.error(e.getMessage());
        }
    }
}
