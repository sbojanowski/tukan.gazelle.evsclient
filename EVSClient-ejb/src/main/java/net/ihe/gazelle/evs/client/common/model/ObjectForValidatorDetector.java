/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.analyzer.AnalyzerBean;
import net.ihe.gazelle.evs.client.common.action.FileToValidate;
import net.ihe.gazelle.evs.client.common.action.FileType;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.action.ValidationType;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.*;

@Entity
@Name("objectForValidatorDetector")
@Table(name = "object_for_validator_detector", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "object_for_validator_detector_sequence", sequenceName = "object_for_validator_detector_id_seq", allocationSize = 1)
public class ObjectForValidatorDetector extends ValidatedObject {

    /**
     *
     */
    private static final long serialVersionUID = -1232813067358180925L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectForValidatorDetector.class);

    public static final String OVD_FILE_PREFIX = "objectForValidatorDetector_";
    public static final String OVD_FILE_SUFFIX = ".mca";

    @Id
    @NotNull
    @GeneratedValue(generator = "object_for_validator_detector_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "messageToAnalyze")
    private byte[] messageToAnalyze;

    @Column(name = "content_analyzer_version")
    private String contentAnalyzerVersion;

    @Column(name = "validation_type_list")
    @OneToMany(mappedBy = "ObjectForVd", targetEntity = ValidationType.class)
    private List<ValidationType> validationTypeList;

    @Column(name = "object_path")
    private String objectPath;

    @Column(name = "edit_object_path")
    private String editedObjectPath;

    @Transient
    private FileToValidate parentFileToValidate;

    @Transient
    private final AnalyzerBean analyzer = new AnalyzerBean();
    private String validationTypeListAsString;

    public ObjectForValidatorDetector(final ObjectForValidatorDetector inObjectForValidatorDetector) {
        this.objectPath = inObjectForValidatorDetector.getObjectPath();
        this.description = inObjectForValidatorDetector.getDescription();
    }

    public ObjectForValidatorDetector() {
    }

    public String getContentAnalyzerVersion() {
        return this.contentAnalyzerVersion;
    }

    public void setContentAnalyzerVersion(final String contentAnalyzerVersion) {
        this.contentAnalyzerVersion = contentAnalyzerVersion;
    }

    public byte[] getMessageToAnalyze() {
        if (this.messageToAnalyze != null) {
            return this.messageToAnalyze.clone();
        }
        else {
            return null;
        }
    }

    public void setMessageToAnalyze(final byte[] messageToAnalyze) {
        this.messageToAnalyze = messageToAnalyze.clone();
    }

    public FileToValidate getParentFileToValidate() {
        final byte[] data = this.getMessageToAnalyze();
        if (data != null) {
            try {

                final FileToValidate fileToValidate = (FileToValidate) FileToValidate.deserialize(data);

                this.setParentFileToValidate(fileToValidate);
            } catch (IOException|ClassNotFoundException e) {
                ObjectForValidatorDetector.LOGGER.error("{}", e);
                FacesMessages.instance().add(Severity.ERROR, e.getMessage());
                return null;
            }
        }
        return this.parentFileToValidate;
    }

    public void setParentFileToValidate(final FileToValidate parentFileToValidate) {
        this.parentFileToValidate = parentFileToValidate;
    }

    public List<ValidationType> getValidationTypeList() {
        return this.validationTypeList;
    }

    public String getValidationTypeListAsString(){
        this.validationTypeListAsString = "";
        if (!this.validationTypeList.isEmpty()){
            for (final ValidationType temp : this.validationTypeList) {
                validationTypeListAsString = validationTypeListAsString.concat(temp.getName()).concat(" ");
            }
        }
        return this.validationTypeListAsString ;
    }

    public void setValidationTypeList(final List<ValidationType> validationTypeList) {
        if (validationTypeList != null){
            this.validationTypeList = new ArrayList<ValidationType>(validationTypeList);
        }
        else {
            this.validationTypeList = null;
        }
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public void setObjectPath(final String objectPath) {
        this.objectPath = objectPath;
    }

    public String getEditedObjectPath() {
        return this.editedObjectPath;
    }

    public void setEditedObjectPath(final String editedObjectPath) {
        this.editedObjectPath = editedObjectPath;
    }

    public String getObjectPath() {
        return this.objectPath;
    }

    @Override
    public String getFilePath() {
        return this.objectPath;
    }

    // Used in XHTML code.
    public AnalyzerBean getValDec() {
        final FileToValidate fileToValidate = this.getParentFileToValidate();
        if (fileToValidate != null){
            this.analyzer.setParentFileToValidate(fileToValidate);
        }
        final ObjectForValidatorDetector analyzedObject = new ObjectForValidatorDetector();
        analyzedObject.setObjectPath(this.objectPath);
        this.analyzer.setAnalyzedObject(analyzedObject);
        return this.analyzer;
    }

    public void addAllValidationTypesFromResult(final FileToValidate fileToValidate) {
        final ArrayList<ValidationType> listValType = new ArrayList<>();
        final List<FileToValidate> listFileToValidate = fileToValidate.getFileToValidateListToDisplay();
        final EntityManager provideEntityManager = EntityManagerService.provideEntityManager();
        for (final FileToValidate ftv : listFileToValidate) {
            final String valType = ftv.getValidationType();
            if (FileType.contains(valType)) {
                ValidationType e = new ValidationType(FileType.getEnum(valType), this);
                e = provideEntityManager.merge(e);
                listValType.add(e);
            }
        }
        provideEntityManager.flush();
        this.setValidationTypeList(listValType);
    }

    /**
     */
    public static ObjectForValidatorDetector createNewObjectForValidatorDetector() {
        ObjectForValidatorDetector ofvd = new ObjectForValidatorDetector();
        ofvd.setOid(OIDGenerator.getNewOid());
        ofvd.setValidationDate(new Date());
        final EntityManager em = EntityManagerService.provideEntityManager();
        ofvd = em.merge(ofvd);
        em.flush();
        if (ofvd == null || ofvd.getId() == 0) {
            ObjectForValidatorDetector.LOGGER.error("An error occurred when creating a new Object For Validator Detector");
            return null;
        }
        final String repository = ApplicationPreferenceManager
                .getStringValue("object_for_validator_detector_repository");
        if (repository == null || repository.isEmpty()) {
            ObjectForValidatorDetector.LOGGER.error("Object For Validator Detector repository doesn't exist in preferences");
            FacesMessages.instance().add(Severity.ERROR,"Object For Validator Detector repository doesn't exist in preferences");
        }
        ofvd.setObjectPath(repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ObjectForValidatorDetector.OVD_FILE_PREFIX
                .concat(ofvd.getId().toString()).concat(
                        ObjectForValidatorDetector.OVD_FILE_SUFFIX));
        return ofvd;
    }

    public static ObjectForValidatorDetector storeObjectForValidatorDetector(
            final ObjectForValidatorDetector messageToStore) {
        final ObjectForValidatorDetector localMessageToStore;
        if (messageToStore.getOid() == null) {
            messageToStore.setOid(OIDGenerator.getNewOid());
            messageToStore.setValidationDate(new Date());
        }
        localMessageToStore = save(ObjectForValidatorDetector.class, messageToStore);
        // Update the cache with the newest values
        GazelleValidationCacheManager
                .refreshGazelleCacheWebService(localMessageToStore.getExternalId(), localMessageToStore.getToolOid(),
                        localMessageToStore.getOid(), "off");
        return localMessageToStore;
    }

    // Marked method as deprecated. Indeed this is now used in the project
    @Deprecated
    public ObjectForValidatorDetector findObjectForValidatorDetector(final ObjectForValidatorDetector obj) {
        final EntityManager em = EntityManagerService.provideEntityManager();
        return em.find(ObjectForValidatorDetector.class, obj.getOid());
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/messageContentAnalyzerDetailedResult.seam?").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null && !this.getPrivacyKey().isEmpty()) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

    public boolean equals(final Object obj) {
        if (! super.equals(obj)) {
            return false;
        }
        final ObjectForValidatorDetector fobj = (ObjectForValidatorDetector) obj;
        if (this.id.equals(fobj.getId())) {  // added fields are tested
            return true;
        }
        return Arrays.equals(this.messageToAnalyze, fobj.getMessageToAnalyze());
    }

}