/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.hl7.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Name("package_name_for_profile_oid")
@Table(name = "package_name_for_profile_oid", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "package_name_for_profile_oid_sequence", sequenceName = "package_name_for_profile_oid_id_seq", allocationSize = 1)
public class PackageNameForProfileOID implements Serializable {
    private static final long serialVersionUID = -3136363545214161909L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PackageNameForProfileOID.class);

    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(generator = "package_name_for_profile_oid_sequence", strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "profile_oid")
    private String profileOID;

    @Column(name = "package_name")
    private String packageName;

    public int getId() {
        return this.id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getProfileOID() {
        return this.profileOID;
    }

    public void setProfileOID(final String profileOID) {
        this.profileOID = profileOID;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(final String packageName) {
        this.packageName = packageName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.packageName == null ? 0 : this.packageName.hashCode());
        result = prime * result + (this.profileOID == null ? 0 : this.profileOID.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final PackageNameForProfileOID other = (PackageNameForProfileOID) obj;
        if (this.packageName == null) {
            if (other.packageName != null) {
                return false;
            }
        } else if (!this.packageName.equals(other.packageName)) {
            return false;
        }
        if (this.profileOID == null) {
            if (other.profileOID != null) {
                return false;
            }
        } else if (!this.profileOID.equals(other.profileOID)) {
            return false;
        }
        return true;
    }

    public PackageNameForProfileOID() {
        // TODO Auto-generated constructor stub
    }

    public PackageNameForProfileOID(final String profileOID, final String packageName) {
        this.profileOID = profileOID;
        this.packageName = packageName;
    }

    public static String findPackageNameWithProfileOID(final String profileOID) {
        if (profileOID == null) {
            return null;
        }
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        final HQLQueryBuilder<PackageNameForProfileOID> queryBuilder = new HQLQueryBuilder<>(entityManager,
                PackageNameForProfileOID.class);
        queryBuilder.addEq("profileOID", profileOID);
        final List<PackageNameForProfileOID> packages = queryBuilder.getList();
        if (packages != null && !packages.isEmpty()) {
            return packages.get(0).getPackageName();
        } else {
            final String validatorUrl = ApplicationPreferenceManager.getStringValue("gazelle_hl7v2_validator_url");
            final ClientRequest request = new ClientRequest(validatorUrl.concat("/rest/GetPackageForProfile"));
            request.queryParameter("oid", profileOID);
            try {
                final ClientResponse<String> response = request.get(String.class);
                if (response.getStatus() == 200 && response.getEntity() != null && !response.getEntity().isEmpty()) {
                    final PackageNameForProfileOID javaPackage = new PackageNameForProfileOID();
                    javaPackage.setPackageName(response.getEntity());
                    javaPackage.setProfileOID(profileOID);
                    entityManager.merge(javaPackage);
                    entityManager.flush();
                    return javaPackage.getPackageName();
                } else {
                    return null;
                }
            } catch (final Exception e) {
                LOGGER.error(e.getMessage(),e);
                return null;
            }
        }
    }
}
