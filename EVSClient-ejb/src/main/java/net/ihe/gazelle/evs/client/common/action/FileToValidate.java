/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class FileToValidate implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6827624080912716763L;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileToValidate.class);

    private static final String LINE_SEPARATOR =  System.getProperty("line.separator");

    private FileToValidate parent;
    private String fileName;
    private String validationType;
    private String docType;
    private String result;
    private String messageContentAnalyzerOid;

    private int startOffset;
    private int endOffset;

    private List<FileToValidate> fileToValidateList;
    private List<FileToValidate> fileToValidateListToDisplay;

    private FileToValidate previousElement;

    private String textLayout;
    private String entireLayout = "";
    private String entireResult = "";

    private Map<String, String> listOfNamespacesForftv = new ConcurrentHashMap<>();

    public Map<String, String> getListOfNamespacesForftv() {
        return this.listOfNamespacesForftv;
    }

    public void setListOfNamespacesForftv(final Map<String, String> listOfNamespacesForftv) {
        this.listOfNamespacesForftv = listOfNamespacesForftv;
    }

    public static byte[] serialize(final Object obj) throws IOException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(obj);
        return out.toByteArray();
    }

    public static Object deserialize(final byte[] data) throws IOException, ClassNotFoundException {
        final ByteArrayInputStream in = new ByteArrayInputStream(data);
        final ObjectInputStream is = new ObjectInputStream(in);
        return is.readObject();
    }

    public String getMessageContentAnalyzerOid() {
        return this.messageContentAnalyzerOid;
    }

    public void setMessageContentAnalyzerOid(final String messageContentAnalyzerOid) {
        this.messageContentAnalyzerOid = messageContentAnalyzerOid;
    }

    public FileToValidate getParent() {
        return this.parent;
    }

    public void setParent(final FileToValidate parent) {
        this.parent = parent;
    }

    public FileToValidate getPreviousElement() {
        return this.previousElement;
    }

    public void setPreviousElement(final FileToValidate previous) {
        this.previousElement = previous;
    }

    public List<FileToValidate> getFileToValidateListToDisplay() {
        if (this.fileToValidateListToDisplay == null) {
            this.fileToValidateListToDisplay = new ArrayList<>();
        }
        return this.fileToValidateListToDisplay;
    }

    public void setFileToValidateListToDisplay(final List<FileToValidate> fileToValidateListToDisplay) {
        if (fileToValidateListToDisplay != null) {
            this.fileToValidateListToDisplay = new ArrayList<FileToValidate>(fileToValidateListToDisplay);
        }
        else {
            this.fileToValidateListToDisplay = null;
        }
    }

    public void addElement(final FileToValidate e) {
        FileToValidate f = e;
        while (f.getParent() != null) {
            FileToValidate.LOGGER.info(f.getParent().getDocType());
            f = f.getParent();
        }
        f.getFileToValidateListToDisplay().add(e);

    }

    public String getDocType() {
        return this.docType;
    }

    public void setDocType(final String docType) {
        this.docType = docType;
    }

    public List<FileToValidate> getFileToValidateList() {

        if (this.fileToValidateList == null) {
            this.fileToValidateList = new ArrayList<>();
        }
        return this.fileToValidateList;
    }

    public void setFileToValidateList(final List<FileToValidate> fileToValidateList) {
        if (fileToValidateList != null) {
            this.fileToValidateList = new ArrayList<FileToValidate>(fileToValidateList);
        }
        else {
            this.fileToValidateList = null;
        }
    }

    public boolean isEmpty() {
        return this.getFileToValidateList().isEmpty();
    }

    public String getResult() {
        if (this.result == null) {
            this.result = "";
        }
        return this.result;
    }

    public void setResult(final String result) {
        this.result = result;
    }

    public void setEntireResult(final String entireResult) {
        this.entireResult = entireResult;
    }

    public String getEntireResult() {

        if (this.fileToValidateList != null) {
            Iterator<FileToValidate> iterator ;
            FileToValidate ftvi = null;

            StringBuilder bld = new StringBuilder(this.entireResult);

            for (iterator = this.fileToValidateList.iterator(); iterator.hasNext(); ) {
                ftvi = iterator.next();
                bld.append(ftvi.getResult());
                bld.append(ftvi.getEntireResult());
            }

            if (!iterator.hasNext()) {
                bld.append( "</td></tr></table>");
            }
            this.entireResult = bld.toString();

        }
        return this.entireResult;
    }

    public String getEntireResult(final String parentResult) {
        this.entireResult = parentResult;
        return this.getEntireResult();
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    public String getValidationType() {
        return this.validationType;
    }

    public void setValidationType(final String validationType) {
        this.validationType = validationType;
    }

    public int getStartOffset() {
        return this.startOffset;
    }

    public void setStartOffset(final int startOffset) {
        this.startOffset = startOffset;
    }

    public int getEndOffset() {
        return this.endOffset;
    }

    public void setEndOffset(final int endOffset) {
        this.endOffset = endOffset;
    }

    public FileToValidate(final FileToValidate parent) {
        this.fileName = "";
        this.validationType = "";
        this.parent = parent;
        this.init();
    }

    public FileToValidate(final String fileName, final String fileToDisplay, final String validationType, final FileToValidate parent) {
        this.fileName = fileName;
        this.validationType = validationType;
        this.parent = parent;
        this.init();

    }

    private void init(){
        this.docType = "";
        this.result = "";
        this.fileToValidateListToDisplay = null;
        this.textLayout = "";
    }

    public void print() {
        this.print("", true);
    }

    public String printInString() {
        return this.printInString("", true);
    }

    public void setEntireLayout(final String entireLayout) {
        this.entireLayout = entireLayout;
    }

    public String getEntireLayout() {

        if (this.fileToValidateList != null) {
            Iterator<FileToValidate> iterator = this.fileToValidateList.iterator();
            for (iterator = this.fileToValidateList.iterator(); iterator.hasNext(); ) {
                final FileToValidate ftvi = iterator.next();
                this.entireLayout += ftvi.getTextLayout() + ftvi.getEntireLayout();
            }
        }
        return this.entireLayout;
    }

    public String getEntireLayout(final String parentTextLayout) {
        this.entireLayout = parentTextLayout;
        if (this.fileToValidateList != null) {
            Iterator<FileToValidate> iterator = this.fileToValidateList.iterator();
            for (iterator = this.fileToValidateList.iterator(); iterator.hasNext(); ) {
                final FileToValidate ftvi = iterator.next();
                this.entireLayout += ftvi.getTextLayout() + ftvi.getEntireLayout();
            }
        }
        return this.entireLayout;
    }

    public String getTextLayout() {
        if (this.textLayout == null) {
            this.textLayout = "";
        }

        return this.textLayout;
    }

    public void setTextLayout(final String textLayout) {
        this.textLayout = textLayout;
    }

    private void print(final String prefix, final boolean isTail) {
        if (this.validationType.isEmpty()) {
            this.setTextLayout(this.getTextLayout() + prefix + (isTail ? "└── " : "├── ") + this.docType + FileToValidate.LINE_SEPARATOR);
        } else {
            this.setTextLayout(this.getTextLayout() + prefix + (isTail ? "└── " : "├── ") + this.docType + " // Validateur : "
                    + this.validationType + FileToValidate.LINE_SEPARATOR);
        }
        if (this.fileToValidateList != null) {
            Iterator<FileToValidate> iterator = this.fileToValidateList.iterator();
            for (iterator = this.fileToValidateList.iterator(); iterator.hasNext(); ) {
                iterator.next().print(prefix + (isTail ? "    " : "│   "), !iterator.hasNext());
            }
        }
    }

    public String printInString(final String prefix, final boolean isTail) {

        if (this.validationType.isEmpty()) {
            this.setResult(this.getResult() + (prefix + (isTail ? "└── " : "├── ") + this.docType) + FileToValidate.LINE_SEPARATOR);
        } else {
            this.setResult(this.getResult() + (prefix + (isTail ? "└── " : "├── ") + this.docType) + " // Validator : " + this.validationType
                            + " FIN" + FileToValidate.LINE_SEPARATOR);
        }
        if (this.fileToValidateList != null) {
            Iterator<FileToValidate> iterator = this.fileToValidateList.iterator();
            for (iterator = this.fileToValidateList.iterator(); iterator.hasNext(); ) {
                final FileToValidate element = iterator.next();
                element.printInString(prefix + (isTail ? "    " : "│   "), !iterator.hasNext());
                this.setPreviousElement(element);
                if (iterator.hasNext() && element.getValidationType() != null && !element.getValidationType()
                        .isEmpty()) {
                    this.addElement(element);
                }
                if ("DOCUMENT".equals(element.getDocType())) {
                    this.addElement(element);
                }
            }
            if (!iterator.hasNext()) {
                final FileToValidate previousElementSelect = this.getPreviousElement();
                if (previousElementSelect != null && previousElementSelect.validationType != null
                        && !previousElementSelect.validationType.isEmpty()) {
                    this.addElement(previousElementSelect);
                }
            }
        }
        return "ok";
    }

    public String printStringToHtml(final String stringToHtml) {
        final StringBuilder sb = new StringBuilder();
        if (stringToHtml != null && !stringToHtml.isEmpty()) {

            final Scanner scanner = new Scanner(stringToHtml);
            while (scanner.hasNextLine()) {

                String line = scanner.nextLine();
                String color = null;
                int i = StringUtils.countMatches(line, "│   ");
                i = i + StringUtils.countMatches(line, "    ");
                if (i == 0) {
                    color = "#000040";
                } else if (i == 1) {
                    color = "#00006F";
                } else if (i == 2) {
                    color = "#00009F";
                } else if (i == 3) {
                    color = "#0000CF";
                } else if (i == 4) {
                    color = "#0000FF";
                } else if (i == 5) {
                    color = "#0F29FF";
                } else if (i == 6) {
                    color = "#1E52FF";
                } else if (i == 7) {
                    color = "#2E7BFF";
                }

                line = line.replace("└── ",
                        "<table border=\"1\" style=\"color:white;font-weight: bold;font-size: 14px;\" bgcolor=\""
                                + color + "\"><tr><td>");
                line = line.replace("├── ",
                        "<table border=\"1\" style=\"color:white;font-weight: bold;font-size: 14px;\" bgcolor=\""
                                + color + "\"><tr><td>");
                line = line.replace("│   ", "");
                line = line.replace("FIN", "</td></tr></table>");
                line = line.replace("    ", "");

                sb.append(line);
            }
            scanner.close();
        }
        this.setResult(sb.toString());
        return this.getResult();
    }

}
