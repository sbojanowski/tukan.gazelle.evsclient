/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.evs.client.common.menu.EVSMenu;
import net.ihe.gazelle.evs.client.common.model.EVSMenuGroup;
import net.ihe.gazelle.evs.client.common.model.EVSMenuGroupQuery;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandardQuery;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Name("menuGroupManager")
@Scope(ScopeType.PAGE)
public class MenuGroupManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1217417913264146712L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MenuGroupManager.class);

    private List<EVSMenuGroup> menuGroups;
    private EVSMenuGroup selectedMenuGroup;
    private List<ReferencedStandard> availableStandards;
    private List<ReferencedStandard> standards;
    private boolean displayList;

    public MenuGroupManager() {
    }

    @Create
    public void listMenuGroups() {
        final EVSMenuGroupQuery query = new EVSMenuGroupQuery();
        query.orderInGui().order(true);
        this.menuGroups = query.getList();
        this.reorderMenuInGui();
        this.displayList = true;
        this.selectedMenuGroup = null;
    }

    public void listStandards() {
        final ReferencedStandardQuery query = new ReferencedStandardQuery();
        query.services();
        query.label().order(true);
        this.standards = query.getList();
    }

    public void listAvailableStandards() {
        final ReferencedStandardQuery query = new ReferencedStandardQuery();
        query.services().available().eq(Boolean.TRUE);
        query.label().order(true);
        this.availableStandards = query.getList();
    }

    public void createNewGroup() {
        final EVSMenuGroup menuGroup = new EVSMenuGroup();
        menuGroup.setCreation(Boolean.TRUE);
        menuGroup.setOrderInGui(Integer.valueOf(this.getOrderInGuiMax() + 1));
        this.setSelectedMenuGroup(menuGroup);
    }

    public void deleteSelectedMenuGroup() {
        this.deleteMenuGroup(this.getSelectedMenuGroup());
    }
    public void deleteMenuGroup(final EVSMenuGroup inGroup) {
        if (!inGroup.getAvailable()) {
            final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            final EVSMenuGroup groupToDelete = entityManager.find(EVSMenuGroup.class, inGroup.getId());
            try {
                entityManager.remove(groupToDelete);
                entityManager.flush();
                this.listMenuGroups();
                EVSMenu.forceUpdate();
            } catch (final RuntimeException e) {
                MenuGroupManager.LOGGER.error(e.getMessage(), e);
                FacesMessages.instance()
                        .add(Severity.ERROR, "Unable to delete the menu group because of the following error: " + e.getMessage());
            }
        } else {
            FacesMessages.instance()
                    .add(Severity.ERROR, "You are trying to delete a menu which is currently used, set it \"not available\" and try again");
            this.setDisplayList(true);
            this.selectedMenuGroup = null;
        }
    }


    public void saveSelectedMenuGroup() {
        if (this.selectedMenuGroup != null) {
            try {
                this.selectedMenuGroup.setCreation(Boolean.FALSE);
                this.selectedMenuGroup.save();
                EVSMenu.forceUpdate();
                this.listMenuGroups();
            } catch (final PersistenceException e) {
                MenuGroupManager.LOGGER.error(e.getMessage(), e);
                FacesMessages.instance().add(Severity.ERROR, "Cannot save this menu group: " + e.getMessage());
            }
        }
    }

    public List<EVSMenuGroup> getMenuGroups() {
        return this.menuGroups;
    }

    public void setMenuGroups(final List<EVSMenuGroup> menuGroups) {
        if (menuGroups != null) {
            this.menuGroups = new ArrayList<EVSMenuGroup>(menuGroups);
        }
        else {
            this.menuGroups = null;
        }
    }

    public EVSMenuGroup getSelectedMenuGroup() {
        return this.selectedMenuGroup;
    }

    public void setSelectedMenuGroup(final EVSMenuGroup selectedMenuGroup) {
        this.selectedMenuGroup = selectedMenuGroup;
        this.listStandards();
        this.displayList = false;
    }

    public List<ReferencedStandard> getAvailableStandards() {
        return this.availableStandards;
    }

    public void setAvailableStandards(final List<ReferencedStandard> availableStandards) {
        if (availableStandards != null) {
            this.availableStandards = new ArrayList<ReferencedStandard>(availableStandards);
        }
        else {
            this.availableStandards = null;
        }
    }

    public List<ReferencedStandard> getStandards() {
        return this.standards;
    }

    public void setStandards(final List<ReferencedStandard> standards) {
        if (standards != null) {
            this.standards = new ArrayList<ReferencedStandard>(standards);
        }
        else {
            this.standards = null;
        }
    }

    public boolean isDisplayList() {
        return this.displayList;
    }

    public void setDisplayList(final boolean displayList) {
        this.displayList = displayList;
    }

    public void moveGroupUp(final EVSMenuGroup selectedMenuGroup){
        try {
            final Integer i = selectedMenuGroup.getOrderInGui();
            EVSMenuGroup tmp = this.menuGroups.get(i-1);
            tmp.setOrderInGui(Integer.valueOf(i - 1));
            tmp.save();
            tmp = this.menuGroups.get(i-2);
            tmp.setOrderInGui(i);
            tmp.save();
            this.listMenuGroups();
        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage(),e);
        }

    }

    public void moveGroupDown(final EVSMenuGroup selectedMenuGroup){
        try {
            final Integer i = selectedMenuGroup.getOrderInGui();
            EVSMenuGroup tmp = this.menuGroups.get(i-1);
            tmp.setOrderInGui(Integer.valueOf(i + 1));
            tmp.save();
            tmp = this.menuGroups.get(i);
            tmp.setOrderInGui(i);
            tmp.save();
            this.listMenuGroups();
        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage(),e);
        }
    }

    public Integer getOrderInGuiMax(){
        Integer maxOrderInGui = Integer.valueOf(0);
        final Iterator i = this.menuGroups.iterator();
        while (i.hasNext()) {
            final EVSMenuGroup menu = (EVSMenuGroup) i.next();
            if (menu.getOrderInGui() > maxOrderInGui){
                maxOrderInGui = menu.getOrderInGui();
            }
        }
        return maxOrderInGui;
    }

    public void reorderMenuInGui(){
        final Iterator i = this.menuGroups.iterator();
        Integer orderInGui = Integer.valueOf(1);
        while (i.hasNext()) {
            final EVSMenuGroup menu = (EVSMenuGroup) i.next();
            menu.setOrderInGui(orderInGui++);
        }

    }
}

