/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.util;

import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.dicom.model.DicomValidatedObject;
import net.ihe.gazelle.evs.client.hl7.model.HL7ValidatedMessage;
import net.ihe.gazelle.evs.client.pdf.model.ValidatedPdf;
import net.ihe.gazelle.evs.client.tls.model.ValidatedCertificates;
import net.ihe.gazelle.evs.client.xml.model.*;

public enum ValidatorType {

    ATNA("ATNA", "/atna/validator.seam?type=ATNA", "SYSLOG", ValidatedAuditMessage.class),

    AUDIT_MESSAGE("AUDIT_MESSAGE", "/atna/validator.seam?type=ATNA", "AUDIT_MESSAGE", ValidatedAuditMessage.class),

    CDA("CDA", "/cda/validator.seam?type=CDA", "CDA", CDAValidatedFile.class),

    DICOM("DICOM", "/dicom/validator.seam", "DICOM", DicomValidatedObject.class),

    DICOM_WEB("DICOM_WEB", "/dicom_web/validator.seam", "DICOM_WEB", ValidatedDICOMWeb.class),

    DSUB("DSUB", "/dsub/validator.seam?type=DSUB", "DSUB", ValidatedDSUB.class),

    HL7V2("HL7v2", "/hl7v2/validator.seam", "HL7", HL7ValidatedMessage.class),

    HL7V3("HL7v3", "/hl7v3/validator.seam?type=HL7v3", "HL7v3", HL7v3ValidatedMessage.class),

    HPD("HPD", "/hpd/validator.seam?type=HPD", "HPD", ValidatedHPD.class),

    PDF("PDF", "/pdf/validator.seam?type=PDF", "PDF", ValidatedPdf.class),

    SAML("SAML", "/saml/validator.seam?type=SAML", "SAML", ValidatedAssertion.class),

    SVS("SVS", "/svs/validator.seam?type=SVS", "SVS", ValidatedSVS.class),

    TLS("TLS", "/tls/validator.seam", "TLS", ValidatedCertificates.class),

    XDS("XDS", "/xds/validator.seam?type=XDS", "XDS", ValidatedXDS.class),

    XDW("XDW", "/xdw/validator.seam?type=XDW", "XDW", ValidatedXDW.class),

    XML("XML", "/xml/validator.seam?type=XML", "XML", ValidatedXML.class),

  // We are now replacing  WADO with DICOM_WEB. DICOM_WEB includes Wado, Qido and Stow
    @Deprecated
    WADO("WADO", "/wado/validator.seam?type=WADO", "WADO", ValidatedWADO.class),

    FHIR("FHIR", "/fhir/validator.seam?type=FHIR", "FHIR", ValidatedFHIR.class);

    private final String friendlyName;
    private final String view;
    private final String proxyType;
    private final Class<? extends ValidatedObject> entityClass;

    ValidatorType(final String friendlyName, final String view, final String proxyType, final Class<? extends ValidatedObject> entityClass) {
        this.friendlyName = friendlyName;
        this.view = view;
        this.proxyType = proxyType;
        this.entityClass = entityClass;
    }

    public String getFriendlyName() {
        return this.friendlyName;
    }

    public String getView() {
        return this.view;
    }

    public String getProxyType() {
        return this.proxyType;
    }

    public Class<? extends ValidatedObject> getEntityClass() {
        return this.entityClass;
    }

    public static ValidatorType getByProxyType(final String proxyType) {
        final ValidatorType[] values = ValidatorType.values();
        for (final ValidatorType validatorType : values) {
            if (validatorType.getProxyType().equalsIgnoreCase(proxyType)) {
                return validatorType;
            }
        }
        return null;
    }

    public static ValidatorType getByFriendlyName(final String friendlyName) {
        final ValidatorType[] values = ValidatorType.values();
        for (final ValidatorType validatorType : values) {
            if (validatorType.getFriendlyName().equalsIgnoreCase(friendlyName)) {
                return validatorType;
            }
        }
        return null;
    }

}
