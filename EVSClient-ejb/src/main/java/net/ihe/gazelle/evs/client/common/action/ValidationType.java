/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;

import net.ihe.gazelle.evs.client.common.model.ObjectForValidatorDetector;
import net.ihe.gazelle.hql.FilterLabel;

import javax.validation.constraints.NotNull;

@Entity
@Table(name = "validation_type", schema = "public")
@SequenceGenerator(name = "validation_type_sequence", sequenceName = "validation_type_id_seq", allocationSize = 1)
public class ValidationType implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5424740914575974827L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "validation_type_sequence")
    @XmlElement(name = "id")
    private Integer id;

    @Column(name = "file_type")
    protected FileType ft;

    @ManyToOne(targetEntity = ObjectForValidatorDetector.class)
    @JoinColumn(name = "object_for_validator_detector_id")
    private ObjectForValidatorDetector ObjectForVd;

    public ValidationType(final FileType ft, final ObjectForValidatorDetector ob) {
        this.ft = ft;
        this.ObjectForVd = ob;
    }

    public ValidationType() {
    }

    @FilterLabel
    public String getName() {
        return this.ft.name();
    }
}
