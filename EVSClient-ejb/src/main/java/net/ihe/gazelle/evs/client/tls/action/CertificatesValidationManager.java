/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.tls.action;

import net.ihe.gazelle.atna.action.pki.ws.*;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.EmailNotificationManager;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.common.model.ValidationService;
import net.ihe.gazelle.evs.client.tls.model.ValidatedCertificates;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.evs.client.util.ValidatorType;
import net.ihe.gazelle.pki.validator.client.CertificateValidatorServiceStub;
import net.ihe.gazelle.pki.validator.client.SOAPExceptionException;
import org.apache.axis2.Constants.Configuration;
import org.apache.commons.io.FileUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Name("certificatesValidationManager")
@Scope(ScopeType.PAGE)
public class CertificatesValidationManager implements Serializable {

    private static final long serialVersionUID = 7920478668366520753L;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CertificatesValidationManager.class);

    private ValidatedCertificates validatedCertificates;
    private List<ValidatedCertificates> validatedCertificatesMatrix;
    private ValidationService selectedValidationService;
    private boolean checkRevocation;
    private transient String selectedValidator;
    private transient File uploadedFile;
    private transient CertificateValidatorServiceStub stub;
    private boolean displayResult;
    private boolean displayResultMatrix;
    private boolean allValidators;
    private String extension = "";
    private ReferencedStandard selectedStandard;
    private boolean validationDone;
    private Boolean showMessageInGUI = Boolean.FALSE;
    private static final String ACCEPTED_FILE_TYPES = "pem, cert, txt";

    public String getAcceptedFileTypes() {
        String result = ApplicationPreferenceManager.getStringValue("X509_file_types");
        if (result != null) {
            return (result);
        }
        else {
            return CertificatesValidationManager.ACCEPTED_FILE_TYPES;
        }
    }
    public String getExtension() {
        return this.extension;
    }

    public void setExtension(final String standardDescription) {
        this.extension = standardDescription;
    }

    public void init() {
        if (this.selectedValidationService == null) {
            this.selectedValidationService = ValidationService.getValidationServiceByKeyword("TLS");
            if (this.selectedValidationService == null) {
                EmailNotificationManager.send(null, "Validation Service not declared",
                        "Validation Service with keyword TLS does not exist in the database");
                FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "gazelle.evs.client.service.seems.unavailable");
                return;
            }
        }

        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams.containsKey("id")) {
            this.validatedCertificates = ValidatedObject
                    .getObjectByID(ValidatedCertificates.class, Integer.valueOf(urlParams.get("id")), null);
            if (this.validatedCertificates == null) {
                FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "net.ihe.gazelle.evs.tls.notAValidIdErrorMessage");
            }
        } else if (urlParams.containsKey("oid")) {
            final ValidatedCertificates certificatesToValidateAgain = ValidatedObject
                    .getObjectByOID(ValidatedCertificates.class, urlParams.get("oid"), null);
            this.validatedCertificates = new ValidatedCertificates(certificatesToValidateAgain);
        }
        if (urlParams.containsKey("extension")) {
            this.setExtension(urlParams.get("extension"));
            final List<ReferencedStandard> selectedStandards = ReferencedStandard
                    .getReferencedStandardFiltered(ValidatorType.TLS.getFriendlyName(), null, this.extension, null);
            if (!selectedStandards.isEmpty()) {
                this.selectedStandard = selectedStandards.get(0);
            }
        }
    }

    public void reset() {
        this.selectedValidationService = ValidationService.getValidationServiceByKeyword("TLS");
        this.selectedValidator = null;
        this.displayResultMatrix = false;
        this.uploadedFile = null;
        this.validatedCertificates = null;
        this.setCheckRevocation(false);
        this.setAllValidators(false);
        this.setDisplayResult(false);

        this.setValidationDone(false);

    }

    public void uploadEventListener(final FileUploadEvent event) {
        FileOutputStream fos = null;
        try {
            this.validatedCertificates = ValidatedCertificates.createNewValidatedCertificates();
            final UploadedFile item = event.getUploadedFile();
            this.uploadedFile = new File(this.validatedCertificates.getPemPath());
            this.uploadedFile.getParentFile().mkdirs();

            this.validatedCertificates.setDescription(item.getName());
            if (item.getData() != null && item.getData().length > 0) {
                fos = new FileOutputStream(this.uploadedFile);
                fos.write(item.getData());
            } else {
                FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "gazelle.evs.client.cda.file.empty");
                this.validatedCertificates = null;
            }
        } catch (final IOException e) {
            CertificatesValidationManager.LOGGER.error("uploadEventListener: an error occurred - {}", e.getMessage());
            return;
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (final IOException e) {
                    CertificatesValidationManager.LOGGER.error("not able to close the FOS", e);
                }
            }
        }
    }

    public SelectItem[] getAvailableValidatorsItems() {

        final List<SelectItem> result = new ArrayList<>();

        final String[] validators = this.getAvailableValidators();

        for (final String str : validators) {
            result.add(new SelectItem(str, str));
        }
        return result.toArray(new SelectItem[result.size()]);
    }

    private String[] getAvailableValidators() {

        final String[] validators = this.getListOfValidators(this.getExtension());

        return validators;
    }

    public void validateMessage() {
        if (this.validatedCertificates == null) {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "gazelle.evs.client.tls.error.no.file");
            this.displayResult = false;
            return;
        } else if (this.validatedCertificates.getPemPath() == null || this.validatedCertificates.getPemPath()
                .isEmpty()) {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "gazelle.evs.client.cda.file.empty");
            this.displayResult = false;
            this.validatedCertificates = null;
            return;
        } else if (!this.isTextFile(this.validatedCertificates.getPemPath())) {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "gazelle.evs.client.tls.bin.file");
            this.displayResult = false;
        } else if (!this.allValidators && this.selectedValidationService == null) {
            CertificatesValidationManager.LOGGER.info("selectedValidationService = null");
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR,"gazelle.evs.client.hl7v3.error.no.service");
            this.displayResult = false;
            this.validatedCertificates = null;
            return;
        } else {

            if (this.allValidators) {
                this.validatedCertificatesMatrix = new ArrayList<>();
                for (final String validator : this.getAvailableValidators()) {
                    this.validatedCertificates = new ValidatedCertificates(this.validatedCertificates);
                    this.validateCertificate(validator);
                    this.validatedCertificatesMatrix.add(this.validatedCertificates);
                }
                this.displayResultMatrix = true;
                this.displayResult = false;
            } else {
                this.validateCertificate(this.selectedValidator);
                this.displayResult = true;
                this.displayResultMatrix = false;
            }

        }
    }

    private boolean isTextFile(final String pemPath) {
        try {
            final byte[] bytes = FileUtils.readFileToByteArray(new File(pemPath));
            for (final byte b : bytes) {
                if (b < 32 && b != '\r' && b != '\n') {
                    return false;
                }
            }
            return true;
        } catch (final IOException e) {
            return false;
        }
    }

    private String[] getListOfValidators(final String discriminator) {
        try {
            if (this.stub == null) {
                this.stub = new CertificateValidatorServiceStub(this.selectedValidationService.getWsdl());
                this.stub._getServiceClient().getOptions().setProperty(Configuration.DISABLE_SOAP_ACTION, Boolean.TRUE);
            }
            final GetListOfValidators getList = new GetListOfValidators();
            final GetListOfValidatorsE getListE = new GetListOfValidatorsE();
            getList.setDescriminator(discriminator);
            getListE.setGetListOfValidators(getList);
            final GetListOfValidatorsResponseE responseE = this.stub.getListOfValidators(getListE);
            final String[] getResponse = responseE.getGetListOfValidatorsResponse().getValidators();
            return getResponse;
        } catch (final RemoteException|SOAPExceptionException e) {
            CertificatesValidationManager.LOGGER.error("an unexpected error occurred{}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void validateCertificate(final String validator) {
        this.validatedCertificates.setUserIp(Util.getUserIpAddress());
        this.validatedCertificates.updateCountryStatistics();
        this.validatedCertificates.setService(this.selectedValidationService);
        this.validatedCertificates.setStandard(this.getSelectedStandard());
        this.validatedCertificates.setValidationContext(validator);

        this.validatedCertificates.setMetadata("");

        try {
            if (this.stub == null) {
                this.stub = new CertificateValidatorServiceStub(this.selectedValidationService.getWsdl());
                this.stub._getServiceClient().getOptions().setProperty(Configuration.DISABLE_SOAP_ACTION, Boolean.TRUE);
            }
            CertificatesValidationManager.LOGGER.info("create request to web service");

            final Validate param = new Validate();

            final File pemFile = new File(this.validatedCertificates.getPemPath());
            param.setCertificatesInPEMFormat(FileUtils.readFileToString(pemFile));

            param.setType(validator);
            param.setCheckRevocation(this.checkRevocation);

            final ValidateE validateE = new ValidateE();
            validateE.setValidate(param);
            final ValidateResponseE resultE = this.stub.validate(validateE);
            final ValidateResponse validateResponse = resultE.getValidateResponse();

            final CertificateValidatorResult result = validateResponse.getDetailedResult();
            final Date validationDate = new Date();

            String detailedResult = TLSValidatorFormatter
                    .getResultAsXml(result, validationDate, this.selectedValidationService, validator, this.checkRevocation);
            if (detailedResult == null) {
                detailedResult = TLSValidatorFormatter.getResultAsText(result);
            }

            this.validatedCertificates.setDetailedResult(detailedResult);
            this.validatedCertificates.setCertificates(result.getValidatedCertificates());
            this.validatedCertificates.setValidationStatus(result.getResult().toString());
            this.validatedCertificates.setValidationDate(validationDate);

        } catch (final IOException e) {
            CertificatesValidationManager.LOGGER
                    .error("an unexpected error occurred during the validation: {}", e.getMessage());
            this.updateAbortedValidation(e);
        }
        CertificatesValidationManager.LOGGER.info("validation done");
        this.setValidationDone(true);
        this.validatedCertificates = ValidatedCertificates.addOrUpdateCertificates(this.validatedCertificates);
    }

    public boolean getValidationDone() {
        return this.validationDone;
    }

    public void setValidationDone(final boolean validationDone) {
        this.validationDone = validationDone;
    }
    private void updateAbortedValidation(final Exception exception) {
        CertificatesValidationManager.LOGGER.info("validation cannot be performed: recording result");
        EmailNotificationManager.send(exception, "TLS Validation cannot be performed", null);
        this.validatedCertificates.setValidationStatus("VALIDATION NOT PERFORMED");
        this.validatedCertificates.setValidationDate(new Date());
    }

    public ValidatedCertificates getValidatedCertificates() {
        return this.validatedCertificates;
    }

    public void setValidatedCertificates(final ValidatedCertificates validatedCertificates) {
        this.validatedCertificates = validatedCertificates;
    }

    public ValidationService getSelectedValidationService() {
        return this.selectedValidationService;
    }

    public void setSelectedValidationService(final ValidationService selectedValidationService) {
        this.selectedValidationService = selectedValidationService;
    }

    public String getSelectedValidator() {
        return this.selectedValidator;
    }

    public void setSelectedValidator(final String selectedValidator) {
        this.selectedValidator = selectedValidator;
    }

    public File getUploadedFile() {
        return this.uploadedFile;
    }

    public void setUploadedFile(final File uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public CertificateValidatorServiceStub getStub() {
        return this.stub;
    }

    public void setStub(final CertificateValidatorServiceStub stub) {
        this.stub = stub;
    }

    public boolean isDisplayResult() {
        return this.displayResult;
    }

    public void setDisplayResult(final boolean displayResult) {
        this.displayResult = displayResult;
    }

    public boolean isCheckRevocation() {
        return this.checkRevocation;
    }

    public void setCheckRevocation(final boolean checkRevocation) {
        this.checkRevocation = checkRevocation;
    }

    public List<ValidatedCertificates> getValidatedCertificatesMatrix() {
        return this.validatedCertificatesMatrix;
    }

    public boolean isDisplayResultMatrix() {
        return this.displayResultMatrix;
    }

    public boolean isAllValidators() {
        return this.allValidators;
    }

    public void setAllValidators(final boolean allValidators) {
        this.allValidators = allValidators;
    }

    public ReferencedStandard getSelectedStandard() {
        return this.selectedStandard;
    }

    public void setSelectedStandard(final ReferencedStandard selectedStandard) {
        this.selectedStandard = selectedStandard;
    }
    public Boolean getShowMessageInGUI() {
        return this.showMessageInGUI;
    }

    public void setShowMessageInGUI(final Boolean showMessageInGUI) {
        this.showMessageInGUI = showMessageInGUI;
    }

    public void setMessageInvalidFileType(){
        FacesMessages.instance().add(Severity.ERROR,"Invalid file type. Expected file types are : "+ this
                .getAcceptedFileTypes());
    }
}
