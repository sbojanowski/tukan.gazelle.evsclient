/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.evs.client.analyzer.AnalyzerBean;

import javax.ejb.Local;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Local
@Path("/")
// base URI will be defined in the web.xml file
public interface GetValidationInfoLocal {

    @GET
    @Path("/GetValidationStatus")
    @Produces(GetValidationInfo.TEXT_PLAIN)
        // path of the service public
    String getValidationStatus(@QueryParam("oid") String oid,
            @DefaultValue("") @QueryParam(GetValidationInfo.CACHE) String queryCache);

    @GET
    @Path("/GetValidationDate")
    @Produces(GetValidationInfo.TEXT_PLAIN)
        // path of the service public
    String getValidationDate(@QueryParam("oid") String oid,
            @DefaultValue("") @QueryParam(GetValidationInfo.CACHE) String queryCache);

    @GET
    @Path("/GetValidationPermanentLink")
    @Produces(GetValidationInfo.TEXT_PLAIN)
    String getValidationPermanentLink(@QueryParam("oid") String oid,
            @DefaultValue("") @QueryParam(GetValidationInfo.CACHE) String queryCache);


    @GET
    @Path("/GetLastResultStatusByExternalId")
    @Produces(GetValidationInfo.TEXT_PLAIN)
    String getLastResultStatusByExternalId(@QueryParam("externalId") String externalId,
            @QueryParam(AnalyzerBean.TOOL_OID) String toolOid, @DefaultValue("") @QueryParam(GetValidationInfo.CACHE) String queryCache);

    @GET
    @Path("/GetValidationPermanentLinkByExternalId")
    @Produces(GetValidationInfo.TEXT_PLAIN)
    String getValidationPermanentLinkByExternalId(@QueryParam("externalId") String externalId,
            @QueryParam(AnalyzerBean.TOOL_OID) String toolOid, @DefaultValue("") @QueryParam(GetValidationInfo.CACHE) String queryCache);

    @GET
    @Path("/GetLastResultStatusAndPermanentLink")
    @Produces(GetValidationInfo.TEXT_PLAIN)
    String getLastResultStatusAndPermanentLink(@QueryParam("oid") String oid,
            @DefaultValue("") @QueryParam(GetValidationInfo.CACHE) String queryCache) ;

    @GET
    @Path("/GetLastResultStatusAndPermanentLinkByExternalId")
    @Produces(GetValidationInfo.TEXT_PLAIN)
    String getLastResultStatusAndPermanentLinkByExternalId(@QueryParam("externalId") String externalId,
            @QueryParam(AnalyzerBean.TOOL_OID) String toolOid, @DefaultValue("") @QueryParam(GetValidationInfo.CACHE) String queryCache) ;
}

// http://localhost:8080/EVSClient/resteasy/GetValidationStatus?oid=1.3.6.1.4.1.12559.11.1.2.1.4.25790
// http://localhost:8080/EVSClient/resteasy/GetValidationDate?oid=1.3.6.1.4.1.12559.11.1.2.1.4.25790
// http://localhost:8080/EVSClient/resteasy/GetValidationPermanentLink?oid=1.3.6.1.4.1.12559.11.1.2.1.4.25790
// http://192.168.20.101:8080/EVSClient/resteasy/GetLastResultStatus?proxyId=13127