/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.model.CallingTool;
import net.ihe.gazelle.evs.client.common.model.CallingToolQuery;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

@Name("callingToolManager")
@Scope(ScopeType.PAGE)
public  class CallingToolManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2378665063859659576L;

    private CallingTool newTool;
    private final List<CallingTool> callingTools = this.listCallingTools();
    private Filter filter;

    public CallingToolManager() {
    }

    @Create
    public  List<CallingTool> listCallingTools() {
        final CallingToolQuery query = new CallingToolQuery();
        query.label().order(true);
        return query.getList();
    }

    public FilterDataModel<CallingTool> listTools() {
        return new FilterDataModel<CallingTool>(CallingToolManager.this.getFilter()) {
            @Override
            protected Object getId(final CallingTool t) {
                return t.getId();
            }
        };
    }

    private Filter<CallingTool> getFilter() {

        if (this.filter == null) {
            final CallingToolQuery query = new CallingToolQuery();
            final HQLCriterionsForFilter<CallingTool> result = query.getHQLCriterionsForFilter();
            this.filter = new Filter<>(result);
        }
        return this.filter;

    }


    public CallingTool.ToolType[] getToolTypes() {
        return CallingTool.ToolType.values();
    }

    public void registerNewTool() {
        this.newTool = new CallingTool();
        this.newTool.setToolType(CallingTool.ToolType.OTHER);
    }

    public void updateTool(final CallingTool tool) {
        if (tool != null) {
            tool.save();
        }
    }

    public void saveNewTool() {
        if (this.newTool != null) {
            if (!this.checkLabelIsUnique(this.newTool.getLabel())) {
                FacesMessages.instance().addFromResourceBundle(Severity.ERROR,
                        "Not saved : Label must be unique. '" + this.newTool.getLabel() + "' already exist in the database");
            } else if (!this.checkOidIsUnique(this.newTool.getOid())) {
                FacesMessages.instance().addFromResourceBundle(Severity.ERROR,
                        "Not saved : OID must be unique. '" + this.newTool.getOid() + "' already exist in the database");
            } else {
                this.newTool.save();

            }
        }
        this.newTool = null;
    }


    public CallingTool getNewTool() {
        return this.newTool;
    }

    public void setNewTool(final CallingTool newTool) {
        this.newTool = newTool;
    }

    public  Boolean checkLabelIsUnique(final String label){
        final Iterator<CallingTool> iterator = this.callingTools.iterator();
        while (iterator.hasNext()){
            final CallingTool tmp = iterator.next();
            if( tmp.getLabel().compareTo(label) == 0 ){
                return  Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    public  Boolean checkOidIsUnique(final String oid) {
        final Iterator<CallingTool> iterator = this.callingTools.iterator();
        while (iterator.hasNext()){
            final CallingTool tmp = iterator.next();
            if( tmp.getOid().compareTo(oid) == 0 ){
                return  Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }
}
