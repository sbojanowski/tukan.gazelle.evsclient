/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.tls.action;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.action.AbstractLogBrowser;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.tls.model.ValidatedCertificates;
import net.ihe.gazelle.evs.client.tls.model.ValidatedCertificatesQuery;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("certificatesSearch")
@Scope(ScopeType.PAGE)
public class ValidatedCertificatesSearch extends AbstractLogBrowser<ValidatedCertificates> {

    /**
     *
     */
    private static final long serialVersionUID = 8910677688317736150L;

    @Override
    protected ValidatedCertificates getObjectByOID(final String oid, final String privacyKey) {
        return ValidatedObject.getObjectByOID(ValidatedCertificates.class, oid, privacyKey);
    }

    @Override
    protected Class<ValidatedCertificates> getEntityClass() {
        return ValidatedCertificates.class;
    }

    @Override
    protected HQLCriterionsForFilter<ValidatedCertificates> getCriterions() {
        final ValidatedCertificatesQuery query = new ValidatedCertificatesQuery();
        final HQLCriterionsForFilter<ValidatedCertificates> result = query.getHQLCriterionsForFilter();

        result.addPath("standard", query.standard().label());
        result.addPath("validationStatus", query.validationStatus());
        result.addPath("privateUser", query.privateUser());
        result.addPath("institutionKeyword", query.institutionKeyword());
        result.addPath("validationContext", query.validationContext());
        result.addPath("validationDate", query.validationDate());

        return result;
    }

    @Override
    public FilterDataModel<ValidatedCertificates> getDataModel() {
        return new FilterDataModel<ValidatedCertificates>(ValidatedCertificatesSearch.this.getFilter()) {
            @Override
            protected Object getId(final ValidatedCertificates t) {
                return t.getId();
            }
        };
    }
}
