/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.util;

import java.io.Serializable;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Stateful
@Name("menuUtil")
@Scope(ScopeType.SESSION)
@GenerateInterface("MenuUtilLocal")
public class MenuUtil implements Serializable, MenuUtilLocal {

    /**
     *
     */
    private static final long serialVersionUID = 7485366342974721903L;

    @Override
    public String getStatisticsUrl(final String validatorFriendlyName, final String extension) {
        final StringBuilder builder = new StringBuilder("/administration/statisticsByType.seam?type=");
        builder.append(validatorFriendlyName);
        if (extension != null && !extension.isEmpty()) {
            builder.append("&extension=");
            builder.append(extension);
        }
        return builder.toString();
    }

    @Override
    @Destroy
    @Remove
    public void destroy() {

    }

}
