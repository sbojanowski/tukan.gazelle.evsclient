/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.filter.HQLQueryBuilderForStatistics;
import net.ihe.gazelle.evs.client.dicom.model.DicomValidatedObject;
import net.ihe.gazelle.evs.client.users.action.SSOIdentity;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.apache.commons.lang.RandomStringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.annotations.Type;
import org.hibernate.criterion.Restrictions;
import org.jboss.seam.Instance;
import org.jboss.seam.security.Identity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <b>Class Description : </b>ValidatedObject<br>
 * <br>
 * This abstract class is intended to be extended by other classes, the ones will describe the various objects the EVS Client can be used for validating them (eg HL7v2, HL7V3, CDA and so ON)
 * <p/>
 * ValidatedObject possesses the following attributes :
 * <ul>
 * <li><b>oid</b> : the object OID given by the validator or by the client if none is sent back</li>
 * <li><b>user</b> : the user who called the service (if it was logged)</li>
 * <li><b>validationDate</b> : validation date</li>
 * <li><b>service</b> : validation service called</li>
 * <li><b>validationStatus</b> : main result of the validation (failed, passed)</li>
 * <li><b>validationContext</b> : validation context (actor, profile, messageType)</li>
 * <li><b>metadata</b> : some more information about the validated object
 * <li><b>detailedResults</b> : String representing the results of the validation
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, May 28th
 * @class ValidatedObject.java
 * @package net.ihe.gazelle.evs.client.common.model
 * @see Aberge@irisa.fr - http://www.ihe-europe.org
 */

@MappedSuperclass
public abstract class ValidatedObject implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7482942984080329263L;


    @Column(name = "oid")
    private String oid;



    @Column(name = "private_user")
    private String privateUser;

    @Column(name = "institution_keyword")
    private String institutionKeyword;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "validation_date")
    protected Date validationDate;

    @OneToOne
    @JoinColumn(name = "service_id")
    private ValidationService service;

    @OneToOne
    @JoinColumn(name = "standard_id")
    private ReferencedStandard standard;

    @Column(name = "user_ip")
    private String userIp;

    /**
     * validatationStatus value must be one of the ValidationStatus enum class (label attribute)
     */
    @Column(name = "validation_status")
    private String validationStatus;

    @Lob
    @Type(type = "text")
    @Column(name = "validation_context")
    private String validationContext;

    @Column(name = "description")
    protected String description;

    @Lob
    @Type(type = "text")
    @Column(name = "detailed_result")
    private String detailedResult;

    @Lob
    @Type(type = "text")
    @Column(name = "metadata")
    private String metadata;

    /**
     * this attribute is already set to true when the user is not logged when the user is logged, the default is false but the user is allowed to change it to make it public
     */
    @Column(name = "is_public")
    private Boolean isPublic ;

    /**
     * when isPublic = false, the user can choose to share this validation result with some others. A privacy key is generated and will be provided in the permanent link as an authentication process
     */
    @Column(name = "privacy_key")
    private String privacyKey;

    /**
     * Identifies the tool from which the file comes (if not EVSClient itself)
     */
    @Column(name = "tool_oid")
    private String toolOid;

    /**
     * Identifies the object in the external tool (if not uploaded in EVSClient)
     */
    @Column(name = "external_id")
    private String externalId;
    /**
     * Add the permanent link without application url
     */
    @Column(name = "permanent_link")
    private String beginPermanentLink;

    @Column(name = "proxy_type")
    private String proxyType;

    /**
     * Add MessageContentAnalyzer Oid if the file come from MessageContentAnalyzer
     */
    @Column(name = "message_content_analyzer_oid")
    private String messageContentAnalyzerOid;

    @Column(name = "lucky_validation")
    private Boolean lucky;

    /**
     * Constructor
     */
    public ValidatedObject() {

    }

    public ValidatedObject(final ValidatedObject vo) {
        if (vo == null) {
            return;
        }
        this.description = vo.description;
        this.detailedResult = vo.detailedResult;
        this.metadata = vo.metadata;
        this.oid = vo.oid;
        this.privateUser = vo.privateUser;
        this.institutionKeyword = vo.institutionKeyword;
        this.service = vo.service;
        this.standard = vo.standard;
        this.userIp = vo.getUserIp();
        this.validationContext = vo.validationContext;
        this.validationDate = vo.validationDate;
        this.validationStatus = vo.validationStatus;
        this.isPublic = vo.getIsPublic();
        this.privacyKey = vo.getPrivacyKey();
    //    this.updateCountryStatistics();
    }

    /**
     * Getters and Setters
     */

    public final boolean isLucky() {
        return this.lucky != null ? this.lucky : false;
    }

    public final void setLucky(final boolean lucky) {
        this.lucky = Boolean.valueOf(lucky);
    }

    public String getMessageContentAnalyzerOid() {
        return this.messageContentAnalyzerOid;
    }

    public final void setMessageContentAnalyzerOid(final String messageContentAnalyzerOid) {
        this.messageContentAnalyzerOid = messageContentAnalyzerOid;
    }

    public String getProxyType() {
        return this.proxyType;
    }

    public void setProxyType(final String proxyType) {
        this.proxyType = proxyType;
    }

    public String getBeginPermanentLink() {
        return this.beginPermanentLink;
    }

    public void setBeginPermanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final String tmp = this.permanentLink();
        this.beginPermanentLink = tmp.substring(applicationUrl.length());
    }

    public String getOid() {
        return this.oid;
    }


    public void setOid(final String oid) {
        this.oid = oid;
        this.setBeginPermanentLink();
    }

    public Date getValidationDate() {
        return this.validationDate != null ? (Date) this.validationDate.clone() : null;
    }

    public void setValidationDate(final Date validationDate) {
        this.validationDate = (Date) validationDate.clone();
    }

    public ValidationService getService() {
        return this.service;
    }

    public void setService(final ValidationService service) {
        this.service = service;
    }

    public String getValidationStatus() {
        return this.validationStatus;
    }

    public void setValidationStatus(final String validationStatus) {
        this.validationStatus = validationStatus;
    }

    public String getValidationContext() {
        return this.validationContext;
    }

    public void setValidationContext(final String validationContext) {
        this.validationContext = validationContext;
    }

    public ReferencedStandard getStandard() {
        return this.standard;
    }

    public void setStandard(final ReferencedStandard standard) {
        this.standard = standard;
    }

    public void setUserIp(final String userIp) {
        this.userIp = userIp;
    }

    public String getUserIp() {
        return this.userIp;
    }

    public final void setDescription(final String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDetailedResult(final String detailedResult) {
        this.detailedResult = detailedResult;
    }

    public String getDetailedResult() {
        return this.detailedResult;
    }

    public void setMetadata(final String metadata) {
        this.metadata = metadata;
    }

    public String getMetadata() {
        return this.metadata;
    }

    public String getPrivateUser() {
        return this.privateUser;
    }
    public void setPrivateUser(final String privateUser) {
        this.privateUser = privateUser;
    }


    public String getInstitutionKeyword() {
        return this.institutionKeyword;
    }
    public void setInstitutionKeyword(final String institutionKeyword) {
        this.institutionKeyword = institutionKeyword;
    }



    public void updateCountryStatistics() {
        if (this.userIp != null && ApplicationPreferenceManager.getBooleanValue("include_country_statistics")) {
            StatisticsPerCountry.updateCountryStatistics(this.userIp);
        }
    }

    @PrePersist
    // @PreUpdate
    public void init() {
        SSOIdentity identity = SSOIdentity.instance();
        if (identity.isLoggedIn()) {
            this.setPrivateUser(identity.getCredentials().getUsername());
            this.setInstitutionKeyword(identity.getInstitutionKeyword());
            this.setIsPublic(Boolean.FALSE);
        } else {
            this.setPrivateUser(null);
            this.setInstitutionKeyword(null);
            this.setIsPublic(Boolean.TRUE);
        }
    }

    public static <T extends ValidatedObject> List<Object[]> getStatisticsPerStatus(final Class<T> clazz,
            final EntityManager entityManager) {
        ValidatedObject.removeNullEntries(clazz);
        final HQLQueryBuilder<T> query = new HQLQueryBuilder<>(entityManager, clazz);
        final Object[] count = { "Total", Integer.valueOf(query.getCount()) };
        final List<Object[]> stats = query.getStatistics("validationStatus");
        stats.add(0, count);
        return stats;
    }

    public static <T extends ValidatedObject> T save(final Class<T> clazz, final T objectToSave) {
        final EntityManager entityManager = EntityManagerService.provideEntityManager();
        final T objectToSave1 = entityManager.merge(objectToSave);
        entityManager.flush();
        return objectToSave1;
    }

    @SuppressWarnings("unchecked")
    public static <T extends ValidatedObject> T getObjectByID(final Class<T> clazz, final Integer id, final String privacyKey) {
        final EntityManager em = EntityManagerService.provideEntityManager();
        final Session s = (Session) em.getDelegate();
        final Criteria c = s.createCriteria(clazz);
        c.add(Restrictions.eqOrIsNull("id", id));
        // ABE: if a privacy key is provided, it must match with the one in DB
        if (privacyKey != null && !privacyKey.isEmpty()) {
            c.add(Restrictions.eqOrIsNull("privacyKey", privacyKey));
        }
        // ABE: otherwise, security checks are applied
        else {
            ValidatedObject.addSecurityRestriction(c);
        }
        try {
            return (T) c.uniqueResult();
        } catch (final HibernateException e) {
            return null;
        }
    }

    public static <T extends ValidatedObject> HQLQueryBuilderForStatistics<T> instanciateHQLQueryBuilder(final Class<T> clazz,
            final EntityManager entityManager) {
        return new HQLQueryBuilderForStatistics<>(entityManager, clazz);
    }

    public static void addSecurityRestriction(final Criteria c) {
        boolean monitor = false;
        String login = null;

        final Identity instance = Identity.instance();
        if (instance != null && Identity.instance().isLoggedIn()) {
            login = Identity.instance().getCredentials().getUsername();
            if (instance.hasRole("monitor_role")|| instance.hasRole("admin_role")) {
                monitor = true;
            }
        }

        // show all messages if monitor else show only public results and those performed by the logged in user
        if (!monitor) {
            if (login == null) {
                c.add(Restrictions.eqOrIsNull("isPublic", Boolean.TRUE));
            } else {
                c.add(Restrictions.or(Restrictions.eqOrIsNull("isPublic", Boolean.TRUE), Restrictions.eqOrIsNull("privateUser", login)));
            }
        }
    }

    /**
     * @param <T>        Object to return (must be a class extending ValidatedObject)
     * @param clazz      Class of the object to return

     */
    public static <T extends ValidatedObject> T getObjectByOID(final Class<T> clazz, final String inOID, final String privacyKey) {
        if (inOID == null || inOID.length() == 0) {
            return null;
        }

        final EntityManager em = EntityManagerService.provideEntityManager();
        final Session s = (Session) em.getDelegate();
        final Criteria c = s.createCriteria(clazz);
        c.add(Restrictions.ilike("oid", inOID));
        // ABE: if a privacy key is provided, it must match with the one in DB
        if (privacyKey != null && !privacyKey.isEmpty()) {
            c.add(Restrictions.eqOrIsNull("privacyKey", privacyKey));
        }
        // ABE: otherwise, security checks are applied
        else {
            ValidatedObject.addSecurityRestriction(c);
        }
        @SuppressWarnings("unchecked")
        final List<T> objects = (List<T>) c.list();
        return !objects.isEmpty() ? objects.get(0) : null;
    }

    public static <T extends ValidatedObject> void removeNullEntries(final Class<T> clazz) {
        final EntityManager entityManager = EntityManagerService.provideEntityManager();
        final HQLQueryBuilder<T> queryBuilder = new HQLQueryBuilder<>(entityManager, clazz);
        queryBuilder.addRestriction(HQLRestrictions.isNull("oid"));
        final List<T> nullObjects = queryBuilder.getList();
        for (final T object : nullObjects) {
            entityManager.remove(object);
        }
        entityManager.flush();
    }

    public abstract String getFilePath();

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.oid == null ? 0 : this.oid.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ValidatedObject other = (ValidatedObject) obj;
        if (this.oid == null) {
            if (other.oid != null) {
                return false;
            }
        } else if (!this.oid.equals(other.oid)) {
            return false;
        }
        return true;
    }

    public abstract String permanentLink();

    public Boolean getIsPublic() {

        if (this.isPublic == null) {
            this.isPublic = Boolean.TRUE;
        }
        return this.isPublic;
    }

    public void setIsPublic(final Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public String getPrivacyKey() {
        return this.privacyKey;
    }

    public void setPrivacyKey(final String privacyKey) {
        this.privacyKey = privacyKey;
    }

    /**
     * generate a privacy key made of 16 alphanumrical characters
     */
    public void generatePrivacyKey() {
        this.setPrivacyKey(RandomStringUtils.randomAlphanumeric(16));
    }

    public String getToolOid() {
        return this.toolOid;
    }

    public void setToolOid(final String toolOid) {
        this.toolOid = toolOid;
    }

    public String getExternalId() {
        return this.externalId;
    }

    public void setExternalId(final String externalId) {
        this.externalId = externalId;
    }

    public boolean isDicomValidatedObject(final ValidatedObject vo) {
        boolean result = false;
        if (vo instanceof DicomValidatedObject) {
            result = true;
        }
        return result;
    }

    public String getFileContent() {
        return Util.getFileContentToString(this.getFilePath());
    }


    public void setFileContent() {

    }
}