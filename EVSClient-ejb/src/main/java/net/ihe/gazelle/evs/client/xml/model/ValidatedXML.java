/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.util.Util;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;

@Entity
@Name("validatedXML")
@Table(name = "validated_xml", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "validated_xml_sequence", sequenceName = "validated_xml_id_seq", allocationSize = 1)
public class ValidatedXML extends AbstractValidatedXMLFile {

    private static final long serialVersionUID = -1443811873360866107L;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ValidatedXML.class);


    private static final String XML_FILE_PREFIX = "validatedXML_";
    private static final String XML_FILE_SUFFIX = ".xml";

    @Id
    @NotNull
    @GeneratedValue(generator = "validated_xml_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "xml_path")
    private String xmlPath;

    @Column(name = "called_method")
    private String schematron;


    public ValidatedXML(final ValidatedXML inXMLObject) {
        this.xmlPath = inXMLObject.getXMLPath();
        this.description = inXMLObject.getDescription();
    }

    public ValidatedXML() {

    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getXMLPath() {
        return this.xmlPath;
    }

    public void setXMLPath(final String xmlPath) {
        this.xmlPath = xmlPath;
    }

    @Override
    public void setFilePath(final String xmlPath) {
        this.xmlPath = xmlPath;
    }

    /**
     */
    public static ValidatedXML createNewValidatedXML() {
        ValidatedXML message = new ValidatedXML();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        message = em.merge(message);
        em.flush();
        if (message == null || message.getId() == 0) {
            ValidatedXML.LOGGER.error("An error occurred when creating a new ValidatedAuditMessage object");
            return null;
        }
        final String repository = ApplicationPreferenceManager.getStringValue("xml_repository");
        message.setXMLPath(
                repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ValidatedXML.XML_FILE_PREFIX
                        .concat(message.getId().toString()).concat(
                        ValidatedXML.XML_FILE_SUFFIX));
        return message;
    }

    /**
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends AbstractValidatedXMLFile> T addOrUpdate(final Class<T> clazz) {
        if (this.getXMLPath() == null || this.getXMLPath().isEmpty()) {
            ValidatedXML.LOGGER.error("The file path is missing, cannot add this object !");
            return (T) this;
        } else {
            final File uploadedFile = new File(this.getXMLPath());
            if (!uploadedFile.exists()) {
                ValidatedXML.LOGGER.error("The given file does not exist: {}", this.getXMLPath());
                return (T) this;
            }
            if (this.getOid() == null || this.getOid().length() == 0) {
                this.setOid(OIDGenerator.getNewOid());
            }
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            final ValidatedXML validatedFile = em.merge(this);
            em.flush();

            //Update the cache with the newest values
            GazelleValidationCacheManager
                    .refreshGazelleCacheWebService(validatedFile.getExternalId(), validatedFile.getToolOid(),
                            validatedFile.getOid(), "off");

            return (T) validatedFile;
        }
    }

    @Override
    public String getFilePath() {
        return this.xmlPath;
    }

    @Override
    public String getSchematron() {
        return this.schematron;
    }

    @Override
    public void setSchematron(final String schematron) {
        this.schematron = schematron;
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/detailedResult.seam?type=XML").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

}
