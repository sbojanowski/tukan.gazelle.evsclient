/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.model;

import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.common.model.ValidationService;
import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;

@MappedSuperclass
public abstract class AbstractValidatedXMLFile extends ValidatedObject {

    private static final long serialVersionUID = 4338772809653957662L;
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractValidatedXMLFile.class);


    @OneToOne
    @JoinColumn(name = "mbv_service_id")
    protected ValidationService mbValidationService;

    @Column(name = "model_based_validator")
    protected String mbvalidator;

    @Lob
    @Type(type = "text")
    @Column(name = "mbv_detailed_result")
    protected String mbvDetailedResult;

    public AbstractValidatedXMLFile() {
    }

    public abstract String getSchematron();

    public abstract void setSchematron(String schematron);

    public String getMbvalidator() {
        return this.mbvalidator;
    }

    public void setMbvalidator(final String mbvalidator) {
        this.mbvalidator = mbvalidator;
    }

    public String getMbvDetailedResult() {
        return this.mbvDetailedResult;
    }

    public void setMbvDetailedResult(final String mbvDetailedResult) {
        this.mbvDetailedResult = mbvDetailedResult;
    }

    public abstract <T extends AbstractValidatedXMLFile> T addOrUpdate(Class<T> clazz);

    public abstract void setFilePath(String filePath);

    public ValidationService getMbValidationService() {
        return this.mbValidationService;
    }

    public void setMbValidationService(final ValidationService mbValidationService) {
        this.mbValidationService = mbValidationService;
    }

}
