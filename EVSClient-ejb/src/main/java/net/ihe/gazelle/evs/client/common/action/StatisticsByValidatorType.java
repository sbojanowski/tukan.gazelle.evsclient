/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.evs.client.common.filter.HQLQueryBuilderForStatistics;
import net.ihe.gazelle.evs.client.common.menu.EVSMenu;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.util.ValidatorType;
import net.ihe.gazelle.evs.client.xml.model.AbstractValidatedXMLFile;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.LocaleSelector;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Name("statisticsForValidator")
@Scope(ScopeType.PAGE)
public class StatisticsByValidatorType implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1494173678944299276L;
    public static final String VALIDATION_DATE = "validationDate";
    public static final String ENTITY_MANAGER = "entityManager";
    public static final String SCHEMATRON = "schematron";
    public static final String STANDARD = EVSMenu.STANDARD_PARAM;
    public static final String EXTENSION = EVSMenu.EXTENSION_PARAM;
    public static final String PRIVATE_USER = "privateUser";
    public static final String INSTITUTION_KEYWORD = "institutionKeyword";
    public static final String TYPE = EVSMenu.TYPE_PARAM;
    private transient ValidatorType validatorType;
    private transient HQLQueryBuilderForStatistics<? extends ValidatedObject> queryBuilder;

    private static final Logger LOGGER = LoggerFactory.getLogger(StatisticsByValidatorType.class);

    private Calendar startDate;
    private Calendar endDate;

    private String specialStatLabel;
    private boolean displaySpecialStatPanel;
    private ReferencedStandard refStandard;
    private String extensionParam;


    public StatisticsByValidatorType() {
    }

    protected enum Interval {


        MONTH(new SimpleDateFormat("MMM yy", LocaleSelector.instance().getLocale())), YEAR(
                new SimpleDateFormat("yyyy", LocaleSelector.instance().getLocale()));

        Interval(final SimpleDateFormat sdf) {
            this.dateFormatter = sdf;
        }

        private final SimpleDateFormat dateFormatter;

        public SimpleDateFormat getDateFormatter() {
            return this.dateFormatter;
        }
    }

    @Create
    public void init() {
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams.containsKey(TYPE)) {
            this.validatorType = ValidatorType.getByFriendlyName(urlParams.get(TYPE));
            this.refStandard = null;
            if (urlParams.containsKey(StatisticsByValidatorType.EXTENSION)) {
                this.extensionParam = urlParams.get(StatisticsByValidatorType.EXTENSION);
            } else {
                this.extensionParam = null;
            }
            String standardLabel = urlParams.get(EVSMenu.STANDARD_PARAM);
            final List<ReferencedStandard> standards = ReferencedStandard
                    .getReferencedStandardFiltered(this.validatorType.getFriendlyName(), null, this.extensionParam, standardLabel);
            if (standards != null && !standards.isEmpty()) {
                this.refStandard = standards.get(0);
                this.extensionParam = this.refStandard.getExtension();
            }
        } else {
            this.validatorType = null;
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "net.ihe.gazelle.evs.NoValidatedTypeProvided");
        }
        this.initializeQueryBuilder();
    }

    public void initializeQueryBuilder() {
        final EntityManager entityManager = (EntityManager) Component.getInstance(StatisticsByValidatorType.ENTITY_MANAGER);
        if (this.validatorType != null) {
            this.queryBuilder = ValidatedObject.instanciateHQLQueryBuilder(this.validatorType.getEntityClass(), entityManager);
            if (this.startDate != null) {
                this.queryBuilder.addRestriction(HQLRestrictions.ge(StatisticsByValidatorType.VALIDATION_DATE, this.startDate
                        .getTime()));
            }
            if (this.endDate != null) {
                this.queryBuilder.addRestriction(HQLRestrictions.lt(StatisticsByValidatorType.VALIDATION_DATE, this.endDate.getTime()));
            }
            if (this.refStandard != null) {
                this.queryBuilder.addEq(StatisticsByValidatorType.STANDARD, this.refStandard);
            }
        } else {
            this.queryBuilder = null;
        }
    }

    public void reset() {
        this.startDate = null;
        this.endDate = null;
        this.initializeQueryBuilder();
    }

    public List<Object[]> getStatisticsPerStatus() {
        // we DO need to reinitialize the querybuilder otherwise some criteria are set and we will not retrieve the expected result !!!
        this.initializeQueryBuilder();
        return this.queryBuilder.getStatistics("validationStatus");
    }

    public List<Object[]> getStatisticsPerPeriod(final StatisticsByValidatorType.Interval interval) {
        // queryBuilder DOES need to be reset otherwise we are using filters previously applied and we do not want that
        this.initializeQueryBuilder();
        // if the startDate attribute is not valued, retrieve the date of the oldest validation for the given criteria
        if (this.startDate == null) {
            this.setStartDate(this.queryBuilder.getMinDate());
            if (this.startDate == null) {
                this.setStartDate(new Date());
            }

        }
        // if the endDate attribute is not values, set it as today
        if (this.endDate == null) {
            this.setEndDate(new Date());
        }

        final Locale locale = LocaleSelector.instance().getLocale();

        final Calendar intervalBegin = Calendar.getInstance(locale);
        final Calendar intervalEnd = Calendar.getInstance(locale);

        intervalBegin
                .set(this.startDate.get(Calendar.YEAR), this.startDate.get(Calendar.MONTH), this.startDate.get(Calendar.DAY_OF_MONTH),
                        0, 0);

        switch (interval) {
            case MONTH:
                // first interval begining of the firstMonth at the end of the current month
                intervalBegin.set(Calendar.DAY_OF_MONTH, 1);
                intervalEnd.set(intervalBegin.get(Calendar.YEAR), intervalBegin.get(Calendar.MONTH),
                        intervalBegin.get(Calendar.DAY_OF_MONTH), 0, 0);
                if (intervalEnd.get(Calendar.MONTH) == Calendar.DECEMBER) {
                    intervalEnd.add(Calendar.YEAR, 1);
                    intervalEnd.set(Calendar.MONTH, Calendar.JANUARY);
                } else {
                    intervalEnd.add(Calendar.MONTH, 1);
                }
                break;
            case YEAR:
                // first interval ends at the end of the current year
                intervalEnd.set(Calendar.DAY_OF_YEAR, 1);
                intervalEnd.set(Calendar.YEAR, this.startDate.get(Calendar.YEAR) + 1);
                intervalEnd.set(Calendar.HOUR_OF_DAY, 0);
                intervalEnd.set(Calendar.MINUTE, 0);
                intervalEnd.set(Calendar.SECOND, 0);
                break;
            default:
                StatisticsByValidatorType.LOGGER.info("interval : not selected");
                break;
        }

        HQLQueryBuilderForStatistics<? extends ValidatedObject> localQueryBuilder;
        final EntityManager entityManager = (EntityManager) Component.getInstance(StatisticsByValidatorType.ENTITY_MANAGER);
        final List<Object[]> statistics = new ArrayList<>(100);

        while (intervalBegin.before(this.endDate)) {
            // recreate a query builder without the restrictions ON dates
            localQueryBuilder = ValidatedObject
                    .instanciateHQLQueryBuilder(this.validatorType.getEntityClass(), entityManager);
            localQueryBuilder.addEq(StatisticsByValidatorType.STANDARD, this.refStandard);
            localQueryBuilder.addRestriction(HQLRestrictions
                    .and(HQLRestrictions.ge(StatisticsByValidatorType.VALIDATION_DATE, intervalBegin.getTime()),
                            HQLRestrictions.lt(StatisticsByValidatorType.VALIDATION_DATE, intervalEnd.getTime())));
            final String dateLabel = interval.getDateFormatter().format(intervalBegin.getTime());

            final Object[] stat = {dateLabel, Integer.valueOf(localQueryBuilder.getCount())};
            statistics.add(stat);

            intervalBegin.set(intervalEnd.get(Calendar.YEAR), intervalEnd.get(Calendar.MONTH),
                    intervalEnd.get(Calendar.DATE), intervalEnd.get(Calendar.HOUR_OF_DAY),
                    intervalEnd.get(Calendar.MINUTE), intervalEnd.get(Calendar.SECOND));

            switch (interval) {
                case MONTH:
                    if (intervalEnd.get(Calendar.MONTH) == Calendar.DECEMBER) {
                        intervalEnd.add(Calendar.YEAR, 1);
                        intervalEnd.set(Calendar.MONTH, Calendar.JANUARY);
                    } else {
                        intervalEnd.add(Calendar.MONTH, 1);
                    }
                    break;
                case YEAR:
                    intervalEnd.add(Calendar.YEAR, 1);
                    break;
                default:
                    break;

            }
        }
        return statistics;
    }

    public List<Object[]> getStatisticsPerIp() {
        if (Identity.instance().hasRole("admin_role")) {
            final List<Object[]> stats = this.queryBuilder.getStatistics("userIp");
            if (!stats.isEmpty()) {
                Collections.sort(stats, StatisticsByValidatorType.STATISTICS_COMPARATOR);
            }
            return stats;
        } else {
            return new ArrayList<>();
        }
    }

    public List<Object[]> getStatisticsPerUser() {
        if (Identity.instance().hasRole("admin_role")) {
            this.queryBuilder.addRestriction(HQLRestrictions.isNotNull(StatisticsByValidatorType.PRIVATE_USER));
            final List<Object[]> stats = this.queryBuilder.getStatistics(StatisticsByValidatorType.PRIVATE_USER);
            Collections.sort(stats, StatisticsByValidatorType.STATISTICS_COMPARATOR);
            return stats;
        } else {
            return new ArrayList<>();
        }
    }

    public List<Object[]> getStatisticsPerValidationService() {
        this.queryBuilder.addRestriction(HQLRestrictions.isNotNull("service"));
        final List<Object[]> stats = this.queryBuilder.getStatistics("service.name");
        if (AbstractValidatedXMLFile.class.isAssignableFrom(this.validatorType.getEntityClass())) {
            // EVSCLT-584: initialize again query builder to compute statistics for mbValidationService. We do not
            // want a filter "is not null service AND is not null mbValidationService", we are only counting validation performed
            // with mbValidationService !!!
            initializeQueryBuilder();
            this.queryBuilder.addRestriction(HQLRestrictions.isNotNull("mbValidationService"));
            List<Object[]> statsToAdd = this.queryBuilder.getStatistics("mbValidationService.name");
            if (!statsToAdd.isEmpty()) {
                stats.addAll(statsToAdd);
            }
        }
        Collections.sort(stats, StatisticsByValidatorType.STATISTICS_COMPARATOR);
        return stats;
    }

    public List<Object[]> getSpecialStatistics() {
        List<Object[]> stats = new ArrayList<>();
        this.initializeQueryBuilder();
        this.displaySpecialStatPanel = false;
        this.specialStatLabel = "";
        switch (this.validatorType) {
            case ATNA:
            case CDA:
            case HL7V3:
            case XML:
            case SAML:
            case FHIR:
                this.queryBuilder.addRestriction(HQLRestrictions.isNotNull(StatisticsByValidatorType.SCHEMATRON));
                stats = this.queryBuilder.getStatistics(StatisticsByValidatorType.SCHEMATRON);
                break;
            case HL7V2:
                this.queryBuilder.addRestriction(HQLRestrictions.isNotNull("profileOid"));
                stats = this.queryBuilder.getStatistics("profileOid");
                this.displaySpecialStatPanel = true;
                this.specialStatLabel = "Profile OID";
                break;
            case TLS:
                this.queryBuilder.addRestriction(HQLRestrictions.isNotNull("validationContext"));
                stats = this.queryBuilder.getStatistics("validationContext");
                this.displaySpecialStatPanel = true;
                this.specialStatLabel = "Validator";
                break;
            case DICOM:
                this.queryBuilder.addRestriction(HQLRestrictions.isNotNull("dicomOperation"));
                stats = this.queryBuilder.getStatistics("dicomOperation");
                this.displaySpecialStatPanel = true;
                this.specialStatLabel = "Dicom Operation";
                break;
            default:
                break;
        }
        if (AbstractValidatedXMLFile.class.isAssignableFrom(this.validatorType.getEntityClass())) {
            // EVSCLT-584: initialize again query builder to compute statistics for mbValidationService. We do not
            // want a filter "is not null service AND is not null mbValidationService", we are only counting validation performed
            // with mbValidationService !!!
            initializeQueryBuilder();
            this.queryBuilder.addRestriction(HQLRestrictions.isNotNull("mbvalidator"));
            this.displaySpecialStatPanel = true;
            this.specialStatLabel = "Schematron/Model-Based validator";
            List<Object[]> statsToAdd = this.queryBuilder.getStatistics("mbvalidator");
            if (!statsToAdd.isEmpty()) {
                stats.addAll(statsToAdd);
            }
        }
        Collections.sort(stats, StatisticsByValidatorType.STATISTICS_COMPARATOR);
        return stats;
    }

    // set start date at the selected date 0:00 am
    public void setStartDate(final Date date) {
        if (date != null) {
            this.startDate = Calendar.getInstance();
            this.startDate.setTime(date);
            this.startDate.set(Calendar.HOUR_OF_DAY, 0);
            this.startDate.set(Calendar.MINUTE, 0);
            this.startDate.set(Calendar.SECOND, 0);
            if (this.endDate != null) {
                if (this.startDate.compareTo(this.endDate) > 0) {
                    this.endDate = (Calendar) this.startDate.clone();
                }
            }
        } else {
            this.startDate = null;
        }

    }

    // set end date at the day AFTER the selected date 0:00am
    public void setEndDate(final Date date) {
        if (date != null) {
            this.endDate = Calendar.getInstance();
            this.endDate.setTime(date);
            this.endDate.set(Calendar.HOUR_OF_DAY, 0);
            this.endDate.set(Calendar.MINUTE, 0);
            this.endDate.set(Calendar.SECOND, 0);
            this.endDate.add(Calendar.DAY_OF_YEAR, +1);
            if (this.startDate != null) {
                if (this.endDate.compareTo(this.startDate) < 0) {
                    this.startDate = (Calendar) this.endDate.clone();
                }
            }
        } else {
            this.endDate = null;
        }

    }

    /**
     * This comparator is used to sort the statistics by decreasing result
     */
    protected static final Comparator<Object[]> STATISTICS_COMPARATOR = new Comparator<Object[]>() {

        @Override
        public int compare(final Object[] o1, final Object[] o2) {
            // escape apostrophe
            o1[0] = ((String) o1[0]).replace("'", "&#39;");
            return ((Number) o2[1]).intValue() - ((Number) o1[1]).intValue();
        }

    };

    public Date getStartDate() {
        if (this.startDate != null) {
            return this.startDate.getTime();
        } else {
            return null;
        }
    }

    public Date getEndDate() {
        if (this.endDate != null) {
            return this.endDate.getTime();
        } else {
            return null;
        }
    }

    public StatisticsByValidatorType.Interval getMONTH() {
        return StatisticsByValidatorType.Interval.MONTH;
    }

    public StatisticsByValidatorType.Interval getYEAR() {
        return StatisticsByValidatorType.Interval.YEAR;
    }

    public ValidatorType getValidatorType() {
        return this.validatorType;
    }

    public String getSpecialStatLabel() {
        return this.specialStatLabel;
    }

    public boolean isDisplaySpecialStatPanel() {
        return this.displaySpecialStatPanel;
    }

    public String getExtensionParam() {
        return this.extensionParam;
    }
}
