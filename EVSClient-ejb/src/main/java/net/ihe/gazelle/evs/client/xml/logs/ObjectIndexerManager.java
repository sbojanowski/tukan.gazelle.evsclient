/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.logs;

import net.ihe.gazelle.com.templates.TemplateId;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Name("objectIndexerManager")
@Scope(ScopeType.PAGE)
public class ObjectIndexerManager implements Serializable {

    /**
     *
     */

    private static final long serialVersionUID = -2123396953355739969L;

    private Filter filter;
    private final  List<ObjectIndexer> objectIndexList = this.getObjectIndexList();

    public ObjectIndexerManager() {
    }

    @Create
    public  List<ObjectIndexer> getObjectIndexList() {
        final ObjectIndexerQuery q = new ObjectIndexerQuery();
        q.id().order(true);
        return q.getList();
    }

    public FilterDataModel<ObjectIndexer> listAllTemplates() {
        return new FilterDataModel<ObjectIndexer>(ObjectIndexerManager.this.getFilter()) {
            @Override
            protected Object getId(final ObjectIndexer t) {
                return t.getId();
            }
        };
    }

    private Filter<ObjectIndexer> getFilter() {

        if (this.filter == null) {
            final ObjectIndexerQuery query = new ObjectIndexerQuery();
            final HQLCriterionsForFilter<ObjectIndexer> result = query.getHQLCriterionsForFilter();
            this.filter = new Filter<>(result);
        }
        return this.filter;

    }

    public List<ObjectIndexer> addTemplatesIfNotExist(final List<TemplateId> tIdList, final List<ObjectIndexer> objectIndexers,
            final String extension) {

        ObjectIndexer objIndexer;
        for (final TemplateId t : tIdList) {
            objIndexer = new ObjectIndexer();
            objIndexer.setIdentifier(t.getId());
            objIndexer.setName(t.getName());
            objIndexer.setType("template");
            objIndexer.setExtension(extension);

            objectIndexers.add(objIndexer);
        }
        return objectIndexers;
    }

    public List<ObjectIndexer> addTemplatesIfNotExistFromXpath(final List<String> results, final String extension) {
        ObjectIndexer objIndexer;
        final List<ObjectIndexer> objectIndexers = new ArrayList<>();
        if (results != null && !results.isEmpty()) {
            for (final String templateId : results) {
                objIndexer = new ObjectIndexer();
                final String templateName = "UNKNOWN";
                objIndexer.setIdentifier(templateId);
                objIndexer.setName(templateName);
                objIndexer.setType("template");
                objIndexer.setExtension(extension);

                objectIndexers.add(objIndexer);
            }
        }
        return objectIndexers;
    }

    public List<ObjectIndexer> removeDuplicate(final List<ObjectIndexer> objectIndexers) {
        final HashSet<ObjectIndexer> noDups = new HashSet<>();
        final List<ObjectIndexer> result = new ArrayList<>();

        for (final ObjectIndexer item : objectIndexers) {
            if (!noDups.contains(item)) {
                result.add(item);
                noDups.add(item);
            }
        }
        return result;
    }


    public void updateObjectIndexer(final ObjectIndexer oi) {
        if (oi != null) {
            final EntityManager em = EntityManagerService.provideEntityManager();
            em.merge(oi);
            em.flush();
        }
    }



}
