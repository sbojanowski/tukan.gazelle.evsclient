/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evs.client.common.model;

import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <b>Class Description : </b>ReferencedStandard<br>
 * <br>
 * This class describes the ReferencedStandard object. ReferencedStandard describes the standard that can be validated using such or such validation service. Examples are HL7v2.4 or HL7v2.3.1 or HL7V3
 * <p/>
 * Attributes of this object are:
 * <ul>
 * <li><b>id</b> : id of the object in the database</li>
 * <li><b>name</b> : name of the standard (eg. HL7)</li>
 * <li><b>extension</b> : possible extension (None if it is not defined)</li>
 * <li><b>description</b> : short description of the standard</li>
 * <li><b>label</b> : label that will be displayed in the user interface (namevversion if not defined)</li>
 * <li><b>validationServices</b> : the list of validation services that can be used to validated this standard</li>
 * </ul>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, June 10th
 * @class ReferencedStandard
 * @package net.ihe.gazelle.evs.client.common.model
 * @see > Aberge@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Name("referencedStandard")
@Table(name = "evsc_referenced_standard", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = { "id",
        "label" }))
@SequenceGenerator(name = "evsc_referenced_standard_sequence", sequenceName = "evsc_referenced_standard_id_seq", allocationSize = 1)
public class ReferencedStandard implements Serializable, Comparable<ReferencedStandard> {

    /**
     *
     */
    private static final long serialVersionUID = 7296266536938828798L;


    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evsc_referenced_standard_sequence")
    private Integer id;

    @NotNull
    @Column(name = "name")
    private String name;

    @Column(name = "extension")
    private String extension;

    @Column(name = "version")
    private String version;

    @Column(name = "description")
    @Lob
    @Type(type = "text")
    private String description;

    @Column(name = "label")
    private String label;

    /**
     * The name to be displayed in menu
     */
    @NotNull
    @Column(name = "menu_display_name")
    private String menuDisplayName;

    @Deprecated
    @Column(name = "path_to_xsl")
    private String pathToXsl;

    /**
     * The path to the icon to be displayed in the drop-down menu
     */
    @Column(name = "icon_path")
    private String iconPath;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "referencedStandard")
    private List<DocStyleSheet> listDocStyleSheet;

    @ManyToMany
    @JoinTable(name = "evsc_standard_service", joinColumns = @JoinColumn(name = "referenced_standard_id"), inverseJoinColumns = @JoinColumn(name = "validation_service_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "referenced_standard_id", "validation_service_id" }))
    private List<ValidationService> services;


    @Transient
    private Boolean creation = Boolean.FALSE;

    public Boolean getCreation() {
        return this.creation;
    }

    public void setCreation(final Boolean creation) {
        this.creation = creation;
    }


    /**
     * Constructor
     */
    public ReferencedStandard() {

    }

    /**
     * Getters and Setters
     */
    public List<DocStyleSheet> getListDocStyleSheet() {
        return this.listDocStyleSheet;
    }

    public void setListDocStyleSheet(final List<DocStyleSheet> listDocStyleSheet) {
        if (listDocStyleSheet != null) {
            this.listDocStyleSheet = new ArrayList<DocStyleSheet>(listDocStyleSheet);
        }
        else {
            this.listDocStyleSheet = null;
        }

    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getExtension() {
        return this.extension;
    }

    public void setExtension(final String extension) {
        this.extension = extension;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public void setServices(final List<ValidationService> services) {
        if (services != null){
            this.services = new ArrayList<ValidationService>(services);
        }
        else {
            this.services = null;
        }
    }

    public List<ValidationService> getServices() {
        return this.services;
    }

    public void setPathToXsl(final String pathToXsl) {
        this.pathToXsl = pathToXsl;
    }

    public String getPathToXsl() {
        return this.pathToXsl;
    }

    /**
     * Returns a list of ReferencedStandard selected thanks to a given value of a given criteria (attribute of type String)
     *

     */
    public static List<ReferencedStandard> getReferencedStandardFiltered(final String inStandardName,
            final String inStandardVersion, final String inStandardExtension, final String inStandardLabel) {
        final ReferencedStandardQuery query = new ReferencedStandardQuery();
        if (inStandardName != null && !inStandardName.isEmpty()) {
            query.name().eq(inStandardName);
        }
        if (inStandardVersion != null && !inStandardVersion.isEmpty()) {
            query.version().eq(inStandardVersion);
        }
        if (inStandardExtension != null && !inStandardExtension.isEmpty()) {
            query.extension().eq(inStandardExtension);
        }
        if (inStandardLabel != null && !inStandardLabel.isEmpty()) {
            query.label().eq(inStandardLabel);
        }
        query.extension().order(true);
        return query.getList();

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.label == null ? 0 : this.label.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ReferencedStandard other = (ReferencedStandard) obj;
        if (this.label == null) {
            if (other.label != null) {
                return false;
            }
        } else if (!this.label.equals(other.label)) {
            return false;
        }
        return true;
    }

    public String getIconPath() {
        return this.iconPath;
    }

    public String getIconName() {
        final File f = new File(this.iconPath);
        return f.getName();
    }

    public void setIconPath(final String iconPath) {
        this.iconPath = iconPath;
    }

    @Override
    public int compareTo(final ReferencedStandard o) {
        if (this.getLabel() != null && o != null && o.getLabel() != null) {
            return this.getLabel().compareTo(o.getLabel());
        } else {
            return 1;
        }
    }

    public String getMenuDisplayName() {
        return this.menuDisplayName;
    }

    public void setMenuDisplayName(final String menuDisplayName) {
        this.menuDisplayName = menuDisplayName;
    }
}
