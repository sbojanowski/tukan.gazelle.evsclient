/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.hl7.action;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.action.AbstractLogBrowser;
import net.ihe.gazelle.evs.client.hl7.model.HL7ValidatedMessage;
import net.ihe.gazelle.evs.client.hl7.model.HL7ValidatedMessageQuery;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;
import java.util.Map;

@Name("hl7Search")
@Scope(ScopeType.PAGE)
public class HL7ValidatedMessageSearch extends AbstractLogBrowser<HL7ValidatedMessage> {

    /**
     *
     */
    private static final long serialVersionUID = 7544153867381687895L;
    private String messageExtract;

    @Override
    public void reset() {
        super.reset();
        this.messageExtract = null;
    }

    public void setMessageExtract(final String messageExtract) {
        this.messageExtract = messageExtract;
    }

    public String getMessageExtract() {
        return this.messageExtract;
    }

    @Override
    protected HL7ValidatedMessage getObjectByOID(final String oid, final String privacyKey) {
        return HL7ValidatedMessage.getMessageByOID(oid, privacyKey);
    }

    @Override
    protected Class<HL7ValidatedMessage> getEntityClass() {
        return HL7ValidatedMessage.class;
    }

    @Override
    protected HQLCriterionsForFilter<HL7ValidatedMessage> getCriterions() {
        final HL7ValidatedMessageQuery hl7ValidatedMessageQuery = new HL7ValidatedMessageQuery();
        final HQLCriterionsForFilter<HL7ValidatedMessage> result = hl7ValidatedMessageQuery.getHQLCriterionsForFilter();
        result.addPath("standard", hl7ValidatedMessageQuery.standard().label());
        result.addPath("validationStatus", hl7ValidatedMessageQuery.validationStatus());
        result.addPath("privateUser", hl7ValidatedMessageQuery.privateUser());
        result.addPath("institutionKeyword", hl7ValidatedMessageQuery.institutionKeyword());
        result.addPath("profileOid", hl7ValidatedMessageQuery.profileOid());
        result.addPath("validationService", hl7ValidatedMessageQuery.service());
        result.addPath("validationDate", hl7ValidatedMessageQuery.validationDate());
        return result;
    }

    @Override
    public void modifyQuery(final HQLQueryBuilder<HL7ValidatedMessage> queryBuilder,
            final Map<String, Object> filterValuesApplied) {
        super.modifyQuery(queryBuilder, filterValuesApplied);
        if (this.messageExtract != null && !this.messageExtract.isEmpty()) {

            queryBuilder.addRestriction(
                    HQLRestrictions.like("validatedMessage", this.messageExtract, HQLRestrictionLikeMatchMode.ANYWHERE));
        }
    }

    @Override
    public FilterDataModel<HL7ValidatedMessage> getDataModel() {
        return new FilterDataModel<HL7ValidatedMessage>(HL7ValidatedMessageSearch.this.getFilter()) {
            @Override
            protected Object getId(final HL7ValidatedMessage t) {
                return t.getId();
            }
        };
    }
}
