/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.tls.action;

/**
 * Collection of string manipulation functions.
 */
public class StringUtil {

    /**
     * The default line-break string used by the methods in this class.
     */
    public static final String LINE_BREAK = System.getProperty("line.separator");

    public static String wrap(final String text) {
        return StringUtil.wrap(text, 160);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, 0, 0, LINE_BREAK, false)</code>.
     *
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrap(final String text, final int screenWidth) {
        return StringUtil.wrap(text, screenWidth, 0, 0, StringUtil.LINE_BREAK, false);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, 0, 0, LINE_BREAK, true)</code>.
     *
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrapTrace(final String text, final int screenWidth) {
        return StringUtil.wrap(text, screenWidth, 0, 0, StringUtil.LINE_BREAK, true);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, 0, 0, lineBreak, false)</code>.
     *
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrap(final String text, final int screenWidth, final String lineBreak) {
        return StringUtil.wrap(text, screenWidth, 0, 0, lineBreak, false);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, indent, indent, LINE_BREAK, false)</code>.
     *
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrap(final String text, final int screenWidth, final int indent) {
        return StringUtil.wrap(text, screenWidth, indent, indent, StringUtil.LINE_BREAK, false);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, firstIndent, indent, LINE_BREAK,
     * false)</code>.
     *
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrap(final String text, final int screenWidth, final int firstIndent, final int indent) {
        return StringUtil.wrap(text, screenWidth, firstIndent, indent, StringUtil.LINE_BREAK, false);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, indent, indent, lineBreak, false)</code>.
     *
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrap(final String text, final int screenWidth, final int indent, final String lineBreak) {
        return StringUtil.wrap(text, screenWidth, indent, indent, lineBreak, false);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, firstIndent, indent, lineBreak,
     * false)</code>.
     *
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrap(final String text, final int screenWidth, final int firstIndent, final int indent, final String lineBreak) {
        return StringUtil.wrap(text, screenWidth, firstIndent, indent, lineBreak, false);
    }

    /**
     * Hard-wraps flow-text. Uses StringBuffer-s instead of String-s. This is a
     * convenience method that equivalent with
     * <code>wrap(text, screenWidth, firstIndent, indent, LINE_BREAK)</code>.
     *
     * @see #wrap(StringBuilder, int, int, int, String, boolean)
     */
    public static StringBuilder wrap(final StringBuilder text, final int screenWidth, final int firstIndent, final int indent) {
        return StringUtil.wrap(text, screenWidth, firstIndent, indent, StringUtil.LINE_BREAK, false);
    }

    /**
     * Hard-wraps flow-text.
     *
     * @param text        The flow-text to wrap. The explicit line-breaks of the source
     *                    text will be kept. All types of line-breaks (UN*X, Mac,
     *                    DOS/Win) are understood.
     * @param screenWidth The (minimum) width of the screen. It does not utilize the
     *                    <code>screenWidth</code>-th column of the screen to store
     *                    characters, except line-breaks (because some terminals/editors
     *                    do an automatic line-break when you write visible character
     *                    there, and some doesn't... so it is unpredicalbe if an
     *                    explicit line-break is needed or not.).
     * @param firstIndent The indentation of the first line
     * @param indent      The indentation of all lines but the first line
     * @param lineBreak   The String used for line-breaks
     * @param traceMode   Set this true if the input text is a Java stack trace. In this
     *                    mode, all lines starting with optional indentation +
     *                    <tt>'at'</tt> + space are treated as location lines, and will
     *                    be indented and wrapped in a silghtly special way.
     * @throws IllegalArgumentException if the number of columns remaining for the text is less than
     *                                  2.
     */
    public static String wrap(final String text, final int screenWidth, final int firstIndent, final int indent, final String lineBreak,
            final boolean traceMode) {
        return StringUtil.wrap(new StringBuilder(text), screenWidth, firstIndent, indent, lineBreak, traceMode).toString();
    }

    /**
     * Hard-wraps flow-text. Uses StringBuffer-s instead of String-s. This is
     * the method that is internally used by all other <code>wrap</code>
     * variations, so if you are working with StringBuffers anyway, it gives
     * better performance.
     *
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static StringBuilder wrap(final StringBuilder text, final int screenWidth, final int firstIndent, final int indent, final String lineBreak,
            final boolean traceMode) {

        if (firstIndent < 0 || indent < 0 || screenWidth < 0) {
            throw new IllegalArgumentException("Negative dimension");
        }

        final int allowedCols = screenWidth - 1;

        if (allowedCols - indent < 2 || allowedCols - firstIndent < 2) {
            throw new IllegalArgumentException("Usable columns < 2");
        }

        final int ln = text.length();
        final int defaultNextLeft = allowedCols - indent;
        int b = 0;
        int e = 0;

        final StringBuilder res = new StringBuilder((int) (ln * 1.2));
        int left = allowedCols - firstIndent;
        for (int i = 0; i < firstIndent; i++) {
            res.append(' ');
        }
        StringBuilder tempb = new StringBuilder(indent + 2);
        tempb.append(lineBreak);
        for (int i = 0; i < indent; i++) {
            tempb.append(' ');
        }
        final String defaultBreakAndIndent = tempb.toString();

        boolean firstSectOfSrcLine = true;
        boolean firstWordOfSrcLine = true;
        int traceLineState = 0;
        int nextLeft = defaultNextLeft;
        String breakAndIndent = defaultBreakAndIndent;
        int wln, x;
        char c, c2;
        do {
            word:
            while (e <= ln) {
                if (e != ln) {
                    c = text.charAt(e);
                } else {
                    c = ' ';
                }
                if (traceLineState > 0 && e > b) {
                    if (c == '.' && traceLineState == 1) {
                        c = ' ';
                    } else {
                        c2 = text.charAt(e - 1);
                        if (c2 == ':') {
                            c = ' ';
                        } else if (c2 == '(') {
                            traceLineState = 2;
                            c = ' ';
                        }
                    }
                }
                if (c != ' ' && c != '\n' && c != '\r' && c != '\t') {
                    e++;
                } else {
                    wln = e - b;
                    if (left >= wln) {
                        res.append(text.substring(b, e));
                        left -= wln;
                        b = e;
                    } else {
                        wln = e - b;
                        if (wln > nextLeft || firstWordOfSrcLine) {
                            final int ob = b;
                            while (wln > left) {
                                if (left > 2 || left == 2 && (firstWordOfSrcLine || !(b == ob && nextLeft > 2))) {
                                    res.append(text.substring(b, b + left - 1));
                                    res.append('-');
                                    res.append(breakAndIndent);
                                    wln -= left - 1;
                                    b += left - 1;
                                    left = nextLeft;
                                } else {
                                    x = res.length() - 1;
                                    if (x >= 0 && res.charAt(x) == ' ') {
                                        res.delete(x, x + 1);
                                    }
                                    res.append(breakAndIndent);
                                    left = nextLeft;
                                }
                            }
                            res.append(text.substring(b, b + wln));
                            b += wln;
                            left -= wln;
                        } else {
                            x = res.length() - 1;
                            if (x >= 0 && res.charAt(x) == ' ') {
                                res.delete(x, x + 1);
                            }
                            res.append(breakAndIndent);
                            res.append(text.substring(b, e));
                            left = nextLeft - wln;
                            b = e;
                        }
                    }
                    firstSectOfSrcLine = false;
                    firstWordOfSrcLine = false;
                    break word;
                }
            }
            int extra = 0;
            space:
            while (e < ln) {
                c = text.charAt(e);
                if (c == ' ') {
                    e++;
                } else if (c == '\t') {
                    e++;
                    extra += 7;
                } else if (c == '\n' || c == '\r') {
                    nextLeft = defaultNextLeft;
                    breakAndIndent = defaultBreakAndIndent;
                    res.append(breakAndIndent);
                    e++;
                    if (e < ln) {
                        c2 = text.charAt(e);
                        if ((c2 == '\n' || c2 == '\r') && c != c2) {
                            e++;
                        }
                    }
                    left = nextLeft;
                    b = e;
                    firstSectOfSrcLine = true;
                    firstWordOfSrcLine = true;
                    traceLineState = 0;
                } else {
                    wln = e - b + extra;
                    if (firstSectOfSrcLine) {
                        int y = allowedCols - indent - wln;
                        if (traceMode && ln > e + 2 && text.charAt(e) == 'a' && text.charAt(e + 1) == 't'
                                && text.charAt(e + 2) == ' ') {
                            if (y > 5 + 3) {
                                y -= 3;
                            }
                            traceLineState = 1;
                        }
                        if (y > 5) {
                            y = allowedCols - y;
                            nextLeft = allowedCols - y;
                            tempb = new StringBuilder(indent + 2);
                            tempb.append(lineBreak);
                            for (int i = 0; i < y; i++) {
                                tempb.append(' ');
                            }
                            breakAndIndent = tempb.toString();
                        }
                    }
                    if (wln <= left) {
                        res.append(text.substring(b, e));
                        left -= wln;
                        b = e;
                    } else {
                        res.append(breakAndIndent);
                        left = nextLeft;
                        b = e;
                    }
                    firstSectOfSrcLine = false;
                    break space;
                }
            }
        } while (e < ln);

        return res;
    }

}
