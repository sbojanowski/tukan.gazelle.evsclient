/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.util.Util;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;

@Entity
@Name("validatedHPD")
@Table(name = "validated_hpd", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "oid"))
@SequenceGenerator(name = "validated_hpd_sequence", sequenceName = "validated_hpd_id_seq", allocationSize = 1)
public class ValidatedHPD extends AbstractValidatedXMLFile {

    /**
     *
     */
    private static final long serialVersionUID = -6043263240728175575L;
    private static final String HPD_FILE_PREFIX = "HPD_";
    private static final String HPD_FILE_SUFFIX = ".xml";

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidatedHPD.class);

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @NotNull
    @GeneratedValue(generator = "validated_hpd_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "file_path")
    private String filePath;


    public ValidatedHPD(final ValidatedHPD validatedFile) {
        this.filePath = validatedFile.getFilePath();
        this.description = validatedFile.getDescription();
    }

    public ValidatedHPD() {

    }

    public static ValidatedHPD createNewValidatedHPD() {
        ValidatedHPD message = new ValidatedHPD();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        message = em.merge(message);
        em.flush();
        if (message == null || message.getId() == 0) {
            ValidatedHPD.LOGGER.error("An error occurred when creating a new ValidatedHPD object");
            return null;
        }
        final String repository = ApplicationPreferenceManager.getStringValue("hpd_repository");
        message.setFilePath(
                repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ValidatedHPD.HPD_FILE_PREFIX
                        .concat(message.getId().toString()).concat(
                        ValidatedHPD.HPD_FILE_SUFFIX));
        return message;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AbstractValidatedXMLFile> T addOrUpdate(final Class<T> clazz) {
        if (this.getFilePath() == null || this.getFilePath().isEmpty()) {
            ValidatedHPD.LOGGER.error("The file path is missing, cannot add this object !");
            return (T) this;
        } else {
            final File uploadedFile = new File(this.getFilePath());
            if (!uploadedFile.exists()) {
                ValidatedHPD.LOGGER.error("The given file does not exist: {}", this.getFilePath());
                return (T) this;
            }
            if (this.getOid() == null || this.getOid().length() == 0) {
                this.setOid(OIDGenerator.getNewOid());
            }
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            final ValidatedHPD validatedFile = em.merge(this);
            em.flush();

            //Update the cache with the newest values
            GazelleValidationCacheManager
                    .refreshGazelleCacheWebService(validatedFile.getExternalId(), validatedFile.getToolOid(),
                            validatedFile.getOid(), "off");

            return (T) validatedFile;
        }
    }

    @Override
    public void setFilePath(final String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String getFilePath() {
        return this.filePath;
    }

    @Override
    public String getSchematron() {
        return null;
    }

    @Override
    public void setSchematron(final String schematron) {
        // no schematron validation available for SVS
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/detailedResult.seam?type=HPD").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

}
