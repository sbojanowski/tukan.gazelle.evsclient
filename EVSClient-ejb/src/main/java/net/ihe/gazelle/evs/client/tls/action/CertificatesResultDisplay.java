/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.tls.action;

import net.ihe.gazelle.evs.client.common.action.AbstractResultDisplayManager;
import net.ihe.gazelle.evs.client.tls.model.ValidatedCertificates;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.evs.client.util.ValidatorType;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.international.LocaleSelector;

import java.io.Serializable;

@Name("certificatesResultDisplay")
@Scope(ScopeType.PAGE)
public class CertificatesResultDisplay extends AbstractResultDisplayManager<ValidatedCertificates>
        implements Serializable {

    @Override
    protected Class<ValidatedCertificates> getEntityClass() {
        return ValidatedCertificates.class;
    }

    private static final long serialVersionUID = 7920478679366520753L;

    @Override
    protected String getValidatorUrl() {
        return "/tls/validator.seam";
    }

    @Override
    public String revalidate() {
        String url = this.getValidatorUrl().concat("?oid=").concat(this.selectedObject.getOid());
        if (this.selectedObject.getStandard() != null) {
            url = url.concat("&extension=").concat(this.selectedObject.getStandard().getExtension());
        }
        return url;
    }

    public String getStringCertificates() {
        if (this.selectedObject != null && this.selectedObject.getCertificates() != null) {
            return CertificatesResultDisplay.rnTobr(this.selectedObject.getCertificates());
        } else {
            return "";
        }
    }

    public String getStringDetailedResult() {
        if (this.selectedObject.getDetailedResult() == null) {
            return null;
        } else if (this.selectedObject.getDetailedResult().startsWith("<?xml")
                && this.selectedObject.getService() != null
                && this.selectedObject.getService().getXslLocation() != null) {
            return Util.transformXMLStringToHTML(this.selectedObject.getDetailedResult(),
                    this.selectedObject.getService().getXslLocation(), LocaleSelector.instance().getLanguage());
        } else {
            return CertificatesResultDisplay.rnTobr(this.selectedObject.getDetailedResult());
        }
    }

    private static String rnTobr(final String result) {
   //     String splitted = StringUtil.wrap(result, 80);
        return result.replaceAll(" ", "&nbsp;").replaceAll("\r\n", "<br \\>").replaceAll("\r", "<br \\>")
                .replaceAll("\n", "<br \\>");
    }

    @Override
    public String performAnotherValidation() {
        StringBuilder url = new StringBuilder(ValidatorType.TLS.getView());
        if (selectedObject.getStandard() != null && selectedObject.getStandard().getExtension() != null){
            url.append("?extension=");
            url.append(selectedObject.getStandard().getExtension());
        }
        return url.toString();
    }
}
