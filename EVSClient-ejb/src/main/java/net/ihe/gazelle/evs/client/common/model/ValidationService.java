/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evs.client.common.model;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description :  </b>ValidationService<br><br>
 * This class describes the Validation Service object
 * <p/>
 * ValidationService possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id of the role in database</li>
 * <li><b>keyword</b> : keyword</li>
 * <li><b>description</b> : description</li>
 * <li><b>label</b> : the label which is displayed in the application web pages</li>
 * <li><b>wsdl</b> : wsdl location of the web service to invoke for validating the message</li>
 * <li><b>available</b> :  boolean to indicate if the service is available or not
 * <li><b>standards</b> : the list of standards that can be validated using this service</li>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, May 27th
 * @class ValidationService.java
 * @package net.ihe.gazelle.hl7.evs.client.model
 * @see aberge@irisa.fr -  http://www.ihe-europe.org
 */

@Entity
@Name("validationService")
@Table(name = "evsc_validation_service", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"id",
        "keyword"}))
@SequenceGenerator(name = "evsc_validation_service_sequence", sequenceName = "evsc_validation_service_id_seq", allocationSize = 1)
public class ValidationService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1346536037461085498L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationService.class);

    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evsc_validation_service_sequence")
    private Integer id;

    @Column(name = "keyword")
    private String keyword;

    @Column(name = "description")
    @Lob
    @Type(type = "text")
    private String description;

    @Column(name = "name")
    private String name;

    @Column(name = "wsdl")
    private String wsdl;

    @Column(name = "available")
    private boolean available;

    @Column(name = "provider")
    private String provider;

    @Column(name = "engine")
    @Pattern(regexp = "schematron|model-based|hapi|null", message = "Please enter 'schematron' or 'model-based' or 'hapi' or 'null'")
    private String engine;

    @Column(name = "xsl_location")
    @Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$|^ftp://.+$)", message = "{gazelle.validator.http}")
    private String xslLocation;

    @Column(name = "report_name")
    private String reportName;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "evsc_standard_service",
            joinColumns = @JoinColumn(name = "validation_service_id"),
            inverseJoinColumns = @JoinColumn(name = "referenced_standard_id"),
            uniqueConstraints = @UniqueConstraint(columnNames = {"referenced_standard_id", "validation_service_id"}))
    private List<ReferencedStandard> standards;

    @Transient
    private Boolean creation = Boolean.FALSE;

    public Boolean getCreation() {
        return this.creation;
    }

    public void setCreation(final Boolean creation) {
        this.creation = creation;
    }


    /**
     * Constructor
     */
    public ValidationService() {

    }


    public enum EngineType {
        MODEL_BASED("model-based"),
        SCHEMATRON("schematron"),
        HAPI("hapi"),
        OTHER("other");

        private final String keyword;

        EngineType(final String keyword) {
            this.keyword = keyword;
        }

        public String getKeyword() {
            return this.keyword;
        }
    }

    /**
     * Getters and Setters
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public void setKeyword(final String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @FilterLabel
    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getWsdl() {
        return this.wsdl;
    }

    public void setWsdl(final String wsdl) {
        this.wsdl = wsdl;
    }

    public boolean isAvailable() {
        return this.available;
    }

    public void setAvailable(final boolean available) {
        this.available = available;
    }

    public void setStandards(final List<ReferencedStandard> standards) {
        if (standards != null) {
            this.standards = new ArrayList<ReferencedStandard>(standards);
        } else {
            this.standards = null;
        }
    }

    public List<ReferencedStandard> getStandards() {
        return this.standards;
    }

    public void setProvider(final String provider) {
        this.provider = provider;
    }

    public String getProvider() {
        return this.provider;
    }

    public String getEngine() {
        return this.engine;
    }

    public void setEngine(final ValidationService.EngineType engineType) {
        this.engine = engineType.getKeyword();
    }

    public void setEngine(final String engineType) {
        this.engine = engineType;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    /**
     * @param inKeyword : keyword string that identifies the validation service to use
     */
    public static ValidationService getValidationServiceByKeyword(final String inKeyword) {
        final ValidationServiceQuery query = new ValidationServiceQuery();
        query.keyword().eq(inKeyword);
        return query.getUniqueResult();
    }

    /**
     * @param inValidationService : validationService object to delete.
     */
    public static boolean deleteService(final ValidationService inValidationService) {
        if (inValidationService != null) {
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            try {
                em.remove(inValidationService);
                return true;
            } catch (final RuntimeException e) {
                ValidationService.LOGGER.error("cannot delete validation service {}", inValidationService.getName());
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param inStandard : Standard for which to return the list of validation services
     * @return : Returns the list of available validation services for the parameter inStandard
     */
    public static List<ValidationService> getListOfAvailableValidationServicesForStandard(
            final ReferencedStandard inStandard) {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        final HQLQueryBuilder<ValidationService> builder = new HQLQueryBuilder<>(entityManager, ValidationService.class);
        builder.addEq("standards.id", inStandard.getId());
        builder.addEq("available", Boolean.TRUE);
        return builder.getList();
    }

    public String getXslLocation() {
        return this.xslLocation;
    }

    public void setXslLocation(final String xslLocation) {
        this.xslLocation = xslLocation;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.keyword == null ? 0 : this.keyword.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ValidationService other = (ValidationService) obj;
        if (this.keyword == null) {
            if (other.keyword != null) {
                return false;
            }
        } else if (!this.keyword.equals(other.keyword)) {
            return false;
        }
        return true;
    }


    public static void removeNullEntries(final Class<ValidationService> entityClass) {

        final EntityManager entityManager = EntityManagerService.provideEntityManager();
        final HQLQueryBuilder<ValidationService> queryBuilder = new HQLQueryBuilder<>(entityManager, entityClass);
        queryBuilder.addRestriction(HQLRestrictions.isNull("oid"));
        final List<ValidationService> nullObjects = queryBuilder.getList();
        for (final ValidationService object : nullObjects) {
            entityManager.remove(object);
        }
        entityManager.flush();
    }


}
