/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file;

import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.common.action.FileToValidate;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTTP implements IAnalyzableContent {

    public static final String START = "start :";
    public static final String END = " end : ";
    private static final Logger LOGGER = LoggerFactory.getLogger(HTTP.class);

    @Override
    public boolean analyze(final File f, final FileToValidate parent, final FileToValidate ftv, final MessageContentAnalyzer mca,
            final int startOffset, final int endOffset) {
        final File file;
        final FileToValidate ftv1 = new FileToValidate(parent);
        final FileToValidate ftv2 = new FileToValidate(parent);

        if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
            file = mca.fileIntoOffsets(f, startOffset, endOffset);
            this.analyze(file, parent, ftv, mca, 0, 0);
            AnalyzerFileUtils.deleteTmpFile(file);
        } else {
            HTTP.LOGGER.info("Extract parts from http");

            ftv.setStartOffset(startOffset + mca.getSaveStartOffset());
            ftv.setEndOffset(endOffset + mca.getSaveStartOffset());
            ftv.setMessageContentAnalyzerOid(OIDGenerator.getNewOid());
            ftv.setDocType("HTTP");
            parent.getFileToValidateList().add(ftv);

            // Separate Header and body
            this.extractHttpParts(f, startOffset, endOffset, mca);

            if (mca.getHttpHeader() != null) {
                ftv1.setDocType("Http Header");
                ftv1.setMessageContentAnalyzerOid(OIDGenerator.getNewOid());
                ftv1.setStartOffset(mca.getHttpHeaderOffsetStart());
                ftv1.setEndOffset(mca.getHttpHeaderOffsetEnd());
                ftv1.setParent(ftv);
                ftv.getFileToValidateList().add(ftv1);
                mca.setHttpHeader(null);
            }
            if (mca.getHttpBody() != null) {
                ftv2.setDocType("Http Body");
                ftv2.setMessageContentAnalyzerOid(OIDGenerator.getNewOid());
                ftv2.setParent(ftv);
                ftv2.setStartOffset(mca.getHttpBodyOffsetStart());
                ftv2.setEndOffset(mca.getHttpBodyOffsetEnd());
                ftv.getFileToValidateList().add(ftv2);
                mca.messageContentAnalyzer(f, mca.getHttpBodyOffsetStart(), mca.getHttpBodyOffsetEnd(), ftv2);
                mca.setHttpBody(null);
            }

        }
        return false;
    }

    // TODO : update the code to match http header and body
    // Extract http header AFTER \s\s
    public boolean extractHttpParts(final File file, final int startOffset, final int endOffset, final MessageContentAnalyzer mca) {
        final File f;
        String fileContent;
        boolean result = false;

        try {
            if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
                mca.setSaveStartOffset(0);
                f = mca.fileIntoOffsets(file, startOffset, endOffset);
                mca.setSaveStartOffset(mca.getSaveStartOffset() + startOffset);
                result = this.extractHttpParts(f, 0, 0, mca);
                AnalyzerFileUtils.deleteTmpFile(f);
                return result;
            } else {
                fileContent = FileUtils.readFileToString(file);
                HTTP.LOGGER.info(fileContent);
                final String firstLineMatch = "^[GET|HEAD|POST|PUT|DELETE|TRACE|CONNECT]{1}.*(HTTP/1.)[0-9]";

                Pattern regex = Pattern.compile(firstLineMatch);
                final Matcher regexMatcher = regex.matcher(fileContent);
                if (regexMatcher.find()) {
                    final String httpReg = "(\\n\\n\\n)";

                    regex = Pattern.compile(httpReg, Pattern.MULTILINE & Pattern.DOTALL);
                    final Matcher regexMatcher2 = regex.matcher(fileContent);
                    if (regexMatcher2.find()) {

                        // HTTP header part
                        if (mca.getSaveStartOffset() != 0) {
                            mca.setHttpHeaderOffsetStart(regexMatcher.start() + mca.getSaveStartOffset());
                            mca.setHttpHeaderOffsetEnd(regexMatcher2.end() + mca.getSaveStartOffset());
                        } else {
                            mca.setHttpHeaderOffsetStart(regexMatcher.start());
                            mca.setHttpHeaderOffsetEnd(regexMatcher2.end());
                        }
                        HTTP.LOGGER.info(HTTP.START + "{}" + HTTP.END + "{}", Integer.valueOf(regexMatcher.start()),
                                Integer.valueOf(regexMatcher2.end()));
                        HTTP.LOGGER.info(HTTP.START + "{}" + HTTP.END + "{}",
                                Integer.valueOf(mca.getHttpHeaderOffsetStart()),
                                Integer.valueOf(mca.getHttpHeaderOffsetEnd()));
                        final String fileContent2 = FileUtils
                                .readFileToString(mca.fileIntoOffsets(file, regexMatcher.start(), regexMatcher2.end()));
                        mca.setHttpHeader(fileContent2);
                        HTTP.LOGGER.info("Http header :\n{}", mca.getHttpHeader());

                        result = true;

                        // HTTP body part
                        if (mca.getSaveStartOffset() != 0) {
                            mca.setHttpBodyOffsetStart(regexMatcher2.end() + mca.getSaveStartOffset());
                            mca.setHttpBodyOffsetEnd(fileContent.length() + mca.getSaveStartOffset());
                        } else {
                            mca.setHttpBodyOffsetStart(regexMatcher2.end());
                            mca.setHttpBodyOffsetEnd(fileContent.length());
                        }
                        HTTP.LOGGER.info(HTTP.START + "{}" + HTTP.END + "{}", Integer.valueOf(regexMatcher2.end()),
                                Integer.valueOf(fileContent.length()));
                        HTTP.LOGGER.info(HTTP.START + "{}" + HTTP.END + "{}",
                                Integer.valueOf(mca.getHttpBodyOffsetStart()),
                                Integer.valueOf(mca.getHttpBodyOffsetEnd()));
                        final String fileContent3 = FileUtils
                                .readFileToString(mca.fileIntoOffsets(file, regexMatcher2.end(), fileContent.length()));
                        mca.setHttpBody(fileContent3);
                        HTTP.LOGGER.info("Http body :\n{}", mca.getHttpBody());
                    }

                }
            }
            return result;
        } catch (final IOException e) {
            HTTP.LOGGER.error(e.getMessage());
            return false;
        }
    }
}
