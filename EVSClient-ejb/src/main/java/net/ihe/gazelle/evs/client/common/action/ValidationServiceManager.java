/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.filter.HibernateDataModel;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;
import net.ihe.gazelle.evs.client.common.model.ValidationService;
import net.ihe.gazelle.evs.client.common.model.ValidationServiceQuery;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Name("validationServiceManager")
@Scope(ScopeType.PAGE)
public class ValidationServiceManager implements QueryModifier<ValidationService> {

    /**
     *
     */
    private static final long serialVersionUID = 7965440142601455594L;
    private ValidationService selectedService;
    private List<ReferencedStandard> availableStandards;
    protected HibernateDataModel<ValidationService> dataModel;

    protected Filter filter;

    protected Class<ValidationService> getEntityClass() {
        return ValidationService.class;

    }
    public FilterDataModel<ValidationService> getValidationServices() {
        return new FilterDataModel<ValidationService>(ValidationServiceManager.this.getFilter()) {
            @Override
            protected Object getId(final ValidationService t) {
                return t.getId();
            }
        };
    }

    private Filter<ValidationService> getFilter() {

        if (this.filter == null) {
            final ValidationServiceQuery query = new ValidationServiceQuery();
            final HQLCriterionsForFilter<ValidationService> result = query.getHQLCriterionsForFilter();
            this.filter = new Filter<>(result);
        }
        return this.filter;

    }


    protected HQLCriterionsForFilter<ValidationService> getCriterions() {
        final ValidationServiceQuery query = new ValidationServiceQuery();
        final HQLCriterionsForFilter<ValidationService> result = query.getHQLCriterionsForFilter();

        result.addPath("name", query.name());
        result.addPath("keyword", query.keyword());
        result.addPath("engine", query.engine());
        result.addPath("available", query.available());

        return result;
    }



    public String save() {
        if (this.selectedService != null) {
            final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            entityManager.merge(this.selectedService);
            entityManager.flush();
            FacesMessages.instance()
                    .addFromResourceBundle("net.ihe.gazelle.evs.ValidationServiceHasBeenSuccessfullySaved");
        }
        return "/administration/validationServices.seam";
    }

    public void add(final ReferencedStandard standard) {
        if (this.selectedService != null) {
            if (this.selectedService.getStandards() == null) {
                this.selectedService.setStandards(new ArrayList<ReferencedStandard>());
                this.selectedService.getStandards().add(standard);
            } else if (this.selectedService.getStandards().isEmpty()) {
                this.selectedService.getStandards().add(standard);
            } else if (!this.selectedService.getStandards().contains(standard)) {
                this.selectedService.getStandards().add(standard);
            }
            this.availableStandards.remove(standard);
        }
    }

    public void remove(final ReferencedStandard standard) {
        if (this.selectedService != null) {
            if (this.selectedService.getStandards() != null && !this.selectedService.getStandards().isEmpty() && this.selectedService
                    .getStandards().contains(standard)) {
                this.selectedService.getStandards().remove(standard);
                this.availableStandards.remove(standard);
            }
        }
    }

    public void initValidationServiceDisplay() {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams.containsKey("id")) {
            try {
                this.selectedService = entityManager.find(ValidationService.class,
                        Integer.valueOf(urlParams.get("id")));
                this.selectedService.setCreation(Boolean.FALSE);

            } catch (final NumberFormatException e) {
                this.selectedService = null;
            }
        } else {
            this.selectedService = new ValidationService();
            this.selectedService.setCreation(Boolean.TRUE);

        }
        final HQLQueryBuilder<ReferencedStandard> builder = new HQLQueryBuilder<>(entityManager,
                ReferencedStandard.class);
        this.availableStandards = builder.getList();
        if (this.availableStandards != null && !this.availableStandards.isEmpty() && this.selectedService != null
                && this.selectedService.getStandards() != null) {
            for (final ReferencedStandard standard : this.selectedService.getStandards()) {
                if (this.availableStandards.contains(standard)) {
                    this.availableStandards.remove(standard);
                }
            }
        }
    }

    public ValidationService getSelectedService() {
        return this.selectedService;
    }

    public List<ReferencedStandard> getAvailableStandards() {
        return this.availableStandards;
    }



    @Override
    public void modifyQuery(final HQLQueryBuilder<ValidationService> queryBuilder, final Map<String, Object> filterValuesApplied) {
       // ValidationServiceQuery validatedObjectQuery = new ValidationServiceQuery(queryBuilder);

    }
}