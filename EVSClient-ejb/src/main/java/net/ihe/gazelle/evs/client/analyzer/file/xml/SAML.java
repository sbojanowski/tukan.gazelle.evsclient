/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file.xml;

import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.analyzer.utils.XmlUtils;
import net.ihe.gazelle.evs.client.util.ValidationContextHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SAML extends Analyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SAML.class);

    @Override
    public Logger getLog() {
        return SAML.LOGGER;
    }

    @Override
    public String getType() {
        return "SAML";
    }

    public static boolean containsSAML(final File file, final MessageContentAnalyzer mca) {
        return SAML.containsAssertionToString(file, 0, 0, mca);
    }

    public static boolean containsAssertionToString(final File file, final int startOffset, final int endOffset,
            final MessageContentAnalyzer mca) {
        final File f;
        if (startOffset >= 0 && endOffset != 0 && startOffset < endOffset) {
            f = mca.fileIntoOffsets(file, startOffset, endOffset);
            final boolean result = SAML.containsAssertionToString(f, 0, 0, mca);
            AnalyzerFileUtils.deleteTmpFile(f);
            return result;
        } else {
            final String fileContent = AnalyzerFileUtils.convertFileToString(file);
            return SAML.containsAssertion(fileContent, mca);
        }
    }

    public static boolean containsAssertion(final String header, final MessageContentAnalyzer mca) {
        if (header != null && !header.isEmpty()) {
            final String assReg = "<([^<]+)?Assertion([\\r|\\n| |>])+.*?</([^<]+)?Assertion>";

            final ValidationContextHandler vch = new ValidationContextHandler(header);

            Pattern regex = Pattern.compile(assReg, Pattern.DOTALL);
            Matcher regexMatcher = regex.matcher(header);
            if (regexMatcher.find()) {
                SAML.LOGGER.info(regexMatcher.group());
                if (XmlUtils.testValidSchemes(regexMatcher.group(1), regexMatcher.group(3))) {

                    regex = Pattern.compile(assReg, Pattern.DOTALL);
                    regexMatcher = regex.matcher(header);
                    if (regexMatcher.find()) {
                        if (regexMatcher.group(1) != null) {
                            vch.setPatternAssertion(regexMatcher.group(1) + "Assertion");
                        } else {
                            vch.setPatternAssertion("Assertion");
                        }
                        vch.parseMessageContext();
                    }
                }
            }

            if (vch.isAssertion()) {
                regexMatcher = regex.matcher(header);
                if (regexMatcher.find()) {
                    SAML.LOGGER.info(regexMatcher.group());
                    mca.setSaveStartOffset(0);
                    mca.setOffsetStart(regexMatcher.start());
                    mca.setOffsetEnd(regexMatcher.end());
                }
                return true;
            }
        } else {
            SAML.LOGGER.error("header is null !");
        }
        return false;
    }
}
