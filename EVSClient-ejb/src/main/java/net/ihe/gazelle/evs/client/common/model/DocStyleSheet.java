/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Name("docStylesheet")
@Table(name = "doc_stylesheet", schema = "public")
@SequenceGenerator(name = "doc_stylesheet_sequence", sequenceName = "doc_stylesheet_id_seq", allocationSize = 1)
public class DocStyleSheet implements Serializable, Comparable<DocStyleSheet> {

    /**
     *
     */
    private static final long serialVersionUID = 1218389994561421605L;

    @Id
    @GeneratedValue(generator = "doc_stylesheet_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @NotNull
    private String name;

    private String path;

    @ManyToOne
    @JoinColumn(name = "referenced_standard_id", nullable = false)
    private ReferencedStandard referencedStandard;

    private String comment;

    public ReferencedStandard getReferencedStandard() {
        return this.referencedStandard;
    }

    public void setReferencedStandard(final ReferencedStandard referencedStandard) {
        this.referencedStandard = referencedStandard;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(final String path) {
        this.path = path;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(final String comment) {
        this.comment = comment;
    }

    public DocStyleSheet() {
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.name == null ? 0 : this.name.hashCode());
        result = prime * result + (this.path == null ? 0 : this.path.hashCode());
        return  prime * result + (this.referencedStandard == null ? 0 : this.referencedStandard.hashCode());
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final DocStyleSheet other = (DocStyleSheet) obj;
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        if (this.path == null) {
            if (other.path != null) {
                return false;
            }
        } else if (!this.path.equals(other.path)) {
            return false;
        }
        if (this.referencedStandard == null) {
            if (other.referencedStandard != null) {
                return false;
            }
        } else if (!this.referencedStandard.equals(other.referencedStandard)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(final DocStyleSheet o) {
        if (this.name == null) {
            return -1;
        }
        if (o.name == null) {
            return 1;
        }
        return this.name.compareTo(o.name);
    }

}
