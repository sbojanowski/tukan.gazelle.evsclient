/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.logs;

import net.ihe.gazelle.evs.client.xml.model.CDAValidatedFile;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Name("objectIndexer")
@Table(name = "object_indexer", schema = "public")
@SequenceGenerator(name = "object_indexer_sequence", sequenceName = "object_indexer_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "instances")
public class ObjectIndexer implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1962295085018434148L;

    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "object_indexer_sequence")
    private Integer id;

    @Column(name = "identifier", nullable = false, unique = true)
    private String identifier;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "extension", nullable = false)
    private String extension;

    @Column(name = "display", nullable = false)
    private boolean display = true;

    @ManyToMany
    @JoinTable(name = "object_indexer_cda_validated_file", joinColumns = @JoinColumn(name = "object_indexer_id"), inverseJoinColumns = @JoinColumn(name = "cda_validated_file_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "object_indexer_id", "cda_validated_file_id" }))
    private List<CDAValidatedFile> validatedCdaFiles;

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getIdentifier() {
        return this.identifier;
    }

    public void setIdentifier(final String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getExtension() {
        return this.extension;
    }

    public void setExtension(final String extension) {
        this.extension = extension;
    }

    public boolean isDisplay() {
        return this.display;
    }

    public void setDisplay(final boolean display) {
        this.display = display;
    }

    public List<CDAValidatedFile> getValidatedCdaFiles() {
        if (this.validatedCdaFiles == null) {
            this.validatedCdaFiles = new ArrayList<>();
        }
        return this.validatedCdaFiles;
    }

    public void setValidatedCdaFiles(final List<CDAValidatedFile> validatedCdaFiles) {
        if (validatedCdaFiles != null) {
            this.validatedCdaFiles = new ArrayList<CDAValidatedFile>(validatedCdaFiles);
        }
        else {
            this.validatedCdaFiles = null;
        }
    }

    public void addValidatedCdaFiles(final List<CDAValidatedFile> validatedCdaFiles) {
        this.getValidatedCdaFiles().addAll(validatedCdaFiles);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o){
            return true;
        }
        if (o == null){
            return false;
        }

        if (this.getClass() != o.getClass()){
            return false;
        }
        final ObjectIndexer oi = (ObjectIndexer) o;
        return this.getIdentifier().equals(oi.getIdentifier());
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.identifier == null ? 0 : this.identifier.hashCode());
        return result;
    }

}
