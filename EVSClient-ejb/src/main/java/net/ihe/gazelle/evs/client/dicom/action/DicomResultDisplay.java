/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.dicom.action;

import net.ihe.gazelle.evs.client.common.action.AbstractResultDisplayManager;
import net.ihe.gazelle.evs.client.common.action.GetValidationInfo;
import net.ihe.gazelle.evs.client.dicom.model.DicomOperation;
import net.ihe.gazelle.evs.client.dicom.model.DicomValidatedObject;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.evs.client.util.ValidatorType;
import org.apache.commons.lang.StringEscapeUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@Name("dicomResultDisplay")
@Scope(ScopeType.PAGE)
public class DicomResultDisplay extends AbstractResultDisplayManager<DicomValidatedObject> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7376694152271876922L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DicomResultDisplay.class);

    @Override
    protected Class<DicomValidatedObject> getEntityClass() {
        return DicomValidatedObject.class;
    }

    @Override
    protected String getValidatorUrl() {
        return "/dicom/validator.seam";
    }

    @Override
    public void downloadValidatedObject() {
        final File tmpFile = new File(this.selectedObject.getFilePath());
        if (!tmpFile.getParentFile().mkdirs()) {
            DicomResultDisplay.LOGGER.error("Unable to create {}", this.selectedObject.getFilePath());
        }

        final FacesContext fc = FacesContext.getCurrentInstance();
        final ExternalContext extCtx = fc.getExternalContext();

        final HttpServletResponse response = (HttpServletResponse) extCtx.getResponse();

        try (InputStream messageStream = new FileInputStream(this.selectedObject.getFilePath()))  {
            response.setContentType(GetValidationInfo.TEXT_PLAIN);
            try (ServletOutputStream servletOutputStream = response.getOutputStream()){

                response.setHeader("Content-Disposition",
                        "attachment;filename=\"" + this.selectedObject.getDescription() + '"');

                response.setContentLength((int) tmpFile.length());
                final byte[] buf = new byte[8192];
                while (true) {

                    final int length = messageStream.read(buf);
                    if (length < 0) {
                        break;
                    }
                    servletOutputStream.write(buf, 0, length);
                }
                messageStream.close();
                servletOutputStream.close();
            }
        } catch (final FileNotFoundException e) {
            DicomResultDisplay.LOGGER.error("Failed to find file", e);
            FacesMessages.instance().add(Severity.ERROR,"Failed to find file");

        } catch (final IOException e) {
            DicomResultDisplay.LOGGER.error("Failed to download file", e);
            FacesMessages.instance().add(Severity.ERROR,"Failed to download file");
        } finally {
             fc.responseComplete();
        }
    }

    public boolean resultIsXml() {
        boolean result = false;
        final String dicomOperation = this.selectedObject.getDicomOperation();
        if (dicomOperation.equals(DicomOperation.DCM4CHE_DUMP2XML.getLabel()) || dicomOperation
                .equals(DicomOperation.PIXELMED_DUMP2XML.getLabel())) {
            result = true;
        }
        return result;
    }

    public String getXML(final String inMessage) {
        String msg = inMessage.replaceAll("<(/){0,1}pre>", "");
        msg = StringEscapeUtils.unescapeHtml(msg);

        final Document doc;
        try {
            doc = Util.string2Document(msg);
            return new XMLOutputter(Format.getPrettyFormat()).outputString(doc);
        } catch (final IOException|JDOMException e) {
            DicomResultDisplay.LOGGER.error("{}", e);
            return msg;
        }
    }

    @Override
    public String performAnotherValidation() {
        StringBuilder url = new StringBuilder(ValidatorType.DICOM.getView());
        if (selectedObject.getStandard() != null && selectedObject.getStandard().getExtension() != null){
            url.append("?extension=");
            url.append(selectedObject.getStandard().getExtension());
        }
        return url.toString();
    }

}
