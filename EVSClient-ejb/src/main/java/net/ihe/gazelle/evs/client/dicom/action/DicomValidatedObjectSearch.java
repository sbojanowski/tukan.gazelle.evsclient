/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.dicom.action;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.action.AbstractLogBrowser;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.dicom.model.DicomValidatedObject;
import net.ihe.gazelle.evs.client.dicom.model.DicomValidatedObjectQuery;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("dicomSearch")
@Scope(ScopeType.PAGE)
public class DicomValidatedObjectSearch extends AbstractLogBrowser<DicomValidatedObject> {

    /**
     *
     */
    private static final long serialVersionUID = -1890799579305953671L;

    @Override
    protected DicomValidatedObject getObjectByOID(final String oid, final String privacyKey) {
        return ValidatedObject.getObjectByOID(DicomValidatedObject.class, oid, privacyKey);
    }

    @Override
    protected Class<DicomValidatedObject> getEntityClass() {
        return DicomValidatedObject.class;
    }

    @Override
    protected HQLCriterionsForFilter<DicomValidatedObject> getCriterions() {
        final DicomValidatedObjectQuery dicomValidatedObjectQuery = new DicomValidatedObjectQuery();
        final HQLCriterionsForFilter<DicomValidatedObject> result = dicomValidatedObjectQuery.getHQLCriterionsForFilter();
        result.addPath("standard", dicomValidatedObjectQuery.standard().label());
        result.addPath("validationStatus", dicomValidatedObjectQuery.validationStatus());
        result.addPath("privateUser", dicomValidatedObjectQuery.privateUser());
        result.addPath("validationService", dicomValidatedObjectQuery.service().name());
        result.addPath("dicomOperation", dicomValidatedObjectQuery.dicomOperation());
        result.addPath("validationDate", dicomValidatedObjectQuery.validationDate());
        return result;
    }

    @Override
    public FilterDataModel<DicomValidatedObject> getDataModel() {
        return new FilterDataModel<DicomValidatedObject>(DicomValidatedObjectSearch.this.getFilter()) {
            @Override
            protected Object getId(final DicomValidatedObject t) {
                return t.getId();
            }
        };
    }
}
