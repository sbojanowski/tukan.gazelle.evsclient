/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.util;

import java.util.Map;
import java.util.Map.Entry;

import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class XmlContentGeneration {

    /**
     * Generates default metadata content
     *
     * @param profileOid: Oid of the the HL7 message profile
     * @param languageEncode: eg English
     */
    public static String generateMetadata(final Map<String, String> metadataContent) {
        final Element root = new Element("MessageMetaData");
        final Document metadata = new Document(root);

        for (final Entry<String,String> entry : metadataContent.entrySet()) {
            final Element dataElement = new Element(entry.getKey());
            dataElement.addContent(entry.getValue());
            root.addContent(dataElement);
        }

        final Format format = Format.getPrettyFormat();
        return new XMLOutputter(format).outputString(metadata);
    }

    /**
     * Generate default validation context content
     *

     */
    public static String generateContext(final String domainName, final String actorName, final String transactionName,
            final String messageType, final String orderControl) {
        final Element root = new Element("ValidationContext");
        final Document context = new Document(root);
        final Element domain = new Element("domain");
        domain.addContent(domainName);
        root.addContent(domain);
        final Element actor = new Element("actor");
        actor.addContent(actorName);
        root.addContent(actor);
        final Element transaction = new Element("transaction");
        transaction.addContent(transactionName);
        root.addContent(transaction);
        final Element msgType = new Element("messageType");
        msgType.addContent(messageType);
        root.addContent(msgType);
        final Element code = new Element("orderControl");
        code.addContent(orderControl);
        root.addContent(code);
        final Format format = Format.getPrettyFormat();
        return new XMLOutputter(format).outputString(context);
    }

    /**
     * Generate a default referenced standard content
     *
     */
    public static String generateReferencedStandard(final ReferencedStandard inStandard) {
        final Element root = new Element("ReferencedStandard");
        final Document standard = new Document(root);
        final Element name = new Element("StandardName");
        name.addContent(inStandard.getName());
        root.addContent(name);
        final Element version = new Element("StandardVersion");
        version.addContent(inStandard.getVersion());
        root.addContent(version);
        final Element extension = new Element("StandardExtension");
        extension.addContent(inStandard.getExtension());
        root.addContent(extension);
        final Format format = Format.getPrettyFormat();
        final String xmlString = new XMLOutputter(format).outputString(standard);
        return xmlString;
    }
}
