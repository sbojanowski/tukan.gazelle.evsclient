/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.pdf.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.util.Util;
import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;

@Entity
@Name("validatedPdf")
@Table(name = "validated_pdf", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "validated_pdf_sequence", sequenceName = "validated_pdf_id_seq", allocationSize = 1)
public class ValidatedPdf extends ValidatedObject {

    private static final long serialVersionUID = -1443811873360866107L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidatedPdf.class);

    private static final String PDF_FILE_PREFIX = "validatedPDF_";
    private static final String PDF_FILE_SUFFIX = ".pdf";
    private static final String STATUS_PASSED_STRING = "PASSED";
    private static final String STATUS_FAILED_STRING = "FAILED";


    @Id
    @NotNull
    @GeneratedValue(generator = "validated_pdf_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "pdf_path")
    private String pdfPath;

    @Lob
    @Type(type = "text")
    private String resultXml;

    @Lob
    @Type(type = "text")
    private String pdfProfiles;

    @Lob
    @Type(type = "text")
    private String testedPdfProfiles;

    private String pdfWellFormed;

    private String pdfValid;




    @Column (name = "validation_service_version")
    private String validationServiceVersion;

    private String  pdfValidColor;
    private String  pdfWellFormedColor;
    private String  pdfProfileColor;

    public ValidatedPdf() {

    }

    public ValidatedPdf(final ValidatedPdf inPdfObject) {
        this.pdfPath = inPdfObject.getPdfPath();
        this.description = inPdfObject.getDescription();
        if (inPdfObject.isLucky()) {
            this.setLucky(inPdfObject.isLucky());
        }
        if (inPdfObject.getMessageContentAnalyzerOid() != null && !inPdfObject.getMessageContentAnalyzerOid()
                .isEmpty()) {
            this.setMessageContentAnalyzerOid(inPdfObject.getMessageContentAnalyzerOid());
        }
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getPdfPath() {
        return this.pdfPath;
    }

    public void setPdfPath(final String pdfPath) {
        this.pdfPath = pdfPath;
    }

    public String getPdfProfiles() {
        return this.pdfProfiles;
    }

    public void setPdfProfiles(final String pdfProfiles) {
        this.pdfProfiles = pdfProfiles;
    }

    public String getTestedPdfProfiles() {
        return this.testedPdfProfiles;
    }

    public void setTestedPdfProfiles(final String testedPdfProfiles) {
        this.testedPdfProfiles = testedPdfProfiles;
    }

    public String getResultXml() {
        return this.resultXml;
    }

    public void setResultXml(final String resultXml) {
        this.resultXml = resultXml;
    }

    public String getPdfWellFormed() {
        return this.pdfWellFormed;
    }

    public void setPdfWellFormed(final String pdfWellFormed) {
        this.pdfWellFormed = pdfWellFormed;
    }

    public String getPdfValid() {
        return this.pdfValid;
    }

    public void setPdfValid(final String pdfValid) {
        this.pdfValid = pdfValid;
        this.setPdfValidColor(pdfValid);
    }

    /**
     * @param inDicomValidatedObject
     */
    public static ValidatedPdf createNewValidatedPdf() {
        ValidatedPdf pdfObject = new ValidatedPdf();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        pdfObject = em.merge(pdfObject);
        em.flush();
        if (pdfObject == null || pdfObject.getId() == 0) {
            ValidatedPdf.LOGGER.error("cannot create PDF");
            return null;
        } else {
            final String pdfRepository = ApplicationPreferenceManager.getStringValue("pdf_repository");
            if (pdfRepository == null || pdfRepository.isEmpty()) {
                ValidatedPdf.LOGGER.error("The repository for PDF files (pdf_repository) has not been set");
                return null;
            } else {
                pdfObject.setPdfPath(pdfRepository + '/' + Util.buildFilePathAccordingToDateAndTime() +'/' + ValidatedPdf.PDF_FILE_PREFIX
                        .concat(pdfObject.getId().toString())
                        .concat(ValidatedPdf.PDF_FILE_SUFFIX));
                return pdfObject;
            }
        }
    }

    /**
     */
    public static ValidatedPdf mergeDicomValidatedObject(ValidatedPdf inPdfObject) {
        if (inPdfObject == null) {
            ValidatedPdf.LOGGER.error("The given ValidatedPdf is null !!!");
            return null;
        } else if (inPdfObject.getPdfPath() == null || inPdfObject.getPdfPath().isEmpty()) {
            ValidatedPdf.LOGGER.error("Cannot merge the given ValidatedPdf because pdfPath is null");
            return null;
        } else {
            final File uploadedFile = new File(inPdfObject.getPdfPath());
            if (!uploadedFile.exists()) {
                ValidatedPdf.LOGGER.error("The given file does not exist: {}", inPdfObject.getPdfPath());
                return null;
            }
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            inPdfObject = em.merge(inPdfObject);
            em.flush();

            // Update the cache with the newest values
            GazelleValidationCacheManager
                    .refreshGazelleCacheWebService(inPdfObject.getExternalId(), inPdfObject.getToolOid(),
                            inPdfObject.getOid(), "off");

            return inPdfObject;
        }
    }

    /**
     */
    public static ValidatedPdf addOrUpdate(ValidatedPdf inMessage) {
        if (inMessage == null) {
            ValidatedPdf.LOGGER.error("the given ValidatedAuditMessage is null");
            return null;
        }
        if (inMessage.getPdfPath() == null || inMessage.getPdfPath().length() == 0) {
            ValidatedPdf.LOGGER.error("The file path is missing, cannot add this object !");
            return inMessage;
        } else {
            final File uploadedFile = new File(inMessage.getPdfPath());
            if (!uploadedFile.exists()) {
                ValidatedPdf.LOGGER.error("The given file does not exist: {}", inMessage.getPdfPath());
                return inMessage;
            }
            if (inMessage.getOid() == null || inMessage.getOid().length() == 0) {
                inMessage.setOid(OIDGenerator.getNewOid());
            }
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            inMessage = em.merge(inMessage);
            em.flush();
            return inMessage;
        }
    }

    @Override
    public String getFilePath() {
        return this.pdfPath;
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/pdfResult.seam?").append("oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

    public String getValidationServiceVersion() {
        return this.validationServiceVersion;
    }

    public void setValidationServiceVersion(final String validationServiceVersion) {
        this.validationServiceVersion = validationServiceVersion;
    }

    public String getPdfWellFormedColor(){
        return this.pdfWellFormedColor;
    }

    public void setPdfWellFormedColor(final String value) {
        this.pdfWellFormedColor = value;

    }

    public String getPdfValidColor(){
        return this.pdfValidColor;
    }

    public void setPdfValidColor(final String value) {
        this.pdfValidColor = value;

    }

    public String getPdfProfileColor(){
        return this.pdfProfileColor;
    }

    public void setPdfProfileColor(final String pdfProfileColor) {
        this.pdfProfileColor = pdfProfileColor;
    }

    public void setStyleAttributes(){
        this.setPdfProfileColor(ValidatedPdf.STATUS_FAILED_STRING);
        this.setPdfWellFormedColor(ValidatedPdf.STATUS_FAILED_STRING);
        this.setPdfValidColor(ValidatedPdf.STATUS_FAILED_STRING);

        if (this.getPdfProfiles().contains("PDF/A")){
            this.setPdfProfileColor(ValidatedPdf.STATUS_PASSED_STRING);
        }
        if ("Valid".equals(this.getPdfValid())){
            this.setPdfValidColor(ValidatedPdf.STATUS_PASSED_STRING);
        }
        if ("Well-formed".equals(this.getPdfWellFormed())){
            this.setPdfWellFormedColor(ValidatedPdf.STATUS_PASSED_STRING);
        }
        this.setValidationStatus(ValidatedPdf.STATUS_FAILED_STRING);
        if (this.getPdfProfileColor().equals(ValidatedPdf.STATUS_PASSED_STRING) && this.getPdfValidColor().equals(ValidatedPdf.STATUS_PASSED_STRING) && this.getPdfWellFormedColor().equals(ValidatedPdf.STATUS_PASSED_STRING) ){
            this.setValidationStatus(ValidatedPdf.STATUS_PASSED_STRING);
        }

    }

    @Override
    public boolean equals(Object obj) {
        if (! super.equals(obj)) {
            return false;
        }
        ValidatedPdf fobj = (ValidatedPdf) obj;

        return id.equals(fobj.getId()) && pdfPath.equals((fobj.getPdfPath()))
                && pdfProfiles.equals(fobj.getPdfProfiles()) ;

    }
}
