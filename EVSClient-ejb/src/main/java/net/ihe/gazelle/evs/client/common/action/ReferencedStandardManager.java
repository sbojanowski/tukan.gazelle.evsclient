/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.menu.EVSMenu;
import net.ihe.gazelle.evs.client.common.model.DocStyleSheet;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandardQuery;
import net.ihe.gazelle.evs.client.util.ValidatorType;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.*;

@Name("referencedStandardManager")
@Scope(ScopeType.PAGE)
public class ReferencedStandardManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3405111152614206073L;

    private ReferencedStandard selectedStandard;

    private DocStyleSheet selectedStyleSheet;

    private final List<SelectItem> friendlyName = new ArrayList<>();

    private SelectItem[] allFriendlyNames;
    private Filter<ReferencedStandard> filter;

    public ReferencedStandardManager() {
    }

    public DocStyleSheet getSelectedStyleSheet() {
        return this.selectedStyleSheet;
    }

    public void setSelectedStyleSheet(final DocStyleSheet selectedStyleSheet) {
        this.selectedStyleSheet = selectedStyleSheet;
    }


    public FilterDataModel<ReferencedStandard> getReferencedStandards() {
        return new FilterDataModel<ReferencedStandard>(ReferencedStandardManager.this.getFilter()) {
            @Override
            protected Object getId(final ReferencedStandard t) {
                return t.getId();
            }
        };
    }

    private Filter<ReferencedStandard> getFilter() {

            if (this.filter == null) {
                final ReferencedStandardQuery query = new ReferencedStandardQuery();
                final HQLCriterionsForFilter<ReferencedStandard> result = query.getHQLCriterionsForFilter();
                this.filter = new Filter<>(result);
            }
            return this.filter;

    }

    @Deprecated
    public String createNewReferencedStandard() {
        return "/administration/referencedStandardDisplay.seam";
    }

    public String backToStandardList() {
        return "/administration/referencedStandards.seam";
    }

    public String displayReferencedStandard(final ReferencedStandard standard) {
        return "/administration/referencedStandardDisplay.seam?id=" + standard.getId();
    }

    public ReferencedStandard getSelectedStandard() {
        return this.selectedStandard;
    }

    public String save() {
        if (this.selectedStandard != null) {
            final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            if (this.selectedStandard.getListDocStyleSheet() != null) {
                for (final DocStyleSheet docst : this.selectedStandard.getListDocStyleSheet()) {
                    entityManager.merge(docst);
                    entityManager.flush();
                }
            }
            this.selectedStandard = entityManager.merge(this.selectedStandard);
            entityManager.flush();
            EVSMenu.forceUpdate();
            FacesMessages.instance()
                    .addFromResourceBundle("net.ihe.gazelle.evs.ReferencedStandardHasBeenSuccessfullySaved");
        }
        return this.backToStandardList();
    }

    public void initReferencedStandardDisplay() {
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams.containsKey("id")) {
            try {
                final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
                this.selectedStandard = entityManager.find(ReferencedStandard.class, Integer.valueOf(urlParams.get("id")));
                this.selectedStandard.setCreation(Boolean.FALSE);
            } catch (final NumberFormatException e) {
                this.selectedStandard = null;
            }
        } else {
            this.selectedStandard = new ReferencedStandard();
            this.selectedStandard.setCreation(Boolean.TRUE);
        }
    }

    public void initSelectedStyleSheet() {
        this.selectedStyleSheet = new DocStyleSheet();
        // this.selectedStyleSheet.setReferencedStandard(this.selectedStandard);
    }

    public void saveModifications() {
        if (this.selectedStyleSheet != null && this.selectedStandard != null) {
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");

            if (this.selectedStandard.getId() == null) {
                this.selectedStandard = em.merge(this.selectedStandard);
                em.flush();
            }
            this.selectedStyleSheet.setReferencedStandard(this.selectedStandard);

            if (this.selectedStyleSheet.getId() == null) {
                this.selectedStyleSheet = em.merge(this.selectedStyleSheet);
                em.flush();
                if (this.selectedStandard.getListDocStyleSheet() == null) {
                    this.selectedStandard.setListDocStyleSheet(new ArrayList<DocStyleSheet>());
                }
                this.selectedStandard.getListDocStyleSheet().add(this.selectedStyleSheet);
            } else {
                this.selectedStyleSheet = em.merge(this.selectedStyleSheet);
                em.flush();
            }
        }
    }

    public void deleteSelectedStyleSheet() {
        if (this.selectedStyleSheet != null && this.selectedStandard != null) {
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            // save the current modifications of the standards (name, etc)
            this.selectedStandard = em.merge(this.selectedStandard);
            em.flush();

            this.selectedStyleSheet = em.find(DocStyleSheet.class, this.selectedStyleSheet.getId());
            em.remove(this.selectedStyleSheet);
            em.flush();
            em.clear();
            this.selectedStandard = em.find(ReferencedStandard.class, this.selectedStandard.getId());
        }
    }

    //Retrieve all the authorize friendly name for Manage referenced standards name
    public void setAllFriendlyNames() {


        final List<ValidatorType> list = Arrays.asList(ValidatorType.values());
        Collections.sort(list);

        for (final ValidatorType validator : list) {
            this.friendlyName.add(new SelectItem(validator.getFriendlyName(), validator.getFriendlyName()));
        }

        this.allFriendlyNames = this.friendlyName.toArray(new SelectItem[this.friendlyName.size()]);
    }


    public SelectItem[] getAllFriendlyNames() {
        this.setAllFriendlyNames();
        return this.allFriendlyNames.clone();
    }

}
