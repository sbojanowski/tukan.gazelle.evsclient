/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.users.action;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import org.apache.commons.lang.StringUtils;
import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.validation.Assertion;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.core.Events;
import org.jboss.seam.log.LogProvider;
import org.jboss.seam.log.Logging;
import org.jboss.seam.security.Identity;
import org.jboss.seam.web.Session;

import javax.ejb.Startup;
import javax.faces.context.FacesContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.*;
import java.util.Map.Entry;

import static org.jboss.seam.annotations.Install.APPLICATION;

@Name("org.jboss.seam.security.identity")
@Scope(ScopeType.SESSION)
@Install(precedence = APPLICATION)
@BypassInterceptors
@Startup
public class SSOIdentity extends Identity {

    private static final long serialVersionUID = -631532323964539777L;

    public static final String AUTHENTICATED_USER = "org.jboss.seam.security.management.authenticatedUser";
    public static final String EVENT_USER_AUTHENTICATED = "org.jboss.seam.security.management.userAuthenticated";
    private static final String SILENT_LOGIN = "org.jboss.seam.security.silentLogin";
    private static final String DEFAULT_ADMIN_USERNAME = "admin";
    private static final String DEFAULT_ADMIN_INSTITUTION = "ADMIN_ORG";

    private static final LogProvider log = Logging.getLogProvider(SSOIdentity.class);
    public static final String INSTITUTION_KEYWORD = "institution_keyword";
    public static final String INSTITUTION_NAME = "institution_name";
    private String institutionKeyword ;
    private String institutionName ;



    @Override
    public String login() {
        SSOIdentity.log.info("Starting authentication");

        try {

            if (super.isLoggedIn()) {
                // If authentication has already occurred during this request
                // via a silent login, and login() is explicitly called then we
                // still want to raise the LOGIN_SUCCESSFUL event, and then
                // return.
                if (Contexts.isEventContextActive() && Contexts.getEventContext().isSet(SSOIdentity.SILENT_LOGIN)) {
                    if (Events.exists()) {
                        Events.instance().raiseEvent(Identity.EVENT_LOGIN_SUCCESSFUL);
                    }
                    return "loggedIn";
                }

                if (Events.exists()) {
                    Events.instance().raiseEvent(Identity.EVENT_ALREADY_LOGGED_IN);
                }
                return "loggedIn";
            }

            this.preAuthenticate();

            if (ApplicationPreferenceManager.getBooleanValue("ip_login")) {
                final Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
                if (request instanceof ServletRequest) {
                    final ServletRequest servletRequest = (ServletRequest) request;
                    String ipRegexp = ApplicationPreferenceManager.getStringValue("ip_login_admin");
                    if (ipRegexp == null) {
                        ipRegexp = "";
                    }
                    final String remoteAddr = servletRequest.getRemoteAddr();
                    if (remoteAddr.matches(ipRegexp)) {
                        this.preAuthenticate();
                        this.acceptExternallyAuthenticatedPrincipal(new IpPrincipal(DEFAULT_ADMIN_USERNAME));
                        this.addRole("admin_role");
                        this.getCredentials().setUsername(DEFAULT_ADMIN_USERNAME);
                        this.postAuthenticate();
                        this.setInstitutionKeyword(DEFAULT_ADMIN_INSTITUTION);
                        this.setInstitutionName(DEFAULT_ADMIN_INSTITUTION);
                        return "loggedIn";
                    }
                }
            }

            AttributePrincipal attributePrincipal = null;
            final Principal casPrincipal = this.getCASPrincipal();
            if (casPrincipal instanceof AttributePrincipal) {
                attributePrincipal = (AttributePrincipal) casPrincipal;
            }

            if (attributePrincipal == null) {
                return super.login();
            } else if (casPrincipal.getName() != null) {
                this.preAuthenticate();

                final String username = casPrincipal.getName();

                SSOIdentity.log.info("Found CAS principal for " + username + ": authenticated");
                final Map attributes = attributePrincipal.getAttributes();
                final Set<Entry> entrySet = attributes.entrySet();
                for (final Entry object : entrySet) {
                    if (INSTITUTION_KEYWORD.equals(object.getKey())){
                        setInstitutionKeyword(object.getValue().toString());
                    }
                    if (INSTITUTION_NAME.equals(object.getKey())){
                        setInstitutionName(object.getValue().toString());
                    }
                    SSOIdentity.log.info("       " + object.getKey() + " = " + object.getValue());
                }

                this.acceptExternallyAuthenticatedPrincipal(casPrincipal);
                if (attributes.get("role_name") != null) {
                    final String roles = (String) attributes.get("role_name");
                    final StringTokenizer st = new StringTokenizer(roles, "[,]");
                    String role;
                    while (st.hasMoreElements()) {
                        role = (String) st.nextElement();
                        role = StringUtils.trimToNull(role);
                        if (role != null) {
                            this.addRole(role);
                        }
                    }
                }
                this.getCredentials().setUsername(username);
                this.postAuthenticate();
                return "loggedIn";

            }

        } catch (final RuntimeException e) {
            this.unAuthenticate();
            throw new RuntimeException(e);
        }

        return null;
    }

    private Principal getCASPrincipal() {
        final HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        final HttpSession session = request.getSession(false);
        final Assertion assertion = (Assertion) (session == null ?
                request.getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION) :
                session.getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION));
        return assertion == null ? null : assertion.getPrincipal();
    }

    @Override
    public boolean isLoggedIn() {
        if (!super.isLoggedIn()) {
            if (this.getCASPrincipal() != null) {
                this.newSessionId();
                this.login();
            }
        }
        return super.isLoggedIn();
    }

    public void newSessionId() {
        final HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        HttpSession session = request.getSession(false);

        final HashMap<String, Object> attributes = new HashMap<>();
        // copy/save all attributes
        @SuppressWarnings("unchecked")
        final Enumeration<String> enames = session.getAttributeNames();
        while (enames.hasMoreElements()) {
            final String name = enames.nextElement();
            if (!"JSESSIONID".equals(name)) {
                attributes.put(name, session.getAttribute(name));
            }
        }

        // create a new session
        session = request.getSession(true);
        // "restore" the session values
        for (final Entry<String, Object> et : attributes.entrySet()) {
            session.setAttribute(et.getKey(),
                    et.getValue()); // <- java.lang.IllegalStateException: setAttribute: Session already invalidated
        }
    }

    @Override
    public void logout() {
        final Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();


        if (request instanceof ServletRequest) {
            final ServletRequest servletRequest = (ServletRequest) request;
            servletRequest.setAttribute(AbstractCasFilter.CONST_CAS_ASSERTION, null);
        }
        final Object session = FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (session instanceof HttpSession) {
            final HttpSession httpSession = (HttpSession) session;
            httpSession.setAttribute(AbstractCasFilter.CONST_CAS_ASSERTION, null);
            Session.instance().invalidate();
        }
        super.logout();
    }


    public static SSOIdentity instance() {
        if (!Contexts.isSessionContextActive()) {
            throw new IllegalStateException("No active session context");
        } else {
            SSOIdentity instance = (SSOIdentity) Component.getInstance(SSOIdentity.class, ScopeType.SESSION);
            if (instance == null) {
                throw new IllegalStateException("No Identity could be created");
            } else {
                return instance;
            }
        }
    }
    public String getInstitutionKeyword() {
        return this.institutionKeyword;
    }

    public void setInstitutionKeyword(String keyword) {
        this.institutionKeyword = keyword;
    }


    public String getInstitutionName() {
        return this.institutionName;
    }

    public void setInstitutionName(String name) {
        this.institutionName = name;
    }

}
