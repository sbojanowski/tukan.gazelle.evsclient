/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

public class ValidationContextHandler extends DefaultHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationContextHandler.class);

    private String patternEnvelope;
    private String patternAssertion;
    private String patternHl7v3;

    private final String message;

    private boolean envelope;
    private boolean assertion;
    private boolean hl7v3;

    public ValidationContextHandler(final String inMessage) {
        this.message = inMessage;
    }

    public String parseMessageContext() {
        try {
            final SAXParserFactory saxFactory = SAXParserFactory.newInstance();
            saxFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            saxFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);

            final SAXParser saxParser = saxFactory.newSAXParser();
            final ByteArrayInputStream bais = new ByteArrayInputStream(this.message.getBytes(StandardCharsets.UTF_8));
            final Reader reader = new InputStreamReader(bais,StandardCharsets.UTF_8);
            final InputSource source = new InputSource(reader);
            source.setEncoding(StandardCharsets.UTF_8.toString());
            // parse file
            saxParser.parse(source, this);
        } catch (SAXException|IOException|ParserConfigurationException e) {
            ValidationContextHandler.LOGGER.error(e.getMessage());
        }
        return this.message;
    }

    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {

        if (this.patternEnvelope != null && qName.matches(this.patternEnvelope)) {
            this.envelope = true;
        } else if (this.patternAssertion != null && qName.matches(this.patternAssertion)) {
            this.assertion = true;
        } else if (this.patternHl7v3 != null && qName.matches(this.patternHl7v3)) {
            this.hl7v3 = true;
        }
    }

    public String getMessage() {
        return this.message;
    }

    public boolean isEnvelope() {
        return this.envelope;
    }

    public boolean isAssertion() {
        return this.assertion;
    }

    public boolean isHl7v3() {
        return this.hl7v3;
    }

    public String getPatternEnvelope() {
        return this.patternEnvelope;
    }

    public void setPatternEnvelope(final String patternEnvelope) {
        this.patternEnvelope = patternEnvelope;
    }

    public String getPatternAssertion() {
        return this.patternAssertion;
    }

    public void setPatternAssertion(final String patternAssertion) {
        this.patternAssertion = patternAssertion;
    }

    public String getPatternHl7v3() {
        return this.patternHl7v3;
    }

    public void setPatternHl7v3(final String patternHl7v3) {
        this.patternHl7v3 = patternHl7v3;
    }

}