/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file;

import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import net.ihe.gazelle.evs.client.common.action.FileToValidate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class SYSLOG implements IAnalyzableContent {
    public static final String SYSLOG = "SYSLOG";
    private static final Logger LOGGER = LoggerFactory.getLogger(SYSLOG.class);

    @Override
    public boolean analyze(final File f, final FileToValidate parent, final FileToValidate ftv, final MessageContentAnalyzer vd,
            final int startOffset, final int endOffset) {
        LOGGER.info("Decode syslog");
        
        // Decode base 64
        final File file;
        file = vd.fileIntoOffsets(f, vd.getSaveStartOffset(), vd.getSaveEndOffset());
        ftv.setDocType(SYSLOG);
        ftv.setStartOffset(vd.getSaveStartOffset());
        ftv.setEndOffset(vd.getSaveEndOffset());

        // Restart test with decoded file
        vd.messageContentAnalyzer(file, 0, 0, ftv);
        AnalyzerFileUtils.deleteTmpFile(file);

        parent.getFileToValidateList().add(ftv);
        return false;
    }

}
