/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file.xml;

import com.google.common.io.Files;
import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.analyzer.utils.AnalyzerFileUtils;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DSUB extends Analyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(DSUB.class);

    @Override
    public Logger getLog() {
        return DSUB.LOGGER;
    }


    @Override
    public String getType() {
        return "DSUB";
    }

    private static final List<String> acceptedTagsDSUB = new ArrayList<>();

    // Define all accepted tag for DSUB. The list of acceptable tags according to OASIS
    // are defined in there :http://docs.oasis-open.org/wsn/b-2.xsd
    // TODO : we may need to be more tolerant here. The validation tool will then report errors.
    public static List<String> getDSUBAcceptedTags() {
        DSUB.acceptedTagsDSUB.add("Subscribe");
        DSUB.acceptedTagsDSUB.add("SubscribeResponse");
        DSUB.acceptedTagsDSUB.add("Unsubscribe");
        DSUB.acceptedTagsDSUB.add("UnsubscribeResponse");
        DSUB.acceptedTagsDSUB.add("Notify");
        DSUB.acceptedTagsDSUB.add("CreatePullPoint");
        DSUB.acceptedTagsDSUB.add("CreatePullPointResponse");
        DSUB.acceptedTagsDSUB.add("DestroyPullPoint");
        DSUB.acceptedTagsDSUB.add("DestroyPullPointResponse");
        DSUB.acceptedTagsDSUB.add("GetMessages");
        DSUB.acceptedTagsDSUB.add("GetMessagesResponse");
        return DSUB.acceptedTagsDSUB;
    }

    public static boolean containsDSUB(final File f, final Map<String, String> namespacesList, final MessageContentAnalyzer mca) {
        File file = null ;
        try {
            file = mca.addMissingNamespaces(f, namespacesList);
        } catch (final IOException e) {
            DSUB.LOGGER.error("Error when writting file : {}\n{}", file, e);
            return false;
        }
        final String fileContent = AnalyzerFileUtils.convertFileToString(file);
        final Boolean res = Boolean.valueOf(DSUB.containsDSUB(fileContent, mca));
        if (res) {
            final String path = mca.setEditedObjectPathWithRepository(mca.getOffsetStart(), mca.getOffsetEnd());
            if (path != null) {
                try {
                    if (mca.getBodyValue() == null) {
                        Files.copy(file, new File(path));
                    } else {
                        Files.move(file, new File(path));
                    }
                } catch (final IllegalArgumentException | IOException e) {
                    DSUB.LOGGER.error("Exception : {}", e);
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error to move file " + file.getAbsolutePath());
                    DSUB.LOGGER.error("Error to rename file {}", file.getAbsolutePath());
                }
            }
            FacesMessages.instance().clearGlobalMessages();
        }
        return res;
    }

    public static boolean containsDSUB(final String fileContent, final MessageContentAnalyzer mca) {
        final List<String> acceptedDSUBTags = DSUB.getDSUBAcceptedTags();

        for (int i = 0; i < acceptedDSUBTags.size(); i++) {
            final String goodTag = acceptedDSUBTags.get(i);
            if (fileContent.contains(goodTag)) {
                DSUB.LOGGER.info(goodTag);

                // First search for a pattern of type <tag/>
                String dsubReg = "<([^<]\\w+:)?\\b"+ goodTag +"\\b([\\r|\\n|\\w|\\W])*?\\/>";

                DSUB.LOGGER.info(dsubReg);

                Pattern regex = Pattern.compile(dsubReg, Pattern.DOTALL);
                Matcher regexMatcher = regex.matcher(fileContent);
                if (regexMatcher.find()) {
                    DSUB.LOGGER.info(regexMatcher.group());

                    mca.setOffsetStart(mca.getSaveStartOffset() + regexMatcher.start() - mca.getMissingTagInducedOffsetParent());
                    mca.setOffsetEnd(mca.getOffsetStart() + regexMatcher.group().length() - mca.getMissingTagInducedOffsetChild());
                    return true;
                    // }oip

                } else {
                    // Search for a pattern <tag>...</tag>
                    dsubReg = "<([^<]\\w+:)?\\b"+goodTag+"\\b([\\r|\\n|\\w|\\W])+.+?\\/([^<]+)?"+goodTag+">";
                    DSUB.LOGGER.info(dsubReg);

                    regex = Pattern.compile(dsubReg, Pattern.DOTALL);
                    regexMatcher = regex.matcher(fileContent);

                    if (regexMatcher.find()) {
                        DSUB.LOGGER.info(regexMatcher.group());
                        mca.setOffsetStart(mca.getSaveStartOffset() + regexMatcher.start() - mca.getMissingTagInducedOffsetParent());
                        mca.setOffsetEnd(mca.getOffsetStart() + regexMatcher.group().length() - mca.getMissingTagInducedOffsetChild());
                        return true;
                    }
                }


            }
        }
        return false;
    }

}
