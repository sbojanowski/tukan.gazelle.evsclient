/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.pdf.action;

import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.evs.client.common.action.AbstractResultDisplayManager;
import net.ihe.gazelle.evs.client.pdf.model.ValidatedPdf;
import net.ihe.gazelle.evs.client.util.Util;
import net.ihe.gazelle.evs.client.util.ValidatorType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.LocaleSelector;
import org.jdom.*;
import org.jdom.input.SAXBuilder;
import org.richfaces.component.UITree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.List;

@Name("pdfResultDisplay")
@Scope(ScopeType.PAGE)
public class PdfResultDisplay extends AbstractResultDisplayManager<ValidatedPdf> implements Serializable {


    private static final long serialVersionUID = 7920478668366529853L;

    private static final SAXBuilder SAX_BUILDER = new SAXBuilder();

    private static final Logger LOGGER = LoggerFactory.getLogger(PdfResultDisplay.class);


    @Override
    public void init(final String objectOid, final String privacyKey) {
        super.init( objectOid,  privacyKey);
        if (this.selectedObject != null) {
            this.selectedObject.setStyleAttributes();
        }
    }

    @Override
    protected Class<ValidatedPdf> getEntityClass() {
        return ValidatedPdf.class;
    }

    @Override
    protected String getValidatorUrl() {
        return "/pdf/validator.seam";
    }

    @Override
    public void downloadResult() {
        ReportExporterManager.exportToFile("text/xml", this.selectedObject.getResultXml(), "validationResult.xml");
    }

    @Override
    public void downloadValidatedObject() {
        try {
            final FacesContext facesContext = FacesContext.getCurrentInstance();
            final byte[] bytes = FileUtils.readFileToByteArray(new File(this.selectedObject.getPdfPath()));
            final String fileName = this.selectedObject.getId() + ".pdf";
            final HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
            final ServletOutputStream servletOutputStream = response.getOutputStream();
            response.setContentType("application/octet-stream");
            response.setContentLength(bytes.length);
            response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + '"');
            servletOutputStream.write(bytes);
            servletOutputStream.flush();
            servletOutputStream.close();
            facesContext.responseComplete();

        } catch (final IOException e) {
            FacesMessages.instance()
                    .addFromResourceBundle("net.ihe.gazelle.evs.pdf.FailToDownloadFile", e.getMessage());
        }

    }

    public String getStringDetailedResult() {
        if (this.selectedObject.getResultXml() == null) {
            return null;
        } else if (this.selectedObject.getService() != null
                && this.selectedObject.getService().getXslLocation() != null) {
            return Util.transformXMLStringToHTML(this.selectedObject.getResultXml(),
                    this.selectedObject.getService().getXslLocation(), LocaleSelector.instance().getLanguage());
        } else {
            return null;
        }
    }

    public GazelleTreeNodeImpl<String> getXmlTree() {
        final GazelleTreeNodeImpl<String> result = new GazelleTreeNodeImpl<>();
        if (this.selectedObject != null && this.selectedObject.getResultXml() != null) {
            final StringReader stringReader = new StringReader(this.selectedObject.getResultXml());
            try {
                final Document document = PdfResultDisplay.SAX_BUILDER.build(stringReader);
                final Element element = document.getRootElement().getChild("properties");
                this.copyElementToNode(element, result, 0);
            } catch (final IOException|JDOMException e) {
                LOGGER.error(e.getMessage(),e);
            }
        }
        return result;
    }

    private void copyElementToNode(final Element element, final GazelleTreeNodeImpl<String> result, int c) {
        final StringBuilder sb = new StringBuilder();
        final int levels = this.getLevels(element, sb);

        if (levels <= 1) {
            result.setData(sb.toString());
        } else {

            final StringBuilder buf = new StringBuilder("<");
            buf.append(element.getName());
            final List<Attribute> attributes = element.getAttributes();
            for (final Attribute attribute : attributes) {
                buf.append(' ');
                buf.append(attribute.getName());
                buf.append("=\"");
                buf.append(attribute.getValue());
                buf.append('"');
            }
            buf.append('>');

            result.setData(buf.toString());

            final List content = element.getContent();
            for (final Object object : content) {
                if (object instanceof Text) {
                    final Text textNode = (Text) object;
                    final String realText = StringUtils.trimToNull(textNode.getTextNormalize());
                    if (realText != null) {
                        final GazelleTreeNodeImpl<String> textTreeNode = new GazelleTreeNodeImpl<>();
                        textTreeNode.setData(realText);
                        c++;
                        result.addChild( Integer.valueOf(c), textTreeNode);
                    }
                }
                if (object instanceof Element) {
                    final Element childElement = (Element) object;
                    final GazelleTreeNodeImpl<String> childNode = new GazelleTreeNodeImpl<>();
                    c++;
                    result.addChild(Integer.valueOf(c), childNode);
                    this.copyElementToNode(childElement, childNode, c);
                }
            }
        }
    }

    private int getLevels(final Element element, final StringBuilder sb) {
        sb.append('<').append(element.getName());
        final List<Attribute> attributes = element.getAttributes();
        for (final Attribute attribute : attributes) {
            sb.append(' ');
            sb.append(attribute.getName());
            sb.append("=\"");
            sb.append(attribute.getValue());
            sb.append('"');
        }
        sb.append('>');

        int i = 0;
        final List content = element.getContent();

        for (final Object object : content) {
            if (i <= 1) {
                if (object instanceof Text) {
                    final Text text = (Text) object;
                    final String realText = StringUtils.trimToNull(text.getTextNormalize());
                    if (realText != null) {
                        i++;
                        sb.append(realText);
                    }
                }
                if (object instanceof Element) {
                    final Element childElement = (Element) object;
                    i++;
                    final int subLevels = this.getLevels(childElement, sb);
                    if (subLevels > 1) {
                        i = 2;
                    }
                }
            }
        }
        sb.append("</");
        sb.append(element.getName());
        sb.append('>');
        return i;
    }

    public Boolean nodeOpened(final UITree tree) {
        return Boolean.TRUE;
    }

    @Override
    public String performAnotherValidation() {
        StringBuilder url = new StringBuilder(ValidatorType.PDF.getView());
        if (selectedObject.getStandard() != null && selectedObject.getStandard().getExtension() != null){
            url.append("?extension=");
            url.append(selectedObject.getStandard().getExtension());
        }
        return url.toString();
    }
}
