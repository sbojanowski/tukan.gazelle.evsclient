/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.logs;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evs.client.common.action.AbstractLogBrowser;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.xml.model.ValidatedXML;
import net.ihe.gazelle.evs.client.xml.model.ValidatedXMLQuery;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;

@Name("xmlSearch")
@Scope(ScopeType.PAGE)
public class XMLSearch extends AbstractLogBrowser<ValidatedXML> {

    private static final long serialVersionUID = -3989216645433570741L;

    @Override
    protected ValidatedXML getObjectByOID(final String oid, final String privacyKey) {
        return ValidatedObject.getObjectByOID(ValidatedXML.class, oid, privacyKey);
    }

    @Override
    protected Class<ValidatedXML> getEntityClass() {
        return ValidatedXML.class;
    }

    @Override
    protected HQLCriterionsForFilter<ValidatedXML> getCriterions() {
        final ValidatedXMLQuery xmlValidatedFileQuery = new ValidatedXMLQuery();
        final HQLCriterionsForFilter<ValidatedXML> result = xmlValidatedFileQuery.getHQLCriterionsForFilter();

        result.addPath("standard", xmlValidatedFileQuery.standard().label());
        result.addPath("validationStatus", xmlValidatedFileQuery.validationStatus());
        result.addPath("privateUser", xmlValidatedFileQuery.privateUser());
        result.addPath("institutionKeyword", xmlValidatedFileQuery.institutionKeyword());
        result.addPath("mbvalidator", xmlValidatedFileQuery.mbvalidator());
        result.addPath("schematron", xmlValidatedFileQuery.schematron());
        result.addPath("validationDate", xmlValidatedFileQuery.validationDate());
        return result;
    }

    @Override
    public FilterDataModel<ValidatedXML> getDataModel() {
        return new FilterDataModel<ValidatedXML>(XMLSearch.this.getFilter()) {
            @Override
            protected Object getId(final ValidatedXML t) {
                return t.getId();
            }
        };
    }
}
