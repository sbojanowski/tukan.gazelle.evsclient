/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.model.StatisticsPerCountry;
import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.evs.client.dicom.model.DicomValidatedObject;
import net.ihe.gazelle.evs.client.hl7.model.HL7ValidatedMessage;
import net.ihe.gazelle.evs.client.pdf.model.ValidatedPdf;
import net.ihe.gazelle.evs.client.tls.model.ValidatedCertificates;
import net.ihe.gazelle.evs.client.util.ValidatorType;
import net.ihe.gazelle.evs.client.xml.model.*;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Name("statisticsManager")
@Scope(ScopeType.PAGE)
public class Statistics implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2463743322212186051L;

    private static final Logger LOGGER = LoggerFactory.getLogger(Statistics.class);

    private transient EntityManager entityManager;

    public Statistics() {
    }

    public List<Object[]> getStatistics() {
        this.entityManager = (EntityManager) Component.getInstance("entityManager");
        List<Object[]> stats = new ArrayList<>();
        stats.add(this.sortStatistics(ValidatedAssertion.class, "Assertions", ValidatorType.SAML));
        stats.add(this.sortStatistics(ValidatedAuditMessage.class, "Audit messages", ValidatorType.ATNA));
        stats.add(this.sortStatistics(CDAValidatedFile.class, "CDA", ValidatorType.CDA));
        stats.add(this.sortStatistics(ValidatedCertificates.class, "Certificates", ValidatorType.TLS));
        stats.add(this.sortStatistics(DicomValidatedObject.class, "DICOM", ValidatorType.DICOM));
        stats.add(this.sortStatistics(ValidatedDSUB.class, "DSUB", ValidatorType.DSUB));
        stats.add(this.sortStatistics(HL7ValidatedMessage.class, "HL7v2.x", ValidatorType.HL7V2));
        stats.add(this.sortStatistics(HL7v3ValidatedMessage.class, "HL7v3", ValidatorType.HL7V3));
        stats.add(this.sortStatistics(ValidatedHPD.class, "HPD", ValidatorType.HPD));
        stats.add(this.sortStatistics(ValidatedPdf.class, "PDF", ValidatorType.PDF));
        stats.add(this.sortStatistics(ValidatedSVS.class, "SVS", ValidatorType.SVS));
        stats.add(this.sortStatistics(ValidatedXDS.class, "XD*", ValidatorType.XDS));
        stats.add(this.sortStatistics(ValidatedXDW.class, "XDW", ValidatorType.XDW));
        stats.add(this.sortStatistics(ValidatedXML.class, "XML", ValidatorType.XML));
        stats.add(this.sortStatistics(ValidatedXML.class, "FHIR", ValidatorType.FHIR));
        return this.computeTotalOnColumns(stats);
    }

    private List<Object[]> computeTotalOnColumns(final List<Object[]> stats) {
        final Object[] total = { "Total", null, Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), null };
        for (final Object[] stat : stats) {
            for (int columnIndex = 2; columnIndex < stat.length - 1; columnIndex++) {
                total[columnIndex] = Integer
                        .valueOf(((Number) total[columnIndex]).intValue() + ((Number) stat[columnIndex]).intValue());
            }
        }
        stats.add(total);
        return stats;
    }

    @SuppressWarnings("unchecked")
    private Object[] sortStatistics(final Class clazz, final String label, final ValidatorType validatorType) {
        final List<Object[]> unsortedStats = ValidatedObject.getStatisticsPerStatus(clazz, this.entityManager);
        // 0: label
        // 1: validator friendly name
        // 2: passed
        // 3: failed
        // 4: aborted
        // 5: not performed
        // 6: undefined
        // 7: total
        final Object[] object = { label, validatorType.getFriendlyName(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), validatorType };

        if (unsortedStats != null && !unsortedStats.isEmpty()) {
            for (final Object[] stat : unsortedStats) {
                final String status = (String) stat[0];
                if (status == null) {
                    object[6] = stat[1];
                } else if ("Total".equalsIgnoreCase(status)) {
                    object[7] = stat[1];
                    continue;
                } else if ("PASSED".equalsIgnoreCase(status)) {
                    object[2] = stat[1];
                    continue;
                } else if ("FAILED".equalsIgnoreCase(status)) {
                    object[3] = stat[1];
                    continue;
                } else if ("ABORTED".equalsIgnoreCase(status)) {
                    object[4] = stat[1];
                    continue;
                } else if ("VALIDATION NOT PERFORMED".equalsIgnoreCase(status)) {
                    object[5] = stat[1];
                    continue;
                } else {
                    object[6] = Integer.valueOf(((Number) object[6]).intValue() + ((Number) stat[1]).intValue());
                }
            }
        }
        return object;
    }

    public void updateCountries(final ValidatorType validator) {
        Statistics.LOGGER.info("validator:{}", validator.getFriendlyName());
        final List<Object[]> stats = new ArrayList<>();
        stats.addAll(this.getStatsForObjectType(validator.getEntityClass()));

        if (ApplicationPreferenceManager.getBooleanValue("include_country_statistics")) {
            for (final Object[] stat : stats) {
                final String ip = (String) stat[0];
                final Integer nbOfCalls = Integer.valueOf(((Number) stat[1]).intValue());
                this.updateCountryStatistics(ip, nbOfCalls);
            }
        }
    }

    private <T> List<Object[]> getStatsForObjectType(final Class<T> clazz) {
        final HQLQueryBuilder<T> builder = new HQLQueryBuilder<>(this.entityManager, clazz);
        return builder.getStatistics("userIp");
    }

    public String getUpdateStatus() {
        return ApplicationPreferenceManager.instance().getStringValue("update_status");
    }

    // @TODO : I think this method belongs to the class StatisticsPerCountry. It is almost a copy of a method there, there is one more
    // argument in this method compare to the one in the other class. At least it would be more coherent to have them all together and avoid the
    // duplication of codes in different classes.
    @SuppressWarnings("unchecked")
    private void updateCountryStatistics(final String ipAddress, final Integer nbOfCallsToAdd) {
        StatisticsPerCountry country;
        final Query query = this.entityManager.createQuery(
                "select c from StatisticsPerCountry c join c.ipAddresses addresses where addresses = :param");
        query.setParameter("param", ipAddress);
        List<StatisticsPerCountry> countries = (List<StatisticsPerCountry>) query.getResultList();
        if (countries != null && !countries.isEmpty()) {
            country = countries.get(0);
            country.setNumberOfCalls(Integer.valueOf(country.getNumberOfCalls() + nbOfCallsToAdd));
            country.save();
        } else {
            // TODO : replace URL with preference value
            final ClientRequest request = new ClientRequest("http://freegeoip.net/xml/" + ipAddress);

            try {
                final ClientResponse<String> response = request.get(String.class);
                if (response.getStatus() == 200 && response.getEntity() != null && !response.getEntity().isEmpty()) {
                    final String info = response.getEntity();
                    String countryName;
                    final String countryNameTag = String.valueOf("<CountryName>");
                    final int pos = info.indexOf(countryNameTag);
                    if (pos > 0) {
                        final int end = info.indexOf("</CountryName");
                        countryName = info.substring(pos + countryNameTag.length() , end);
                        Statistics.LOGGER.info("countryName{}", countryName);
                        final HQLQueryBuilder<StatisticsPerCountry> builder = new HQLQueryBuilder<>(this.entityManager,
                                StatisticsPerCountry.class);
                        builder.addEq("country", countryName);
                        countries = builder.getList();
                        if (countries != null && !countries.isEmpty()) {
                            country = countries.get(0);
                            country.getIpAddresses().add(ipAddress);
                            country.setNumberOfCalls(Integer.valueOf(country.getNumberOfCalls() + nbOfCallsToAdd));
                            country.save();
                        } else {
                            country = new StatisticsPerCountry(countryName, ipAddress);
                            country.setNumberOfCalls(nbOfCallsToAdd);
                            country.save();
                        }
                    }
                }
            } catch (final Exception e) {
                Statistics.LOGGER
                        .error("an error occurred when retrieving the country for ip address: {}", ipAddress, e);
            }
        }
    }
}
