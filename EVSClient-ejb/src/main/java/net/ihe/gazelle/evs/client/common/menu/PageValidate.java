/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;
import net.ihe.gazelle.evs.client.common.model.ReferencedStandard;

import java.lang.ref.Reference;
import java.util.Locale;

public class PageValidate implements Page {

    private static final String DEFAULT_LABEL = "gazelle.evs.client.validate";
    private String url;
    private String label;
    private String name;
    private final String extension;
    private String standardLabel;

    public PageValidate(final ReferencedStandard standard){
        this.url = null;
        this.label = null;
        if (standard != null){
            this.extension = standard.getExtension();
            if (standard.getName() != null) {
                this.name = standard.getName().toLowerCase(Locale.getDefault());
            }
            this.standardLabel = standard.getLabel();
        } else {
            this.extension = null;
            this.name = null;
            this.standardLabel = null;
        }
    }

    public PageValidate(final String url, final String label, final String name, final String extension, final String standardLabel) {
        this.url = url;
        this.label = label;
        if (name != null) {
            this.name = name.toLowerCase(Locale.getDefault());
        }
        this.extension = extension;
        this.standardLabel = standardLabel;
    }

    @Override
    public String getId() {
        return this.getLabel();
    }

    @Override
    public String getLabel() {
        if (this.label == null) {
            this.label = PageValidate.DEFAULT_LABEL;
        }
        return this.label;
    }

    @Override
    public String getLink() {
        if (this.url == null) {
            final StringBuilder builder = new StringBuilder("/");
            builder.append(this.name).append("/validator.seam");
            EVSMenu.appendUrlParameters(builder, this.standardLabel, this.extension);
            this.url = builder.toString();
        }
        return this.url;
    }



    @Override
    public String getIcon() {
        return "fa fa-check-square-o";
    }

    @Override
    public Authorization[] getAuthorizations() {
        return new Authorization[]{Authorizations.ALL};
    }

    @Override
    public String getMenuLink() {
        return this.getLink().replace(".xhtml", ".seam");
    }
}
