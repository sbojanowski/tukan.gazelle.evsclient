/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.util;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GetValidationInfo;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tika.io.IOUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

@Scope(ScopeType.PAGE)
public class ViewFileUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewFileUtils.class);
    private static final String OS = System.getProperty("os.name").toLowerCase(Locale.getDefault());

    public void viewFile(final String fileToDisplay, final File dicomFileToDisplay, final String validationType, final FacesContext facesContext) {
        this.viewFile( fileToDisplay,  dicomFileToDisplay,  validationType,  facesContext.getExternalContext(),
                 facesContext) ;
    }
    public void viewFile(final String fileToDisplay, final File dicomFileToDisplay, final String validationType, final ExternalContext extCtx,
            final FacesContext facesContext) {
        File file = null;
        final HttpServletResponse response = (HttpServletResponse) extCtx.getResponse();
        ByteArrayInputStream messageStream = null;
        if (validationType != null && "DICOM".equals(validationType)) {
            response.setHeader("Content-Disposition", "attachment; filename=\"" + dicomFileToDisplay.getName() + '"');
            final byte[] fileByte;
            try {
                fileByte = IOUtils.toByteArray(new FileInputStream(dicomFileToDisplay));
                messageStream = new ByteArrayInputStream(fileByte);
            } catch (final FileNotFoundException e) {
                ViewFileUtils.LOGGER.error("{}", e);
            } catch (final IOException e) {
                ViewFileUtils.LOGGER.error("{}", e);
            }
        } else {
            messageStream = new ByteArrayInputStream(fileToDisplay.getBytes(StandardCharsets.UTF_8));
        }

        ServletOutputStream servletOutputStream = null;
        try {
            response.setContentType(GetValidationInfo.TEXT_PLAIN);
            file = File.createTempFile("temp-file-to-download", ".tmp");
            if (fileToDisplay != null) {
                FileUtils.writeStringToFile(file, fileToDisplay);
            } else {
                file = dicomFileToDisplay;
            }
            response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + '"');

            servletOutputStream = response.getOutputStream();

            if (validationType != null && "DICOM".equals(validationType)) {
                final byte[] result = this.dicom2txt(dicomFileToDisplay);
                response.setContentLength(result.length);
                final ByteArrayInputStream input = new ByteArrayInputStream(result);
                IOUtils.copyLarge(input, servletOutputStream);
            } else {
                if (fileToDisplay != null) {
                    response.setContentLength(fileToDisplay.length());
                }
                else {
                    ViewFileUtils.LOGGER.error("fileToDisplay is null and this should not be the case");
                }
                final byte[] buf = new byte[8192];
                while (true) {

                    final int length = messageStream.read(buf);
                    if (length < 0) {
                        break;
                    }
                    servletOutputStream.write(buf, 0, length);
                }
            }
            if (messageStream != null) {
                try {
                    messageStream.close();
                } catch (final IOException ignore) {
                }
            }
            if (servletOutputStream != null) {
                try {
                    servletOutputStream.close();
                } catch (final IOException ignore) {
                }
            }
            facesContext.responseComplete();
            if (!file.delete()){
                ViewFileUtils.LOGGER.error("Unable to delete file");
            }

        } catch (final IOException e) {
            if (file != null) {
                if (!file.delete()){
                    ViewFileUtils.LOGGER.error("Unable to delete file");
                }
            }
            ViewFileUtils.LOGGER.error("Failed to download file", e);
            try {
                IOUtils.write("Failed to download file " + ExceptionUtils.getFullStackTrace(e), servletOutputStream);
            } catch (final IOException e1) {
                ViewFileUtils.LOGGER.error("Failed to send error to browser", e);
            }
        }
    }

    public static boolean isWindows() {
        return ViewFileUtils.OS.indexOf("win") >= 0;
    }

    public static boolean isMac() {
        return ViewFileUtils.OS.indexOf("mac") >= 0;
    }

    public static boolean isUnix() {
        return ViewFileUtils.OS.indexOf("nix") >= 0 || ViewFileUtils.OS.indexOf("nux") >= 0 || ViewFileUtils.OS.indexOf("aix") > 0;
    }

    public static boolean isSolaris() {
        return ViewFileUtils.OS.indexOf("sunos") >= 0;
    }

    public byte[] dicom2txt(final File dicomMessage) {

        final File dcmFile = dicomMessage;
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ViewFileUtils.LOGGER.info(ViewFileUtils.OS);
        String pathToDcmDump = ApplicationPreferenceManager.getStringValue("path_to_dcmDump");
        if (pathToDcmDump == null){
            if (ViewFileUtils.isMac()){
                pathToDcmDump = "/opt/local/bin/dcmdump";
            } else {
                pathToDcmDump = "/usr/bin/dcmdump";
            }
        }
        final File dcmdump = new File(pathToDcmDump);

        if (!dcmdump.exists()) {
            ViewFileUtils.LOGGER.error("Please install dcmdump (from DCMTK) in {}", pathToDcmDump);
            FacesMessages.instance().add(Severity.ERROR, "dcmdump is not installed ON the server please contact your administrator");
        } else {
            try {
                final CommandLine commandLine = new CommandLine(pathToDcmDump);
                commandLine.addArgument(dcmFile.getCanonicalPath());
                final PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
                final DefaultExecutor executor = new DefaultExecutor();
                executor.setStreamHandler(streamHandler);
                executor.execute(commandLine);
            } catch (final IOException e) {
                ViewFileUtils.LOGGER.error("{}", e);
                FacesMessages.instance().add(Severity.ERROR, "Error in the transformation of DICOM to text !");
            }
        }
        return outputStream.toByteArray();
    }
}
