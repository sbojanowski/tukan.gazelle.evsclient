/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import net.ihe.gazelle.evs.client.util.Util;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;

@Entity
@Name("validatedAuditMessage")
@Table(name = "atna_validated_message", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "atna_validated_message_sequence", sequenceName = "atna_validated_message_id_seq", allocationSize = 1)
public class ValidatedAuditMessage extends AbstractValidatedXMLFile {

    /**
     *
     */
    private static final long serialVersionUID = 475978417841282758L;

    private static final String FILE_PREFIX = "auditMessage_";
    private static final String FILE_EXTENSION = ".xml";

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidatedAuditMessage.class);

    @Id
    @NotNull
    @Column(name = "id")
    @GeneratedValue(generator = "atna_validated_message_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "message_file_path")
    private String messagePath;

    @Column(name = "schematron")
    private String schematron;


    public ValidatedAuditMessage(final ValidatedAuditMessage message) {
        this.messagePath = message.getMessagePath();
        this.setDescription(message.getDescription());
    }

    public ValidatedAuditMessage() {

    }

    /**
     * Getters and Setters
     */
    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getMessagePath() {
        return this.messagePath;
    }

    public void setMessagePath(final String messagePath) {
        this.messagePath = messagePath;
    }

    @Override
    public String getSchematron() {
        return this.schematron;
    }

    @Override
    public void setSchematron(final String schematron) {
        this.schematron = schematron;
    }

    /**
     * Static methods
     */

    /**
     * creation of a new object in order to compute the name of uploaded file
     */
    public static ValidatedAuditMessage createNewValidatedAuditMessage() {
        ValidatedAuditMessage message = new ValidatedAuditMessage();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        message = em.merge(message);
        em.flush();
        if (message == null || message.getId() == 0) {
            ValidatedAuditMessage.LOGGER.error("An error occurred when creating a new ValidatedAuditMessage object");
            return null;
        }
        final String repository = ApplicationPreferenceManager.getStringValue("atna_repository");
        message.setMessagePath(
                repository + '/' + Util.buildFilePathAccordingToDateAndTime() + '/' + ValidatedAuditMessage.FILE_PREFIX
                        .concat(message.getId().toString()).concat(
                        ValidatedAuditMessage.FILE_EXTENSION));
        return message;
    }

    /**
     * @return
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends AbstractValidatedXMLFile> T addOrUpdate(final Class<T> clazz) {
        if (this.getMessagePath() == null || this.getMessagePath().isEmpty()) {
            ValidatedAuditMessage.LOGGER.error("The file path is missing, cannot add this object !");
            return (T) this;
        } else {
            final File uploadedFile = new File(this.getMessagePath());
            if (!uploadedFile.exists()) {
                ValidatedAuditMessage.LOGGER.error("The given file does not exist: {}", this.getMessagePath());
                return (T) this;
            }
            if (this.getOid() == null || this.getOid().length() == 0) {
                this.setOid(OIDGenerator.getNewOid());
            }
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            final ValidatedAuditMessage message = em.merge(this);
            em.flush();

            //Update the cache with the newest values
            GazelleValidationCacheManager
                    .refreshGazelleCacheWebService(message.getExternalId(), message.getToolOid(), message.getOid(),
                            "off");

            return (T) message;
        }
    }

    @Override
    public String getFilePath() {
        return this.messagePath;
    }

    @Override
    public void setFilePath(final String filePath) {
        this.messagePath = filePath;
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/detailedResult.seam?type=ATNA").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }
}
