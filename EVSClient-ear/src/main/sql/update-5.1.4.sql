UPDATE evsc_validation_service set report_name='EVSClientReport' where name='Model-Based CDA Validator';

INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'reports_path', 'reports', 'java.lang.String', 'path to jasper reports');