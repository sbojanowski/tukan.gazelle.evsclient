INSERT INTO cmn_application_preference (id, class_name, description, preference_name, preference_value)
  VALUES (nextval('cmn_application_preference_id_seq'), 'java.lang.Boolean', 'Execute script to check patient id before the document upload on the server', 'check_CDA_PIDs', 'false');
INSERT INTO cmn_application_preference (id, class_name, description, preference_name, preference_value)
  VALUES (nextval('cmn_application_preference_id_seq'), 'java.lang.String', 'URL where the script with the differents CDA rules are stored', 'check_CDA_PIDs_script_localization', 'http://localhost/ruleScript.js');
