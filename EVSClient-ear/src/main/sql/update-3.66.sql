UPDATE evsc_referenced_standard set menu_display_name = 'Audit messages' where name = 'ATNA';
UPDATE evsc_referenced_standard set menu_display_name = 'CDA' where name = 'CDA';
UPDATE evsc_referenced_standard set menu_display_name = 'DICOM' where name = 'DICOM';
UPDATE evsc_referenced_standard set menu_display_name = 'HL7v3' where name = 'HL7v3';
UPDATE evsc_referenced_standard set menu_display_name = 'HL7v2.x' where name = 'HL7v2';
UPDATE evsc_referenced_standard set menu_display_name = 'Healthcare Provider Directory' where name = 'HPD';
UPDATE evsc_referenced_standard set menu_display_name = 'PDF' where name = 'PDF';
UPDATE evsc_referenced_standard set menu_display_name = 'DSUB' where name = 'DSUB';
UPDATE evsc_referenced_standard set menu_display_name = 'Assertions' where name = 'SAML';
UPDATE evsc_referenced_standard set menu_display_name = 'Sharing Value Set' where name = 'SVS';
UPDATE evsc_referenced_standard set menu_display_name = 'XD* metadata' where name = 'XDS';
UPDATE evsc_referenced_standard set menu_display_name = 'XDW' where name = 'XDW';
UPDATE evsc_referenced_standard set menu_display_name = 'Certificates' where name = 'TLS';
UPDATE evsc_referenced_standard set menu_display_name = 'XML' where name = 'XML';

UPDATE evsc_referenced_standard set icon_path = '/img/logs.gif' where name = 'ATNA';
UPDATE evsc_referenced_standard set icon_path = '/img/HL7logo.gif' where name = 'CDA';
UPDATE evsc_referenced_standard set icon_path = '/img/dicomlogo.gif' where name = 'DICOM';
UPDATE evsc_referenced_standard set icon_path = '/img/HL7logo.gif' where name = 'HL7v3';
UPDATE evsc_referenced_standard set icon_path = '/img/HL7logo.gif' where name = 'HL7v2';
UPDATE evsc_referenced_standard set icon_path = '/img/config-HiRes.gif' where name = 'HPD';
UPDATE evsc_referenced_standard set icon_path = '/img/pdf.png' where name = 'PDF';
UPDATE evsc_referenced_standard set icon_path = '/img/notify.png' where name = 'DSUB';
UPDATE evsc_referenced_standard set icon_path = '/img/oasis.gif' where name = 'SAML';
UPDATE evsc_referenced_standard set icon_path = '/img/config-HiRes.gif' where name = 'SVS';
UPDATE evsc_referenced_standard set icon_path = '/img/config-HiRes.gif' where name = 'XDS';
UPDATE evsc_referenced_standard set icon_path = '/img/workflow.png' where name = 'XDW';
UPDATE evsc_referenced_standard set icon_path = '/img/tls.png' where name = 'TLS';
UPDATE evsc_referenced_standard set icon_path = '/img/xml.png' where name = 'XML';

INSERT INTO evsc_menu_group (id, label, is_available, icon_path, order_in_gui) VALUES (1, 'IHE', true, '/img/IHE.gif', 1);
INSERT INTO evsc_menu_group (id, label, is_available, icon_path, order_in_gui) VALUES (2, 'epSOS', true, '/img/epSOS.gif', 2);
INSERT INTO evsc_menu_group (id, label, is_available, icon_path, order_in_gui) VALUES (3, 'CI-SIS France', true, '/img/asp.jpg', 3);
--INSERT INTO evsc_menu_group (id, label, available, icon_path, order_in_gui) VALUES (4, 'ELGA', false, '/img/elga.png', 4);
--INSERT INTO evsc_menu_group (id, label, available, icon_path, order_in_gui) VALUES (5, 'eSanté', false, '/img/lux-flag.png', 5);
--INSERT INTO evsc_menu_group (id, label, available, icon_path, order_in_gui) VALUES (6, 'Arsenàl.IT', false, '/img/logo_arsenal.png', 6);
--INSERT INTO evsc_menu_group (id, label, available, icon_path, order_in_gui) VALUES (7, 'GE Healthcare', false, '/img/logo_ge.png', 7);
SELECT pg_catalog.setval('evsc_menu_group_id_seq', 3, true);
-- the lines below are valid in IHE Europe instance of EVS Client
-- referenced_standard_id might not be the same in all instances
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (1, 1);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (1, 4);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (1, 6);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (1, 9);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (1, 15);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (1, 17);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (1, 18);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (1, 19);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (1, 22);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (1, 25);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (1, 26);

INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (2, 7);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (2, 8);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (2, 14);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (2, 16);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (2, 20);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (2, 27);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (2, 15);
INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (2, 7);

INSERT INTO evsc_menu_standard (menu_group_id, referenced_standard_id) VALUES (3, 21);

DELETE FROM cmn_application_preference where preference_name = 'display_IHE_menu';
DELETE FROM cmn_application_preference where preference_name = 'display_epSOS_menu';
DELETE FROM cmn_application_preference where preference_name = 'display_ASIP_menu';
DELETE FROM cmn_application_preference where preference_name = 'display_KELA_menu';
DELETE FROM cmn_application_preference where preference_name = 'dicom3tools_path';
DELETE FROM cmn_application_preference where preference_name = 'display_ELGA_menu';
DELETE FROM cmn_application_preference where preference_name = 'display_GE_menu';
DELETE FROM cmn_application_preference where preference_name = 'display_ESANTE_menu';
DELETE FROM cmn_application_preference where preference_name = 'display_ARSENAL_menu';
DELETE FROM cmn_application_preference where preference_name = 'repository_wsdl_location';
DELETE FROM cmn_application_preference where preference_name = 'result_xslt_location';

-- calling tools feature
UPDATE cda_validated_file SET tool_oid = 'proxy_oid' where proxy_id is not null;
UPDATE cda_validated_file SET tool_oid = 'tm_oid' where tm_id is not null;
UPDATE cda_validated_file SET external_id = proxy_id where proxy_id is not null;
UPDATE cda_validated_file SET external_id = tm_id where tm_id is not null;
ALTER TABLE cda_validated_file DROP COLUMN proxy_id;
ALTER TABLE cda_validated_file DROP COLUMN tm_id;

UPDATE atna_validated_message SET tool_oid = 'proxy_oid' where proxy_id is not null;
UPDATE atna_validated_message SET tool_oid = 'tm_oid' where tm_id is not null;
UPDATE atna_validated_message SET external_id = proxy_id where proxy_id is not null;
UPDATE atna_validated_message SET external_id = tm_id where tm_id is not null;
ALTER TABLE atna_validated_message DROP COLUMN proxy_id;
ALTER TABLE atna_validated_message DROP COLUMN tm_id;

UPDATE dicom_validated_object SET tool_oid = 'proxy_oid' where proxy_id is not null;
UPDATE dicom_validated_object SET tool_oid = 'tm_oid' where tm_id is not null;
UPDATE dicom_validated_object SET external_id = proxy_id where proxy_id is not null;
UPDATE dicom_validated_object SET external_id = tm_id where tm_id is not null;
ALTER TABLE dicom_validated_object DROP COLUMN proxy_id;
ALTER TABLE dicom_validated_object DROP COLUMN tm_id;

UPDATE hl7_validated_message SET tool_oid = 'proxy_oid' where proxy_id is not null;
UPDATE hl7_validated_message SET tool_oid = 'tm_oid' where tm_id is not null;
UPDATE hl7_validated_message SET external_id = proxy_id where proxy_id is not null;
UPDATE hl7_validated_message SET external_id = tm_id where tm_id is not null;
ALTER TABLE hl7_validated_message DROP COLUMN proxy_id;
ALTER TABLE hl7_validated_message DROP COLUMN tm_id;

UPDATE hl7v3_validated_message SET tool_oid = 'proxy_oid' where proxy_id is not null;
UPDATE hl7v3_validated_message SET tool_oid = 'tm_oid' where tm_id is not null;
UPDATE hl7v3_validated_message SET external_id = proxy_id where proxy_id is not null;
UPDATE hl7v3_validated_message SET external_id = tm_id where tm_id is not null;
ALTER TABLE hl7v3_validated_message DROP COLUMN proxy_id;
ALTER TABLE hl7v3_validated_message DROP COLUMN tm_id;

UPDATE saml_validated_assertion SET tool_oid = 'proxy_oid' where proxy_id is not null;
UPDATE saml_validated_assertion SET tool_oid = 'tm_oid' where tm_id is not null;
UPDATE saml_validated_assertion SET external_id = proxy_id where proxy_id is not null;
UPDATE saml_validated_assertion SET external_id = tm_id where tm_id is not null;
ALTER TABLE saml_validated_assertion DROP COLUMN proxy_id;
ALTER TABLE saml_validated_assertion DROP COLUMN tm_id;

UPDATE validated_dsub SET tool_oid = 'proxy_oid' where proxy_id is not null;
UPDATE validated_dsub SET tool_oid = 'tm_oid' where tm_id is not null;
UPDATE validated_dsub SET external_id = proxy_id where proxy_id is not null;
UPDATE validated_dsub SET external_id = tm_id where tm_id is not null;
ALTER TABLE validated_dsub DROP COLUMN proxy_id;
ALTER TABLE validated_dsub DROP COLUMN tm_id;

UPDATE validated_hpd SET tool_oid = 'proxy_oid' where proxy_id is not null;
UPDATE validated_hpd SET tool_oid = 'tm_oid' where tm_id is not null;
UPDATE validated_hpd SET external_id = proxy_id where proxy_id is not null;
UPDATE validated_hpd SET external_id = tm_id where tm_id is not null;
ALTER TABLE validated_hpd DROP COLUMN proxy_id;
ALTER TABLE validated_hpd DROP COLUMN tm_id;

UPDATE validated_pdf SET tool_oid = 'proxy_oid' where proxy_id is not null;
UPDATE validated_pdf SET tool_oid = 'tm_oid' where tm_id is not null;
UPDATE validated_pdf SET external_id = proxy_id where proxy_id is not null;
UPDATE validated_pdf SET external_id = tm_id where tm_id is not null;
ALTER TABLE validated_pdf DROP COLUMN proxy_id;
ALTER TABLE validated_pdf DROP COLUMN tm_id;

UPDATE validated_svs SET tool_oid = 'proxy_oid' where proxy_id is not null;
UPDATE validated_svs SET tool_oid = 'tm_oid' where tm_id is not null;
UPDATE validated_svs SET external_id = proxy_id where proxy_id is not null;
UPDATE validated_svs SET external_id = tm_id where tm_id is not null;
ALTER TABLE validated_svs DROP COLUMN proxy_id;
ALTER TABLE validated_svs DROP COLUMN tm_id;

UPDATE validated_xds SET tool_oid = 'proxy_oid' where proxy_id is not null;
UPDATE validated_xds SET tool_oid = 'tm_oid' where tm_id is not null;
UPDATE validated_xds SET external_id = proxy_id where proxy_id is not null;
UPDATE validated_xds SET external_id = tm_id where tm_id is not null;
ALTER TABLE validated_xds DROP COLUMN proxy_id;
ALTER TABLE validated_xds DROP COLUMN tm_id;

UPDATE validated_xdw SET tool_oid = 'proxy_oid' where proxy_id is not null;
UPDATE validated_xdw SET tool_oid = 'tm_oid' where tm_id is not null;
UPDATE validated_xdw SET external_id = proxy_id where proxy_id is not null;
UPDATE validated_xdw SET external_id = tm_id where tm_id is not null;
ALTER TABLE validated_xdw DROP COLUMN proxy_id;
ALTER TABLE validated_xdw DROP COLUMN tm_id;

UPDATE validated_xml SET tool_oid = 'proxy_oid' where proxy_id is not null;
UPDATE validated_xml SET tool_oid = 'tm_oid' where tm_id is not null;
UPDATE validated_xml SET external_id = proxy_id where proxy_id is not null;
UPDATE validated_xml SET external_id = tm_id where tm_id is not null;
ALTER TABLE validated_xml DROP COLUMN proxy_id;
ALTER TABLE validated_xml DROP COLUMN tm_id;


