update evsc_referenced_standard set label = 'HL7v2.x - IHE' where label = 'HL7v2.3.1';
update evsc_referenced_standard set version = '2.x' where label = 'HL7v2.x - IHE';
update evsc_referenced_standard set description = 'HL7v2.x messages defined by IHE' where label = 'HL7v2.x - IHE';
update hl7_validated_message set standard_id = (select id from evsc_referenced_standard where label = 'HL7v2.x - IHE');
delete from evsc_standard_service where referenced_standard_id in (select id from evsc_referenced_standard where name = 'HL7v2' and label != 'HL7v2.x - IHE');
delete from evsc_referenced_standard where name = 'HL7v2' and label != 'HL7v2.x - IHE';