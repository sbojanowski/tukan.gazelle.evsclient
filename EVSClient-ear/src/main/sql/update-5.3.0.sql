INSERT INTO cmn_application_preference (id, class_name, description, preference_name, preference_value)
  VALUES (nextval('cmn_application_preference_id_seq'), 'java.lang.String', 'Where to store FHIR resources', 'fhir_repository', '	/opt/EVSClient_prod/validatedObjects/FHIR');
INSERT INTO cmn_application_preference (id, class_name, description, preference_name, preference_value)
  VALUES (nextval('cmn_application_preference_id_seq'), 'java.lang.String', 'Extensions allowed for uploaded files', 'FHIR_file_types', 'xml,json');
ALTER TABLE evsc_referenced_standard ALTER COLUMN description TYPE text;
ALTER TABLE evsc_validation_service ALTER COLUMN description TYPE text;