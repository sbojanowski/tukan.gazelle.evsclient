INSERT INTO public.evsc_menu_group (id, is_available, icon_path, label, order_in_gui) VALUES (1, true, '/img/IHE.gif', 'IHE', 1);
INSERT INTO public.evsc_menu_group (id, is_available, icon_path, label, order_in_gui) VALUES (7, true, 'https://www.hl7.org/fhir/2015May/assets/images/fhir-logo-www.png', 'FHIR®©', 2);
INSERT INTO public.evsc_menu_group (id, is_available, icon_path, label, order_in_gui) VALUES (8, true, 'https://gazelle.ihe.net/logo/healtheway-logo.gif', 'Sequoia', 3);
INSERT INTO public.evsc_menu_group (id, is_available, icon_path, label, order_in_gui) VALUES (2, true, '/img/epSOS.gif', 'epSOS', 4);
INSERT INTO public.evsc_menu_group (id, is_available, icon_path, label, order_in_gui) VALUES (3, true, '/img/asp.jpg', 'CI-SIS France', 5);
INSERT INTO public.evsc_menu_group (id, is_available, icon_path, label, order_in_gui) VALUES (5, true, 'https://gazelle.ihe.net/EVSClient/img/logs.gif', 'e-SENS', 7);
INSERT INTO public.evsc_menu_group (id, is_available, icon_path, label, order_in_gui) VALUES (6, true, '/img/Germany-Flag-icon.png', 'IHE-D', 8);
INSERT INTO public.evsc_menu_group (id, is_available, icon_path, label, order_in_gui) VALUES (9, false, 'https://gazelle.ihe.net/logo/expand.png', 'EXPAND', 6);