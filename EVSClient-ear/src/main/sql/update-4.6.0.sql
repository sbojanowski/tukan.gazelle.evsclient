UPDATE cda_validated_file SET templates_listed='false';
UPDATE object_indexer SET display='true';
UPDATE object_indexer SET extension='TBD';
UPDATE evsc_referenced_standard set icon_path = '/img/WS_Trust.gif' where name = 'WS_TRUST';