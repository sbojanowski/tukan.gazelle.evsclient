update evsc_referenced_standard  set icon_path  =  '/resources' || icon_path  where icon_path like '/img/%';
update evsc_menu_group  set icon_path  =  '/resources' || icon_path  where icon_path like '/img/%';
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'x_validation_enabled', 'true', 'java.lang.Boolean', 'Indicates if the X validation module is enabled');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'assertion_manager_url', 'http://gazelle.ihe.net/AssertionManagerGui', 'java.lang.String', 'URL to access Assertion Manager');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'xvalidator_directory', '/opt/EVSClient_prod/xvalidation', 'java.lang.String', 'Where to store X validation files');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'xvalidation_root_oid', '1.1.1.1.1.', 'java.lang.String', 'OID for X validation reports');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'svs_repository_url', 'http://gazelle.ihe.net', 'java.lang.String', 'Default SVS repository for X validation');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'xvalidator_xsl_url', 'http://localhost:8380/EVSClient/stylesheet/gazelleXValidator.xsl', 'java.lang.String', 'Stylesheet for displaying validators');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'x_validation_validator', 'false', 'java.lang.Boolean', 'Whether to enable or not the validator feature of X Validation module');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'x_validation_editor', 'true', 'java.lang.Boolean', 'Whether to enable or not the editor feature of X Validation module');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'path_to_dcmDump', '/usr/bin/dcmdump', 'java.lang.Boolean', 'Path to DcmDump executable on the system hosting the application');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'NUMBER_OF_ITEMS_PER_PAGE', '20', 'java.lang.String', 'Number of lines in tables');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'application_gazelle_documentation_url', 'https://gazelle.ihe.net/gazelle-documentation/evs-client/user.html', 'java.lang.String', 'Link to the documentation');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'application_admin_title', 'Project Manager', 'java.lang.String', '');

INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'use_specific_smtp', 'false', 'java.lang.Boolean', 'If true will use information from smtp_host, smtp_usernamen and smtp_password. Otherwize will use localhost on port 25  ');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'smtp_host', 'localhost', 'java.lang.String', 'SMTP Host for sending emails');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'smtp_username', 'username', 'java.lang.String', 'SMTP Username for sending emails');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'smtp_password', 'password', 'java.lang.String', 'SMTP Password for sending emails');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'smtp_port', '25', 'java.lang.Integer', 'SMTP Port for sending emails');


update cda_validated_file set cda_file_path = replace(cda_file_path, 'CDA/validatedCDA', 'CDA' || '/' || date_part('year', validation_date) || '/' || date_part('month', validation_date) || '/'  || date_part('day', validation_date) || '/CDA' )   where cda_file_path like '%CDA/validatedCDA%';
update dicom_validated_object set object_path = replace(object_path, 'DICOM/validatedDICOM', 'DICOM' || '/' || date_part('year', validation_date) || '/' || date_part('month', validation_date) || '/'  || date_part('day', validation_date) || '/DICOM' )   where object_path like '%DICOM/validatedDICOM%';
update atna_validated_message set message_file_path = replace(message_file_path, 'ATNA/auditMessage', 'ATNA' || '/' || date_part('year', validation_date) || '/' || date_part('month', validation_date) || '/'  || date_part('day', validation_date) || '/ATNA' )   where message_file_path like '%ATNA/auditMessage%';
update validated_dsub set dsub_path = replace(dsub_path, 'DSUB/validatedDSUB', 'DSUB' || '/' || date_part('year', validation_date) || '/' || date_part('month', validation_date) || '/'  || date_part('day', validation_date) || '/DSUB' )   where dsub_path like '%DSUB/validatedDSUB%';
update validated_hpd set file_path = replace(file_path, 'HPD/HPD', 'HPD' || '/' || date_part('year', validation_date) || '/' || date_part('month', validation_date) || '/'  || date_part('day', validation_date) || '/HPD' )   where file_path like '%HPD/HPD%';
update validated_pdf set pdf_path = replace(pdf_path, 'PDF/validatedPDF', 'PDF' || '/' || date_part('year', validation_date) || '/' || date_part('month', validation_date) || '/'  || date_part('day', validation_date) || '/PDF' )   where pdf_path like '%PDF/validatedPDF%';
update validated_svs set file_path = replace(file_path, 'SVS/SVS', 'SVS' || '/' || date_part('year', validation_date) || '/' || date_part('month', validation_date) || '/'  || date_part('day', validation_date) || '/SVS' )   where file_path like '%SVS/SVS%';
update object_for_validator_detector set object_path = replace(object_path, 'validatorDetector/objectForValidatorDetector', 'validatorDetector' || '/' || date_part('year', validation_date) || '/' || date_part('month', validation_date) || '/'  || date_part('day', validation_date) || '/validatorDetector' )   where object_path like '%validatorDetector/objectForValidatorDetector%';
update tls_validated_certificate set pem_file_path = replace(pem_file_path, 'TLS/tlsCertificates', 'TLS' || '/' || date_part('year', validation_date) || '/' || date_part('month', validation_date) || '/'  || date_part('day', validation_date) || '/TLS' )   where pem_file_path like '%TLS/tlsCertificates%';
update hl7v3_validated_message set message_file_path = replace(message_file_path, 'HL7v3/validatedHL7v3', 'HL7v3' || '/' || date_part('year', validation_date) || '/' || date_part('month', validation_date) || '/'  || date_part('day', validation_date) || '/HL7v3' )   where message_file_path like '%HL7v3/validatedHL7v3%';

update saml_validated_assertion set file_path = replace(file_path, 'SAML/assertion', 'SAML' || '/' || date_part('year', validation_date) || '/' || date_part('month', validation_date) || '/'  || date_part('day', validation_date) || '/SAML' )   where file_path like '%SAML/assertion%';
update validated_xds set xds_path = replace(xds_path, ''XDS/validatedXDS'', ''XDS'' || ''/'' || date_part(''year'', validation_date) || ''/'' || date_part(''month'', validation_date) || ''/''  || date_part(''day'', validation_date) || ''/XDS'' )   where xds_path like ''%XDS/validatedXDS%'';
update validated_xdw set xdw_path = replace(xdw_path, 'XDW/validatedXDW', 'XDW' || '/' || date_part('year', validation_date) || '/' || date_part('month', validation_date) || '/'  || date_part('day', validation_date) || '/XDW' )   where xdw_path like '%XDW/validatedXDW%';

update validated_xml set xml_path = replace(xml_path, 'XML/validatedXML', 'XML' || '/' || date_part('year', validation_date) || '/' || date_part('month', validation_date) || '/'  || date_part('day', validation_date) || '/XML' )   where xml_path like '%XML//validatedXML%';


update doc_stylesheet set path = replace(path, 'http://gazelle.ihe.net/', 'https://gazelle.ihe.net/')  ;
update evsc_referenced_standard set icon_path='fa fa-book' where icon_path = '/img/logs.gif';
update evsc_referenced_standard set icon_path='gzl-font gzl-dicom' where icon_path = '/img/dicomlogo.gif';
update evsc_referenced_standard set icon_path='gzl-font gzl-hl7' where icon_path = '/img/HL7logo.gif';
update evsc_referenced_standard set icon_path='fa fa-bookmark' where icon_path = '/img/oasis.gif';
update evsc_referenced_standard set icon_path='fa fa-certificate' where icon_path = '/img/tls.png';
update evsc_referenced_standard set icon_path='fa fa-phone' where name = 'HPD';
update evsc_referenced_standard set icon_path='fa fa-flag' where name = 'DSUB';
update evsc_referenced_standard set icon_path='fa fa-file-pdf-o' where name = 'PDF';
update evsc_referenced_standard set icon_path='fa fa-file-pdf-o' where name = 'XDW';

update evsc_referenced_standard set icon_path='fa fa-code' where icon_path = '/img/config-HiRes.gif';

