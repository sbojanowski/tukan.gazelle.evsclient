  CREATE INDEX cda_validated_file_table_id_index ON cda_validated_file(id);

  CREATE INDEX object_indexer_extension_index ON object_indexer(extension);
  CREATE INDEX object_indexer_identifier_index ON object_indexer(identifier);
  CREATE INDEX object_indexer_name_index ON object_indexer(name);

  CREATE INDEX object_indexer_cda_validated_file_object_indexer_id_index ON object_indexer_cda_validated_file (object_indexer_id);
  CREATE INDEX object_indexer_cda_validated_file_cda_validated_file_id_index ON object_indexer_cda_validated_file (cda_validated_file_id);

CREATE INDEX atna_validated_message_id_index ON atna_validated_message(id);
CREATE INDEX atna_validated_message_oid_index ON atna_validated_message(oid);
CREATE INDEX hl7v3_validated_message_id_index ON hl7v3_validated_message(id);
CREATE INDEX hl7v3_validated_message_oid_index ON hl7v3_validated_message(oid);
CREATE INDEX cda_validated_file_id_index ON cda_validated_file(id);
CREATE INDEX validated_xml_id_index ON validated_xml(id);
CREATE INDEX validated_wado_id_index ON validated_wado(id);
CREATE INDEX validated_svs_id_index ON validated_svs(id);
CREATE INDEX validated_hpd_id_index ON validated_hpd(id);


CREATE INDEX cmn_application_preference_id_index ON cmn_application_preference(id);
CREATE INDEX cmn_application_preference_preference_name_index ON cmn_application_preference(preference_name);
CREATE INDEX cmn_application_preference_preference_value_index ON cmn_application_preference(preference_value);
CREATE INDEX cmn_application_preference_class_name_index ON cmn_application_preference(class_name);

create index object_indexer_extension_index on object_indexer(extension);
create index object_indexer_name_index on object_indexer(name);
create index object_indexer_display_index on object_indexer(display);
create index object_indexer_identifier_index on object_indexer(identifier);
create index object_indexer_type_index on object_indexer(type);
CREATE index object_indexer_cda_validated_file_object_indexer_id_index on object_indexer_cda_validated_file(object_indexer_id);
CREATE index object_indexer_cda_validated_file_cda_validated_file_id_index on object_indexer_cda_validated_file(cda_validated_file_id);

create index cda_validated_file_oid_index on cda_validated_file(oid);
create index cda_validated_file_oid_index on cda_validated_file(oid);
create index cda_validated_file_is_public_index on cda_validated_file(is_public);
create index cda_validated_file_private_user_index on cda_validated_file(private_user);
