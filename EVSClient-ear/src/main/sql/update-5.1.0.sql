INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'cda_scorecard_xsl_path', 'https://gazelle.ihe.net/xsl/scorecard/cda-scorecard.xsl', 'java.lang.String', 'XSL to transform CDA scorecard');
UPDATE cda_validated_file SET scorecard_status = 3 where model_based_validator is NULL ;
UPDATE cda_validated_file SET scorecard_status = 0 where model_based_validator is not NULL ;
UPDATE cda_validated_file SET privacy_key = null where is_public = true;
UPDATE atna_validated_message SET privacy_key = null where is_public = true;
UPDATE dicom_validated_object SET privacy_key = null where is_public = true;
UPDATE hl7_validated_message SET privacy_key = null where is_public = true;
UPDATE hl7v3_validated_message SET privacy_key = null where is_public = true;
UPDATE validated_pdf SET privacy_key = null where is_public = true;
UPDATE tls_validated_certificate SET privacy_key = null where is_public = true;
UPDATE saml_validated_assertion SET privacy_key = null where is_public = true;
UPDATE validated_hpd SET privacy_key = null where is_public = true;
UPDATE validated_svs SET privacy_key = null where is_public = true;
UPDATE validated_xds SET privacy_key = null where is_public = true;
UPDATE validated_xdw SET privacy_key = null where is_public = true;
UPDATE validated_wado SET privacy_key = null where is_public = true;
UPDATE validated_xml SET privacy_key = null where is_public = true;
UPDATE validated_dicomweb SET privacy_key = null where is_public = true;
UPDATE validated_dsub SET privacy_key = null where is_public = true;


-- fix the validated_xml table, some entries where not registered with the correct standard
UPDATE validated_xml SET standard_id = (select id from evsc_referenced_standard where extension = 'FHIR') where called_method like 'FHIR%' and standard_id = (select id from evsc_referenced_standard where extension = 'WSTrust');
UPDATE validated_xml SET standard_id = (select id from evsc_referenced_standard where extension = 'SMP (eSENS)') where called_method like 'SMP%' and standard_id = (select id from evsc_referenced_standard where extension = 'WSTrust');
UPDATE validated_xml SET standard_id = (select id from evsc_referenced_standard where extension = 'eID (eSENS)') where called_method like 'eID%' and standard_id = (select id from evsc_referenced_standard where extension = 'WSTrust');
UPDATE validated_xml SET standard_id = (select id from evsc_referenced_standard where extension = 'REM (eSENS)') where called_method like 'REM%' and standard_id = (select id from evsc_referenced_standard where extension = 'WSTrust');

-- this entity has been removed, it was never used
DROP TABLE validated_ws_trust;
DROP TABLE evsc_user_user_role;
DROP TABLE evsc_user;
DROP TABLE evsc_user_role;
DROP TABLE user_permission;
DROP TABLE user_role_group;