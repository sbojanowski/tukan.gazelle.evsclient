UPDATE atna_validated_message SET is_public = true where private_user is null; 
UPDATE atna_validated_message SET is_public = false where private_user is not null; 

UPDATE cda_validated_file SET is_public = true where private_user is null; 
UPDATE cda_validated_file SET is_public = false where private_user is not null; 

UPDATE dicom_validated_object SET is_public = true where private_user is null; 
UPDATE dicom_validated_object SET is_public = false where private_user is not null; 

UPDATE hl7_validated_message SET is_public = true where private_user is null; 
UPDATE hl7_validated_message SET is_public = false where private_user is not null; 

UPDATE hl7v3_validated_message SET is_public = true where private_user is null; 
UPDATE hl7v3_validated_message SET is_public = false where private_user is not null; 

UPDATE saml_validated_assertion SET is_public = true where private_user is null; 
UPDATE saml_validated_assertion SET is_public = false where private_user is not null; 

UPDATE tls_validated_certificate SET is_public = true where private_user is null; 
UPDATE tls_validated_certificate SET is_public = false where private_user is not null; 

UPDATE validated_pdf SET is_public = true where private_user is null; 
UPDATE validated_pdf SET is_public = false where private_user is not null; 

UPDATE validated_svs SET is_public = true where private_user is null; 
UPDATE validated_svs SET is_public = false where private_user is not null; 

UPDATE validated_xds SET is_public = true where private_user is null; 
UPDATE validated_xds SET is_public = false where private_user is not null; 

UPDATE validated_xdw SET is_public = true where private_user is null; 
UPDATE validated_xdw SET is_public = false where private_user is not null; 
