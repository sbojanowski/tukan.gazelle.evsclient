/**
 * Check Kela PIDs
 * @author Youn Cadoret
 *
 * file:///home/ycadoret/Documents/js_rule.js
 *
 */


function ruleControl(newXml) {

    /*<------------ var declaration ------------>*/
    var message, popUpMessage, title, tagResult, root, extension, found ='', pathRoot, code;
    var listValidExtension = [];
    var listInvalidExtension = [];
    var i = 0;
    var docValidation = "false";
    var localValidate = document.getElementById('scriptResult');

    //<== Regex rules declaration ==>
    var extensionRegex = new RegExp('^[0-3]{1}[0-9]{5}(A|-)9{1}[0-9]{2}[0-9A-Z]{1}$');
    //var rootRegex = new RegExp('^1{1}\.2{1}\.246{1}\.21{1}$');
    // add another Regex ...
    //</== Regex rules declaration ==>

    //<== xPath declaration ==>
    var pathExtension = "//*[@root='1.2.246.21']/@extension";
    var pathCode = "//*[@codeSystem='1.2.246.21']/@code";
    // add another xPath ...
    //</== xPath declaration ==>
    /*<------------ /var declaration ------------>*/



    /*<------------ check rules ------------>*/
    //<== Check_extensions id ==>
    if (newXml.evaluate) {
        var nodes = newXml.evaluate(pathExtension, newXml, null, XPathResult.ANY_TYPE, null);
        var result = nodes.iterateNext();
        while (result) {
            extension = result.value;
            if (extension.match(extensionRegex)) {
                docValidation = "true";
                listValidExtension.push(extension);
            } else {
                listInvalidExtension.push(extension);
                found += "\u2022 root='1.2.246.21' extension='" + extension + "' \n";
                i = i + 1;
            }
            result = nodes.iterateNext();
        }
    }
    //</== Check_extensions id ==>


    //<== Check_code PAT ==>
    if (newXml.evaluate) {
        var nodesCode = newXml.evaluate(pathCode, newXml, null, XPathResult.ANY_TYPE, null);
        var resultCode = nodesCode.iterateNext();
        if (resultCode != null) {
            code = resultCode.value;
            if (code.match(extensionRegex)) {
                docValidation = "true";
            } else {
                found += "\u2022 codseSystem='1.2.246.21' code='" + code + "' \n";
                i = i + 1;
            }
        }
    }
    //</== Check_code PAT ==>



    /*
     //<== Check_root id ==>
     if (newXml.evaluate) {
     listValidExtension.forEach(function (listExt) {
     pathRoot = "//*[@extension='" + listExt + "']/@root";
     nodes = newXml.evaluate(pathRoot, newXml, null, XPathResult.ANY_TYPE, null);
     result = nodes.iterateNext();
     root = result.value;
     if (root.match(rootRegex)) {
     docValidation = "true";
     } else {
     found += "\u2022 <id root='" + root + "' extension='" + listExt + "'/> \n";
     i = i + 1;
     }
     });

     listInvalidExtension.forEach(function (listInvExt) {
     pathRoot = "//*[@extension='" + listInvExt + "']/@root";
     nodes = newXml.evaluate(pathRoot, newXml, null, XPathResult.ANY_TYPE, null);
     result = nodes.iterateNext();
     root = result.value;
     if (root.match(rootRegex)) {
     docValidation = "true";
     found += "\u2022 <id root='" + root + "' extension='" + listInvExt + "'/> \n";
     } else {
     found += "\u2022 <id root='" + root + "' extension='" + listInvExt + "'/> \n";
     i = i + 1;
     }
     });
     }
     //</== Check_root id ==>
     */

    //add another rules ...

    /*<------------ /check rules ------------>*/


    //<== Message Result ==>
    if (i == 0 && docValidation == "true") {
        $(".rf-fu-btn-upl").click();
        message = "Document Successfully Verified";
        localValidate.style.color = "green";
        localValidate.style.border = "5px solid rgba(214, 233, 198)";
        localValidate.style.background = "rgba(214, 233, 198)";
        localValidate.innerText = "Document successfully verified for potential patient data exposure";
        //popUpMessage = message+"\n\n"+ found;

    } else {
        $(".rf-fu-btn-upl").hide();
        tagResult = "DANGER : ";
        title = tagResult + "Potential Patient Data Exposure";
        message = "It seems that the uploaded document contains real patient data, therefore the document";
        message += " will not be further processed and will not be send to the validation tool. Please review ";
        message += "the document and make sure that it does not contain real patient data.";
        localValidate.style.color = "red";
        localValidate.style.border = "5px solid rgba(235, 204, 209)";
        localValidate.style.background = "rgba(235, 204, 209)";
        localValidate.innerText = 'Document seems to expose patient data cannot be processed';
        popUpMessage = title + "\n\n" + message + "\n\n" + found;
        window.alert(popUpMessage);
    }

    //</== Message Result ==>

}



