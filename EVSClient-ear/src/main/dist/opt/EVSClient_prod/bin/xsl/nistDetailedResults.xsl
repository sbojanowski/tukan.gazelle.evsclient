<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ EVS Client is part of the Gazelle Test Bed
  ~ Copyright (C) 2006-2016 IHE
  ~ mailto :eric DOT poiseau AT inria DOT fr
  ~
  ~ See the NOTICE file distributed with this work for additional information
  ~ regarding copyright ownership.  This code is licensed
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"
                omit-xml-declaration="yes"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p>
                <xd:b>Created on:</xd:b>
                Oct 26th, 2010
            </xd:p>
            <xd:p>
                <xd:b>Author:</xd:b>
                Anne-Gaëlle BERGE, IHE Development, INRIA Rennes
            </xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <div>
            <xsl:if
                    test="ValidationReport/ReportDetails/Hl7v2ValidationReport/MessageValidation/Result/@ErrorCount &gt; 0">
                <p id="errors">
                    <b>
                        <u>Error Level</u>
                    </b>
                </p>
                <xsl:for-each
                        select="ValidationReport/ReportDetails/Hl7v2ValidationReport/MessageValidation/Result/Error">
                    <table width="100%" class="Error">
                        <tr>
                            <td valign="top" width="10%">
                                <b>Failure Type</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="@FailureType"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="10%">
                                <b>Failure Serveriy</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="@FailureSeverity"/>
                            </td>
                        </tr>
                        <xsl:if test="count(Location) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Location</b>
                                </td>
                                <td align="left">
                                    Line:
                                    <xsl:value-of select="Location/Line"/> ; Column:
                                    <xsl:value-of select="Location/Column"/>
                                    <br/>
                                    <xsl:if test="count(Location/EPath)">
                                        EPath:
                                        <xsl:value-of select="Location/EPath"/>
                                    </xsl:if>
                                </td>
                            </tr>
                        </xsl:if>
                        <tr>
                            <td valign="top" width="10%">
                                <b>Description</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="Description"/>
                            </td>
                        </tr>
                        <xsl:if test="count(ElementContent) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Element content</b>
                                </td>
                                <td align="left">
                                    <xsl:value-of select="ElementContent"/>
                                </td>
                            </tr>
                        </xsl:if>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if
                    test="ValidationReport/ReportDetails/Hl7v2ValidationReport/MessageValidation/Result/@WarningCount &gt; 0">
                <p id="warnings">
                    <b>
                        <u>Warning Level</u>
                    </b>
                </p>
                <xsl:for-each
                        select="ValidationReport/ReportDetails/Hl7v2ValidationReport/MessageValidation/Result/Warning">
                    <table width="100%" class="Warning">
                        <tr>
                            <td valign="top" width="10%">
                                <b>Failure Type</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="@FailureType"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="10%">
                                <b>Failure Serveriy</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="@FailureSeverity"/>
                            </td>
                        </tr>
                        <xsl:if test="count(Location) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Location</b>
                                </td>
                                <td align="left">
                                    Line:
                                    <xsl:value-of select="Location/Line"/> ; Column:
                                    <xsl:value-of select="Location/Column"/>
                                    <br/>
                                    <xsl:if test="count(Location/EPath)">
                                        EPath:
                                        <xsl:value-of select="Location/EPath"/>
                                    </xsl:if>
                                </td>
                            </tr>
                        </xsl:if>
                        <tr>
                            <td valign="top" width="10%">
                                <b>Description</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="Description"/>
                            </td>
                        </tr>
                        <xsl:if test="count(ElementContent) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Element content</b>
                                </td>
                                <td align="left">
                                    <xsl:value-of select="ElementContent"/>
                                </td>
                            </tr>
                        </xsl:if>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if
                    test="ValidationReport/ReportDetails/Hl7v2ValidationReport/MessageValidation/Result/@UserCount &gt; 0">
                <p id="users">
                    <b>
                        <u>User Level</u>
                    </b>
                </p>
                <xsl:for-each
                        select="ValidationReport/ReportDetails/Hl7v2ValidationReport/MessageValidation/Result/User">
                    <table width="100%" class="Note">
                        <tr>
                            <td valign="top" width="10%">
                                <b>Failure Type</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="@FailureType"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="10%">
                                <b>Failure Serveriy</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="@FailureSeverity"/>
                            </td>
                        </tr>
                        <xsl:if test="count(Location) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Location</b>
                                </td>
                                <td align="left">
                                    Line:
                                    <xsl:value-of select="Location/Line"/> ; Column:
                                    <xsl:value-of select="Location/Column"/>
                                    <br/>
                                    <xsl:if test="count(Location/EPath)">
                                        EPath:
                                        <xsl:value-of select="Location/EPath"/>
                                    </xsl:if>
                                </td>
                            </tr>
                        </xsl:if>
                        <tr>
                            <td valign="top" width="10%">
                                <b>Description</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="Description"/>
                            </td>
                        </tr>
                        <xsl:if test="count(ElementContent) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Element content</b>
                                </td>
                                <td align="left">
                                    <xsl:value-of select="ElementContent"/>
                                </td>
                            </tr>
                        </xsl:if>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if
                    test="ValidationReport/ReportDetails/Hl7v2ValidationReport/MessageValidation/Result/@IgnoreCount &gt; 0">
                <p id="ignores">
                    <b>
                        <u>Ignore Failures</u>
                    </b>
                </p>
                <xsl:for-each
                        select="ValidationReport/ReportDetails/Hl7v2ValidationReport/MessageValidation/Result/Ignore">
                    <table width="100%" class="Unknown">
                        <tr>
                            <td valign="top" width="10%">
                                <b>Failure Type</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="@FailureType"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="10%">
                                <b>Failure Serveriy</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="@FailureSeverity"/>
                            </td>
                        </tr>
                        <xsl:if test="count(Location) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Location</b>
                                </td>
                                <td align="left">
                                    Line:
                                    <xsl:value-of select="Location/Line"/> ; Column:
                                    <xsl:value-of select="Location/Column"/>
                                    <br/>
                                    <xsl:if test="count(Location/EPath)">
                                        EPath:
                                        <xsl:value-of select="Location/EPath"/>
                                    </xsl:if>
                                </td>
                            </tr>
                        </xsl:if>
                        <tr>
                            <td valign="top" width="10%">
                                <b>Description</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="Description"/>
                            </td>
                        </tr>
                        <xsl:if test="count(ElementContent) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Element content</b>
                                </td>
                                <td align="left">
                                    <xsl:value-of select="ElementContent"/>
                                </td>
                            </tr>
                        </xsl:if>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
        </div>
    </xsl:template>
</xsl:stylesheet>