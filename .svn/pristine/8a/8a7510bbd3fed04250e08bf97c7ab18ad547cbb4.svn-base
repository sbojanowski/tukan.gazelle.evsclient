package net.ihe.gazelle.evs.client.integration;

import junit.framework.TestCase;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub.*;
import net.ihe.gazelle.sch.validator.ws.client.SOAPExceptionException;
import org.apache.axis2.Constants;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;

/**
 * Class Description : Integration testing related to webservices used on the validation of CDA Documents using the tool SchematronValidator.
 * The Schematron validator offer a webservice that allow to get the list of schematron, and allow to validate a document against a type of validation
 * <p/>
 * The aim of this class is to test the reachability of this webservice
 *
 * @author abderrazek boufahja / Kereval / IHE-Europe
 * @version 1.0 - 22 jul 2013
 * @see > abderrazek.boufahja@ihe-europe.com  -  http://www.ihe-europe.org
 */
public class SCHValidatorTest extends TestCase {

    private static final String SCH_ENDPOINT =
             "https://gazelle.ihe.net/SchematronValidator-ejb/GazelleObjectValidatorService/GazelleObjectValidator?wsdl";

    /**
     * Get the list of vailable schematrons related to a kind of document
     *
     * @throws RemoteException
     * @throws SOAPException
     */
    public void testListSchematrons() throws RemoteException, SOAPExceptionException {
        GazelleObjectValidatorServiceStub stub = new GazelleObjectValidatorServiceStub(SCH_ENDPOINT);
        stub._getServiceClient().getOptions().setProperty(Constants.Configuration.DISABLE_SOAP_ACTION, true);
        GetSchematronsForAGivenType params = new GetSchematronsForAGivenType();
        params.setFileType("CDA-IHE");
        GetSchematronsForAGivenTypeE paramsE = new GetSchematronsForAGivenTypeE();
        paramsE.setGetSchematronsForAGivenType(params);
        GetSchematronsForAGivenTypeResponseE responseE = stub.getSchematronsForAGivenType(paramsE);
        net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub.Schematron[] schematrons = responseE
                .getGetSchematronsForAGivenTypeResponse().getSchematrons();
        assertTrue(schematrons.length > 0);
    }

    /**
     * validate a document with schematronValidator according to a kind of schemtron
     *
     * @throws RemoteException
     * @throws SOAPExceptionException
     */
    public void testValidationDocumentAgainstSchematron() throws IOException, SOAPExceptionException {
        GazelleObjectValidatorServiceStub stub = new GazelleObjectValidatorServiceStub(SCH_ENDPOINT);
        stub._getServiceClient().getOptions().setProperty(Constants.Configuration.DISABLE_SOAP_ACTION, true);
        ValidateObject params = new ValidateObject();
        params.setXmlMetadata("IHE - ITI - Cross-Enterprise Sharing of Scanned Documents (XDS-SD)");
        params.setXmlReferencedStandard("CDA-IHE");
//        params.setBase64ObjectToValidate(Base64.encodeFromFile("src/test/resources/cda/sample1.xml"));
        params.setBase64ObjectToValidate(DatatypeConverter.printBase64Binary(Files.readAllBytes(Paths.get("src/test/resources/cda/sample1.xml"))));
        ValidateObjectE paramsE = new ValidateObjectE();
        paramsE.setValidateObject(params);
        ValidateObjectResponseE responseE = stub.validateObject(paramsE);
        String detailedResult = responseE.getValidateObjectResponse().getValidationResult();
        assertTrue(detailedResult.contains("<ValidationTestResult>"));
    }

}
