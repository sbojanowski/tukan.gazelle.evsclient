/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.action;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;

/**
 * Created by epoiseau ON 20/06/2016.
 */
@Name("preferenceKeyValidator")
@Scope(ScopeType.PAGE)
public class PreferenceKeyValidator implements Validator, Serializable {

    private static final long serialVersionUID = 5035588606311769149L;

    @Override
    public void validate(final FacesContext context, final UIComponent component,
            final Object object) throws ValidatorException {
        final String label = String.valueOf(object);
        boolean valid = true;

        if (!ApplicationConfiguration.checkPreferenceKeyIsUnique(label)) {
            valid = false;
        }
        if (!valid) {
            final FacesMessage message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Preference key already exist",
                    "Preference key must be unique. '" + label + "' is already in use.");
            throw new ValidatorException(message);
        }
    }
}
