/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.validator;

import net.ihe.gazelle.evs.client.xml.model.FhirObjectType;
import net.ihe.gazelle.evs.client.xml.model.ValidatedFHIR;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

@Name("fhirValidator")
@Scope(ScopeType.PAGE)
public class FHIRValidator extends AbstractXMLValidator<ValidatedFHIR> {

    /**
     *
     */
    private static final long serialVersionUID = -8048195075625428371L;
    private FhirObjectType selectedObjectType = FhirObjectType.XML;

    public boolean isValidatingUrl() {
        return FhirObjectType.URL.equals(selectedObjectType);
    }

    @Override
    protected ValidatedFHIR newInstance() {
        return ValidatedFHIR.createNewValidatedFHIR(selectedObjectType);
    }

    @Override
    public Class<ValidatedFHIR> getEntityClass() {
        return ValidatedFHIR.class;
    }

    public FhirObjectType getSelectedObjectType() {
        return selectedObjectType;
    }

    public void setSelectedObjectType(FhirObjectType selectedObjectType) {
        this.selectedObjectType = selectedObjectType;
        if (isValidatingUrl()
                && (validatedFile == null || !FhirObjectType.XML.equals(validatedFile.getValidatedObjectType()))) {
            validatedFile = ValidatedFHIR.createNewValidatedFHIR(selectedObjectType);
        } else if (!isValidatingUrl() && validatedFile != null && FhirObjectType.URL.equals(validatedFile.getValidatedObjectType())) {
            validatedFile = null;
        }
    }

    public List<SelectItem> getObjectTypes() {
        List<SelectItem> items = new ArrayList<SelectItem>();
        for (FhirObjectType type : FhirObjectType.values()) {
            items.add(new SelectItem(type, type.getLabel()));
        }
        return items;
    }

    @Override
    public void init(final String validatorType){
        super.init(validatorType);
        if (this.validatedFile != null){
            this.selectedObjectType = validatedFile.getValidatedObjectType();
        }
    }

    @Override
    public void validateFile() {
        if (isValidatingUrl()) {
            if (getUrlToValidate() == null || getUrlToValidate().isEmpty()) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "You must entered the URL to validate");
                return;
            }
        }
        super.validateFile();
    }

    @Override
    public void reset() {
        this.selectedObjectType = FhirObjectType.XML;
        setUrlToValidate(null);
        super.reset();
    }

    @Override
    public String getFileContent() {
        if (validatedFile != null) {
            switch (validatedFile.getValidatedObjectType()) {
                case XML:
                    return super.getFileContent();
                case JSON:
                    return ValidatedFHIR.formatJsonContent(validatedFile, getIndentMessageInGUI());
                case URL:
                    return validatedFile.getFileContent();
                default:
                    return null;
            }
        } else {
            return null;
        }
    }


}
