<!--
  ~ EVS Client is part of the Gazelle Test Bed
  ~ Copyright (C) 2006-2016 IHE
  ~ mailto :eric DOT poiseau AT inria DOT fr
  ~
  ~ See the NOTICE file distributed with this work for additional information
  ~ regarding copyright ownership.  This code is licensed
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->

<!DOCTYPE composition PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<ui:composition xmlns:h="http://java.sun.com/jsf/html" xmlns:s="http://jboss.org/schema/seam/taglib"
                xmlns:g="http://www.ihe.net/gazelle" xmlns:f="http://java.sun.com/jsf/core"
                xmlns:rich="http://richfaces.org/rich" xmlns:gdk="http://www.ihe.net/gazellecdk"
                xmlns:a4j="http://richfaces.org/a4j" xmlns:ui="http://java.sun.com/jsf/facelets"
                xmlns="http://www.w3.org/1999/xhtml" template="//layout/template.xhtml">
    <ui:param name="pageName" value="#{messages['gazelle.evs.client.admin.menu.callingTools']}"/>
    <ui:param name="pageNameUrl" value="administration/callingTools.seam"/>
    <ui:param name="pageNameTitle" value="#{messages['gazelle.evs.client.admin.menu.callingTools']}"/>
    <ui:define name="body">
        <h:form id="newToolForm">
            <s:div styleClass="form-horizontal">

                <s:div id="newToolDiv" rendered="#{callingToolManager.newTool != null}">
                    <ui:decorate template="/layout/panels/_panel_title_footer_id.xhtml">
                        <ui:define name="panel_title">
                            <h:outputText value="#{messages['gazelle.evs.client.RegisterANewTool']}"/>
                        </ui:define>


                        <ui:param name="panel_id" value="newToolPanel"/>

                        <s:decorate id="newLabelDecorate" template="/layout/form/_form_field_horizontal_inline.xhtml">
                            <ui:param name="id" value="newLabel"/>
                            <ui:define name="label">#{messages['gazelle.evs.client.Label']}</ui:define>
                            <h:inputText value="#{callingToolManager.newTool.label}" id="newLabel" required="true"
                                         styleClass="form-control">
                                        <f:validator  binding="#{callingToolLabelValidator}" />
                                <a4j:ajax event="blur" render="newLabelDecorate" execute="@this"/>
                            </h:inputText>
                        </s:decorate>

                        <s:decorate id="newOIDDecorate" template="/layout/form/_form_field_horizontal_inline.xhtml">
                            <ui:param name="id" value="newOID"/>
                            <ui:define name="label">#{messages['gazelle.evs.client.saml.oid']}</ui:define>
                            <h:inputText value="#{callingToolManager.newTool.oid}" id="newOID" required="true"
                                         styleClass="form-control">
                                        <f:validator  binding="#{callingToolOidValidator}" />

                                <a4j:ajax event="blur" render="newOIDDecorate" execute="@this"/>
                            </h:inputText>
                        </s:decorate>

                        <s:decorate id="newUrlDecorate" template="/layout/form/_form_field_horizontal_inline.xhtml">
                            <ui:param name="id" value="newUrl"/>
                            <ui:define name="label">#{messages['gazelle.evs.client.URLforSendingBackResults']})</ui:define>
                            <h:inputText value="#{callingToolManager.newTool.url}" id="newUrl" required="true" styleClass="form-control">
                                <a4j:ajax event="blur" render="newUrlDecorate" execute="@this"/>
                            </h:inputText>
                        </s:decorate>

                        <s:decorate id="newToolTipDecorate" template="/layout/form/_form_field_horizontal_inline.xhtml">
                            <ui:param name="id" value="newToolTip"/>
                            <ui:define name="label">#{messages['gazelle.evs.client.ToolType']}</ui:define>
                            <h:selectOneMenu value="#{callingToolManager.newTool.toolType}" id="newToolTip" styleClass="form-control gzl-select-text" >
                                <f:selectItems value="#{callingToolManager.toolTypes}" var="type" label="#{type.label}" />
                            </h:selectOneMenu>
                        </s:decorate>

                        <ui:define name="panel_footer">
                            <a4j:commandButton value="#{messages['gazelle.evs.Save']}"
                                               actionListener="#{callingToolManager.saveNewTool()}" styleClass=" gzl-btn-blue"
                                               render="globalForm newToolForm"/>
                            <a4j:commandButton value="#{messages['net.ihe.gazelle.evs.Cancel']}"
                                                   actionListener="#{callingToolManager.setNewTool(null)}" byPassUpdates="true"
                                                   immediate="true" styleClass="gzl-btn" render="globalForm newToolForm" execute="@all"/>
                        </ui:define>
                    </ui:decorate>
                </s:div>
            </s:div>
        </h:form>

        <h:form id="globalForm">
            <s:div id="globalDiv" rendered="#{callingToolManager.newTool == null}">


                <ui:decorate template="/layout/panels/_panel_title_footer_id.xhtml">
                    <ui:define name="panel_title">
                        <h:outputText value="#{messages['gazelle.evs.client.CallingTools']}"/>
                    </ui:define>


                    <ui:param name="panel_id" value="allToolsPanel"/>

                    <h:outputText
                            value="#{messages['gazelle.evs.client.CallingToolsAreTheApplicationsWhichSendFilesToTh']})"/>
                    <rich:dataTable value="#{callingToolManager.listTools()}" var="tool"
                                    rows="#{dataScrollerMemory.numberOfResultsPerPage}"
                                    id="toolList"
                                    render="ds" styleClass="table table-striped  table-responsive">
                        <g:column sortBy="#{'label'}" filterBy="#{'label'}" filterEvent="onkeyup"
                                  sortOrder="ascending">
                            <ui:define name="header">#{messages['gazelle.evs.client.Label']}</ui:define>
                            <rich:inplaceInput value="#{tool.label}" showControls="true" size="16">
                                <a4j:ajax event="change" render="toolList"
                                          listener="#{callingToolManager.updateTool(tool)}"/>
                            </rich:inplaceInput>
                        </g:column>
                        <g:column sortBy="#{'oid'}" filterBy="#{'oid'}" filterEvent="onkeyup">
                            <ui:define name="header">#{messages['gazelle.evs.client.saml.oid']}</ui:define>
                            <rich:inplaceInput value="#{tool.oid}" showControls="true" size="16">
                                <a4j:ajax event="change" render="toolList"
                                          listener="#{callingToolManager.updateTool(tool)}"/>
                            </rich:inplaceInput>
                        </g:column>

                        <g:column sortBy="#{'toolType'}" >
                            <ui:define name="header">#{messages['gazelle.evs.client.ToolType']}</ui:define>
                            <h:outputText value="#{tool.toolType.label}"/>
                        </g:column>

                        <g:column sortBy="#{'url'}" filterBy="#{'url'}" filterEvent="onkeyup">
                            <ui:define name="header">#{messages['gazelle.evs.client.URL']}</ui:define>
                            <rich:inplaceInput value="#{tool.url}" showControls="true" size="16">
                                <a4j:ajax event="change" render="toolList"
                                          listener="#{callingToolManager.updateTool(tool)}"/>
                            </rich:inplaceInput>
                        </g:column>
                        <f:facet name="footer">
                            <ui:include src="/util/datatableFooter.xhtml">
                                <ui:param name="dataScrollerId" value="ds"/>
                                <ui:param name="dataTableId" value="toolList"/>
                            </ui:include>

                        </f:facet>

                    </rich:dataTable>

                    <ui:define name="panel_footer">

                        <a4j:commandButton value="#{messages['gazelle.evs.client.RegisterANewTool']}"
                                           actionListener="#{callingToolManager.registerNewTool()}"
                                           styleClass="gzl-btn-blue" rendered="#{callingToolManager.newTool == null}"
                                           render="globalForm newToolForm" execute="@this"/>
                    </ui:define>

                </ui:decorate>

            </s:div>
        </h:form>



    </ui:define>
</ui:composition>


