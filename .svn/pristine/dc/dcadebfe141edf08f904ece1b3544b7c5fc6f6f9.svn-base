<!--
  ~ EVS Client is part of the Gazelle Test Bed
  ~ Copyright (C) 2006-2016 IHE
  ~ mailto :eric DOT poiseau AT inria DOT fr
  ~
  ~ See the NOTICE file distributed with this work for additional information
  ~ regarding copyright ownership.  This code is licensed
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->

<!DOCTYPE composition PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<ui:composition xmlns:h="http://java.sun.com/jsf/html" xmlns:s="http://jboss.org/schema/seam/taglib"
                xmlns:g="http://www.ihe.net/gazelle" xmlns:f="http://java.sun.com/jsf/core"
                xmlns:rich="http://richfaces.org/rich" xmlns:gdk="http://www.ihe.net/gazellecdk"
                xmlns:a4j="http://richfaces.org/a4j" xmlns:ui="http://java.sun.com/jsf/facelets"
                xmlns="http://www.w3.org/1999/xhtml" template="//layout/template.xhtml">
    <ui:param name="pageName" value="#{messages['net.ihe.gazelle.ManageTemplates']}"/>
    <ui:param name="pageNameUrl" value="administration/adminTemplates.seam"/>
    <ui:param name="pageNameTitle" value="#{messages['net.ihe.gazelle.ManageTemplates']}"/>
    <ui:define name="body">
        <rich:messages globalOnly="true" styleClass="message"/>
        <s:div id="globalDiv">
            <h:form id="globalForm">
                <rich:panel id="templatesAction">
                    <f:facet name="header">#{messages['net.ihe.gazelle.CDATemplatesActions']}</f:facet>
                    <s:decorate template="/layout/form/_edit.xhtml">
                        <ui:param name="id" value="cb"/>
                        <ui:define name="label">#{messages['net.ihe.gazelle.ScanTemplatesDuringCDAFileIsValidated']}
                        </ui:define>
                        <h:selectBooleanCheckbox id="cb" value="#{resultDisplayManager.scanTemplates}">
                            <a4j:ajax event="click" execute="@this" render="globalForm"/>
                        </h:selectBooleanCheckbox>
                    </s:decorate>
                    <br/>
                    <a4j:commandButton value="#{messages['net.ihe.gazelle.UpdateTemplateList']}"
                                       action="#{resultDisplayManager.updateTemplateList()}" styleClass="gzl-btn-blue"
                                       render="globalForm" execute="@this"/>

                    <h:outputText
                            value="#{messages['net.ihe.gazelle.TheseTemplatesAreExtractedFromGazelleObjectCheck']}"/>
                </rich:panel>
                <rich:panel id="allTemplatesPanel">
                    <f:facet name="header">#{messages['net.ihe.gazelle.CDATemplatesList']}</f:facet>
                    <rich:dataTable value="#{objectIndexerManager.listAllTemplates()}" var="template"
                                    rows="#{dataScrollerMemory.numberOfResultsPerPage}"
                                    id="templatesList"
                                    render="ds" styleClass="table table-striped  table-responsive">
                        <g:column sortBy="#{'identifier'} " filterBy="#{'identifier'}"
                                  filterEvent="onkeyup" sortOrder="ascending">
                            <ui:define name="header">#{messages['net.ihe.gazelle.TemplateOID']}</ui:define>
                            <h:outputText value="#{template.identifier}" escape="false"/>
                        </g:column>
                        <g:column sortBy="#{'name'}" filterBy="#{'name'}" filterEvent="onkeyup">
                            <ui:define name="header">#{messages['net.ihe.gazelle.TemplateName']}</ui:define>
                            <rich:inplaceInput value="#{template.name}" showControls="true" size="16">
                                <a4j:ajax event="change" render="templatesList"
                                          listener="#{objectIndexerManager.updateObjectIndexer(template)}"/>
                            </rich:inplaceInput>
                        </g:column>
                        <g:column sortBy="#{'extension'}" filterBy="#{'extension'}" filterEvent="onkeyup">
                            <ui:define name="header">#{messages['net.ihe.gazelle.TemplateExtension']}</ui:define>
                            <rich:inplaceInput value="#{template.extension}" showControls="true" size="16">
                                <a4j:ajax event="change" render="templatesList"
                                          listener="#{objectIndexerManager.updateObjectIndexer(template)}"/>
                            </rich:inplaceInput>
                        </g:column>
                        <g:column sortBy="#{'display'}">
                            <ui:define name="header">
                                <s:div>
                                    <h:outputText value="#{messages['net.ihe.gazelle.IsDisplayed']}"/>

                                    <h:commandLink immediate="true"
                                                   action="#{resultDisplayManager.displaySelection('globalForm:templatesList')}"
                                                   render="templatesList">
                                        <s:span styleClass="icon-check"/>
                                    </h:commandLink>
                                    <h:commandLink immediate="true"
                                                   action="#{resultDisplayManager.hideSelection('globalForm:templatesList')}"
                                                   render="templatesList">
                                        <s:span styleClass="icon-check-empty"/>
                                    </h:commandLink>

                                </s:div>
                            </ui:define>
                            <center>
                                <h:selectBooleanCheckbox id="displayedTemplates" value="#{template.display}">
                                    <a4j:ajax event="click" render="templatesList"
                                              listener="#{objectIndexerManager.updateObjectIndexer(template)}"/>
                                </h:selectBooleanCheckbox>
                            </center>
                        </g:column>
                        <f:facet name="footer">
                                <ui:include src="/util/datatableFooter.xhtml">
                                    <ui:param name="dataScrollerId" value="ds"/>
                                    <ui:param name="dataTableId" value="templatesList"/>
                                </ui:include>
                        </f:facet>
                    </rich:dataTable>
                </rich:panel>
            </h:form>
        </s:div>
    </ui:define>
</ui:composition>
