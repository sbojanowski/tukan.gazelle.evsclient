/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.xml.model;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evs.client.common.action.GazelleValidationCacheManager;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Name("validatedWADO")
@Table(name = "validated_wado", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "validated_wado_sequence", sequenceName = "validated_wado_id_seq", allocationSize = 1)
public class ValidatedWADO extends AbstractValidatedXMLFile {

    private static final long serialVersionUID = -1443811873360866107L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidatedWADO.class);

    @Id
    @NotNull
    @GeneratedValue(generator = "validated_wado_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "validated_url")
    @Lob
    @Type(type = "text")
    private String validatedUrl;

    @Column(name = "file_path")
    private String filePath;

    public ValidatedWADO() {
    }

    @Override
    public void setFilePath(final String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String getFilePath() {
        return this.filePath;
    }

    public String getValidatedUrl() {
        return this.validatedUrl;
    }

    public void setValidatedUrl(final String validatedUrl) {
        this.validatedUrl = validatedUrl;
    }


    public ValidatedWADO(final ValidatedWADO inWADOObject) {
        this.validatedUrl = inWADOObject.getValidatedUrl();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     */
    public static ValidatedWADO createNewValidatedWado() {
        ValidatedWADO message = new ValidatedWADO();
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        message = em.merge(message);
        em.flush();
        if (message == null || message.getId() == 0) {
            ValidatedWADO.LOGGER.error("An error occurred while creating a ValidatedWADO object");
            return null;
        }
        return message;
    }

    /**
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends AbstractValidatedXMLFile> T addOrUpdate(final Class<T> clazz) {
        if (this.getOid() == null || this.getOid().length() == 0) {
            this.setOid(OIDGenerator.getNewOid());
        }
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        final ValidatedWADO validatedFile = em.merge(this);
        em.flush();

        // Update the cache with the newest values
        GazelleValidationCacheManager
                .refreshGazelleCacheWebService(validatedFile.getExternalId(), validatedFile.getToolOid(),
                        validatedFile.getOid(), "off");

        return (T) validatedFile;
    }

    @Override
    public String getSchematron() {
        return null;
    }

    @Override
    public void setSchematron(final String schematron) {
        return;
    }

    @Override
    public String permanentLink() {
        final String applicationUrl = ApplicationPreferenceManager.getStringValue("application_url");
        final StringBuilder builder = new StringBuilder(applicationUrl);
        builder.append("/wadoResult.seam?type=WADO").append("&oid=").append(this.getOid());
        if (this.getPrivacyKey() != null) {
            builder.append("&privacyKey=").append(this.getPrivacyKey());
        }
        return builder.toString();
    }

}
