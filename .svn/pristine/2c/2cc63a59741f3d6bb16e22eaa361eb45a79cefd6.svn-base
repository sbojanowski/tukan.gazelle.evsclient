/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.analyzer.file.xml;

import net.ihe.gazelle.evs.client.analyzer.MessageContentAnalyzer;
import net.ihe.gazelle.evs.client.common.action.FileToValidate;
import net.ihe.gazelle.evs.client.common.model.OIDGenerator;
import org.slf4j.Logger;

public abstract class Analyzer implements IAnalyzable {

    private static final String BASE64 = "BASE64";
    private static final String DSUB = "DSUB";

    @Override
    public void analyze(final FileToValidate ftv, final FileToValidate ftv1, final MessageContentAnalyzer vd) {
        this.analyze(ftv, ftv1, this.getType(), vd);
    }

    private void analyze(final FileToValidate parent, final FileToValidate ftv, final String type, final MessageContentAnalyzer vd) {
        this.getLog().info("Launch {} validation", type);
        if (parent.getParent().getDocType().equals(Analyzer.BASE64)) {
            ftv.setStartOffset(vd.getBase64OffsetStart());
            ftv.setEndOffset(vd.getBase64OffsetEnd());
        } else if (type.equals(Analyzer.DSUB)) {
            ftv.setStartOffset(vd.getOffsetStart());
            ftv.setEndOffset(vd.getOffsetEnd());
        } else {
            ftv.setStartOffset(vd.getOffsetStart() + vd.getSaveStartOffset());
            ftv.setEndOffset(vd.getOffsetEnd() + vd.getSaveStartOffset());
        }
        ftv.setValidationType(type);
        ftv.setMessageContentAnalyzerOid(OIDGenerator.getNewOid());

        ftv.setDocType(type);
        parent.getFileToValidateList().add(ftv);
    }

    public abstract String getType();

    public abstract Logger getLog();

}
