/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.filter;

import net.ihe.gazelle.evs.client.common.model.ValidatedObject;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.beans.HQLOrder;
import net.ihe.gazelle.hql.beans.HQLRestrictionValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.Date;

public class HQLQueryBuilderForStatistics<T extends ValidatedObject> extends HQLQueryBuilder<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(HQLQueryBuilderForStatistics.class);

    public HQLQueryBuilderForStatistics(final EntityManager entityManager, final Class<T> entityClass) {
        super(entityManager, entityClass);
    }

    public HQLQueryBuilderForStatistics(final Class<T> entityClass) {
        super(entityClass);
    }

    public Date getMinDate() {
        final StringBuilder sb = new StringBuilder("select min(validationDate) ");
        // First build where, to get all paths in from
        final StringBuilder sbWhere = new StringBuilder();
        final HQLRestrictionValues values = this.buildQueryWhere(sbWhere);
        // Build order, to get all paths in from
        this.buildQueryOrder(sbWhere);

        // add order columns if needed, cannot sort without it!
        // will be removed at the end (result list will be Object[] then)
        for (final HQLOrder order : this.getOrders()) {
            sb.append(", ").append(order.getPath());
        }
        sb.append(this.buildQueryFrom()).append(sbWhere);

        final Query query = this.createRealQuery(sb);

        this.buildQueryWhereParameters(query, values);
        try {
            return (Date) query.getSingleResult();
        } catch (final NoResultException e) {
            LOGGER.error(e.getMessage(),e);
            return null;
        }
    }

}
