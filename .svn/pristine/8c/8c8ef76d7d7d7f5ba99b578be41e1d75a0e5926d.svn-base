<!--
  ~ EVS Client is part of the Gazelle Test Bed
  ~ Copyright (C) 2006-2016 IHE
  ~ mailto :eric DOT poiseau AT inria DOT fr
  ~
  ~ See the NOTICE file distributed with this work for additional information
  ~ regarding copyright ownership.  This code is licensed
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->

<ui:composition xmlns:h="http://java.sun.com/jsf/html" xmlns:s="http://jboss.org/schema/seam/taglib"
                xmlns:g="http://www.ihe.net/gazelle" xmlns:f="http://java.sun.com/jsf/core"
                xmlns:rich="http://richfaces.org/rich" xmlns:gdk="http://www.ihe.net/gazellecdk"
                xmlns:a4j="http://richfaces.org/a4j" xmlns:ui="http://java.sun.com/jsf/facelets"
                xmlns="http://www.w3.org/1999/xhtml">




    #{CDAResultDisplayManager.initTemplatePanel()}

    <s:span rendered="#{detailedResult.nistValidation != null}">
        <s:decorate template="/layout/display/_display.xhtml"
                    rendered="#{detailedResult.nistValidation.contains('Summary: Errors were found')}">
            <ui:define name="label"><a href="#nist">#{messages['net.ihe.gazelle.evs.NistValidation']}</a>
            </ui:define>
            <h:outputText value="#{messages['gazelle.evs.client.failed']}" styleClass="FAILED"/>
        </s:decorate>
        <s:decorate template="/layout/display/_display.xhtml"
                    rendered="#{!detailedResult.nistValidation.contains('Summary: Errors were found')}">
            <ui:define name="label"><a href="#nist">#{messages['net.ihe.gazelle.evs.NistValidation']}</a>
            </ui:define>
            <h:outputText value="#{messages['gazelle.evs.client.passed']}" styleClass="PASSED"/>
        </s:decorate>
    </s:span>

    <s:div rendered="#{detailedResult.documentWellFormed != null}">
        <ui:decorate template="/layout/panels/_panel_title.xhtml">
            <ui:define name="panel_title">
                #{messages['net.ihe.gazelle.evs.XMLValidationReport']}
                <h:outputText value="#{detailedResult.documentWellFormed.result}"
                              styleClass="#{detailedResult.documentWellFormed.result}"/>
            </ui:define>


            <h:outputText value="#{messages['net.ihe.gazelle.evs.TheXMLDocumentIsWellformed']}" styleClass="PASSED"
                          rendered="#{(detailedResult.documentWellFormed.result == null) or (detailedResult.documentWellFormed.result.equals('PASSED'))}"/>
            <s:div rendered="#{(detailedResult.documentWellFormed.result != null) and (detailedResult.documentWellFormed.result.equals('FAILED'))}">
                <h:outputText value="#{messages['net.ihe.gazelle.evs.TheXMLDocumentIsNotWellformed']}"/>
                <rich:list value="#{detailedResult.documentWellFormed.getXSDMessage()}" var="mess" type="unordered"><a
                        href="#line_#{mess.lineNumber}">
                    <h:outputText value="#{mess.severity}:  &lt;#{mess.lineNumber},#{mess.columnNumber}&gt; "/>
                </a>
                    <h:outputText value="#{mess.message}"/>
                </rich:list>
            </s:div>
        </ui:decorate>
    </s:div>

    <s:div rendered="#{detailedResult.documentValidXSD != null}">
        <ui:decorate template="/layout/panels/_panel_title.xhtml">
            <ui:define name="panel_title">
                #{messages['net.ihe.gazelle.evs.XSDValidationDetailedResults']} <h:outputText value="#{detailedResult.documentValidXSD.result}"
                                                                                              styleClass="#{detailedResult.documentValidXSD.result}"/>
            </ui:define>


            <h:outputText value="#{messages['net.ihe.gazelle.evs.TheXMLDocumentIsValid']}" styleClass="PASSED"
                          rendered="#{(detailedResult.documentValidXSD.result == null) or (detailedResult.documentValidXSD.result.equals('PASSED'))}"/>
            <s:div rendered="#{(detailedResult.documentValidXSD.result != null) and (detailedResult.documentValidXSD.result.equals('FAILED'))}">
                <h:outputText styleClass="FAILED"
                              value="#{messages['net.ihe.gazelle.evs.TheXMLDocumentIsNotValidBecauseOfTheFollowingRea']}"/>
                <rich:list value="#{detailedResult.documentValidXSD.getXSDMessage()}" var="mess" type="unordered"><a
                        href="#line_#{mess.lineNumber}" onclick="updateColorationOfLines(#{mess.lineNumber})">
                    <h:outputText value="#{mess.severity}:  &lt;#{mess.lineNumber},#{mess.columnNumber}&gt; "/>
                </a>
                    <h:outputText value="#{mess.message}"/>
                </rich:list>
            </s:div>
        </ui:decorate>
    </s:div>

    <s:div rendered="#{detailedResult.getMDAValidation() != null }">
        <s:div id="soggmbv">
            <f:facet name="header"><a name="mbv">#{messages['net.ihe.gazelle.evs.ModelBasedValidationDetails']}</a>
            </f:facet>
            <s:div rendered="#{detailedResult.getTemplateDesc() != null }">

                <s:decorate template="/layout/display/_display.xhtml" >
                    <ui:define name="label">#{messages['net.ihe.gazelle.evs.ShowTemplatesTreeexperimental']}) </ui:define>
                    <h:selectBooleanCheckbox value="#{bean.showTemplates}">
                        <a4j:ajax event="change" render="mbvView" execute="@this"
                                  listener="#{bean.resetSelectedTemplate()}" oncomplete=" jq162('.panel-left').resizable({handleSelector: '.splitter', resizeHeight: false });"/>
                    </h:selectBooleanCheckbox>
                </s:decorate>
            </s:div>

            <s:div id="mbvView">
                <s:span rendered="#{!bean.showTemplates}">
                    <f:subview id="subview_1">
                        <ui:include  src="templateDesc.xhtml"/>
                    </f:subview>
                </s:span>

                <s:span rendered="#{bean.showTemplates}" id="templpluscontid">
                    <a4j:region>
                        <div class="panel-container">
                            <div class="panel-left">


                                    <s:div id="tree">

                                        <rich:tree id="treeTemp" value="#{bean.getListTemplatesArchitecture()}" var="node"
                                                   style="vertical-align:top"
                                                   rowKeyConverter="org.richfaces.IntegerSequenceRowKeyConverter"
                                                   execute="@this" render="mbvDetails"
                                                   nodeType="#{node.data.class.simpleName}"
                                                   toggleType="client"
                                                   selectionType="ajax"
                                                   onselectionchange="filterOnSelectedTemplate(node.data.location)"
                                                   selectionChangeListener="#{bean.myTreeSelectionChange}">

                                            <rich:treeNode type="Template" iconClass="#{bean.getIconClassForTemplate(node.data)}"
                                                           style="cursor:pointer;"
                                                           expanded="#{true}"
                                                           width="15" height="15"  >

                                                <s:div style="vertical-align:top;" >
                                                    <dl style="margin-bottom:0px" >
                                                        <a4j:repeat value="#{node.data.templateId}" var="var">
                                                            <dd>
                                                                <h:outputText
                                                                        value="#{var.id}- #{messages['net.ihe.gazelle.evs.UNKNOWN']}"
                                                                        style="color:grey;"
                                                                        rendered="#{var.name.equals('UNKNOWN')}"/>
                                                                <h:outputText value="#{var.id} - #{var.name}"
                                                                              style="color:black;"
                                                                              rendered="#{!var.name.equals('UNKNOWN')}"/>


                                                            </dd>

                                                        </a4j:repeat>
                                                    </dl>
                                                </s:div>

                                            </rich:treeNode>
                                        </rich:tree>

                                    </s:div>


                            </div>
                            <div class="splitter">
                            </div>
                            <div class="panel-right">

                                <s:div id="mbvDetails">
                                    <f:subview id="subview_2">
                                        <ui:include src="templateDesc.xhtml"/>
                                    </f:subview>
                                </s:div>
                            </div>

                        </div>
                    </a4j:region>

                </s:span>
            </s:div>
        </s:div>
    </s:div>

    <s:div rendered="#{(detailedResult.nistValidation != null) and (!detailedResult.nistValidation.equals(''))}">
        <rich:collapsiblePanel switchType="client">
            <f:facet name="header"><a name="nist">#{messages['net.ihe.gazelle.evs.NistValidation']}</a></f:facet>
            <h:outputText value="#{detailedResult.nistValidation}" escape="false"/>
        </rich:collapsiblePanel>
    </s:div>




</ui:composition>
