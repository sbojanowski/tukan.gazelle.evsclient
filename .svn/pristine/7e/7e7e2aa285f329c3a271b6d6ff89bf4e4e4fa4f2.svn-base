/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evs.client.common.menu;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.pages.Authorization;

import org.jboss.seam.security.Identity;

public enum Authorizations implements Authorization {

    ALL,

    LOGGED,

    ADMIN,

    MONITOR,

    SCHEMATRON, CROSS_VALIDATION;

    @Override
    public boolean isGranted(final Object... context) {
        switch (this) {
            case ALL:
                return true;
            case LOGGED:
                return Identity.instance().isLoggedIn();
            case ADMIN:
                return Identity.instance().hasRole("admin_role");
            case MONITOR:
                return Identity.instance().hasRole("monitor_role");
            case SCHEMATRON:
                return ApplicationPreferenceManager.getBooleanValue("display_SCHEMATRON_menu");
            case CROSS_VALIDATION:
                return ApplicationPreferenceManager.getBooleanValue("x_validation_enabled");
            default:
                return false;
        }
    }

}
