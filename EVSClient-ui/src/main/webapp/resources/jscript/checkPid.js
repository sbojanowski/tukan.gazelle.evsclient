/**
 * Check Kela PIDs
 * @author Youn Cadoret
 *
 */

var getUrl = document.getElementsByClassName('scriptLocalization')[0].innerHTML;
jq162.getScript(getUrl);

function check_file() {
    $(".rf-fu-btn-clr").hide();
    var fileInput = document.getElementsByClassName('uploadedFileDiv');
    var localValidate = document.getElementById('scriptResult');
    var file = fileInput[0].getElementsByClassName("rf-fu-inp")[0].files[0];
    var textType = /text.*/;
    var xmlFileContent = "";

    if (file.type.match(textType)) {
        var reader = new FileReader();

        reader.onload = function (e) {
            xmlFileContent = reader.result;

            //<== Convert_document_to_XML ==>
            var parser, newXml;
            parser = new DOMParser();
            newXml = parser.parseFromString(xmlFileContent, "text/xml");
            //</== Convert_document_to_XML ==>

            //call control function
            ruleControl(newXml);
        };
        reader.readAsText(file);
    } else {
        localValidate.innerText = "File not supported !";
    }
}
