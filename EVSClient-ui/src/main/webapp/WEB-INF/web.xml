<?xml version='1.0' encoding='UTF-8'?>
<!--
  ~ EVS Client is part of the Gazelle Test Bed
  ~ Copyright (C) 2006-2016 IHE
  ~ mailto :eric DOT poiseau AT inria DOT fr
  ~
  ~ See the NOTICE file distributed with this work for additional information
  ~ regarding copyright ownership.  This code is licensed
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->
<!-- File : web.xml @author Jean-Renan Chatel / INRIA Rennes IHE development 
	Project @see > Jchatel@irisa.fr - http://www.ihe-europe.org -->
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee"
         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
         version="3.0">
    <filter>
        <filter-name>Seam Filter</filter-name>
        <filter-class>org.jboss.seam.servlet.SeamFilter</filter-class>
        <init-param>
            <param-name>createTempFiles</param-name>
            <param-value>false</param-value>
        </init-param>
        <init-param>
            <param-name>maxRequestSize</param-name>
            <param-value>100000000</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>Seam Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    <!-- richfaces -->
    <context-param>
        <param-name>org.ajax4jsf.VIEW_HANDLERS</param-name>
        <param-value>com.sun.facelets.FaceletViewHandler</param-value>
    </context-param>
    <context-param>
        <param-name>org.richfaces.skin</param-name>
        <param-value>plain</param-value>
    </context-param>
    <context-param>
        <param-name>org.richfaces.enableControlSkinning</param-name>
        <param-value>false</param-value>
    </context-param>
    <context-param>
        <param-name>org.richfaces.enableControlSkinningClasses</param-name>
        <param-value>false</param-value>
    </context-param>
    <context-param>
        <param-name>org.richfaces.resourceOptimization.enabled</param-name>
        <param-value>true</param-value>
    </context-param>
    <context-param>
        <param-name>javax.faces.FACELETS_REFRESH_PERIOD</param-name>
        <param-value>-1</param-value>
    </context-param>

    <!--<context-param>-->
        <!--<param-name>facelets.RECREATE_VALUE_EXPRESSION_ON_BUILD_BEFORE_RESTORE</param-name>-->
        <!--<param-value>false</param-value>-->
    <!--</context-param>-->
    <context-param>
        <param-name>org.richfaces.push.jms.enabled</param-name>
        <param-value>true</param-value>
    </context-param>

    <context-param>
        <param-name>com.sun.faces.sendPoweredByHeader</param-name>
        <param-value>false</param-value>
    </context-param>

    <!-- This takes care of the session expiration problem -->
    <listener>
        <listener-class>net.ihe.gazelle.common.session.GazelleSessionListener</listener-class>
    </listener>
    <filter>
    <filter-name>SessionTimeoutFilter</filter-name>
    <filter-class>net.ihe.gazelle.common.session.SessionTimeoutFilter</filter-class>
</filter>
    <filter-mapping> <filter-name>SessionTimeoutFilter</filter-name>
        <url-pattern>*.seam</url-pattern> </filter-mapping>
    <!-- Set the timeout delay (minutes) -->
    <session-config>
        <session-timeout>30</session-timeout>
    </session-config>
    <!-- seam -->
    <listener>
        <listener-class>org.jboss.seam.servlet.SeamListener</listener-class>
    </listener>
    <servlet>
        <servlet-name>Seam Resource Servlet</servlet-name>
        <servlet-class>org.jboss.seam.servlet.SeamResourceServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>Seam Resource Servlet</servlet-name>
        <url-pattern>/seam/resource/*</url-pattern>
    </servlet-mapping>
    <servlet>
        <servlet-name>Resource Servlet</servlet-name>
        <servlet-class>org.richfaces.webapp.ResourceServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>Resource Servlet</servlet-name>
        <url-pattern>/org.richfaces.resources/*</url-pattern>
    </servlet-mapping>
    <!-- Include Gazelle taglibs as libraries -->
    <context-param>
        <param-name>javax.faces.FACELETS_LIBRARIES</param-name>
        <param-value>
            /WEB-INF/classes/META-INF/gazelle.taglib.xml;
        </param-value>
    </context-param>
    <!-- JSF -->
    <context-param>
        <param-name>javax.faces.DEFAULT_SUFFIX</param-name>
        <param-value>.xhtml</param-value>
    </context-param>
    <context-param>
        <param-name>facelets.DEVELOPMENT</param-name>
        <param-value>false</param-value>
    </context-param>
    <context-param>
        <param-name>javax.faces.STATE_SAVING_METHOD</param-name>
        <param-value>server</param-value>
    </context-param>
    <servlet>
        <servlet-name>Faces Servlet</servlet-name>
        <servlet-class>javax.faces.webapp.FacesServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>Faces Servlet</servlet-name>
        <url-pattern>*.seam</url-pattern>
    </servlet-mapping>
    <security-constraint>
        <display-name>Restrict raw XHTML and XLS Documents</display-name>
        <web-resource-collection>
            <web-resource-name>XHTMLandXLS</web-resource-name>
            <url-pattern>*.xhtml,*.xls,*.html,*.php</url-pattern>
        </web-resource-collection>
        <auth-constraint/>
    </security-constraint>
    <!-- PDF and XLS Generation -->
    <servlet>
        <servlet-name>Document Store Servlet</servlet-name>
        <servlet-class>org.jboss.seam.document.DocumentStoreServlet</servlet-class>
    </servlet>
    <context-param>
        <param-name>webAppRootKey</param-name>
        <param-value>seam-excel</param-value>
    </context-param>
    <servlet-mapping>
        <servlet-name>Document Store Servlet</servlet-name>
        <url-pattern>*.pdf</url-pattern>
    </servlet-mapping>
    <servlet-mapping>
        <servlet-name>Document Store Servlet</servlet-name>
        <url-pattern>*.csv</url-pattern>
    </servlet-mapping>
    <servlet-mapping>
        <servlet-name>Document Store Servlet</servlet-name>
        <url-pattern>*.xls</url-pattern>
    </servlet-mapping>
    <context-param>
        <param-name>org.apache.myfaces.COMPRESS_STATE_IN_CLIENT</param-name>
        <param-value>true</param-value>
    </context-param>
    <context-param>
        <param-name>javax.faces.FACELETS_SKIP_COMMENTS</param-name>
        <param-value>true</param-value>
    </context-param>
    <!-- File upload servlet -->
    <servlet>
        <servlet-name>Remote file upload</servlet-name>
        <servlet-class>net.ihe.gazelle.evs.client.servlet.Upload</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>Remote file upload</servlet-name>
        <url-pattern>/upload</url-pattern>
    </servlet-mapping>
    <!-- CAS -->

    <context-param>
        <param-name>configurationStrategy</param-name>
        <param-value>PROPERTY_FILE</param-value>
    </context-param>

    <context-param>
        <param-name>configFileLocation</param-name>
        <param-value>/opt/gazelle/cas/file.properties</param-value>
    </context-param>

    <filter>
        <filter-name>CAS Single Sign Out Filter</filter-name>
        <filter-class>org.jasig.cas.client.session.SingleSignOutFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CAS Single Sign Out Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    <listener>
        <listener-class>org.jasig.cas.client.session.SingleSignOutHttpSessionListener</listener-class>
    </listener>

    <filter>
        <filter-name>Gazelle CAS Authentication Filter</filter-name>
        <filter-class>net.ihe.gazelle.cas.client.authentication.AuthenticationFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>Gazelle CAS Authentication Filter</filter-name>
        <url-pattern>/cas/login</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>Gazelle CAS logout filter</filter-name>
        <filter-class>net.ihe.gazelle.cas.client.authentication.LogoutFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>Gazelle CAS logout filter</filter-name>
        <url-pattern>/cas/logout.seam</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>CAS Validation Filter</filter-name>
        <filter-class>org.jasig.cas.client.validation.Cas30ProxyReceivingTicketValidationFilter
        </filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CAS Validation Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>CAS HttpServletRequest Wrapper Filter</filter-name>
        <filter-class>org.jasig.cas.client.util.HttpServletRequestWrapperFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CAS HttpServletRequest Wrapper Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>


    <context-param>
        <param-name>resteasy.jndi.resources</param-name>
        <param-value>java:app/EVSClient-ejb/GetValidationInfo,java:app/version/VersionProvider</param-value>
        <!--If you need to declare more than one resource, separate them by comas -->
    </context-param>
    <!-- The following lines are required only if you decide not to use the
          application base path as base URI for your REST services -->
    <context-param>
        <param-name>resteasy.servlet.mapping.prefix</param-name>
        <param-value>/rest</param-value>
    </context-param>
    <!-- end of optional lines -->
    <listener>
        <listener-class>org.jboss.resteasy.plugins.server.servlet.ResteasyBootstrap</listener-class>
    </listener>
    <servlet>
        <servlet-name>Resteasy</servlet-name>
        <servlet-class>org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>Resteasy</servlet-name>
        <url-pattern>/rest/*</url-pattern>
    </servlet-mapping>
</web-app>
