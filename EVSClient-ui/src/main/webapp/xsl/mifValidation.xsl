<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ EVS Client is part of the Gazelle Test Bed
  ~ Copyright (C) 2006-2016 IHE
  ~ mailto :eric DOT poiseau AT inria DOT fr
  ~
  ~ See the NOTICE file distributed with this work for additional information
  ~ regarding copyright ownership.  This code is licensed
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"
                omit-xml-declaration="yes"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p>
                <xd:b>Created on:</xd:b>
                July 1st, 2011
            </xd:p>
            <xd:p>
                <xd:b>Author:</xd:b>
                Anne-Gaëlle BERGE, IHE Development, INRIA Rennes
            </xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <div class="styleResultBackground" id="mif">
            <table>
                <tr>
                    <td>
                        <b>Result</b>
                    </td>
                    <td>
                        <xsl:choose>
                            <xsl:when
                                    test="contains(detailedResult/MIFValidation/Result, 'PASSED')">
                                <div class="PASSED">
                                    PASSED
                                </div>
                            </xsl:when>
                            <xsl:otherwise>
                                <div class="FAILED">
                                    FAILED
                                </div>
                            </xsl:otherwise>
                        </xsl:choose>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <b>Summary</b>
                    </td>
                    <td>
                        <xsl:choose>
                            <xsl:when
                                    test="detailedResult/MIFValidation/ValidationCounters/NrOfValidationErrors &gt; 0">
                                <a href="#miferrors">
                                    <xsl:value-of
                                            select="detailedResult/MIFValidation/ValidationCounters/NrOfValidationErrors"/>
                                    error(s)
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                No error
                            </xsl:otherwise>
                        </xsl:choose>
                        <br/>
                        <xsl:choose>
                            <xsl:when
                                    test="detailedResult/MIFValidation/ValidationCounters/NrOfValidationWarnings &gt; 0">
                                <a href="#mifwarnings">
                                    <xsl:value-of
                                            select="detailedResult/MIFValidation/ValidationCounters/NrOfValidationWarnings"/>
                                    warning(s)
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                No warning
                            </xsl:otherwise>
                        </xsl:choose>
                        <br/>
                        <xsl:choose>
                            <xsl:when
                                    test="detailedResult/MIFValidation/ValidationCounters/NrOfValidationNotes &gt; 0">
                                <a href="#mifinfo">
                                    <xsl:value-of
                                            select="detailedResult/MIFValidation/ValidationCounters/NrOfValidationInfos"/>
                                    info(s)
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                No info
                            </xsl:otherwise>
                        </xsl:choose>

                    </td>
                </tr>
            </table>
            <xsl:if test="count(detailedResult/MIFValidation/Error) &gt; 0">
                <p id="miferrors">Errors</p>
                <xsl:for-each select="detailedResult/MIFValidation/Error">

                    <table class="Error" width="100%">
                        <tr>
                            <td valign="top">
                                <b>Location</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="@location"/>
                                ( Line:
                                <xsl:value-of select="@startLine"/>
                                , Column:
                                <xsl:value-of select="@startColumn"/>
                                )
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Description</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="@message"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="count(detailedResult/MIFValidation/Warning) &gt; 0">
                <p id="mifwarnings">Warnings</p>
                <xsl:for-each select="detailedResult/MIFValidation/Warning">
                    <table class="Warning" width="100%">
                        <tr>
                            <td valign="top">
                                <b>Location</b>
                            </td>
                            <td>
                                <xsl:value-of select="@location"/>
                                ( Line:
                                <xsl:value-of select="@startLine"/>
                                , Column:
                                <xsl:value-of select="@startColumn"/>
                                )
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Description</b>
                            </td>
                            <td>
                                <xsl:value-of select="@message"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="count(detailedResult/MIFValidation/Info) &gt; 0">
                <p id="mifinfos">Infos</p>
                <xsl:for-each select="detailedResult/MIFValidation/Info">

                    <table class="Note" width="100%">
                        <tr>
                            <td valign="top">
                                <b>Location</b>
                            </td>
                            <td>
                                <xsl:value-of select="@location"/>
                                ( Line:
                                <xsl:value-of select="@startLine"/>
                                , Column:
                                <xsl:value-of select="@startColumn"/>
                                )
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Description</b>
                            </td>
                            <td>
                                <xsl:value-of select="@message"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="count(detailedResult/MIFValidation/Problem) &gt; 0">
                <p>Other problems</p>
                <xsl:for-each select="detailedResult/MIFValidation/Problem">

                    <table class="unkown" width="100%">
                        <tr>
                            <td valign="top">
                                <b>Location</b>
                            </td>
                            <td>
                                <xsl:value-of select="@location"/>
                                ( Line:
                                <xsl:value-of select="@startLine"/>
                                - Column:
                                <xsl:value-of select="@startColumn"/>
                                )
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Description</b>
                            </td>
                            <td>
                                <xsl:value-of select="@message"/>
                            </td>
                        </tr>

                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
        </div>
    </xsl:template>
</xsl:stylesheet>