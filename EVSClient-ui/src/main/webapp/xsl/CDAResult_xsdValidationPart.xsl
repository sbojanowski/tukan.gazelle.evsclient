<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ EVS Client is part of the Gazelle Test Bed
  ~ Copyright (C) 2006-2016 IHE
  ~ mailto :eric DOT poiseau AT inria DOT fr
  ~
  ~ See the NOTICE file distributed with this work for additional information
  ~ regarding copyright ownership.  This code is licensed
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p>
                <xd:b>Created on:</xd:b>
                Aug 31, 2010
            </xd:p>
            <xd:p>
                <xd:b>Modified on:</xd:b>
                July 1st, 2011
            </xd:p>
            <xd:p>
                <xd:b>Author:</xd:b>
                Anne-Gaëlle BERGE, IHE Development, INRIA Rennes
            </xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <div class="styleResultBackground" id="xsd">
            <xsl:if test="count(detailedResult/DocumentValidCDA) = 1">
                <xsl:choose>
                    <xsl:when test="detailedResult/DocumentValidCDA/Result = 'PASSED'">
                        <p class="PASSED">This document is valid according to HL7 schema (<a
                                href="http://gazelle.ihe.net/xsd/CDA.xsd">CDA.xsd</a>)
                        </p>
                    </xsl:when>
                    <xsl:otherwise>
                        <p class="FAILED">This document is not valid according to HL7 schema (<a
                                href="http://gazelle.ihe.net/xsd/CDA.xsd">CDA.xsd</a>) for the following reason(s)
                        </p>
                        <xsl:if test="count(detailedResult/DocumentValidCDA/*) &gt; 3">
                            <ul>
                                <xsl:for-each select="detailedResult/DocumentValidCDA/*">
                                    <xsl:if test="contains(current(), 'error')">
                                        <li>
                                            <xsl:value-of select="current()"/>
                                        </li>
                                    </xsl:if>
                                </xsl:for-each>
                            </ul>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
            <xsl:if test="count(detailedResult/DocumentValidEpsos) = 1">
                <xsl:choose>
                    <xsl:when test="detailedResult/DocumentValidEpsos/result = 'PASSED'">
                        <p class="PASSED">This document is valid according to epSOS schema (<a
                                href="http://gazelle.ihe.net/xsd/CDA_extended.xsd">CDA_extended.xsd</a>)
                        </p>
                    </xsl:when>
                    <xsl:otherwise>
                        <p class="FAILED">This document is not valid according to epSOS schema (<a
                                href="http://gazelle.ihe.net/xsd/CDA_extended.xsd">CDA_extended.xsd</a>) for the
                            following reason(s)
                        </p>
                        <xsl:if test="count(detailedResult/DocumentValidEpsos/*) &gt; 3">
                            <ul>
                                <xsl:for-each select="detailedResult/DocumentValidEpsos/*">
                                    <xsl:if test="contains(current(), 'error')">
                                        <li>
                                            <xsl:value-of select="current()"/>
                                        </li>
                                    </xsl:if>
                                </xsl:for-each>
                            </ul>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
        </div>
    </xsl:template>
</xsl:stylesheet>